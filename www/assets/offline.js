"use strict";

/* jshint ignore:start */



/* jshint ignore:end */

define('offline/adapters/application', ['exports', 'ember-data', 'ember', 'offline/config/environment', 'ember-simple-auth/mixins/data-adapter-mixin', 'lodash/lodash'], function (exports, _emberData, _ember, _offlineConfigEnvironment, _emberSimpleAuthMixinsDataAdapterMixin, _lodashLodash) {
    var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

    function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

    var inflector = _ember['default'].Inflector.inflector;

    function expectEmbeddedRecord(serializer, name) {
        if (serializer.isEmbeddedRecordsMixin) {
            // these functions appear to be missing doc strings
            return serializer.hasDeserializeRecordsOption(name) || serializer.hasSerializeRecordsOption(name);
        }
        return false;
    }

    exports['default'] = _emberData['default'].RESTAdapter.extend(_emberSimpleAuthMixinsDataAdapterMixin['default'], {
        storage: _ember['default'].inject.service(),
        network: _ember['default'].inject.service(),
        namespace: _offlineConfigEnvironment['default'].apiNameSpace,
        authorizer: 'authorizer:authorizer',
        urlTemplate: '/:modelName/:id',

        urlTemplateParams: function urlTemplateParams(modelName, id) {
            return {
                id: id,
                modelName: inflector.pluralize(modelName)
            };
        },

        buildURL: function buildURL(modelName, id) {
            var host = this.get('host');
            if (!host) {
                host = 'http://localhost:5000/';
            }
            var prefix = this.urlPrefix();
            var params = this.urlTemplateParams(modelName, id);
            var path = this.get('urlTemplate').replace(/\/:(.+?)(?=\/|$)/g, function (_, param) {
                return '/' + (params[param] || '');
            });
            return [host, prefix, path].map(function (item) {
                if (item) {
                    return item.replace(/(^\/|\/$)/g, '');
                }
                return '';
            }).join('/');
        },

        /**
         * Use device storage if we're offline or `adapterOptions.device` is true
         * @param {{ adapterOptions }} snapshot
         * @returns {Boolean}
         * @private
         */
        shouldUseStorage: function shouldUseStorage(snapshot) {
            if (snapshot.adapterOptions && snapshot.adapterOptions.hasOwnProperty('device')) {
                return snapshot.adapterOptions.device;
            }
            if (!this.get('network.online')) {
                snapshot.adapterOptions = _lodashLodash['default'].extend(snapshot.adapterOptions || {}, { device: true });
                return true;
            }
            return false;
        },

        _storageGroup: function _storageGroup(modelName) {
            return modelName;
        },

        _serializeEmbeddedId: function _serializeEmbeddedId(modelName, id) {
            return modelName + '@' + id;
        },

        _deserializeEmbeddedId: function _deserializeEmbeddedId(ref) {
            return ref.split('@');
        },

        /**
         * Fetch one or more items from device storage, then fetch the embedded things for each record
         * What is retrieved is either the serialized data sent to the server, or the raw response from the server
         *
         * @param {Function} type      DS.Model class instance for the record being inserted
         * @returns {DS.PromiseArray}  An array of promises, one for each record to be fetched
         * @private
         */
        _getFromStorage: function _getFromStorage(type, id) {
            var adapter = this,
                modelName = type.modelName,
                serializer = this.store.serializerFor(modelName);

            /*
             * We need to get an item from device storage, but each item may require other items on the device,
             * and those items may be one thing or many things.
             * Here we return a promise which resolves to an array of promises each resolving to a hash of promises,
             * each attr in the hash resolves to either an array of promises or a single promise!
             */
            return this.get('storage').select(this._storageGroup(modelName), id).then(function (records) {
                records = _lodashLodash['default'].isArray(records) ? records : [records];

                var resolvingRecords = records.map(function (record) {

                    var relatedSelects = {};
                    type.eachRelationship(function (name, relationship) {
                        if (record[name] !== null && typeof record[name] !== 'undefined' && expectEmbeddedRecord(serializer, name)) {
                            if (relationship.kind === 'hasMany') {
                                relatedSelects[name] = _ember['default'].RSVP.all(record[name].map(function (reference, i) {
                                    var _adapter$_deserializeEmbeddedId = adapter._deserializeEmbeddedId(reference);

                                    var _adapter$_deserializeEmbeddedId2 = _slicedToArray(_adapter$_deserializeEmbeddedId, 2);

                                    var type = _adapter$_deserializeEmbeddedId2[0];
                                    var id = _adapter$_deserializeEmbeddedId2[1];

                                    var model = adapter.store.modelFor(type);
                                    return adapter._getFromStorage(model, id).then(function (selected) {
                                        // replace the ID with the item
                                        record[name][i] = selected[0];
                                    });
                                }));
                            } else if (relationship.kind === 'belongsTo') {
                                var _adapter$_deserializeEmbeddedId3 = adapter._deserializeEmbeddedId(record[name]);

                                var _adapter$_deserializeEmbeddedId32 = _slicedToArray(_adapter$_deserializeEmbeddedId3, 2);

                                var _type = _adapter$_deserializeEmbeddedId32[0];
                                var _id = _adapter$_deserializeEmbeddedId32[1];

                                var model = adapter.store.modelFor(_type);
                                relatedSelects[name] = adapter._getFromStorage(model, _id).then(function (selected) {
                                    record[name] = selected[0];
                                });
                            }
                        }
                    });

                    return _ember['default'].RSVP.hash(relatedSelects).then(function () {
                        return record;
                    });
                });

                return _ember['default'].RSVP.all(resolvingRecords);
            }, function (error) {
                return [null];
            });
        },

        /**
         * Retreive data from the device, then fixup each record.
         * By default, pluralizes the hash key for many records.
         * If the response expected from the server is different, then that transformation should be mocked here
         *
         * ```
         * import ApplicationAdapter from './application';
         *
         * export default ApplicationAdapter.extend({
         *     ...
         *     getFromStorage(type, id) {
         *         return this._getFromStorage(type, id).then(function (records) {
         *             // transform the response in some way, then
         *             return records;
         *         });
         *     }
         * });
         * ```
         *
         * @param {Function} type      DS.Model class instance for the record being inserted
         * @returns {Promise.<Object>} Identical to the response expected when fetching from the server
         * @private
         */
        getFromStorage: function getFromStorage(type, id) {
            var modelName = type.modelName;
            return this._getFromStorage(type, id).then(function (records) {
                var payload = {};
                if (!id) {
                    payload[inflector.pluralize(modelName)] = records;
                } else {
                    payload[modelName] = records[0];
                }
                return payload;
            });
        },

        /**
         * Delete a single item from device storage
         * By default, returns the record that was deleted keyed by the record's modelName
         * @param {Function} type      DS.Model class instance for the record being inserted
         * @returns {Promise.<String>} A promise which resolves to hashified data (e.g., {"user": {"id": 123, ...}})
         * @private
         */
        delFromStorage: function delFromStorage(type, id) {
            return this.get('storage')['delete'](this._storageGroup(type.modelName), id).then(function (item) {
                var hash = {};
                hash[type.modelName] = item;
                return hash;
            });
        },

        /**
         * Add a single item to device storage (added before normalization but after serialization)
         *
         * @param   {Function} type     DS.Model class instance for the record being inserted
         * @param   {Object} data       The data we want to insert or update
         * @param   {Boolean} overwrite If true, always overwrite existing record, otherwise try to merge
         * @returns {Promise.<Object>}  A promise which resolves to an object containing the inserted data
         *                              and the ember-data id it was inserted using
         * @private
         */
        _setIntoStorage: function _setIntoStorage(type, data, overwrite) {
            var _this = this;

            var modelName = type.modelName;

            var attrs = {},
                relatedInserts = [];
            var serializer = this.store.serializerFor(modelName);
            var primaryKey = _ember['default'].get(serializer, 'primaryKey');
            var keyMapping = _ember['default'].get(serializer, 'attrs'); // normalized to serialized attribute name mapping

            // only insert things that are relevant
            attrs[primaryKey] = data[primaryKey];
            type.eachAttribute(function (name, attribute) {
                attrs[name] = data[name];
            });
            // ensure side loaded materials update their respective record stored on the device
            type.eachRelationship(function (name, relationship) {
                var inverse = relationship.options.inverse || modelName;

                // `attrs` defines a mapping between the serialized and normalized forms of an attr name
                // use whatever form we have
                if (keyMapping && !data.hasOwnProperty(name)) {
                    var serialized = keyMapping[name];
                    if (serialized && serialized.key) {
                        serialized = serialized.key;
                    }
                    if (typeof serialized === 'string') {
                        name = serialized;
                    }
                }

                if (data[name] !== null && typeof data[name] !== 'undefined' && expectEmbeddedRecord(serializer, name)) {
                    if (relationship.kind === 'hasMany') {
                        attrs[name] = Array.apply(undefined, _toConsumableArray(data[name]));
                        relatedInserts.push.apply(relatedInserts, _toConsumableArray(data[name].map(function (item, i) {
                            var model = null;
                            if (relationship.options.polymorphic) {
                                model = _this.store.modelFor(item.type); // TODO: use the typeKey?
                            } else {
                                    model = _this.store.modelFor(relationship.type);
                                }
                            item[inverse] = item[inverse] || data[primaryKey];
                            return _this._setIntoStorage(model, item, overwrite).then(function (inserted) {
                                attrs[name][i] = _this._serializeEmbeddedId(model.modelName, inserted.id);
                            });
                        })));
                    } else if (relationship.kind === 'belongsTo') {
                        (function () {
                            var model = null;
                            if (relationship.options.polymorphic) {
                                model = _this.store.modelFor(data[name].type);
                            } else {
                                model = _this.store.modelFor(relationship.type);
                            }
                            data[name][inverse] = data[name][inverse] || data[primaryKey];
                            relatedInserts.push(_this._setIntoStorage(model, data[name], overwrite).then(function (inserted) {
                                attrs[name] = _this._serializeEmbeddedId(model.modelName, inserted.id);
                            }));
                        })();
                    }
                } else {
                    attrs[name] = data[name];
                }
            });

            var storageGroup = this._storageGroup(modelName);

            return _ember['default'].RSVP.all(relatedInserts).then(function () {
                if (overwrite) {
                    return _this.get('storage').insert(storageGroup, data[primaryKey], attrs);
                } else {
                    return _this.get('storage').select(storageGroup, data[primaryKey]).then(function () {
                        return _this.get('storage').update(storageGroup, data[primaryKey], attrs);
                    }, function () {
                        return _this.get('storage').insert(storageGroup, data[primaryKey], attrs);
                    });
                }
            }).then(function (inserted) {
                return {
                    id: data[primaryKey],
                    inserted: inserted
                };
            });
        },

        /**
         * Wraps _setIntoStorage to allow data to be transformed before insertion
         * By default, returns the hashified data (e.g., {'user': {'id': '123', ...}})
         *
         * @param type
         * @param data
         * @param overwrite
         * @returns {Promise.<Object>}
         */
        setIntoStorage: function setIntoStorage(type, data) {
            var overwrite = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];

            var hash = {},
                modelName = type.modelName;

            if (data.hasOwnProperty(modelName)) {
                hash = data;
                data = hash[modelName];
            } else {
                hash[modelName] = data;
            }

            return this._setIntoStorage(type, data, overwrite).then(function () {
                return hash;
            });
        },

        findAll: function findAll(store, type, sinceToken, snapshotRecordArray) {
            var _this2 = this;

            if (this.shouldUseStorage(snapshotRecordArray)) {
                return this.getFromStorage(type);
            }
            return this._super(store, type, sinceToken, snapshotRecordArray).then(function (payload) {
                var records = payload[inflector.pluralize(type.modelName)];
                return _ember['default'].RSVP.all(records.map(function (r) {
                    return _this2.setIntoStorage(type, r, false);
                })).then(function () {
                    return payload;
                });
            });
        },

        /**
         * Fetch from device storage,
         * or make a RESTful request to the server and save the response onto the device for later.
         * @param store
         * @param type
         * @param id
         * @param snapshot
         * @returns {Promise}
         */
        findRecord: function findRecord(store, type, id, snapshot) {
            var _this3 = this;

            if (this.shouldUseStorage(snapshot)) {
                return this.getFromStorage(type, id);
            }
            return this._super(store, type, id, snapshot).then(function (payload) {
                return _this3.setIntoStorage(type, payload, false);
            });
        },

        createRecord: function createRecord(store, type, snapshot) {
            if (this.shouldUseStorage(snapshot)) {
                var data = this.serialize(snapshot, { includeId: true });
                return this.setIntoStorage(type, data, true);
            }
            return this._super(store, type, snapshot);
        },

        updateRecord: function updateRecord(store, type, snapshot) {
            var _this4 = this;

            if (this.shouldUseStorage(snapshot)) {
                var _ret2 = (function () {
                    var data = _this4.serialize(snapshot, { includeId: true });
                    return {
                        v: _this4.getFromStorage(type, snapshot.id).then(function () {
                            return _this4.setIntoStorage(type, data, false);
                        }, function () {
                            return new _emberData['default'].AdapterError('record does not exist');
                        })
                    };
                })();

                if (typeof _ret2 === 'object') return _ret2.v;
            }
            return this._super(store, type, snapshot);
        },

        /**
         * Remove a record from the device if offline, otherwise issue a DELETE request
         * TODO: these deletions are not carried over when we go online, however currently nothing NEEDS to
         * @param store
         * @param type
         * @param snapshot
         * @returns {Ember.RSVP.Promise}
         */
        deleteRecord: function deleteRecord(store, type, snapshot) {
            var _this5 = this;

            if (this.shouldUseStorage(snapshot)) {
                return this.delFromStorage(type, snapshot.id)['catch'](function () {
                    return new _emberData['default'].AdapterError('record does not exist');
                });
            }
            return this._super(store, type, snapshot).then(function (payload) {
                return _this5.delFromStorage(type, snapshot.id)['finally'](function () {
                    return payload;
                });
            });
        },

        isSuccess: function isSuccess(status, headers, payload) {
            if (payload.errors) {
                return false;
            }
            return this._super(status, headers, payload);
        },

        normalizeErrorResponse: function normalizeErrorResponse(status, headers, payload) {
            return payload.extra;
        }
    });
});
define('offline/adapters/asset', ['exports', 'ember', 'offline/adapters/application'], function (exports, _ember, _offlineAdaptersApplication) {
    exports['default'] = _offlineAdaptersApplication['default'].extend({
        project: _ember['default'].inject.service('current-project'),
        urlTemplate: '/surveys/:survey/assets/:id',
        urlTemplateParams: function urlTemplateParams(modelName, id) {
            return {
                id: id,
                survey: this.get('project.path')
            };
        }
    });
});
define('offline/adapters/hash', ['exports', 'ember', 'offline/adapters/application'], function (exports, _ember, _offlineAdaptersApplication) {
    exports['default'] = _offlineAdaptersApplication['default'].extend({
        urlTemplate: '/surveys/:survey/hash',
        urlTemplateParams: function urlTemplateParams(_, path) {
            return { survey: path };
        },
        shouldReloadRecord: function shouldReloadRecord() {
            return true;
        }
    });
});
define('offline/adapters/interviewer', ['exports', 'ember', 'offline/adapters/application'], function (exports, _ember, _offlineAdaptersApplication) {
    exports['default'] = _offlineAdaptersApplication['default'].extend({
        project: _ember['default'].inject.service('current-project'),
        urlTemplate: '/surveys/:survey/interviewers/:id',
        urlTemplateParams: function urlTemplateParams(_, id) {
            return {
                id: id,
                survey: this.get('project.path')
            };
        }
    });
});
define('offline/adapters/message', ['exports', 'ember', 'offline/adapters/application'], function (exports, _ember, _offlineAdaptersApplication) {
    exports['default'] = _offlineAdaptersApplication['default'].extend({
        project: _ember['default'].inject.service('current-project'),
        urlTemplate: '/surveys/:survey/messages/:id',
        urlTemplateParams: function urlTemplateParams(_, id) {
            return {
                id: id,
                survey: this.get('project.path')
            };
        }
    });
});
define('offline/adapters/respondent', ['exports', 'ember', 'ember-data', 'offline/adapters/application', 'lodash/lodash'], function (exports, _ember, _emberData, _offlineAdaptersApplication, _lodashLodash) {

    var QUEUE = { payload: [], pending: {} };

    /**
     * Coalesce all POST requests into one single POST containing all respondents.
     * Then individually resolve the respondents that were returned,
     * and reject those that weren't
     * @param {Function} $ajax
     * @param {Object} options
     * @returns {jQuery.jqXHR}
     */
    function dequeue($ajax, options) {
        var pending = QUEUE.pending;
        options.data = JSON.stringify({ respondents: QUEUE.payload.splice(0) });
        options.success = function resolve(data, textStatus, jqXHR) {
            for (var i = 0; i < data.respondents.length; i++) {
                var payload = data.respondents[i];
                var copyXHR = Object.create(jqXHR);
                if (payload.hasOwnProperty('code')) {
                    copyXHR.status = payload.code;
                    delete payload.code;
                } else {
                    payload = { respondent: payload };
                }
                pending[data.respondents[i].id].resolve(payload, textStatus, copyXHR);
                delete pending[data.respondents[i].id];
            }
        };
        /**
         * Everything failed, resolve each inFlight respondent with the properly formatted error response
         * @param {Object} jqXHR
         * @param {String{ textStatus
         * @param {String} errorThrown
         */
        options.error = function reject(jqXHR, textStatus, errorThrown) {
            for (var i in pending) {
                if (pending.hasOwnProperty(i)) {
                    var payload = {
                        id: i,
                        errors: [{ answers: [errorThrown] }]
                    };
                    pending[i].resolve(payload, textStatus, jqXHR);
                    delete pending[i];
                }
            }
        };
        $ajax(options);
    }

    exports['default'] = _offlineAdaptersApplication['default'].extend({
        project: _ember['default'].inject.service('current-project'),
        urlTemplate: '/surveys/:survey/respondents/:id',
        urlTemplateParams: function urlTemplateParams(_, id) {
            return {
                id: id,
                survey: this.get('project.path')
            };
        },
        generateIdForRecord: function generateIdForRecord() {
            return Math.random().toString(36).substr(2, 5);
        },
        /**
         * Respondents stored on the device are keyed by their interviewer
         * @returns {string}
         * @private
         */
        _storageGroup: function _storageGroup() {
            return 'respondent:' + this.get('project.interviewer.id');
        },
        /**
         * Always retrieve respondents from device
         * @param store
         * @param type
         * @param sinceToken
         * @param snapshotRecordArray
         */
        findAll: function findAll(store, type, sinceToken, snapshotRecordArray) {
            snapshotRecordArray.adapterOptions = _lodashLodash['default'].extend(snapshotRecordArray.adapterOptions || {}, { device: true });
            return this._super(store, type, sinceToken, snapshotRecordArray);
        },
        /**
         * Prevent queries
         * @param store
         * @param type
         * @param completed
         */
        query: function query(store, type, _query) {
            return _ember['default'].RSVP.reject(new _emberData['default'].AdapterError('respondent::query is not supported, use respondent::findAll instead'));
        },
        /**
         * Always retrieve respondent from device
         * @param store
         * @param type
         * @param id
         * @param snapshot
         */
        findRecord: function findRecord(store, type, id, snapshot) {
            snapshot.adapterOptions = _lodashLodash['default'].extend(snapshot.adapterOptions || {}, { device: true });
            return this._super(store, type, id, snapshot);
        },
        /**
         * Save partials to device, unless adapterOptions.device is explicitly false
         * @param store
         * @param type
         * @param snapshot
         * @returns {Ember.RSVP.Promise}
         */
        createRecord: function createRecord(store, type, snapshot) {
            var _this = this;

            if (snapshot.record.get('partial')) {
                if (snapshot.adapterOptions && !snapshot.adapterOptions.device) {
                    // noop
                } else {
                        snapshot.adapterOptions = _lodashLodash['default'].extend(snapshot.adapterOptions || {}, { device: true });
                    }
            }

            // update the project-level completion / partial upload sucess status
            var handleSuccess = function handleSuccess(payload) {
                var prop = 'partials';
                if (snapshot.record.get('completed')) {
                    prop = 'completes';
                    _this.incrementProperty('project.interviewer.total_completes');
                    _this.incrementProperty('project.interviewer.valid_completes');
                }
                _this.incrementProperty('project.status.' + prop + '.count');
                _this.get('project.status.' + prop).setProperties({
                    success: true,
                    failure: false,
                    errcode: false
                });
                return payload;
            };

            // update the project-level completion / partial upload error status
            var handleError = function handleError(error) {
                var prop = snapshot.record.get('completed') ? 'completes' : 'partials';
                var invalid = error instanceof _emberData['default'].InvalidError;
                _this.get('project.status.' + prop).setProperties({
                    success: false,
                    failure: invalid,
                    errcode: !invalid
                });
                throw error;
            };

            var promise = this._super(store, type, snapshot);

            if (snapshot.adapterOptions.device) {
                return promise;
            }
            return promise.then(handleSuccess, handleError);
        },
        /**
         * POST everything (i.e., always overwrite this record id on device and backend with new data when saving)
         * @param store
         * @param type
         * @param snapshot
         * @returns {Ember.RSVP.Promise}
         */
        updateRecord: function updateRecord(store, type, snapshot) {
            return this.createRecord(store, type, snapshot);
        },
        /**
         * Always only remove records from the device when deleting
         * @param store
         * @param type
         * @param snapshot
         * @returns {Ember.RSVP.Promise}
         */
        deleteRecord: function deleteRecord(store, type, snapshot) {
            snapshot.adapterOptions = _lodashLodash['default'].extend(snapshot.adapterOptions || {}, { device: true });
            return this._super(store, type, snapshot);
        },
        /**
         * Anything persisted to the backend should be unloaded (removed from memory) and dropped from device storage
         * @param   {Number} status
         * @param   {Object} headers
         * @param   {Object} payload
         * @param   {Object} requestData - the original request information
         * @returns {Object | DS.AdapterError} response
         */
        handleResponse: function handleResponse(status, headers, payload, requestData) {
            var _this2 = this;

            if (requestData.method === 'POST') {
                if (status === 200) {
                    // current record is inFlight, wait for state change
                    this.store.peekRecord('respondent', payload.respondent.id).on('didUpdate', function (record) {
                        _this2.get('storage')['delete'](_this2._storageGroup(), record.id).then(function () {
                            return _this2.store.unloadRecord(record);
                        });
                    });
                }
            }

            return this._super(status, headers, payload, requestData);
        },
        /**
         * Ember so graciously nests XHR success and error logic within jQuery's success / error hooks
         * rather than having jQuery resolve / reject some promise and handling the logic in a Promise.then()
         * We need to retain a reference to these functions, and tell jQuery to resolve / reject them in response
         * to the bulk POST.
         * @param options
         */
        _ajaxRequest: function _ajaxRequest(options) {
            var _this3 = this;

            if (options.type === 'POST') {
                var _super = this._super.bind(this);
                var data = JSON.parse(options.data);
                // avoid posting twice really fast
                if (QUEUE.pending.hasOwnProperty(data.respondent.id)) {
                    return _ember['default'].run.later(function () {
                        return _this3._ajaxRequest(options);
                    }, 1000);
                }
                QUEUE.payload.push(data.respondent);
                QUEUE.pending[data.respondent.id] = { resolve: options.success, reject: options.error };
                _ember['default'].run.debounce(this, dequeue, _super, options, 1000);
            } else {
                this._super(options);
            }
        }
    });
});
define('offline/adapters/survey', ['exports', 'ember', 'offline/adapters/application', 'lodash/lodash'], function (exports, _ember, _offlineAdaptersApplication, _lodashLodash) {
    exports['default'] = _offlineAdaptersApplication['default'].extend({
        project: _ember['default'].inject.service('current-project'),
        serviceWorker: _ember['default'].inject.service('service-worker'),
        urlTemplate: '/surveys/:survey',
        urlTemplateParams: function urlTemplateParams(_, id) {
            return { survey: id };
        },
        /**
         * Reload when we have a different hash in the store
         * @param {DS.Store} store
         * @param {DS.Snapshot} snapshot
         * @returns {boolean}
         */
        shouldReloadRecord: function shouldReloadRecord(store, snapshot) {
            // The include parameter tells the server which relationships to sideload in a response
            if (snapshot.include) {
                var includeMissingRelation;

                var _ret = (function () {
                    includeMissingRelation = false;

                    var include = _lodashLodash['default'].isString(snapshot.include) ? snapshot.include.split(',') : snapshot.include;
                    snapshot.eachRelationship(function (name, relationship) {
                        // are we asking to include something that we don't currently have?
                        if (include.indexOf(name) > -1) {
                            if (relationship.kind === 'hasMany' && !snapshot.hasMany(name)) {
                                includeMissingRelation = true;
                            } else if (relationship.kind === 'belongsTo' && !snapshot.belongsTo(name)) {
                                includeMissingRelation = true;
                            }
                        }
                    });
                    if (includeMissingRelation) {
                        return {
                            v: true
                        };
                    }
                })();

                if (typeof _ret === 'object') return _ret.v;
            }

            var latestHash = store.peekRecord('hash', this.get('project.path'));
            if (latestHash) {
                return latestHash.get('value') !== snapshot.record.get('hash');
            }
            return true;
        },
        shouldReloadAll: function shouldReloadAll() {
            return false; // TODO?
        },
        shouldBackgroundReloadRecord: function shouldBackgroundReloadRecord() {
            return false;
        },
        shouldBackgroundReloadAll: function shouldBackgroundReloadAll() {
            return false;
        },
        /**
         * Cache assets when downloading a survey and side-loading asset records
         * @returns {Promise}
         */
        findRecord: function findRecord(store, type, id, snapshot) {
            var _this = this;

            if (snapshot.include) {
                var include = _lodashLodash['default'].isString(snapshot.include) ? snapshot.include.split(',') : snapshot.include;
                if (include.indexOf('assets') > -1) {
                    return this._super.apply(this, arguments).then(function (response) {
                        var urls = response.survey.assets.map(function (a) {
                            return a.id;
                        });
                        return _this.get('serviceWorker').precache(urls).then(function () {
                            return response;
                        })['catch'](function (data) {
                            _ember['default'].Logger.warn(data.error);
                            return response;
                        });
                    });
                }
            }
            return this._super.apply(this, arguments);
        }
    });
});
define('offline/adapters/user', ['exports', 'ember', 'offline/adapters/application', 'offline/config/environment', 'offline/instance-initializers/current-user'], function (exports, _ember, _offlineAdaptersApplication, _offlineConfigEnvironment, _offlineInstanceInitializersCurrentUser) {
    exports['default'] = _offlineAdaptersApplication['default'].extend({
        getFromStorage: function getFromStorage(type, _) {
            return this._super(type, _offlineInstanceInitializersCurrentUser.CURRENT_USER_ID);
        },
        delFromStorage: function delFromStorage(type, _) {
            return this._super(type, _offlineInstanceInitializersCurrentUser.CURRENT_USER_ID);
        },
        setIntoStorage: function setIntoStorage(type, data) {
            var modelName = type.modelName;
            data[modelName][this.store.serializerFor(modelName).get('primaryKey')] = _offlineInstanceInitializersCurrentUser.CURRENT_USER_ID;
            return this._super(type, data, /* overwrite = */true);
        },
        /**
         * Always use the session cookie when fetching user info
         * If the current session cookie belongs to an active user session,
         * then the session will be authenticated and persisted to localStorage
         * otherwise the cookie won't be set into localStorage and authentication must pass /login
         */
        ajaxOptions: function ajaxOptions() {
            var authenticator = _ember['default'].getOwner(this).lookup('authenticator:authenticator');
            var cookie = authenticator.getCookie();
            var session = authenticator.makeSession(cookie);
            var beacon_login = window.localStorage.getItem('beacon_login');

            var hash = this._super.apply(this, arguments);
            var beforeSend = hash.beforeSend;

            hash.beforeSend = function (xhr) {
                if (beforeSend) {
                    beforeSend(xhr);
                }
                xhr.setRequestHeader('x-apikey', session.apikey);
                if (_offlineConfigEnvironment['default'].environment === 'mobile') {
                    xhr.setRequestHeader('BEACON_LOGIN', beacon_login);
                    xhr.setRequestHeader('native-app-request', true);
                }
            };
            return hash;
        }
    });
});
define('offline/adapters/variable', ['exports', 'offline/adapters/application', 'ember', 'ember-data', 'lodash/lodash'], function (exports, _offlineAdaptersApplication, _ember, _emberData, _lodashLodash) {

    /**
     * Fake backend for uploading responses
     */
    exports['default'] = _offlineAdaptersApplication['default'].extend({
        project: _ember['default'].inject.service('current-project'),
        respondent: _ember['default'].computed.alias('project.respondent'),
        /**
         * When we submit a page, we validate all variables on the page by default,
         * commit those values if they pass, then save them to the respondent's 'answers'
         * @param store
         * @param type
         * @param snapshot
         * @returns {Promise}
         */
        updateRecord: function updateRecord(store, type, snapshot) {
            if (!snapshot.adapterOptions || snapshot.adapterOptions.validate !== false) {
                var errors = snapshot.belongsTo('question').record.verifyInput(snapshot.record);

                if (errors && errors.length) {
                    return _ember['default'].RSVP.reject(new _emberData['default'].InvalidError([{ value: errors }]));
                }
            }

            var data = {},
                respondent = this.get('respondent');
            store.serializerFor(type.modelName).serializeIntoHash(data, type, snapshot, { includeId: true });

            var answers = respondent.get('answers');
            answers[data.variable.label] = data.variable.value;
            respondent.set('answers', answers);

            return _ember['default'].RSVP.resolve(data);
        },
        createRecord: function createRecord() {
            return _ember['default'].RSVP.reject(new _emberData['default'].AdapterError('invalid attempt to create new variable'));
        },
        /**
         * Get the correct value for this variable (the value most recently committed to this respondent)
         * @param store
         * @param type
         * @param id
         * @param snapshot
         * @returns {Promise}
         */
        findRecord: function findRecord(store, type, id, snapshot) {
            var data = _lodashLodash['default'].clone(snapshot.record.data);
            data.id = id;
            data.value = this.get('respondent.answers')[data.label] || null;
            return _ember['default'].RSVP.Promise.resolve({ variable: data });
        }
    });
});
define('offline/app', ['exports', 'ember', 'offline/resolver', 'ember-load-initializers', 'offline/config/environment'], function (exports, _ember, _offlineResolver, _emberLoadInitializers, _offlineConfigEnvironment) {

  var App = undefined;

  _ember['default'].MODEL_FACTORY_INJECTIONS = true;

  App = _ember['default'].Application.extend({
    modulePrefix: _offlineConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _offlineConfigEnvironment['default'].podModulePrefix,
    Resolver: _offlineResolver['default']
  });

  (0, _emberLoadInitializers['default'])(App, _offlineConfigEnvironment['default'].modulePrefix);

  exports['default'] = App;
});
define('offline/authenticators/authenticator', ['exports', 'ember', 'offline/config/environment', 'ember-simple-auth/authenticators/base'], function (exports, _ember, _offlineConfigEnvironment, _emberSimpleAuthAuthenticatorsBase) {
    var RSVP = _ember['default'].RSVP;
    var $ = _ember['default'].$;

    var FKEY_RX = /.*(?:^|;)\s*HERMES_FKEY=([^;\s]*)/;

    exports['default'] = _emberSimpleAuthAuthenticatorsBase['default'].extend({
        network: _ember['default'].inject.service(),
        currentUser: _ember['default'].inject.service('current-user'),
        environment: _offlineConfigEnvironment['default'].environment,
        namespace: _offlineConfigEnvironment['default'].authNameSpace,
        storage: window.localStorage,

        /**
         * @description
         * Called by the session service to authenticate a user. The data this resolves
         * with is set on the session service `data.authenticated` property.
         * @param {string} email
         * @param {string} password
         * @returns {Promise}
         */
        authenticate: function authenticate(email, password) {
            var _this = this;

            var online = this.get('network').get('online');
            var cookie = this.getCookie();

            if (!email && !password && cookie && online) {
                return this.eagerAuth(cookie);
            } else if (email && password && !online) {
                return RSVP.reject(this.get('intl').t('Unable to log in while offline'));
            }

            return new RSVP.Promise(function (resolve, reject) {
                var settings = {
                    method: 'POST',
                    contentType: 'application/x-www-form-urlencoded',
                    data: _this.makePayload(email, password)
                };

                _this.makeRequest('login', settings).then(function (response, status, xhr) {
                    if (_this.environment === 'mobile') {
                        _this.storage.setItem('auth_key', response.apikey);
                        _this.storage.setItem('beacon_login', response.beacon_login);
                    }

                    var cookie = _this.getCookie();

                    if (cookie) {
                        _ember['default'].run(function () {
                            resolve(_this.makeSession(cookie));
                        });
                    }

                    _ember['default'].run(function () {
                        reject('Failed to Authenticate');
                    });
                }, function (xhr) {
                    _ember['default'].run(function () {
                        reject(xhr.responseJSON.error || _this.get('intl').t('Failed to Authenticate'));
                    });
                });
            });
        },

        /**
         * @description
         * Attempt to restore a user from the session data object. If successful they
         * don't need to log in again. This is invoked by the session on app startup.
         * @param {Object} session
         * @returns {Promise}
         */
        restore: function restore(session) {
            var online = this.get('network').get('online');
            var cookie = this.getCookie();

            if (cookie && 'session ' + cookie !== session.apikey) {
                session = this.makeSession(cookie);
            }

            // if the key is expired a failed request will log us out
            if (session.apikey) {
                return RSVP.resolve(session);
            }
            return RSVP.reject();
        },

        /**
         * @description
         * Invalidate a session, i.e. log them out.
         * Note: A user can't log out when they are offline. And when they are
         * online we defer the logging out till respondent data has been uploaded.
         * @returns {Promise}
         */
        invalidate: function invalidate() {
            var online = this.get('network').get('online');

            if (online) {
                // only allow logging off if they are online
                // TODO: Defer logging out until respondents have been uploaded
                return RSVP.resolve(this.makeRequest('login', { data: { logout: 1 } }));
            }
            // I want to get off MR BONES WILD RIDE
            return RSVP.reject();
        },

        makeRequest: function makeRequest(resource) {
            var settings = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

            _ember['default'].assign(settings, {
                url: this.get('namespace') + resource
            });

            return $.ajax(settings);
        },

        makeSession: function makeSession(cookie) {
            var expires = parseInt(cookie.split('-')[1]) * 1000;
            return {
                apikey: 'session ' + cookie,
                expires: expires
            };
        },

        getCookie: function getCookie() {
            var cookie = this.environment === 'mobile' ? this.storage.getItem('auth_key') : document.cookie.match(FKEY_RX)[1];
            return cookie === null ? null : cookie;
        },

        makePayload: function makePayload(email, password, persistent) {
            var clientTime = Math.floor(new Date().getTime() / 1000);
            var payload = 'client_time=' + clientTime + '&username=' + email + '&password=' + password + '&env=' + this.environment;

            if (persistent) {
                payload += '&remember=on';
            }

            return payload;
        },

        /**
         * Attempt to eagerly login if we have a cookie set through a portal login.
         * @param {string} cookie formkey cookie
         * @returns {Promise}
         */
        eagerAuth: function eagerAuth(cookie) {
            var _this2 = this;

            // logging out will update the form cookie expire epoch part
            if (!cookie || this.isExpired(cookie)) {
                return RSVP.reject();
            }
            // eagerAuth should fail if this session is no longer valid
            return new _ember['default'].RSVP.Promise(function (resolve, reject) {
                _this2.get('currentUser').load().then(function () {
                    return resolve(_this2.makeSession(cookie));
                }, function () {
                    return reject();
                });
            });
        },

        /**
         * @description
         * When a user logs out the response will set the cookie with an
         * expired value === server epoch
         * @param {string} cookie formkey cookie
         * @returns {boolean}
         */
        isExpired: function isExpired(cookie) {
            var servertime = parseInt(cookie.split('-')[0]) * 1000;
            var expires = parseInt(cookie.split('-')[1]) * 1000;

            return expires < Date.now() || expires <= servertime;
        },

        /**
         * @description
         * Make a password reset request if we are online.
         * @param {string} email
         * @returns {Promise}
         */
        forgotPassword: function forgotPassword(email) {
            var _this3 = this;

            var online = this.get('network').get('online');

            if (!online) {
                return RSVP.reject(this.get('intl').t('Unable to send password reset request while offline'));
            }

            return new RSVP.Promise(function (resolve, reject) {
                var settings = {
                    method: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({ email: email })
                };

                _this3.makeRequest('forgotPassword', settings).then(function (response, status, xhr) {
                    _ember['default'].run(resolve);
                }, function (xhr) {
                    _ember['default'].run(reject);
                });
            });
        }
    });
});
define('offline/authorizers/authorizer', ['exports', 'ember', 'ember-simple-auth/authorizers/base'], function (exports, _ember, _emberSimpleAuthAuthorizersBase) {
    exports['default'] = _emberSimpleAuthAuthorizersBase['default'].extend({
        /**
         * Used to authorize outgoing requests.
         * The `DataAdapterMixin` will use this authorizer to inject a header with
         * the `block` callback into the request.
         * @param {object} sessionData
         * @param {Function} block
         */
        authorize: function authorize(sessionData, block) {
            var apikey = sessionData.apikey;
            var beacon_login = window.localStorage.getItem('beacon_login');

            if (apikey) {
                block('x-apikey', apikey);
                block('BEACON_LOGIN', beacon_login);
            }
        }
    });
});
define("offline/cldrs/en", ["exports"], function (exports) {
  /*jslint eqeq: true*/
  exports["default"] = [{ "locale": "en-US", "parentLocale": "en" }, { "locale": "en", "pluralRuleFunction": function pluralRuleFunction(n, ord) {
      var s = String(n).split("."),
          v0 = !s[1],
          t0 = Number(s[0]) == n,
          n10 = t0 && s[0].slice(-1),
          n100 = t0 && s[0].slice(-2);if (ord) return n10 == 1 && n100 != 11 ? "one" : n10 == 2 && n100 != 12 ? "two" : n10 == 3 && n100 != 13 ? "few" : "other";return n == 1 && v0 ? "one" : "other";
    }, "fields": { "year": { "displayName": "year", "relative": { "0": "this year", "1": "next year", "-1": "last year" }, "relativeTime": { "future": { "one": "in {0} year", "other": "in {0} years" }, "past": { "one": "{0} year ago", "other": "{0} years ago" } } }, "month": { "displayName": "month", "relative": { "0": "this month", "1": "next month", "-1": "last month" }, "relativeTime": { "future": { "one": "in {0} month", "other": "in {0} months" }, "past": { "one": "{0} month ago", "other": "{0} months ago" } } }, "day": { "displayName": "day", "relative": { "0": "today", "1": "tomorrow", "-1": "yesterday" }, "relativeTime": { "future": { "one": "in {0} day", "other": "in {0} days" }, "past": { "one": "{0} day ago", "other": "{0} days ago" } } }, "hour": { "displayName": "hour", "relativeTime": { "future": { "one": "in {0} hour", "other": "in {0} hours" }, "past": { "one": "{0} hour ago", "other": "{0} hours ago" } } }, "minute": { "displayName": "minute", "relativeTime": { "future": { "one": "in {0} minute", "other": "in {0} minutes" }, "past": { "one": "{0} minute ago", "other": "{0} minutes ago" } } }, "second": { "displayName": "second", "relative": { "0": "now" }, "relativeTime": { "future": { "one": "in {0} second", "other": "in {0} seconds" }, "past": { "one": "{0} second ago", "other": "{0} seconds ago" } } } } }];
});
define("offline/cldrs/es", ["exports"], function (exports) {
  /*jslint eqeq: true*/
  exports["default"] = [{ "locale": "es-MX", "parentLocale": "es-419", "fields": { "year": { "displayName": "año", "relative": { "0": "este año", "1": "el año próximo", "-1": "el año pasado" }, "relativeTime": { "future": { "one": "dentro de {0} año", "other": "dentro de {0} años" }, "past": { "one": "hace {0} año", "other": "hace {0} años" } } }, "month": { "displayName": "mes", "relative": { "0": "este mes", "1": "el mes próximo", "-1": "el mes pasado" }, "relativeTime": { "future": { "one": "en {0} mes", "other": "en {0} meses" }, "past": { "one": "hace {0} mes", "other": "hace {0} meses" } } }, "day": { "displayName": "día", "relative": { "0": "hoy", "1": "mañana", "2": "pasado mañana", "-2": "antier", "-1": "ayer" }, "relativeTime": { "future": { "one": "dentro de {0} día", "other": "dentro de {0} días" }, "past": { "one": "hace {0} día", "other": "hace {0} días" } } }, "hour": { "displayName": "hora", "relativeTime": { "future": { "one": "dentro de {0} hora", "other": "dentro de {0} horas" }, "past": { "one": "hace {0} hora", "other": "hace {0} horas" } } }, "minute": { "displayName": "minuto", "relativeTime": { "future": { "one": "dentro de {0} minuto", "other": "dentro de {0} minutos" }, "past": { "one": "hace {0} minuto", "other": "hace {0} minutos" } } }, "second": { "displayName": "segundo", "relative": { "0": "ahora" }, "relativeTime": { "future": { "one": "dentro de {0} segundo", "other": "dentro de {0} segundos" }, "past": { "one": "hace {0} segundo", "other": "hace {0} segundos" } } } } }, { "locale": "es-419", "parentLocale": "es" }, { "locale": "es", "pluralRuleFunction": function pluralRuleFunction(n, ord) {
      if (ord) return "other";return n == 1 ? "one" : "other";
    } }];
});
define("offline/cldrs/fr", ["exports"], function (exports) {
  /*jslint eqeq: true*/
  exports["default"] = [{ "locale": "fr-fr", "parentLocale": "fr" }, { "locale": "fr", "pluralRuleFunction": function pluralRuleFunction(n, ord) {
      if (ord) return n == 1 ? "one" : "other";return n >= 0 && n < 2 ? "one" : "other";
    }, "fields": { "year": { "displayName": "année", "relative": { "0": "cette année", "1": "l’année prochaine", "-1": "l’année dernière" }, "relativeTime": { "future": { "one": "dans {0} an", "other": "dans {0} ans" }, "past": { "one": "il y a {0} an", "other": "il y a {0} ans" } } }, "month": { "displayName": "mois", "relative": { "0": "ce mois-ci", "1": "le mois prochain", "-1": "le mois dernier" }, "relativeTime": { "future": { "one": "dans {0} mois", "other": "dans {0} mois" }, "past": { "one": "il y a {0} mois", "other": "il y a {0} mois" } } }, "day": { "displayName": "jour", "relative": { "0": "aujourd’hui", "1": "demain", "2": "après-demain", "-2": "avant-hier", "-1": "hier" }, "relativeTime": { "future": { "one": "dans {0} jour", "other": "dans {0} jours" }, "past": { "one": "il y a {0} jour", "other": "il y a {0} jours" } } }, "hour": { "displayName": "heure", "relativeTime": { "future": { "one": "dans {0} heure", "other": "dans {0} heures" }, "past": { "one": "il y a {0} heure", "other": "il y a {0} heures" } } }, "minute": { "displayName": "minute", "relativeTime": { "future": { "one": "dans {0} minute", "other": "dans {0} minutes" }, "past": { "one": "il y a {0} minute", "other": "il y a {0} minutes" } } }, "second": { "displayName": "seconde", "relative": { "0": "maintenant" }, "relativeTime": { "future": { "one": "dans {0} seconde", "other": "dans {0} secondes" }, "past": { "one": "il y a {0} seconde", "other": "il y a {0} secondes" } } } } }];
});
define("offline/cldrs/ja", ["exports"], function (exports) {
  /*jslint eqeq: true*/
  exports["default"] = [{ "locale": "ja-jp", "parentLocale": "ja" }, { "locale": "ja", "pluralRuleFunction": function pluralRuleFunction(n, ord) {
      if (ord) return "other";return "other";
    }, "fields": { "year": { "displayName": "年", "relative": { "0": "今年", "1": "翌年", "-1": "昨年" }, "relativeTime": { "future": { "other": "{0} 年後" }, "past": { "other": "{0} 年前" } } }, "month": { "displayName": "月", "relative": { "0": "今月", "1": "翌月", "-1": "先月" }, "relativeTime": { "future": { "other": "{0} か月後" }, "past": { "other": "{0} か月前" } } }, "day": { "displayName": "日", "relative": { "0": "今日", "1": "明日", "2": "明後日", "-2": "一昨日", "-1": "昨日" }, "relativeTime": { "future": { "other": "{0} 日後" }, "past": { "other": "{0} 日前" } } }, "hour": { "displayName": "時", "relativeTime": { "future": { "other": "{0} 時間後" }, "past": { "other": "{0} 時間前" } } }, "minute": { "displayName": "分", "relativeTime": { "future": { "other": "{0} 分後" }, "past": { "other": "{0} 分前" } } }, "second": { "displayName": "秒", "relative": { "0": "今すぐ" }, "relativeTime": { "future": { "other": "{0} 秒後" }, "past": { "other": "{0} 秒前" } } } } }];
});
define("offline/cldrs/pt", ["exports"], function (exports) {
  /*jslint eqeq: true*/
  exports["default"] = [{ "locale": "pt-PT", "parentLocale": "pt", "pluralRuleFunction": function pluralRuleFunction(n, ord) {
      var s = String(n).split("."),
          v0 = !s[1];if (ord) return "other";return n == 1 && v0 ? "one" : "other";
    }, "fields": { "year": { "displayName": "ano", "relative": { "0": "este ano", "1": "próximo ano", "-1": "ano passado" }, "relativeTime": { "future": { "one": "dentro de {0} ano", "other": "dentro de {0} anos" }, "past": { "one": "há {0} ano", "other": "há {0} anos" } } }, "month": { "displayName": "mês", "relative": { "0": "este mês", "1": "próximo mês", "-1": "mês passado" }, "relativeTime": { "future": { "one": "dentro de {0} mês", "other": "dentro de {0} meses" }, "past": { "one": "há {0} mês", "other": "há {0} meses" } } }, "day": { "displayName": "dia", "relative": { "0": "hoje", "1": "amanhã", "2": "depois de amanhã", "-2": "anteontem", "-1": "ontem" }, "relativeTime": { "future": { "one": "dentro de {0} dia", "other": "dentro de {0} dias" }, "past": { "one": "há {0} dia", "other": "há {0} dias" } } }, "hour": { "displayName": "hora", "relativeTime": { "future": { "one": "dentro de {0} hora", "other": "dentro de {0} horas" }, "past": { "one": "há {0} hora", "other": "há {0} horas" } } }, "minute": { "displayName": "minuto", "relativeTime": { "future": { "one": "dentro de {0} minuto", "other": "dentro de {0} minutos" }, "past": { "one": "há {0} minuto", "other": "há {0} minutos" } } }, "second": { "displayName": "segundo", "relative": { "0": "agora" }, "relativeTime": { "future": { "one": "dentro de {0} segundo", "other": "dentro de {0} segundos" }, "past": { "one": "há {0} segundo", "other": "há {0} segundos" } } } } }, { "locale": "pt" }];
});
define('offline/components/atm1d-element', ['exports', 'ember', 'lodash/lodash'], function (exports, _ember, _lodashLodash) {

    var BREAKPOINT = 768;
    var CSS_VALUE = /^(Auto)|([0-9]+(?:px|em|%)?)$/;

    var SELECTORS = {
        buttons: '.sq-atm1d-buttons',
        button: '.sq-atm1d-button',
        row: '.sq-atm1d-row'
    };

    var Classes = _ember['default'].Object.extend({
        viewMode: null,
        showInput: null,
        buttonAlign: null,
        contentAlign: null
    });

    /**
     * Make sense of something like { max-height: "100%"; }
     */
    function parseCSSValue(value) {
        return CSS_VALUE.test(value) ? value : null;
    }

    /**
     * A View Prototype
     * @interface
     */
    var Atm1dView = _ember['default'].Object.extend({
        question: null,
        $element: null,

        classes: _ember['default'].computed(function () {
            return Classes.create();
        }),

        vertical: false,
        tiled: false,
        multicol: false,
        horizontal: false,
        /**
         * Large and small viewport settings
         */
        large_buttonAlign: 'left',
        large_contentAlign: 'center',
        large_maxHeight: 'none',
        large_maxWidth: '150px',
        large_minHeight: '70px',
        large_minWidth: '55px',
        small_buttonAlign: 'left',
        small_contentAlign: 'center',
        small_maxHeight: 'none',
        small_maxWidth: '100%',
        small_minHeight: '70px',
        small_minWidth: '55px',

        /**
         * Return the object that should be rendered
         * @returns {Vertical|Tiled|Multicol|Horizontal}
         */
        render: function render() {
            return this;
        },
        /**
         * Balances the sizes of all buttons so they are of equal size
         * Remove the rules applied in updateCSS so each btn is just a block with a set height and width
         * @returns {Promise}
         */
        balanceSize: function balanceSize() {
            var _this = this;

            var $buttons = this.$element.find(SELECTORS.button);
            var $images = this.$element.find('img');
            var loadedImages = 0;
            this.outerWidth = this.outerHeight = this.innerWidth = this.innerHeight = 0;

            return new _ember['default'].RSVP.Promise(function (resolve, reject) {

                var self = _this;

                function onload() {
                    if (++loadedImages < $images.length) {
                        return;
                    }
                    $buttons.each(function () {
                        var $this = _ember['default'].$(this);
                        self.outerWidth = Math.max(self.outerWidth, $this.outerWidth(true));
                        self.outerHeight = Math.max(self.outerHeight, $this.outerHeight(true));
                        self.innerWidth = Math.max(self.innerWidth, $this.innerWidth());
                        self.innerHeight = Math.max(self.innerHeight, $this.innerHeight());
                    });
                    $buttons.css({
                        display: 'block',
                        width: self.innerWidth + 'px',
                        height: self.innerHeight + 'px',
                        minwidth: '0',
                        minHeight: '0',
                        maxWidth: 'none',
                        maxHeight: 'none'
                    });
                    _ember['default'].run.later(resolve);
                }

                for (var i = 0; i < $images.length; i++) {
                    if ($images[i].complete) {
                        loadedImages++;
                        continue;
                    }
                    $images[i].onload = onload;
                }
                if (loadedImages === $images.length) {
                    onload();
                }
            });
        },
        /**
         * Apply a set of rules to our buttons to prepare them for rendering and adjusting:
         * - update our classes from large_* to small_*, or vice versa
         * - set the min/max width/height
         * - set as inline-table so that the size of the btn is no smaller than its contents
         * @returns {Promise}
         */
        updateCSS: function updateCSS() {
            var layout = window.innerWidth <= BREAKPOINT ? 'small' : 'large';
            var prefix = 'atm1d:' + layout;
            var settings = this.question.get('styleAttributes');

            var viewMode = this.viewMode;
            var showInput = settings['atm1d:showInput'] === '0' ? 'false' : 'true';
            var buttonAlign = settings[prefix + '_buttonAlign'] || this[layout + '_buttonAlign'];
            var contentAlign = settings[prefix + '_contentAlign'] || this[layout + '_contentAlign'];

            this.get('classes').setProperties({ viewMode: viewMode, showInput: showInput, buttonAlign: buttonAlign, contentAlign: contentAlign });

            this.$element.find(SELECTORS.button).css({
                display: 'inline-table',
                width: 'auto',
                height: 'auto',
                minWidth: parseCSSValue(settings[prefix + '_minWidth']) || this[layout + '_minWidth'],
                minHeight: parseCSSValue(settings[prefix + '_minHeight']) || this[layout + '_minHeight'],
                maxWidth: parseCSSValue(settings[prefix + '_maxWidth']) || this[layout + '_maxWidth'],
                maxHeight: parseCSSValue(settings[prefix + '_maxHeight']) || this[layout + '_maxHeight']
            });

            // NOOP: run on next tick of event loop, after page has rerendered
            return new _ember['default'].RSVP.Promise(function (resolve, _) {
                return resolve();
            });
        }
    });

    /**
     * Vertical display
     * Buttons are stacked vertically.
     * Wrapping does not occur.
     * Left-aligning of button icons, Left-aligning of button contents (text)
     * @extends {Atm1dView}
     * @constructor
     */
    var Vertical = Atm1dView.extend({
        vertical: true,
        viewMode: 'vertical',
        large_buttonAlign: 'left',
        large_contentAlign: 'left',
        large_maxHeight: '100%',
        large_maxWidth: '100%',
        large_minWidth: '280px',
        large_minHeight: '56px',
        small_buttonAlign: 'left',
        small_contentAlign: 'left',
        small_maxHeight: '100%',
        small_maxWidth: '100%',
        small_minWidth: '75%',
        small_minHeight: '56px'
    });

    /**
     * Tiled display
     * @extends {Atm1dView}
     * @constructor
     */
    var Tiled = Atm1dView.extend({
        init: function init() {
            this._super.apply(this, arguments);
            this.numCols = Number(this.question.get('styleAttributes')['atm1d:numCols']);
            if (isNaN(this.numCols) || this.numCols < 1) {
                this.numCols = null;
            }
        },
        tiled: true,
        viewMode: 'tiled',
        /**
         * Determine the width such that there are only N number of columns,
         * where N = "atm1d:numCols" or 1.
         * If we cannot fit N columns in the view, then collapse to Vertical.
         * @this {Tiled}
         */
        render: function render() {
            if (this.numCols) {
                var width = this.outerWidth * this.numCols;
                if (width > this.$element.innerWidth()) {
                    return Vertical.create({
                        question: this.question,
                        $element: this.$element
                    });
                }
                this.$element.width(width);
            }
            return this;
        }
    });

    /**
     * Multicol display
     * @extends {Atm1dView}
     * @constructor
     */
    var Multicol = Atm1dView.extend({
        init: function init() {
            this._super.apply(this, arguments);
            this.numCols = Number(this.question.get('styleAttributes')['atm1d:numCols']);
            if (isNaN(this.numCols) || this.numCols < 1) {
                this.numCols = null;
            }
        },
        multicol: true,
        viewMode: 'multicol',
        /**
         * Update to the current view.
         * Determine the width such that there are only N number of columns,
         * where N = "atm1d:numCols" or 1.
         * Determine the height such that the number of buttons in each column are equal (within 1).
         * If we cannot fit N columns in the view, then collapse to Vertical.
         * @TODO(brian)
         * @this {Multicol}
         */
        render: function render() {
            var width, height;
            var $buttons = this.$element.find(SELECTORS.button);
            if (this.numCols) {
                width = this.outerWidth * this.numCols;
                if (width > this.$element.innerWidth()) {
                    return Vertical.create({
                        question: this.question,
                        $element: this.$element
                    });
                }
                height = this.outerHeight * ($buttons.length / this.numCols);
            } else {
                width = this.width;
                var buttonsPerRow = width / $buttons.outerWidth();
                var buttonsPerCol = $buttons.length / buttonsPerRow;
                $buttons.width(width / buttonsPerRow);
                height = this.outerHeight * buttonsPerCol;
            }
            this.$element.width(width);
            this.$element.height(height);
            return this;
        }
    });

    /**
     * Horizontal display
     * @extends {Atm1dView}
     * @constructor
     */
    var Horizontal = Atm1dView.extend({
        init: function init() {
            this._super.apply(this, arguments);
            this.numCols = this.question.get('enabled.rows.length') || 1;
        },
        horizontal: true,
        viewMode: 'horizontal',
        /**
         * Attempt to render as a Horizontal view.
         * If all rows cannot fit within the viewPort, then change to Vertical.
         * @this {Horizontal}
         */
        render: function render() {
            var width = this.outerWidth * this.numCols;
            if (width > this.$element.innerWidth()) {
                return Vertical.create({
                    question: this.question,
                    $element: this.$element
                });
            }
            return this;
        }
    });

    var DEFAULT_Atm1dView_MODE = 'vertical';
    var Atm1dView_MODES = {
        vertical: Vertical,
        tiled: Tiled,
        multicol: Multicol,
        horizontal: Horizontal
    };

    exports['default'] = _ember['default'].Component.extend({
        classNames: ['sq-atm1d-widget', 'sq-atm1d-large'],
        classNameBindings: ['showInput'],
        showInput: _ember['default'].computed.reads('rendered.classes.showInput'),

        // the DEFINED viewMode
        viewmode: _ember['default'].computed('question.styleAttributes', function () {
            var view = this.get('question.styleAttributes')['atm1d:viewMode'] || DEFAULT_Atm1dView_MODE;
            return Atm1dView_MODES[view].create({ question: this.get('question') });
        }).readOnly(),

        // the RENDERED viewMode
        rendered: _ember['default'].computed('viewmode', {
            get: function get() {
                return this._rendered = this._rendered || this.get('viewmode');
            },
            /**
             * Maybe adjust the view
             * @param {Vertical|Tiled|Multicol|Horizontal} value
             */
            set: function set(_, value) {
                if (this._rendered !== value) {
                    this._rendered = value;
                    this.calculateView();
                }
                return this._rendered;
            }
        }),

        /**
         * @private
         * @returns {Promise}
         */
        calculateView: function calculateView() {
            var _this2 = this;

            return this.get('rendered').updateCSS().then(function () {
                return _this2.get('rendered').balanceSize();
            }).then(function () {
                return _this2.get('rendered').render();
            }).then(function (rendered) {
                // handle the very rare case that the question was destroyed before rendering could complete
                if (_this2.get('isDestroyed') || _this2.get('isDestroying')) {
                    return _this2.get('rendered');
                }
                return _this2.set('rendered', rendered);
            });
        },

        didInsertElement: function didInsertElement() {
            var _this3 = this;

            this.get('rendered').set('$element', _ember['default'].$(this.element));
            var timer,
                width = window.innerWidth;
            var handleResize = function handleResize() {
                if (width !== window.innerWidth) {
                    width = window.innerWidth;
                    _ember['default'].run.cancel(timer);
                    timer = _ember['default'].run.later(function () {
                        _this3.set('rendered', _this3.get('viewmode'));
                    });
                }
            };
            _ember['default'].$(window).resize(handleResize);
            this.on('willDestroyElement', function () {
                _ember['default'].run.cancel(timer);
                _ember['default'].$(window).off('resize', handleResize);
            });
        },

        didRender: function didRender() {
            var _this4 = this;

            _ember['default'].run.next(function () {
                return _this4.calculateView();
            });
        }

    });
});
/* global window */
define('offline/components/checkbox-element', ['exports', 'ember', 'offline/components/question-element'], function (exports, _ember, _offlineComponentsQuestionElement) {
    exports['default'] = _offlineComponentsQuestionElement['default'].extend({
        listCell: 'components/question-1d-list-cell/basic-cell'
    });
});
define('offline/components/checkbox-input', ['exports', 'ember', 'offline/mixins/question-input', 'offline/models/checkbox'], function (exports, _ember, _offlineMixinsQuestionInput, _offlineModelsCheckbox) {
    exports['default'] = _ember['default'].Checkbox.extend(_offlineMixinsQuestionInput['default'], {
        classNames: ['input', 'clickable'],
        checked: _ember['default'].computed.alias('variable.value'),
        value: _offlineModelsCheckbox.TRUE
    });
});
define('offline/components/comment-element', ['exports', 'ember', 'offline/components/question-element'], function (exports, _ember, _offlineComponentsQuestionElement) {
  exports['default'] = _offlineComponentsQuestionElement['default'].extend({});
});
define('offline/components/completes-summary', ['exports', 'ember'], function (exports, _ember) {

    /**
     * Display four states: (1) queued (2) uploading (3) uploaded (4) failed upload
     * Queued:    we have respondents (completes) that should be uploaded when online
     * Uploading: any respondent belonging to this project is in state isSaving
     * Uploaded:  we successfully uploaded
     * Failed Upload: backend rejected one or more respondents
     */
    exports['default'] = _ember['default'].Component.extend({
        tagName: 'div',
        classNames: ['panel-constrained', 'layout-constrained', 'completed'],
        network: _ember['default'].inject.service(),
        project: _ember['default'].inject.service('current-project'),

        status: _ember['default'].computed.alias('project.status'),
        completes: _ember['default'].computed.filterBy('project.survey.respondents', 'completed', true),

        uploadAttempted: false,
        willAttemptLater: _ember['default'].computed('network.online', 'completes.[]', function () {
            return !this.get('network.online') && this.get('completes').length > 0;
        }),

        anyUploading: _ember['default'].computed('completes.@each.isSaving', function () {
            return this.get('completes').any(function (r) {
                return r.get('isSaving');
            });
        }).readOnly(),
        anyUploaded: _ember['default'].computed('anyUploading', 'status.completes.count', function () {
            return !this.get('anyUploading') && this.get('status.completes.success');
        }).readOnly(),
        completesInQueue: _ember['default'].computed('anyUploading', 'completes.[]', function () {
            return !this.get('anyUploading') && this.get('completes').length > 0;
        }).readOnly(),

        uploadWhenOnline: _ember['default'].observer('network.online', function () {
            if (this.get('network.online')) {
                var options = { adapterOptions: { device: false } };
                this.get('completes').forEach(function (r) {
                    if (r.get('completed') && !r.get('isSaving')) {
                        r.save(options);
                    }
                });
            }
        }),

        /**
         * Display NEW information the next time we render these summary banners
         */
        didDestroyElement: function didDestroyElement() {
            this.get('project.status.completes').setProperties({
                count: 0,
                success: false,
                failure: false,
                errcode: false
            });
        },

        actions: {
            attemptUpload: function attemptUpload() {
                var _this = this;

                _ember['default'].RSVP.all(this.get('completes').map(function (c) {
                    return c.save({ adapterOptions: { device: false } });
                }))['finally'](function () {
                    return _this.set('uploadAttempted', true);
                });
            },
            clearStatus: function clearStatus(what) {
                this.set('status.completes.' + what, false);
            }
        }
    });
});
define('offline/components/exit-page', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Component.extend({
        tagName: 'div',
        classNames: ['survey-complete'],
        exitpage: null,
        surveyCompleteMessage: _ember['default'].computed('exitpage.cdata', function () {
            return this.get('exitpage.cdata');
        }),
        actions: {
            goHome: function goHome() {
                this.sendAction('goHome');
            },
            startNewSurvey: function startNewSurvey() {
                this.sendAction('startNewSurvey');
            }
        }
    });
});
define('offline/components/float-element', ['exports', 'ember', 'offline/components/question-element'], function (exports, _ember, _offlineComponentsQuestionElement) {
  exports['default'] = _offlineComponentsQuestionElement['default'].extend({});
});
define('offline/components/float-input', ['exports', 'ember', 'offline/components/number-input'], function (exports, _ember, _offlineComponentsNumberInput) {
  exports['default'] = _offlineComponentsNumberInput['default'].extend({});
});
define('offline/components/forgot-form', ['exports', 'ember'], function (exports, _ember) {

    var emailRx = /\S+@\S+\.\w{2,25}/;

    exports['default'] = _ember['default'].Component.extend({
        classNames: ['form-wrapper'],
        actions: {
            validate: function validate() {
                var _this = this;

                var email = this.get('email');
                var errorMsgs = this.set('errorMsgs', []);
                this.set('successMsg', null);

                if (!emailRx.test(email)) {
                    errorMsgs.pushObject(this.get('intl').t('Please provide a valid email address'));
                }

                if (!errorMsgs.length) {
                    this.get('forgotPassword')(email).then(function () {
                        _this.set('successMsg', _this.get('intl').t('An email has been sent to the email address that you supplied'));
                        // redirect to login?
                    });
                }
            }
        }
    });
});
define('offline/components/html-element', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Component.extend({
        classNames: ['comment']
    });
});
define('offline/components/image-element', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Component.extend({
        classNames: ['image']
    });
});
define('offline/components/imgupload-element', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Component.extend({});
});
define('offline/components/language-menu', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Component.extend({
        languages: [{
            code: 'en-us',
            name: 'English (US)'
        }, {
            code: 'ja-jp',
            name: '日本語'
        }, {
            code: 'pt-pt',
            name: 'Português (Brasil)'
        }, {
            code: 'es-mx',
            name: 'Español'
        }, {
            code: 'fr-fr',
            name: 'Français (France)'
        }],
        language: 'en-us',
        actions: {
            changeLanguage: function changeLanguage(code) {
                this.get('intl').setLocale(code);
            }
        }
    });
});
define('offline/components/login-form', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Component.extend({
        classNames: ['form-wrapper'],
        masked: true,
        standardLogin: true,
        inputType: _ember['default'].computed('masked', function () {
            return this.get('masked') ? 'password' : 'text';
        }),

        actions: {
            toggleMask: function toggleMask() {
                this.toggleProperty('masked');
            },

            standardLogin: function standardLogin() {
                this.set('standardLogin', true);
            },

            codeLogin: function codeLogin() {
                this.set('standardLogin', false);
            },

            /**
             * Verify email and password fields are populated before passing off
             * to the authenticator.
             */
            validate: function validate() {
                var _getProperties = this.getProperties('email', 'password', 'token');

                var email = _getProperties.email;
                var password = _getProperties.password;
                var token = _getProperties.token;

                var errorMsgs = this.set('errorMsgs', []);

                if (this.get('standardLogin')) {
                    if (!email) {
                        // we also allow user names instead of emails in some cases (shared user)
                        // so we don't bother checking for a proper email.
                        errorMsgs.pushObject(this.get('intl').t('Please provide a email address or user name'));
                    }
                    if (!password) {
                        errorMsgs.pushObject(this.get('intl').t('Please provide a password'));
                    }
                } else {
                    if (!token) {
                        errorMsgs.pushObject(this.get('intl').t('Please provide a access code'));
                    }
                }

                if (!errorMsgs.length) {
                    // TODO: token authentication
                    this.get('authenticate')(email, password);
                }
            }
        }
    });
});
define('offline/components/logout-button', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Component.extend({
        session: _ember['default'].inject.service(),
        actions: {
            invalidateSession: function invalidateSession() {
                this.get('session').invalidate();
            }
        }
    });
});
define('offline/components/noanswer-input', ['exports', 'ember'], function (exports, _ember) {

    // TODO: extend from exclusive
    exports['default'] = _ember['default'].Checkbox.extend({
        question: null,
        noanswer: null,
        variable: null,
        checked: _ember['default'].computed.alias('variable.value'),
        didReceiveAttrs: function didReceiveAttrs() {
            var _getProperties = this.getProperties('question', 'noanswer');

            var question = _getProperties.question;
            var noanswer = _getProperties.noanswer;

            this.variable = question.naVariableFor(noanswer.data);
        },
        siblingChecked: _ember['default'].observer('question.variables.@each.value', function () {
            if (this.get('question.variables').any(function (variable) {
                return variable.get('isSelected');
            })) {
                this.variable.set('value', false); // shown but not checked
            }
        }),
        noanswerChecked: _ember['default'].observer('variable.value', function () {
            if (this.variable.get('value')) {
                this.get('question.variables').forEach(function (variable) {
                    return variable.set('value', null);
                });
            }
        })
    });
});
define('offline/components/number-element', ['exports', 'ember', 'offline/components/question-element'], function (exports, _ember, _offlineComponentsQuestionElement) {
  exports['default'] = _offlineComponentsQuestionElement['default'].extend({});
});
define('offline/components/number-input', ['exports', 'ember', 'offline/components/text-input'], function (exports, _ember, _offlineComponentsTextInput) {
    exports['default'] = _offlineComponentsTextInput['default'].extend({
        type: 'number',
        value: _ember['default'].computed('variable.value', {
            // Number(null) -> 0
            get: function get(_) {
                return parseInt(this.variable.get('value'));
            },
            set: function set(_, value) {
                return this.variable.set('value', parseInt(value));
            }
        })
    });
});
define('offline/components/partials-summary', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Component.extend({
        tagName: 'div',
        classNames: ['panel-constrained', 'layout-constrained', 'partial'],
        network: _ember['default'].inject.service(),
        project: _ember['default'].inject.service('current-project'),
        status: _ember['default'].computed.alias('project.status'),
        partials: _ember['default'].computed.filterBy('project.survey.respondents', 'partial', true),
        isCollapsed: _ember['default'].computed('partials.[]', {
            get: function get() {
                return !this.get('partials').length;
            },
            set: function set(_, v) {
                return v;
            }
        }),
        actions: {
            toggleCollapsed: function toggleCollapsed() {
                this.toggleProperty('isCollapsed');
            },
            clearStatus: function clearStatus(what) {
                this.set('status.partials.' + what, false);
            },
            removePartial: function removePartial(respondent) {
                if (!respondent.get('isSaving')) {
                    respondent.destroyRecord();
                }
            },
            uploadPartial: function uploadPartial(respondent) {
                if (!respondent.get('isSaving')) {
                    respondent.save({ adapterOptions: { device: false } });
                }
            },
            resumeSurvey: function resumeSurvey(respondent) {
                if (!respondent.get('isSaving')) {
                    this.sendAction('resumeSurvey', respondent);
                }
            }
        }
    });
});
define('offline/components/projects-list-item', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Component.extend({
        classNames: ['projects-list-item'],
        classNameBindings: ['surveyClosed'],
        store: _ember['default'].inject.service(),
        network: _ember['default'].inject.service(),
        locks: _ember['default'].inject.service('simple-locks'),
        project: _ember['default'].inject.service('current-project'),
        online: _ember['default'].computed.readOnly('network.online'),
        hasSeen: _ember['default'].computed.readOnly('interviewer.has_seen'),
        downloaded: _ember['default'].computed.readOnly('interviewer.survey.downloaded'),
        surveyClosed: _ember['default'].computed('interviewer.survey.state', function () {
            return this.get('interviewer.survey.state').toLowerCase() === 'closed' || !this.get('interviewer.active');
        }),
        canNavigate: _ember['default'].computed('online', 'downloaded', 'surveyClosed', function () {
            return !this.get('surveyClosed') && (this.get('online') || this.get('downloaded'));
        }),

        downloading: false,
        uploading: false,
        updating: false,
        partials: 0,
        readyToUpload: 0,

        // Watch for survey changes. The shallow survey copy that is fetched perdiodically
        // when online contains the latest hash. If that doesn't match the downloaded hash
        // then download.
        _updateWatch: _ember['default'].observer('interviewer.survey.needsUpdate', function () {
            if (this.get('downloaded') && this.get('interviewer.survey.needsUpdate') && this.get('online')) {
                this.send('download', 'updating');
            }
        }).on('init'),

        /**
         * Before each render if the project is downloaded we gather the counts, partial and
         * on device completes. If online we try to upload the on device completes. Because
         * there are possibly many of these components and they are performing async operations
         * that depend on a global service (current-project) we make them acquire a lock. When
         * they are finished they must release the lock so the next component can run its
         * operations.
         */
        willRender: function willRender() {
            var _this = this;

            var lockId = undefined;

            if (this.get('downloaded')) {
                lockId = this.get('locks').acquire('projects-lock', function (lockRelease) {
                    _this.get('project').set('path', _this.get('interviewer.survey.path'));
                    _this.get('store').findAll('respondent').then(function (respondents) {
                        var readyToUploadCount = 0;
                        var partialCount = 0;
                        var savingRespondents = [];
                        var needsUnload = [];

                        respondents.forEach(function (resp) {
                            if (!resp.get('isSaving') && resp.get('partial')) {
                                ++partialCount;
                            } else if (!resp.get('isSaving') && resp.get('completed')) {
                                ++readyToUploadCount;
                                if (_this.get('online')) {
                                    var saving = resp.save({ adapterOptions: { device: false } });
                                    savingRespondents.push(saving);
                                }
                            }
                            needsUnload.push(resp);
                        });

                        _this.set('partials', partialCount);
                        _this.set('readyToUpload', readyToUploadCount);
                        if (savingRespondents.length > 0) {
                            _this.set('uploading', true);
                        }

                        _ember['default'].RSVP.allSettled(savingRespondents).then(function () {
                            needsUnload.forEach(function (r) {
                                r.unloadRecord();
                            });
                            if (_this.get('uploading')) {
                                _this.set('uploading', false);
                            }
                            _this.get('project').set('path', null);
                            lockRelease(); // tres important!
                        });
                    });
                });
            }

            if (lockId) {
                this.set('lockId', lockId);
            }
        },

        willDestroy: function willDestroy() {
            if (this.get('lockId')) {
                this.get('locks').remove('projects-lock', this.get('lockId'));
            }

            // mark all surveys that are currently displayed as being seen
            // as soon as they are destroyed. This includes toggling the survey
            // state filter.
            if (!this.get('hasSeen')) {
                var interviewer = this.get('interviewer');
                interviewer.set('has_seen', true);
                interviewer.save({ adapterOptions: { device: true } });
            }
        },

        actions: {
            download: function download(downloadType) {
                var _this2 = this;

                var store = this.get('store');
                var timer = undefined;

                downloadType = downloadType || 'downloading';

                if (this.get('online')) {
                    (function () {
                        var path = _this2.get('interviewer.survey.path');

                        _this2.set(downloadType, true);
                        store.findRecord('hash', path).then(function () {
                            store.findRecord('survey', path, { include: 'elements,messages,exitpages,variables,assets' }).then(function (survey) {
                                timer = _ember['default'].run.later(_this2, function () {
                                    this.set(downloadType, false);
                                }, 2000);
                            });
                        });
                    })();
                } else {
                    this.send('warning', 'download');
                }
                this.on('willDestroyElement', function () {
                    _ember['default'].run.cancel(timer);
                });
            },
            remove: function remove() {
                var _this3 = this;

                var hash = this.get('store').peekRecord('hash', this.get('interviewer.survey.path'));

                this.get('interviewer.survey').then(function (survey) {
                    survey.removeRelationships().then(function () {
                        if (hash) {
                            hash.destroyRecord({ adapterOptions: { device: true } });
                        }
                        // this hides the component. The reason we are hiding it is because
                        // it will still appear in the filtered list until the projects-list
                        // component rerenders. We don't want to force a rerender just to take
                        // it out of the filtered list of projects when we can hide it and
                        // patiently wait.
                        _this3.set('isVisible', false);
                    });
                });
            },
            warning: function warning(type) {
                var timer = undefined;

                switch (type) {
                    case 'closed':
                        this.set('warning', this.get('intl').t('This project is closed.'));
                        break;
                    case 'download':
                        this.set('warning', this.get('intl').t('You need to be online to download this project.'));
                        break;
                }
                timer = _ember['default'].run.later(this, function () {
                    this.set('warning', '');
                }, 5000);
                this.on('willDestroyElement', function () {
                    _ember['default'].run.cancel(timer);
                });
            }
        }
    });
});
define('offline/components/projects-list', ['exports', 'ember', 'lodash/lodash'], function (exports, _ember, _lodashLodash) {
    exports['default'] = _ember['default'].Component.extend({
        store: _ember['default'].inject.service(),
        poller: _ember['default'].inject.service(),
        network: _ember['default'].inject.service(),
        uiState: _ember['default'].inject.service('ui-state'),
        currentUser: _ember['default'].inject.service('current-user'),
        online: _ember['default'].computed.readOnly('network.online'),
        refreshing: false,
        testing: _ember['default'].computed.alias('uiState.projectList.testing'), // filter for project type
        interviewersFiltered: _ember['default'].computed('testing', 'refreshing', function () {
            var _this = this;

            var interviewers = this.get('interviewers');

            return interviewers.filter(function (interviewer, i, _) {
                var state = interviewer.get('survey.state');
                var downloaded = interviewer.get('survey.downloaded');

                if (_this.get('testing')) {
                    return state === 'testing' || state === 'dev';
                }
                return state === 'live' || state === 'closed' && downloaded;
            }).sortBy('survey.adminTitle');
        }),
        // interviewers are already sorted by survey title, this just pushes closed surveys to the bottom.
        interviewersSorted: _ember['default'].computed.sort('interviewersFiltered', function (interviewerA, interviewerB) {
            var stateA = interviewerA.get('survey.state');
            var stateB = interviewerB.get('survey.state');

            if (stateA === 'live') {
                return -1;
            } else if (stateA === 'closed' && stateB !== 'closed') {
                return 1;
            }
            return 0;
        }),

        init: function init() {
            var action = this.get('actions.refresh');

            this._super.apply(this, arguments);
            // get fresh list of projects every 30sec when online
            this.get('poller').startPoll('projects-list', this, action, 30000, { online: true });
        },

        didInsertElement: function didInsertElement() {
            var me = this;
            var $projectsList = _ember['default'].$('#projects-list-pulldown');
            var $projectsRefresh = _ember['default'].$('#projects-refresh');
            var startY = undefined;
            var diff = 0;
            var limit = 50;

            $projectsRefresh.hide();

            var touchMv = _lodashLodash['default'].throttle(function (e) {
                e.preventDefault();
                var currentY = e.changedTouches[0].pageY;
                diff = currentY - startY;
                $projectsRefresh.show();

                if (diff > 0) {
                    $projectsList.css('margin-top', (diff <= limit ? diff : limit) + 'px');
                }
            }, 50, { leading: true }); // Even a 50ms throttle is a difference of 6 calls vs 50 calls
            // and visually it looks just as good.

            $projectsList.on('touchstart', function (e) {
                startY = e.changedTouches[0].pageY;
            });

            $projectsList.on('touchmove', touchMv);

            $projectsList.on('touchend', function (e) {
                if (diff >= limit && me.get('online')) {
                    me.send('refresh');
                }
                diff = 0;
                $projectsList.css('margin-top', '0px');
                $projectsRefresh.hide();
            });
        },

        actions: {
            /**
             * @description
             * Reload models needed for this route
             */
            refresh: function refresh() {
                var _this2 = this;

                this.set('refreshing', true);
                this.get('currentUser').load().then(function () {
                    // ugh
                    _this2.get('store').findAll('survey', { reload: true }).then(function () {
                        _ember['default'].run.later(_this2, function () {
                            this.set('refreshing', false);
                        }, 1000); // don't flash
                    });
                });
            }
        }
    });
});
define('offline/components/projects-nav', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Component.extend({
        classNames: ['projects-nav-wrapper'],
        dropdown: false,
        actions: {
            toggleTesting: function toggleTesting() {
                this.toggleProperty('testing');
                this.send('toggleDropdown');
            },
            toggleDropdown: function toggleDropdown() {
                this.toggleProperty('dropdown');
            }

        }
    });
});
define('offline/components/question-element', ['exports', 'ember', 'offline/templates/components/question-element/1d-list', 'offline/templates/components/question-element/2d-grid'], function (exports, _ember, _offlineTemplatesComponentsQuestionElement1dList, _offlineTemplatesComponentsQuestionElement2dGrid) {

    /**
     * Base for our survey question components
     */
    // http://emberjs.com/deprecations/v2.x/#toc_ember-component-defaultlayout
    exports['default'] = _ember['default'].Component.extend({
        didInsertElement: function didInsertElement() {
            if (this.get('isGrid')) {
                this.gridBreakdown();
            }
        },
        layout: _ember['default'].computed('isGrid', function () {
            if (this.get('isGrid')) {
                return _offlineTemplatesComponentsQuestionElement2dGrid['default'];
            }
            return _offlineTemplatesComponentsQuestionElement1dList['default'];
        }),
        cells: _ember['default'].computed('isGrid', 'question.rows.@each.displayed', 'question.cols.@each.displayed', function () {
            if (!this.get('isGrid')) {
                if (this.get('question.rows.firstObject.label')) {
                    return this.get('question.enabled.rows');
                }
                return this.get('question.enabled.cols');
            }
        }),
        isGrid: _ember['default'].computed(function () {
            var _get$data = this.get('question').data;
            var rows = _get$data.rows;
            var cols = _get$data.cols;

            return rows.any(function (row) {
                return row.get('label');
            }) && cols.any(function (col) {
                return col.get('label');
            });
        }),
        listCell: _ember['default'].computed(function () {
            return 'components/question-1d-list-cell/' + this.get('question.type') + '-cell';
        }),
        gridBreakdown: function gridBreakdown() {
            var _this = this;

            var $element = _ember['default'].$('.question');
            var $tableElement = $element.find('.question-2d .answer-wrapper');

            function toggleGridMode() {
                $tableElement.addClass('grid-mode');
                if ($tableElement.width() > $element.width()) {
                    $tableElement.removeClass('grid-mode');
                }
            }

            toggleGridMode();

            var toggleGridModeDebounce = function toggleGridModeDebounce() {
                _ember['default'].run.debounce(_this, toggleGridMode, 50);
            };

            _ember['default'].$(window).on('resize', toggleGridModeDebounce);

            this.on('willDestroyElement', function () {
                _ember['default'].$(window).off('resize', toggleGridModeDebounce);
            });
        }
    });
});
define('offline/components/question-error', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Component.extend({
        question: null,
        classNames: ['error-label'],
        errors: _ember['default'].computed('question.variables.@each.errorMessages', function () {
            return this.get('question.variables').reduce(function (errors, variable) {
                variable.get('errorMessages').forEach(function (errmsg) {
                    if (errors.indexOf(errmsg) === -1) {
                        errors.push(errmsg);
                    }
                });
                return errors;
            }, []);
        })
    });
});
define('offline/components/radio-element', ['exports', 'ember', 'offline/components/question-element'], function (exports, _ember, _offlineComponentsQuestionElement) {
    exports['default'] = _offlineComponentsQuestionElement['default'].extend({
        listCell: 'components/question-1d-list-cell/basic-cell'
    });
});
define('offline/components/radio-input', ['exports', 'ember', 'offline/mixins/question-input'], function (exports, _ember, _offlineMixinsQuestionInput) {
    exports['default'] = _ember['default'].Component.extend(_offlineMixinsQuestionInput['default'], {
        tagName: 'input',
        type: 'radio',
        classNames: [],
        attributeBindings: ['type', 'checked'],
        value: _ember['default'].computed('question', 'row', 'col', function () {
            var row = this.get('row.data') || null,
                col = this.get('col.data') || null,
                question = this.get('question');
            return question.valueFor(row, col);
        }).readOnly(),
        change: function change() {
            this.variable.set('value', this.get('value'));
        },
        checked: _ember['default'].on('didInsertElement', _ember['default'].computed('variable.value', function () {
            return this.variable.get('value') === this.get('value');
        }))
    });
});
define('offline/components/select-element', ['exports', 'ember', 'offline/components/question-element'], function (exports, _ember, _offlineComponentsQuestionElement) {
  exports['default'] = _offlineComponentsQuestionElement['default'].extend({});
});
define('offline/components/select-input', ['exports', 'ember', 'offline/mixins/question-input'], function (exports, _ember, _offlineMixinsQuestionInput) {
    exports['default'] = _ember['default'].Component.extend(_offlineMixinsQuestionInput['default'], {
        tagName: 'div',
        classNames: ['select-wrap'],
        didInsertElement: function didInsertElement() {
            var options = this.$('option').toArray(),
                value = this.variable.get('value'),
                option;
            while (options.length) {
                option = options.pop();
                if (Number(option.value) === value) {
                    break;
                }
            }
            option.selected = true;
        },
        change: function change(_ref) {
            var target = _ref.target;

            // Number("") -> 0, parseInt("") -> NaN
            this.variable.set('value', parseInt(target.value));
        }
    });
});
define('offline/components/text-element', ['exports', 'ember', 'offline/components/question-element'], function (exports, _ember, _offlineComponentsQuestionElement) {
    exports['default'] = _offlineComponentsQuestionElement['default'].extend({
        question: null,
        preText: _ember['default'].computed(function () {
            return this.get('question.preText') || this.get('question.rows').any(function (row) {
                return row.get('preText');
            }) || this.get('question.cols').any(function (col) {
                return col.get('preText');
            });
        }),
        postText: _ember['default'].computed(function () {
            return this.get('question.postText') || this.get('question.rows').any(function (row) {
                return row.get('postText');
            }) || this.get('question.cols').any(function (col) {
                return col.get('postText');
            });
        })
    });
});
define('offline/components/text-input', ['exports', 'ember', 'offline/mixins/question-input'], function (exports, _ember, _offlineMixinsQuestionInput) {
    exports['default'] = _ember['default'].TextField.extend(_offlineMixinsQuestionInput['default'], {
        classNames: ['input', 'clickable', 'text'],
        classNameBindings: ['open:oe'],
        /**
         * Effective size for Text and Number question inputs
         */
        size: _ember['default'].computed(function () {
            var row = this.get('row.data') || {},
                col = this.get('col.data') || {},
                question = this.get('question.data');
            if (row.size > -1) {
                return row.size;
            } else if (col.size > -1) {
                return col.size;
            } else {
                return question.size;
            }
        }).readOnly(),
        value: _ember['default'].computed.alias('variable.value')
    });
});
define('offline/components/textarea-element', ['exports', 'ember', 'offline/components/text-element'], function (exports, _ember, _offlineComponentsTextElement) {
    exports['default'] = _offlineComponentsTextElement['default'].extend({
        listCell: 'components/question-1d-list-cell/text-cell'
    });
});
define('offline/components/textarea-input', ['exports', 'ember', 'offline/mixins/question-input'], function (exports, _ember, _offlineMixinsQuestionInput) {
    exports['default'] = _ember['default'].TextArea.extend(_offlineMixinsQuestionInput['default'], {
        value: _ember['default'].computed.alias('variable.value'),
        rows: _ember['default'].computed(function () {
            return this.question.get('height');
        }).readOnly(),
        cols: _ember['default'].computed(function () {
            return this.question.get('width');
        }).readOnly()
    });
});
define('offline/components/user-menu', ['exports', 'ember', 'offline/config/environment'], function (exports, _ember, _offlineConfigEnvironment) {
    exports['default'] = _ember['default'].Component.extend({
        currentUser: _ember['default'].inject.service('current-user'),
        userName: _ember['default'].computed.alias('currentUser.display_name'),
        init: function init() {
            this._super.apply(this, arguments);
        },
        isOpened: _ember['default'].computed({
            get: function get() {
                return this._isOpened;
            },
            set: function set(_, value) {
                // TODO: change target to parent component under `body` element
                var bodyElement = document.querySelector(_offlineConfigEnvironment['default'].APP.rootElement);
                if (value) {
                    bodyElement.classList.add('user-menu-open');
                } else {
                    bodyElement.classList.remove('user-menu-open');
                }
                return this._isOpened = value;
            }
        }),
        willDestroyElement: function willDestroyElement() {
            this.set('isOpened', false);
        },
        actions: {
            toggleMenu: function toggleMenu() {
                this.toggleProperty('isOpened');
            }
        }
    });
});
define('offline/components/videocapture-element', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Component.extend({});
});
define('offline/controllers/authenticated/admin', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Controller.extend({
        project: _ember['default'].inject.service('current-project'),
        capacity: _ember['default'].inject.service(),
        actions: {
            practiceSurvey: function practiceSurvey() {
                /* TODO: create or update the practice record for this interviewer on this project */
                var respondent = this.get('project.survey.respondents').createRecord();
                this.transitionToRoute('authenticated.respview.survey.page', respondent.id, {
                    queryParams: { practice: true }
                });
            },
            startSurvey: function startSurvey() {
                var respondent = this.get('project.survey.respondents').createRecord();
                this.transitionToRoute('authenticated.respview.survey.page', respondent.id, {
                    queryParams: { practice: false }
                });
            },
            resumeSurvey: function resumeSurvey(respondent) {
                respondent.set('isResuming', true);
                this.transitionToRoute('authenticated.respview.survey.page', respondent.id, {
                    queryParams: { practice: false }
                });
            },
            removeSurvey: function removeSurvey() {
                var survey = this.get('project.survey');
                var hash = this.store.peekRecord('hash', survey.get('path'));

                survey.removeRelationships().then(function () {
                    if (hash) {
                        hash.destroyRecord({ adapterOptions: { device: true } });
                    }
                });
                this.send('backToProjects');
            },
            backToProjects: function backToProjects() {
                this.transitionToRoute('authenticated.projects');
            }
        }
    });
});
define('offline/controllers/authenticated/respview', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Controller.extend({
        queryParams: ['practice'],
        practice: false,
        project: _ember['default'].inject.service('current-project'),
        actions: {
            endPractice: function endPractice() {
                this.get('project.ssession').destroy();
                this.transitionToRoute('authenticated.admin', this.get('project.path'));
            },
            endSurvey: function endSurvey() {
                this.get('project.ssession').destroy();
                this.transitionToRoute('authenticated.admin', this.get('project.path'));
            },
            saveSurvey: function saveSurvey() {
                var _this = this;

                this.get('project.ssession').saveForLater().then(function () {
                    return _this.send('endSurvey');
                });
            }
        }
    });
});
define('offline/controllers/authenticated/respview/error', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Controller.extend({});
});
define('offline/controllers/authenticated/respview/exitpage', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Controller.extend({
        project: _ember['default'].inject.service('current-project'),
        actions: {
            goHome: function goHome() {
                this.transitionToRoute('authenticated.admin', this.get('project.path'));
            },
            startNewSurvey: function startNewSurvey() {
                this.transitionToRoute('authenticated.respview.survey.page');
            }
        }
    });
});
define('offline/controllers/authenticated/respview/survey', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Controller.extend({
        project: _ember['default'].inject.service('current-project'),
        survey: _ember['default'].computed.alias('project.ssession'),
        actions: {
            goNext: function goNext() {
                this.get('survey').goNext();
            },
            goBack: function goBack() {
                this.get('survey').goPrev();
            }
        }
    });
});
define('offline/controllers/authenticated/respview/survey/page', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Controller.extend({
        project: _ember['default'].inject.service('current-project'),
        currentpage: _ember['default'].computed.alias('project.ssession.currentpage')
    });
});
define('offline/controllers/login/forgot', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Controller.extend({
        session: _ember['default'].inject.service(),
        errorMsgs: [],
        actions: {
            forgotPassword: function forgotPassword(email) {
                var _this = this;

                return this.get('session').forgotPassword(email)['catch'](function (error) {
                    _this.get('errorMsgs').pushObject(error);
                });
            }
        }
    });
});
define('offline/controllers/login/index', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Controller.extend({
        session: _ember['default'].inject.service(),
        errorMsgs: [],

        init: function init() {
            this._super.apply(this, arguments);
            // attempt to eagerly authenticate a user
            this.get('session').authenticate('authenticator:authenticator');
        },

        actions: {
            authenticate: function authenticate(email, password) {
                var _this = this;

                this.get('session').authenticate('authenticator:authenticator', email, password)['catch'](function (error) {
                    _this.get('errorMsgs').pushObject(error);
                });
            }
        }
    });
});
define('offline/formats', ['exports'], function (exports) {
  exports['default'] = {
    time: {
      hhmmss: {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }
    },
    date: {
      hhmmss: {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }
    },
    number: {
      EUR: {
        style: 'currency',
        currency: 'EUR',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      },
      USD: {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      }
    }
  };
});
define('offline/helpers/app-version', ['exports', 'ember', 'offline/config/environment'], function (exports, _ember, _offlineConfigEnvironment) {
  exports.appVersion = appVersion;
  var version = _offlineConfigEnvironment['default'].APP.version;

  function appVersion() {
    return version;
  }

  exports['default'] = _ember['default'].Helper.helper(appVersion);
});
define('offline/helpers/format-date', ['exports', 'ember'], function (exports, _ember) {
  var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

  exports.formatDate = formatDate;

  /**
   * Convert a Date object into a formatted string according to 'format'
   * @param {Date} date
   * @param {String} format
   * @returns {String}
   */

  function formatDate(_ref) {
    var _ref2 = _slicedToArray(_ref, 2);

    var date = _ref2[0];
    var format = _ref2[1];

    return moment(date).format(format);
  }

  exports['default'] = _ember['default'].Helper.helper(formatDate);
});
define('offline/helpers/format-html-message', ['exports', 'ember-intl/helpers/format-html-message'], function (exports, _emberIntlHelpersFormatHtmlMessage) {
  /**
   * Copyright 2015, Yahoo! Inc.
   * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
   */

  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberIntlHelpersFormatHtmlMessage['default'];
    }
  });
});
define('offline/helpers/format-message', ['exports', 'ember-intl/helpers/format-message'], function (exports, _emberIntlHelpersFormatMessage) {
  /**
   * Copyright 2015, Yahoo! Inc.
   * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
   */

  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberIntlHelpersFormatMessage['default'];
    }
  });
});
define('offline/helpers/format-number', ['exports', 'ember-intl/helpers/format-number'], function (exports, _emberIntlHelpersFormatNumber) {
  /**
   * Copyright 2015, Yahoo! Inc.
   * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
   */

  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberIntlHelpersFormatNumber['default'];
    }
  });
});
define('offline/helpers/format-relative', ['exports', 'ember-intl/helpers/format-relative'], function (exports, _emberIntlHelpersFormatRelative) {
  /**
   * Copyright 2015, Yahoo! Inc.
   * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
   */

  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberIntlHelpersFormatRelative['default'];
    }
  });
});
define('offline/helpers/format-time', ['exports', 'ember-intl/helpers/format-time'], function (exports, _emberIntlHelpersFormatTime) {
  /**
   * Copyright 2015, Yahoo! Inc.
   * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
   */

  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberIntlHelpersFormatTime['default'];
    }
  });
});
define('offline/helpers/intl-get', ['exports', 'ember-intl/helpers/intl-get'], function (exports, _emberIntlHelpersIntlGet) {
  /**
   * Copyright 2015, Yahoo! Inc.
   * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
   */

  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberIntlHelpersIntlGet['default'];
    }
  });
});
define('offline/helpers/l', ['exports', 'ember-intl/helpers/l'], function (exports, _emberIntlHelpersL) {
  /**
   * Copyright 2015, Yahoo! Inc.
   * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
   */

  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberIntlHelpersL['default'];
    }
  });
});
define('offline/helpers/pluralize', ['exports', 'ember-inflector/lib/helpers/pluralize'], function (exports, _emberInflectorLibHelpersPluralize) {
  exports['default'] = _emberInflectorLibHelpersPluralize['default'];
});
define('offline/helpers/question-component-name', ['exports', 'ember', 'lodash/lodash'], function (exports, _ember, _lodashLodash) {
    var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

    exports.questionComponentName = questionComponentName;

    function questionComponentName(params /*, hash */) {
        var _params = _slicedToArray(params, 1);

        var question = _params[0];

        var _question$getProperties = question.getProperties('uses', 'type');

        var uses = _question$getProperties.uses;
        var type = _question$getProperties.type;

        if (_lodashLodash['default'].isArray(uses) && uses.length) {
            return uses[0].name + '-element';
        } else {
            return type + '-element';
        }
    }

    exports['default'] = _ember['default'].Helper.helper(questionComponentName);
});
define('offline/helpers/singularize', ['exports', 'ember-inflector/lib/helpers/singularize'], function (exports, _emberInflectorLibHelpersSingularize) {
  exports['default'] = _emberInflectorLibHelpersSingularize['default'];
});
define('offline/helpers/t-html', ['exports', 'ember-intl/helpers/format-html-message'], function (exports, _emberIntlHelpersFormatHtmlMessage) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberIntlHelpersFormatHtmlMessage['default'];
    }
  });
});
define('offline/helpers/t', ['exports', 'ember-intl/helpers/t'], function (exports, _emberIntlHelpersT) {
  /**
   * Copyright 2015, Yahoo! Inc.
   * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
   */

  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberIntlHelpersT['default'];
    }
  });
});
define('offline/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'offline/config/environment'], function (exports, _emberCliAppVersionInitializerFactory, _offlineConfigEnvironment) {
  var _config$APP = _offlineConfigEnvironment['default'].APP;
  var name = _config$APP.name;
  var version = _config$APP.version;
  exports['default'] = {
    name: 'App Version',
    initialize: (0, _emberCliAppVersionInitializerFactory['default'])(name, version)
  };
});
define('offline/initializers/container-debug-adapter', ['exports', 'ember-resolver/container-debug-adapter'], function (exports, _emberResolverContainerDebugAdapter) {
  exports['default'] = {
    name: 'container-debug-adapter',

    initialize: function initialize() {
      var app = arguments[1] || arguments[0];

      app.register('container-debug-adapter:main', _emberResolverContainerDebugAdapter['default']);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }
  };
});
define('offline/initializers/data-adapter', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `data-adapter` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'data-adapter',
    before: 'store',
    initialize: _ember['default'].K
  };
});
define('offline/initializers/ember-cli-mirage', ['exports', 'ember-cli-mirage/utils/read-modules', 'offline/config/environment', 'offline/mirage/config', 'ember-cli-mirage/server', 'lodash/object/assign'], function (exports, _emberCliMirageUtilsReadModules, _offlineConfigEnvironment, _offlineMirageConfig, _emberCliMirageServer, _lodashObjectAssign) {
  exports.startMirage = startMirage;
  exports['default'] = {
    name: 'ember-cli-mirage',
    initialize: function initialize(application) {
      if (arguments.length > 1) {
        // Ember < 2.1
        var container = arguments[0],
            application = arguments[1];
      }

      if (_shouldUseMirage(_offlineConfigEnvironment['default'].environment, _offlineConfigEnvironment['default']['ember-cli-mirage'])) {
        startMirage(_offlineConfigEnvironment['default']);
      }
    }
  };

  function startMirage() {
    var env = arguments.length <= 0 || arguments[0] === undefined ? _offlineConfigEnvironment['default'] : arguments[0];

    var environment = env.environment;
    var modules = (0, _emberCliMirageUtilsReadModules['default'])(env.modulePrefix);
    var options = (0, _lodashObjectAssign['default'])(modules, { environment: environment, baseConfig: _offlineMirageConfig['default'], testConfig: _offlineMirageConfig.testConfig });

    return new _emberCliMirageServer['default'](options);
  }

  function _shouldUseMirage(env, addonConfig) {
    var userDeclaredEnabled = typeof addonConfig.enabled !== 'undefined';
    var defaultEnabled = _defaultEnabled(env, addonConfig);

    return userDeclaredEnabled ? addonConfig.enabled : defaultEnabled;
  }

  /*
    Returns a boolean specifying the default behavior for whether
    to initialize Mirage.
  */
  function _defaultEnabled(env, addonConfig) {
    var usingInDev = env === 'development' && !addonConfig.usingProxy;
    var usingInTest = env === 'test';

    return usingInDev || usingInTest;
  }
});
define('offline/initializers/ember-data', ['exports', 'ember-data/setup-container', 'ember-data/-private/core'], function (exports, _emberDataSetupContainer, _emberDataPrivateCore) {

  /*
  
    This code initializes Ember-Data onto an Ember application.
  
    If an Ember.js developer defines a subclass of DS.Store on their application,
    as `App.StoreService` (or via a module system that resolves to `service:store`)
    this code will automatically instantiate it and make it available on the
    router.
  
    Additionally, after an application's controllers have been injected, they will
    each have the store made available to them.
  
    For example, imagine an Ember.js application with the following classes:
  
    App.StoreService = DS.Store.extend({
      adapter: 'custom'
    });
  
    App.PostsController = Ember.Controller.extend({
      // ...
    });
  
    When the application is initialized, `App.ApplicationStore` will automatically be
    instantiated, and the instance of `App.PostsController` will have its `store`
    property set to that instance.
  
    Note that this code will only be run if the `ember-application` package is
    loaded. If Ember Data is being used in an environment other than a
    typical application (e.g., node.js where only `ember-runtime` is available),
    this code will be ignored.
  */

  exports['default'] = {
    name: 'ember-data',
    initialize: _emberDataSetupContainer['default']
  };
});
define('offline/initializers/ember-simple-auth', ['exports', 'ember', 'offline/config/environment', 'ember-simple-auth/configuration', 'ember-simple-auth/initializers/setup-session', 'ember-simple-auth/initializers/setup-session-service'], function (exports, _ember, _offlineConfigEnvironment, _emberSimpleAuthConfiguration, _emberSimpleAuthInitializersSetupSession, _emberSimpleAuthInitializersSetupSessionService) {
  exports['default'] = {
    name: 'ember-simple-auth',
    initialize: function initialize(registry) {
      var config = _offlineConfigEnvironment['default']['ember-simple-auth'] || {};
      config.baseURL = _offlineConfigEnvironment['default'].baseURL;
      _emberSimpleAuthConfiguration['default'].load(config);

      (0, _emberSimpleAuthInitializersSetupSession['default'])(registry);
      (0, _emberSimpleAuthInitializersSetupSessionService['default'])(registry);
    }
  };
});
define('offline/initializers/export-application-global', ['exports', 'ember', 'offline/config/environment'], function (exports, _ember, _offlineConfigEnvironment) {
  exports.initialize = initialize;

  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_offlineConfigEnvironment['default'].exportApplicationGlobal !== false) {
      var value = _offlineConfigEnvironment['default'].exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = _ember['default'].String.classify(_offlineConfigEnvironment['default'].modulePrefix);
      }

      if (!window[globalName]) {
        window[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete window[globalName];
          }
        });
      }
    }
  }

  exports['default'] = {
    name: 'export-application-global',

    initialize: initialize
  };
});
define('offline/initializers/injectStore', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `injectStore` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'injectStore',
    before: 'store',
    initialize: _ember['default'].K
  };
});
define('offline/initializers/intl', ['exports'], function (exports) {
  exports.initialize = initialize;

  function initialize(application) {
    application.inject('route', 'intl', 'service:intl');
    application.inject('component', 'intl', 'service:intl');
    application.inject('authenticators', 'intl', 'service:intl');
  }

  exports['default'] = {
    name: 'intl',
    initialize: initialize
  };
});
define('offline/initializers/model-fragments', ['exports', 'model-fragments', 'model-fragments/transforms/fragment', 'model-fragments/transforms/fragment-array', 'model-fragments/transforms/array'], function (exports, _modelFragments, _modelFragmentsTransformsFragment, _modelFragmentsTransformsFragmentArray, _modelFragmentsTransformsArray) {
  exports['default'] = {
    name: "fragmentTransform",
    before: "store",

    initialize: function initialize(application) {
      application.register('transform:fragment', _modelFragmentsTransformsFragment['default']);
      application.register('transform:fragment-array', _modelFragmentsTransformsFragmentArray['default']);
      application.register('transform:array', _modelFragmentsTransformsArray['default']);
    }
  };
});
// Import the full module to ensure monkey-patchs are applied before any store
// instances are created. Sad face for side-effects :(
define('offline/initializers/store', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `store` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'store',
    after: 'ember-data',
    initialize: _ember['default'].K
  };
});
define('offline/initializers/transforms', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `transforms` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'transforms',
    before: 'store',
    initialize: _ember['default'].K
  };
});
define('offline/instance-initializers/current-user', ['exports', 'ember'], function (exports, _ember) {
    exports.initialize = initialize;

    var CURRENT_USER_ID = 'me';

    // special thanks to http://miguelcamba.com/blog/2015/06/18/how-to-inject-the-current-user-using-ember-simple-auth/

    function initialize(appInstance) {
        var store = appInstance.lookup('service:store');

        var CurrentUserClass = _ember['default'].ObjectProxy.extend({
            load: function load() {
                var _this = this;

                return store.findRecord('user', CURRENT_USER_ID, { include: 'interviewers' }).then(function (user) {
                    return _this.set('content', user);
                });
            }
        });

        CurrentUserClass.reopenClass({
            isServiceFactory: true
        });

        appInstance.register('service:current-user', CurrentUserClass.create(), { instantiate: false, singleton: true });
    }

    exports['default'] = {
        name: 'current-user',
        after: 'ember-simple-auth',
        initialize: initialize
    };
    exports.CURRENT_USER_ID = CURRENT_USER_ID;
});
define("offline/instance-initializers/ember-data", ["exports", "ember-data/-private/instance-initializers/initialize-store-service"], function (exports, _emberDataPrivateInstanceInitializersInitializeStoreService) {
  exports["default"] = {
    name: "ember-data",
    initialize: _emberDataPrivateInstanceInitializersInitializeStoreService["default"]
  };
});
define('offline/instance-initializers/ember-intl', ['exports', 'ember', 'offline/config/environment', 'ember-intl/utils/links'], function (exports, _ember, _offlineConfigEnvironment, _emberIntlUtilsLinks) {
  exports.instanceInitializer = instanceInitializer;
  var warn = _ember['default'].warn;

  function filterBy(type) {
    return Object.keys(requirejs._eak_seen).filter(function (key) {
      return key.indexOf(_offlineConfigEnvironment['default'].modulePrefix + '/' + type + '/') === 0;
    });
  }

  function instanceInitializer(instance) {
    var service = undefined;

    if (typeof instance.lookup === 'function') {
      service = instance.lookup('service:intl');
    } else {
      service = instance.container.lookup('service:intl');
    }

    if (typeof Intl === 'undefined') {
      warn('[ember-intl] Intl API is unavailable in this environment.\nSee: ' + _emberIntlUtilsLinks['default'].polyfill, false, {
        id: 'ember-intl-undefined-intljs'
      });
    }

    var cldrs = filterBy('cldrs');

    if (!cldrs.length) {
      warn('[ember-intl] project is missing CLDR data\nIf you are asynchronously loading translation, see: ${links.asyncTranslations}.', false, {
        id: 'ember-intl-missing-cldr-data'
      });
    } else {
      cldrs.map(function (moduleName) {
        return requirejs(moduleName, null, null, true)['default'];
      }).forEach(function (locale) {
        return locale.forEach(service.addLocaleData);
      });
    }

    filterBy('translations').forEach(function (moduleName) {
      var localeSplit = moduleName.split('\/');
      var localeName = localeSplit[localeSplit.length - 1];
      service.addTranslations(localeName, requirejs(moduleName, null, null, true)['default']);
    });
  }

  exports['default'] = {
    name: 'ember-intl',
    initialize: instanceInitializer
  };
});
/* globals requirejs, Intl */

/**
 * Copyright 2015, Yahoo! Inc.
 * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */
define('offline/instance-initializers/ember-simple-auth', ['exports', 'ember-simple-auth/instance-initializers/setup-session-restoration'], function (exports, _emberSimpleAuthInstanceInitializersSetupSessionRestoration) {
  exports['default'] = {
    name: 'ember-simple-auth',
    initialize: function initialize(instance) {
      (0, _emberSimpleAuthInstanceInitializersSetupSessionRestoration['default'])(instance);
    }
  };
});
define('offline/instance-initializers/service-worker', ['exports', 'ember', 'offline/config/environment'], function (exports, _ember, _offlineConfigEnvironment) {
    exports.initialize = initialize;
    var _Ember$Logger = _ember['default'].Logger;
    var info = _Ember$Logger.info;
    var warn = _Ember$Logger.warn;
    var error = _Ember$Logger.error;

    function initialize(appInstance) {

        var ServiceWorkerService = _ember['default'].Object.extend({
            define: function define() {
                return _ember['default'].RSVP.Promise.reject({ error: 'Unimplemented: Service Worker API not supported' });
            },
            precache: function precache() {
                return _ember['default'].RSVP.Promise.reject({ error: 'Unimplemented: Service Worker API not supported' });
            }
        });

        if (_offlineConfigEnvironment['default'].serviceWorker.register && "serviceWorker" in window.navigator) {
            (function () {
                var serviceWorker = window.navigator.serviceWorker;

                ServiceWorkerService.reopen({
                    _sendMessage: function _sendMessage(message) {
                        return new _ember['default'].RSVP.Promise(function (resolve, reject) {
                            var channel = new MessageChannel();
                            channel.port1.onmessage = function (event) {
                                if (event.data.error) {
                                    reject(event.data);
                                } else {
                                    resolve(event.data);
                                }
                            };
                            serviceWorker.controller.postMessage(message, [channel.port2]);
                        });
                    },
                    define: function define() {
                        var variables = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

                        return this._sendMessage({ variables: variables, action: 'define' });
                    },
                    precache: function precache() {
                        var urls = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];

                        return this._sendMessage({ urls: urls, action: 'precache' });
                    }
                });

                serviceWorker.register(_offlineConfigEnvironment['default'].rootURL + 'sw.js', { scope: '/' }).then(function (registration) {
                    var activeWorker = registration.active;

                    registration.onupdatefound = function () {
                        registration.installing.onstatechange = function () {
                            if (this.state === "installed") {
                                if (activeWorker) {
                                    info("App updated. Restart for the new version.");
                                } else {
                                    info("App ready for offline use.");
                                }
                            }
                        };
                    };
                })['catch'](function (err) {
                    error(err);
                });
            })();
        } else {
            warn('Cannot support offline features');
        }

        appInstance.register('service:service-worker', ServiceWorkerService, { singleton: true });
    }

    exports['default'] = {
        name: 'service-worker',
        initialize: initialize
    };
});
define("offline/mirage/config", ["exports", "offline/mirage/fixtures/surveys"], function (exports, _offlineMirageFixturesSurveys) {

    var USER = {
        "id": 1,
        "locale": "en_US",
        "display_name": "developer",
        "interviewers": []
    };

    // generate an interviewer record for each survey
    _offlineMirageFixturesSurveys["default"].forEach(function (survey) {
        USER.interviewers.push({
            "user_id": USER.id,
            "description": USER.display_name,
            "interviewer_id": generateHash(survey.path + USER.id),
            "created_by": 1,
            "created_on": null,
            "last_updated_on": null,
            "last_respondent_upload_on": null,
            "total_completes": 0,
            "valid_completes": 0,
            "last_updated_by": 1,
            "active": true,
            "path": survey.path
        });
    });

    /**
     * @param {String} string
     * @returns {String}
     */
    function generateHash(string) {
        var hash = 0,
            chr = undefined,
            len = undefined;
        for (var i = 0, _len = string.length; i < _len; i++) {
            chr = string.charCodeAt(i);
            hash = (hash << 5) - hash + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash.toString();
    }

    exports.USER = USER;
    exports.SURVEYS = _offlineMirageFixturesSurveys["default"];

    exports["default"] = function () {

        this.namespace = '/api/v1/offline';

        this.get('/users/me', function () {
            return { "user": USER };
        });

        this.get('/surveys', function (schema, request) {
            return {
                surveys: _offlineMirageFixturesSurveys["default"].map(function (s) {
                    return {
                        path: s.path,
                        state: s.state,
                        compat: s.compat,
                        adminTitle: s.adminTitle,
                        respondentsTitle: s.respondentsTitle,
                        hash: s.hash,
                        version: s.version
                    };
                })
            };
        });

        this.get('/surveys/*id', function (schema, request) {
            var survey = _offlineMirageFixturesSurveys["default"].find(function (survey) {
                return survey.path === request.params.id;
            });
            return { 'survey': survey };
        });

        this.get('/surveys/*path/hash', function (schema, request) {
            var survey = _offlineMirageFixturesSurveys["default"].find(function (survey) {
                return survey.path === request.params.path;
            });
            return { 'hash': { "id": survey.path, "value": survey.hash } };
        });

        this.get('/surveys/*path/interviewers/:id', function (schema, request) {
            var interviewer = USER.interviewers.find(function (i) {
                return i.interviewer_id === request.params.id;
            });
            return { 'interviewer': interviewer };
        });

        this.post('/surveys/*path/respondents', function (schema, request) {
            return request.requestBody;
        });
    };
});
define('offline/mirage/fixtures/surveys',['exports','lodash/lodash'],function(exports,_lodashLodash){ // generated via the beacon api
var surveys={'scripts/test/surveys/offline/elements':{"adminTitle":"Offline Project","assets":[],"compat":135,"elements":[{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.8","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"suspend","uid":8,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"atleast":1,"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.16","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.9","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":16,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.15","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.9","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":15,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select all buttons that apply.","cond":null,"fir":{"inherit":false,"off":1,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"cols","groups":[],"id":"scripts/test/surveys/offline/elements.9","keywordCoder":1,"label":"q1","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"example 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.10","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.9","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":10,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"example 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.11","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.9","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":11,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"example 3","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.12","index":2,"label":"r3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.9","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":12,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"example 4","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.13","index":3,"label":"r4","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.9","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":13,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"example 5 (skip to q4)","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.14","index":4,"label":"r5","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.9","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":14,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sst":1,"styleAttributes":{"atm1d:viewMode":"tiled"},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"New Button Select (Multi-Select) Question ${q1.r1.text}","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"checkbox","uid":9,"uses":["atm1d.8"],"variables":["scripts/test/surveys/offline/elements.q1r1","scripts/test/surveys/offline/elements.q1r2","scripts/test/surveys/offline/elements.q1r3","scripts/test/surveys/offline/elements.q1r4","scripts/test/surveys/offline/elements.q1r5"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.52","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"suspend","uid":52,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":"q1.r5.valueOf()","id":"scripts/test/surveys/offline/elements.53","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"(q1.r5)","sst":1,"styleAttributes":{"builder:title":"New Skip"},"target":"q4","topLabel":true,"translateable":1,"type":"goto","uid":53,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.54","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"suspend","uid":54,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.64","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.55","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":64,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.63","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.55","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":63,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"cols","groups":[],"id":"scripts/test/surveys/offline/elements.55","keywordCoder":1,"label":"q2","mode":"all","noanswer":[],"optional":0,"parent":null,"pipeMultiple":"multiple","postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":"q1.r1.valueOf()","averages":1,"cdata":"answer 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.56","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.55","percentages":1,"postText":null,"preText":null,"py_autofill":"q1.r1","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":56,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":"q1.r2.valueOf()","averages":1,"cdata":"answer 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.57","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.55","percentages":1,"postText":null,"preText":null,"py_autofill":"q1.r2","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":57,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":"q1.r3.valueOf()","averages":1,"cdata":"answer 3","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.58","index":2,"label":"r3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.55","percentages":1,"postText":null,"preText":null,"py_autofill":"q1.r3","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":58,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":"q1.r4.valueOf()","averages":1,"cdata":"answer 4","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.59","index":3,"label":"r4","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.55","percentages":1,"postText":null,"preText":null,"py_autofill":"q1.r4","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":59,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":"q1.r5.valueOf()","averages":1,"cdata":"answer 5","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.60","index":4,"label":"r5","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.55","percentages":1,"postText":null,"preText":null,"py_autofill":"q1.r5","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":60,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":"(thisQuestion.count === 0)","averages":1,"cdata":"<i>None of These Classifications Apply</i>","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.61","index":5,"label":"none","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.55","percentages":1,"postText":null,"preText":null,"py_autofill":"thisQuestion.count == 0","py_cond":"","randomize":1,"size":-1,"styleAttributes":{"builder:none":true},"topLabel":false,"translateable":1,"type":"row","uid":61,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":"(thisQuestion.count > 1)","averages":1,"cdata":"Multiple Classifications Apply","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.62","index":6,"label":"multiple","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.55","percentages":1,"postText":null,"preText":null,"py_autofill":"thisQuestion.count gt 1","py_cond":"","randomize":1,"size":-1,"styleAttributes":{"builder:multiple":true},"topLabel":false,"translateable":1,"type":"row","uid":62,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sbase":"(OPTIONAL)","sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"New Autofill","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"checkbox","uid":55,"variables":["scripts/test/surveys/offline/elements.q2r1","scripts/test/surveys/offline/elements.q2r2","scripts/test/surveys/offline/elements.q2r3","scripts/test/surveys/offline/elements.q2r4","scripts/test/surveys/offline/elements.q2r5","scripts/test/surveys/offline/elements.q2none","scripts/test/surveys/offline/elements.q2multiple"],"where":{"data":false,"execute":true,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.65","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"suspend","uid":65,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","builderHint":"{\"rocochoName\":\"radiogrid\"}","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.71","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.66","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":71,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"Sample Column #1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.69","index":0,"label":"c1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.66","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":69,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"Sample Column #2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.70","index":1,"label":"c2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.66","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":70,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select one for each option","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/elements.66","keywordCoder":1,"label":"q3","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"Sample Row #1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.67","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.66","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":67,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Sample Row #2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.68","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.66","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":68,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"New Single Select Question [pipe: q2]","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"radio","uid":66,"variables":["scripts/test/surveys/offline/elements.q3r1","scripts/test/surveys/offline/elements.q3r2"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.72","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"suspend","uid":72,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Term with a question on the same page","cmsEditable":1,"cond":"q3.r1.c1.valueOf()","id":"scripts/test/surveys/offline/elements.73","incidence":1,"mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"(q3.r1.c1)","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"term","uid":73,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"atleast":1,"blankValue":"auto","builderHint":"{\"rocochoName\":\"checkboxgrid\"}","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.79","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.74","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":79,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"Col 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.75","index":0,"label":"c1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.74","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":75,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"Col 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.76","index":1,"label":"c2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.74","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":76,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select all that apply for each option","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/elements.74","keywordCoder":1,"label":"q4","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"Term","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.77","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.74","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":77,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Don't Term","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.78","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.74","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":78,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"New Multi-Select Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"checkbox","uid":74,"variables":["scripts/test/surveys/offline/elements.q4r1c1","scripts/test/surveys/offline/elements.q4r1c2","scripts/test/surveys/offline/elements.q4r2c1","scripts/test/surveys/offline/elements.q4r2c2"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.80","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"suspend","uid":80,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Term on a page all by itself","cmsEditable":1,"cond":"q4.c1.r1.valueOf()","id":"scripts/test/surveys/offline/elements.81","incidence":1,"mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"(q4.c1.r1)","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"term","uid":81,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.82","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"suspend","uid":82,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"Enter block (go to q6)","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.84","index":0,"label":"ch1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.83","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":84,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"Skip block (go to q9)","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.85","index":1,"label":"ch2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.83","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":85,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.87","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.83","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":87,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select one for each","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/elements.83","keywordCoder":1,"label":"q5","minRanks":-1,"mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.86","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.83","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":86,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"New Dropdown Menu Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"select","uid":83,"variables":["scripts/test/surveys/offline/elements.q5"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.88","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"suspend","uid":88,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":"q5.r1.valueOf()","countVisibleOnly":1,"elements":[{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.96","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.90","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":96,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":false,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.95","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.90","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":95,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Enter a number","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"cols","groups":[],"id":"scripts/test/surveys/offline/elements.90","keywordCoder":1,"label":"q6","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.91","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.90","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":91,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.92","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.90","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":92,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 3","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.93","index":2,"label":"r3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.90","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":93,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 4","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.94","index":3,"label":"r4","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.90","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":94,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"shuffle":{"choices":false,"cols":false,"groups":false,"none":false,"rows":true},"size":10,"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"New Number Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"number","uid":90,"variables":["scripts/test/surveys/offline/elements.q6r1","scripts/test/surveys/offline/elements.q6r2","scripts/test/surveys/offline/elements.q6r3","scripts/test/surveys/offline/elements.q6r4"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.97","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"suspend","uid":97,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"atleast":1,"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.104","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.98","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":104,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.103","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.98","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":103,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select all that apply","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"cols","groups":[],"id":"scripts/test/surveys/offline/elements.98","keywordCoder":1,"label":"q7","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 1","cmsEditable":1,"cond":"(q6.r1.val > 10)","extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.99","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.98","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"q6.r1.val > 10","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":99,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 2","cmsEditable":1,"cond":"(q6.r2.val > 10)","extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.100","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.98","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"q6.r2.val > 10","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":100,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 3","cmsEditable":1,"cond":"(q6.r3.val > 10)","extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.101","index":2,"label":"r3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.98","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"q6.r3.val > 10","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":101,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 4","cmsEditable":1,"cond":"(q6.r4.val > 10)","extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.102","index":3,"label":"r4","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.98","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"q6.r4.val > 10","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":102,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"shuffle":{"choices":false,"cols":false,"groups":false,"none":false,"rows":true},"shuffleBy":"q6","sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"The following rows were 10 or more in the previous question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"checkbox","uid":98,"variables":["scripts/test/surveys/offline/elements.q7r1","scripts/test/surveys/offline/elements.q7r2","scripts/test/surveys/offline/elements.q7r3","scripts/test/surveys/offline/elements.q7r4"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.109","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.105","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":109,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.108","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.105","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":108,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select one","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"cols","groups":[],"id":"scripts/test/surveys/offline/elements.105","keywordCoder":1,"label":"q8","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"Skip to term","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.106","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.105","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":106,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Continue","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.107","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.105","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":107,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"shuffle":{"choices":false,"cols":false,"groups":false,"none":false,"rows":true},"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"New Single Select Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"radio","uid":105,"variables":["scripts/test/surveys/offline/elements.q8"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"id":"scripts/test/surveys/offline/elements.89","label":"b1","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"q5.r1","sst":1,"styleAttributes":{"builder:title":"New Block"},"topLabel":true,"translateable":1,"type":"block","uid":89,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.110","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"suspend","uid":110,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":"q8.r1.valueOf()","id":"scripts/test/surveys/offline/elements.111","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"q8.r1","sst":1,"styleAttributes":{"builder:title":"New Skip"},"target":"term","topLabel":true,"translateable":1,"type":"goto","uid":111,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.115","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.112","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":115,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.114","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.112","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":114,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Be specific","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/elements.112","keywordCoder":1,"label":"q9","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.113","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.112","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":113,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"size":25,"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"New Text Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"text","uid":112,"variables":["scripts/test/surveys/offline/elements.q9"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"codecheck":1,"cond":"0","elements":[{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.128","label":"term","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"label","uid":128,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.129","label":"noqual","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"label","uid":129,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/elements.130","mode":"all","now":1,"parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"finish","uid":130,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"id":"scripts/test/surveys/offline/elements.127","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"0","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"if","uid":127,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.134","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.131","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":134,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.133","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.131","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":133,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/elements.131","keywordCoder":1,"label":"qtime","mode":"all","noanswer":[],"optional":1,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.132","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.131","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":132,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sbase":"(OPTIONAL)","size":6,"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"Total Interview Time","topLabel":true,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"float","uid":131,"variables":["scripts/test/surveys/offline/elements.qtime"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":false}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.194","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.191","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":194,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.193","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.191","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":193,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/elements.191","keywordCoder":1,"label":"start_date","mode":"all","noanswer":[],"optional":1,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/elements.192","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/elements.191","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":192,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sbase":"(OPTIONAL)","size":20,"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"Survey start time","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"text","uid":191,"variables":["scripts/test/surveys/offline/elements.start_date"],"where":{"data":true,"execute":false,"none":false,"notdp":false,"report":false,"summary":false,"survey":false}},{"cdata":"Please select $(which) $(count) $(box) (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.37","label":"q1,sys_check-error","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":37,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"answers","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.38","label":"q1,sys_box.plur","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":38,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"answer","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.39","label":"q1,sys_box.sing","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":39,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select at least $(count) answers (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.40","label":"q1,sys_check-error-atLeast-plur-column","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":40,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select at least $(count) answers (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.41","label":"q1,sys_check-error-atLeast-plur-row","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":41,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select at least $(count) answer (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.42","label":"q1,sys_check-error-atLeast-sing-column","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":42,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select at least $(count) answer (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.43","label":"q1,sys_check-error-atLeast-sing-row","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":43,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select at most $(count) answers (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.44","label":"q1,sys_check-error-atMost-plur-column","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":44,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select at most $(count) answers (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.45","label":"q1,sys_check-error-atMost-plur-row","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":45,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select at most $(count) answer (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.46","label":"q1,sys_check-error-atMost-sing-column","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":46,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select at most $(count) answer (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.47","label":"q1,sys_check-error-atMost-sing-row","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":47,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select exactly $(count) answers (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.48","label":"q1,sys_check-error-exactly-plur-column","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":48,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select exactly $(count) answers (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.49","label":"q1,sys_check-error-exactly-plur-row","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":49,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select exactly $(count) answer (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.50","label":"q1,sys_check-error-exactly-sing-column","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":50,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Please select exactly $(count) answer (you selected $(actual)).","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.51","label":"q1,sys_check-error-exactly-sing-row","mode":"all","parent":"scripts/test/surveys/offline/elements.9","postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":51,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"errors":["Offline survey doesn't support \"featurephone\" you have \"smartphone,tablet,featurephone,desktop\""],"exitpages":[{"cdata":"","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/elements.4","mode":"all","name":"interviewerId","parent":null,"postText":null,"preText":null,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"var","uid":4,"variable":"scripts/test/surveys/offline/elements.interviewerId","where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"TERMINATED","cmsEditable":1,"cond":"terminated","id":"scripts/test/surveys/offline/elements.5","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"terminated","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"exit","uid":5,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"OVERQUOTA","cmsEditable":1,"cond":"overquota","id":"scripts/test/surveys/offline/elements.6","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"overquota","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"exit","uid":6,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"QUALIFIED","cmsEditable":1,"cond":"qualified","id":"scripts/test/surveys/offline/elements.7","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"qualified","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"exit","uid":7,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"hash":"1a9e074ee8144ca67e77c480a67d22c4","messages":[{"id":"scripts/test/surveys/offline/elements.english.noAnswerSelected","lang":"english","name":"sys_noAnswerSelected","survey":"scripts/test/surveys/offline/elements","text":"Please select an answer."},{"id":"scripts/test/surveys/offline/elements.english.extraInfo","lang":"english","name":"sys_extraInfo","survey":"scripts/test/surveys/offline/elements","text":"Please specify the required extra information."},{"id":"scripts/test/surveys/offline/elements.english.notWhole","lang":"english","name":"sys_notWhole","survey":"scripts/test/surveys/offline/elements","text":"Please specify a whole number."},{"id":"scripts/test/surveys/offline/elements.english.notWholeNumber","lang":"english","name":"sys_notWholeNumber","survey":"scripts/test/surveys/offline/elements","text":"Please specify a whole number."},{"id":"scripts/test/surveys/offline/elements.english.notPositive","lang":"english","name":"sys_notPositive","survey":"scripts/test/surveys/offline/elements","text":"Please specify a positive number."},{"id":"scripts/test/surveys/offline/elements.english.valueAtLeast","lang":"english","name":"sys_valueAtLeast","survey":"scripts/test/surveys/offline/elements","text":"Sorry, but the value must be at least $(min)."},{"id":"scripts/test/surveys/offline/elements.english.valueAtMost","lang":"english","name":"sys_valueAtMost","survey":"scripts/test/surveys/offline/elements","text":"Sorry, but the value must be no more than $(max)."},{"id":"scripts/test/surveys/offline/elements.english.notValidDecimalNumber","lang":"english","name":"sys_notValidDecimalNumber","survey":"scripts/test/surveys/offline/elements","text":"Please specify a valid decimal number."},{"id":"scripts/test/surveys/offline/elements.english.textNoAnswerSelected","lang":"english","name":"sys_textNoAnswerSelected","survey":"scripts/test/surveys/offline/elements","text":"Please provide an answer"},{"id":"scripts/test/surveys/offline/elements.english.extraSelect","lang":"english","name":"sys_extraSelect","survey":"scripts/test/surveys/offline/elements","text":"Since you specified extra information, please also select a corresponding answer."},{"id":"scripts/test/surveys/offline/elements.english.check-error-atLeast-plur-column","lang":"english","name":"sys_check-error-atLeast-plur-column","survey":"scripts/test/surveys/offline/elements","text":"Please check at least $(count) boxes in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/elements.english.check-error-atLeast-plur-row","lang":"english","name":"sys_check-error-atLeast-plur-row","survey":"scripts/test/surveys/offline/elements","text":"Please check at least $(count) boxes in this row (you checked $(actual))."},{"id":"scripts/test/surveys/offline/elements.english.check-error-atMost-plur-column","lang":"english","name":"sys_check-error-atMost-plur-column","survey":"scripts/test/surveys/offline/elements","text":"Please check at most $(count) boxes in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/elements.english.check-error-atMost-plur-row","lang":"english","name":"sys_check-error-atMost-plur-row","survey":"scripts/test/surveys/offline/elements","text":"Please check at most $(count) boxes in this row (you checked $(actual))."},{"id":"scripts/test/surveys/offline/elements.english.check-error-exactly-plur-column","lang":"english","name":"sys_check-error-exactly-plur-column","survey":"scripts/test/surveys/offline/elements","text":"Please check exactly $(count) boxes in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/elements.english.check-error-exactly-plur-row","lang":"english","name":"sys_check-error-exactly-plur-row","survey":"scripts/test/surveys/offline/elements","text":"Please check exactly $(count) boxes in this row (you checked $(actual))."},{"id":"scripts/test/surveys/offline/elements.english.check-error-atLeast-sing-column","lang":"english","name":"sys_check-error-atLeast-sing-column","survey":"scripts/test/surveys/offline/elements","text":"Please check at least 1 box in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/elements.english.check-error-atLeast-sing-row","lang":"english","name":"sys_check-error-atLeast-sing-row","survey":"scripts/test/surveys/offline/elements","text":"Please check at least 1 box in this row (you checked $(actual))."},{"id":"scripts/test/surveys/offline/elements.english.check-error-atMost-sing-column","lang":"english","name":"sys_check-error-atMost-sing-column","survey":"scripts/test/surveys/offline/elements","text":"Please check at most 1 box in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/elements.english.check-error-atMost-sing-row","lang":"english","name":"sys_check-error-atMost-sing-row","survey":"scripts/test/surveys/offline/elements","text":"Please check at most 1 box in this row (you checked $(actual))."},{"id":"scripts/test/surveys/offline/elements.english.check-error-exactly-sing-column","lang":"english","name":"sys_check-error-exactly-sing-column","survey":"scripts/test/surveys/offline/elements","text":"Please check exactly 1 box in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/elements.english.check-error-exactly-sing-row","lang":"english","name":"sys_check-error-exactly-sing-row","survey":"scripts/test/surveys/offline/elements","text":"Please check exactly 1 box in this row (you checked $(actual))."}],"path":"scripts/test/surveys/offline/elements","respondentsTitle":"Survey","state":"testing","variables":[{"col":null,"id":"scripts/test/surveys/offline/elements.interviewerId","label":"interviewerId","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"interviewerId"},{"col":null,"id":"scripts/test/surveys/offline/elements.qtime","label":"qtime","oe":false,"qlabel":"qtime","row":null,"survey":"scripts/test/surveys/offline/elements","type":"float","vgroup":"qtime"},{"col":null,"id":"scripts/test/surveys/offline/elements.session","label":"session","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"session"},{"col":null,"id":"scripts/test/surveys/offline/elements.userAgent","label":"userAgent","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"userAgent"},{"col":null,"id":"scripts/test/surveys/offline/elements.q1r1","label":"q1r1","oe":false,"qlabel":"q1","row":"r1","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q1"},{"col":null,"id":"scripts/test/surveys/offline/elements.q1r2","label":"q1r2","oe":false,"qlabel":"q1","row":"r2","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q1"},{"col":null,"id":"scripts/test/surveys/offline/elements.q1r3","label":"q1r3","oe":false,"qlabel":"q1","row":"r3","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q1"},{"col":null,"id":"scripts/test/surveys/offline/elements.q1r4","label":"q1r4","oe":false,"qlabel":"q1","row":"r4","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q1"},{"col":null,"id":"scripts/test/surveys/offline/elements.q1r5","label":"q1r5","oe":false,"qlabel":"q1","row":"r5","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q1"},{"col":null,"id":"scripts/test/surveys/offline/elements.q3r1","label":"q3r1","oe":false,"qlabel":"q3","row":"r1","survey":"scripts/test/surveys/offline/elements","type":"single","values":[1,2],"vgroup":"q3"},{"col":null,"id":"scripts/test/surveys/offline/elements.q3r2","label":"q3r2","oe":false,"qlabel":"q3","row":"r2","survey":"scripts/test/surveys/offline/elements","type":"single","values":[1,2],"vgroup":"q3"},{"col":null,"id":"scripts/test/surveys/offline/elements.q2r1","label":"q2r1","oe":false,"qlabel":"q2","row":"r1","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q2"},{"col":null,"id":"scripts/test/surveys/offline/elements.q2r2","label":"q2r2","oe":false,"qlabel":"q2","row":"r2","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q2"},{"col":null,"id":"scripts/test/surveys/offline/elements.q2r3","label":"q2r3","oe":false,"qlabel":"q2","row":"r3","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q2"},{"col":null,"id":"scripts/test/surveys/offline/elements.q2r4","label":"q2r4","oe":false,"qlabel":"q2","row":"r4","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q2"},{"col":null,"id":"scripts/test/surveys/offline/elements.q2r5","label":"q2r5","oe":false,"qlabel":"q2","row":"r5","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q2"},{"col":null,"id":"scripts/test/surveys/offline/elements.q2none","label":"q2none","oe":false,"qlabel":"q2","row":"none","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q2"},{"col":null,"id":"scripts/test/surveys/offline/elements.q2multiple","label":"q2multiple","oe":false,"qlabel":"q2","row":"multiple","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q2"},{"col":null,"id":"scripts/test/surveys/offline/elements.q5","label":"q5","oe":false,"qlabel":"q5","row":null,"survey":"scripts/test/surveys/offline/elements","type":"single","values":[1,2],"vgroup":"q5"},{"col":"c1","id":"scripts/test/surveys/offline/elements.q4r1c1","label":"q4r1c1","oe":false,"qlabel":"q4","row":"r1","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q4r1"},{"col":"c2","id":"scripts/test/surveys/offline/elements.q4r1c2","label":"q4r1c2","oe":false,"qlabel":"q4","row":"r1","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q4r1"},{"col":"c1","id":"scripts/test/surveys/offline/elements.q4r2c1","label":"q4r2c1","oe":false,"qlabel":"q4","row":"r2","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q4r2"},{"col":"c2","id":"scripts/test/surveys/offline/elements.q4r2c2","label":"q4r2c2","oe":false,"qlabel":"q4","row":"r2","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q4r2"},{"col":null,"id":"scripts/test/surveys/offline/elements.q7r1","label":"q7r1","oe":false,"qlabel":"q7","row":"r1","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q7"},{"col":null,"id":"scripts/test/surveys/offline/elements.q7r2","label":"q7r2","oe":false,"qlabel":"q7","row":"r2","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q7"},{"col":null,"id":"scripts/test/surveys/offline/elements.q7r3","label":"q7r3","oe":false,"qlabel":"q7","row":"r3","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q7"},{"col":null,"id":"scripts/test/surveys/offline/elements.q7r4","label":"q7r4","oe":false,"qlabel":"q7","row":"r4","survey":"scripts/test/surveys/offline/elements","type":"multiple","vgroup":"q7"},{"col":null,"id":"scripts/test/surveys/offline/elements.q6r1","label":"q6r1","oe":false,"qlabel":"q6","row":"r1","survey":"scripts/test/surveys/offline/elements","type":"number","vgroup":"q6"},{"col":null,"id":"scripts/test/surveys/offline/elements.q6r2","label":"q6r2","oe":false,"qlabel":"q6","row":"r2","survey":"scripts/test/surveys/offline/elements","type":"number","vgroup":"q6"},{"col":null,"id":"scripts/test/surveys/offline/elements.q6r3","label":"q6r3","oe":false,"qlabel":"q6","row":"r3","survey":"scripts/test/surveys/offline/elements","type":"number","vgroup":"q6"},{"col":null,"id":"scripts/test/surveys/offline/elements.q6r4","label":"q6r4","oe":false,"qlabel":"q6","row":"r4","survey":"scripts/test/surveys/offline/elements","type":"number","vgroup":"q6"},{"col":null,"id":"scripts/test/surveys/offline/elements.q9","label":"q9","oe":false,"qlabel":"q9","row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"q9"},{"col":null,"id":"scripts/test/surveys/offline/elements.q8","label":"q8","oe":false,"qlabel":"q8","row":null,"survey":"scripts/test/surveys/offline/elements","type":"single","values":[1,2],"vgroup":"q8"},{"col":null,"id":"scripts/test/surveys/offline/elements.source","label":"source","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"source"},{"col":null,"id":"scripts/test/surveys/offline/elements.decLang","label":"decLang","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"decLang"},{"col":null,"id":"scripts/test/surveys/offline/elements.start_date","label":"start_date","oe":false,"qlabel":"start_date","row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"start_date"},{"col":null,"id":"scripts/test/surveys/offline/elements.date","label":"date","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"date"},{"col":null,"id":"scripts/test/surveys/offline/elements.markers","label":"markers","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"markers"},{"col":null,"id":"scripts/test/surveys/offline/elements.url","label":"url","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"url"},{"col":null,"id":"scripts/test/surveys/offline/elements.list","label":"list","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"list"},{"col":null,"id":"scripts/test/surveys/offline/elements.dcua","label":"dcua","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"dcua"},{"col":null,"id":"scripts/test/surveys/offline/elements.record","label":"record","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"record"},{"col":null,"id":"scripts/test/surveys/offline/elements.ipAddress","label":"ipAddress","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/elements","type":"text","vgroup":"ipAddress"}],"version":1},'scripts/test/surveys/offline/environment':{"adminTitle":"environment","assets":[],"compat":135,"elements":[{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/environment.8","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"suspend","uid":8,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.16","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.9","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":16,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"Tomster","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.14","index":0,"label":"c1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.9","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":14,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"Zoey","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.15","index":1,"label":"c2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.9","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":15,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select one","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/environment.9","keywordCoder":1,"label":"q1","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.10","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.9","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":10,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.11","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.9","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":11,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 3","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.12","index":2,"label":"r3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.9","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":12,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 4","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.13","index":3,"label":"r4","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.9","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":13,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"2D Radio Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"radio","uid":9,"variables":["scripts/test/surveys/offline/environment.q1r1","scripts/test/surveys/offline/environment.q1r2","scripts/test/surveys/offline/environment.q1r3","scripts/test/surveys/offline/environment.q1r4"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"atleast":1,"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.25","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.17","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":25,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"Col 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.23","index":0,"label":"c1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.17","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":23,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"Col 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.24","index":1,"label":"c2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.17","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":24,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select all that apply","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/environment.17","keywordCoder":1,"label":"q2","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.18","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.17","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":18,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.19","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.17","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":19,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 3","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.20","index":2,"label":"r3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.17","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":20,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 4","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.21","index":3,"label":"r4","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.17","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":21,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 5","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.22","index":4,"label":"r5","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.17","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":22,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"2D Checkbox Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"checkbox","uid":17,"variables":["scripts/test/surveys/offline/environment.q2r1c1","scripts/test/surveys/offline/environment.q2r1c2","scripts/test/surveys/offline/environment.q2r2c1","scripts/test/surveys/offline/environment.q2r2c2","scripts/test/surveys/offline/environment.q2r3c1","scripts/test/surveys/offline/environment.q2r3c2","scripts/test/surveys/offline/environment.q2r4c1","scripts/test/surveys/offline/environment.q2r4c2","scripts/test/surveys/offline/environment.q2r5c1","scripts/test/surveys/offline/environment.q2r5c2"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"Choice 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.27","index":0,"label":"ch1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.26","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":27,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"Choice 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.28","index":1,"label":"ch2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.26","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":28,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"Choice 3","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.29","index":2,"label":"ch3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.26","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":29,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"Choice 4","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.30","index":3,"label":"ch4","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.26","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":30,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"Col 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.35","index":0,"label":"c1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.26","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":35,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"Col 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.36","index":1,"label":"c2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.26","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":36,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select one for each","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/environment.26","keywordCoder":1,"label":"q3","minRanks":-1,"mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.31","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.26","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":31,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.32","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.26","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":32,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 3","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.33","index":2,"label":"r3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.26","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":33,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 4","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.34","index":3,"label":"r4","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.26","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":34,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"2D Select Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"select","uid":26,"variables":["scripts/test/surveys/offline/environment.q3r1c1","scripts/test/surveys/offline/environment.q3r1c2","scripts/test/surveys/offline/environment.q3r2c1","scripts/test/surveys/offline/environment.q3r2c2","scripts/test/surveys/offline/environment.q3r3c1","scripts/test/surveys/offline/environment.q3r3c2","scripts/test/surveys/offline/environment.q3r4c1","scripts/test/surveys/offline/environment.q3r4c2"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.43","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.37","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":43,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.42","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.37","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":42,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select one","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"cols","groups":[],"id":"scripts/test/surveys/offline/environment.37","keywordCoder":1,"label":"q1b","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"Gryffindor","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.38","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.37","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":38,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Ravenclaw","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.39","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.37","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":39,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Hufflepuff","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.40","index":2,"label":"r3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.37","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":40,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Slytherin","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.41","index":3,"label":"r4","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.37","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":41,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"1D Radio Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"radio","uid":37,"variables":["scripts/test/surveys/offline/environment.q1b"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"atleast":1,"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.50","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.44","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":50,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.49","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.44","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":49,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select all that apply","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"cols","groups":[],"id":"scripts/test/surveys/offline/environment.44","keywordCoder":1,"label":"q2b","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"Java","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.45","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.44","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":45,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Python","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.46","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.44","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":46,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"JavaScript","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.47","index":2,"label":"r3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.44","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":47,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Ruby","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.48","index":3,"label":"r4","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.44","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":48,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"1D Checkbox Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"checkbox","uid":44,"variables":["scripts/test/surveys/offline/environment.q2br1","scripts/test/surveys/offline/environment.q2br2","scripts/test/surveys/offline/environment.q2br3","scripts/test/surveys/offline/environment.q2br4"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"[pipe: q1]","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.52","index":0,"label":"ch1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.51","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":52,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"[pipe: q2]","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.53","index":1,"label":"ch2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.51","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":53,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"[pipe: q3]","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.54","index":2,"label":"ch3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.51","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":54,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"[pipe: q1b]","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.55","index":3,"label":"ch4","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.51","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":55,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.57","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.51","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":57,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Select one for each","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/environment.51","keywordCoder":1,"label":"q3b","minRanks":-1,"mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.56","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.51","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":56,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"1D Select Question -- nested pipes","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"select","uid":51,"variables":["scripts/test/surveys/offline/environment.q3b"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.64","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.58","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":64,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":false,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"Col 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.61","index":0,"label":"c1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.58","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":61,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"Col 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.62","index":1,"label":"c2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.58","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":62,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"averages":1,"cdata":"Col 3","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.63","index":2,"label":"c3","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.58","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":63,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Enter a number","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/environment.58","keywordCoder":1,"label":"q4","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 1","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.59","index":0,"label":"r1","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.58","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":59,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"aggregate":1,"autofill":null,"averages":1,"cdata":"Row 2","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.60","index":1,"label":"r2","mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.58","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":60,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"size":10,"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"2D Number Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"number","uid":58,"variables":["scripts/test/surveys/offline/environment.q4r1c1","scripts/test/surveys/offline/environment.q4r1c2","scripts/test/surveys/offline/environment.q4r1c3","scripts/test/surveys/offline/environment.q4r2c1","scripts/test/surveys/offline/environment.q4r2c2","scripts/test/surveys/offline/environment.q4r2c3"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.68","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.65","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":68,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.67","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.65","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":67,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Enter a number","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/environment.65","keywordCoder":1,"label":"q4b","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.66","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.65","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":66,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"size":6,"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"1D Float Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"float","uid":65,"variables":["scripts/test/surveys/offline/environment.q4b"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.72","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.69","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":72,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.71","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.69","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":71,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"Be specific","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/environment.69","keywordCoder":1,"label":"q5","mode":"all","noanswer":[],"optional":0,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.70","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.69","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":70,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"size":25,"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"1D Text Question","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"text","uid":69,"variables":["scripts/test/surveys/offline/environment.q5"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"and here is some text...","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/environment.73","label":"simple_resource","mode":"all","parent":null,"postText":null,"preText":null,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"res","uid":73,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"codecheck":1,"cond":"0","elements":[{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/environment.79","label":"term","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"label","uid":79,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/environment.80","label":"noqual","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"label","uid":80,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":null,"id":"scripts/test/surveys/offline/environment.81","mode":"all","now":1,"parent":null,"postText":null,"preText":null,"py_cond":"","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"finish","uid":81,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"id":"scripts/test/surveys/offline/environment.78","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"0","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"if","uid":78,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.85","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.82","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":85,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.84","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.82","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":84,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/environment.82","keywordCoder":1,"label":"qtime","mode":"all","noanswer":[],"optional":1,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.83","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.82","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":83,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sbase":"(OPTIONAL)","size":6,"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"Total Interview Time","topLabel":true,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"float","uid":82,"variables":["scripts/test/surveys/offline/environment.qtime"],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":false}},{"adim":{"auto":1,"choices":false,"cols":false,"rows":false},"blankValue":"auto","cdata":"","choiceCond":null,"choiceGroups":{"report":true,"restrict":true,"survey":true},"choiceNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"choices":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.145","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.142","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"choice","uid":145,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"cmsEditable":1,"colCond":null,"colGroups":{"report":true,"restrict":true,"survey":true},"colNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"cols":[{"aggregate":1,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.144","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.142","percentages":1,"postText":null,"preText":null,"py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"col","uid":144,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"comment":"","cond":null,"fir":{"inherit":1,"off":false,"on":false},"fwidth":-1,"groupDepth":1,"grouping":"rows","groups":[],"id":"scripts/test/surveys/offline/environment.142","keywordCoder":1,"label":"start_date","mode":"all","noanswer":[],"optional":1,"parent":null,"postText":null,"preText":null,"py_choiceCond":"","py_colCond":"","py_cond":"","py_rowCond":"","randomize":1,"rowCond":null,"rowGroups":{"report":true,"restrict":true,"survey":true},"rowNets":{"report":true,"restrict":true,"rsort":false,"sort":false},"rows":[{"aggregate":1,"autofill":null,"averages":1,"cdata":"","cmsEditable":1,"cond":null,"extraError":1,"groups":[],"id":"scripts/test/surveys/offline/environment.143","index":0,"mode":"all","openSize":10,"optional":0,"parent":"scripts/test/surveys/offline/environment.142","percentages":1,"postText":null,"preText":null,"py_autofill":"","py_cond":"","randomize":1,"size":-1,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"row","uid":143,"value":-1,"variables":[],"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"sbase":"(OPTIONAL)","size":20,"sst":1,"styleAttributes":{},"surveyDisplay":{"auto":false,"desktop":false,"inherit":1,"mobile":false},"title":"Survey start time","topLabel":true,"translateable":1,"tsengaged":1,"tv":{"auto":1,"force":false,"off":false},"type":"text","uid":142,"variables":["scripts/test/surveys/offline/environment.start_date"],"where":{"data":true,"execute":false,"none":false,"notdp":false,"report":false,"summary":false,"survey":false}}],"errors":[],"exitpages":[{"cdata":"Thank you for taking our survey.","cmsEditable":1,"cond":"terminated","id":"scripts/test/surveys/offline/environment.4","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"terminated","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"exit","uid":4,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Thank you for taking our survey. Your efforts are greatly appreciated!","cmsEditable":1,"cond":"qualified","id":"scripts/test/surveys/offline/environment.5","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"qualified","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"exit","uid":5,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"Thank you for taking our survey.","cmsEditable":1,"cond":"overquota","id":"scripts/test/surveys/offline/environment.6","mode":"all","parent":null,"postText":null,"preText":null,"py_cond":"overquota","sst":1,"styleAttributes":{},"topLabel":true,"translateable":1,"type":"exit","uid":6,"where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}},{"cdata":"","cmsEditable":1,"cond":"","id":"scripts/test/surveys/offline/environment.7","mode":"all","name":"interviewerId","parent":null,"postText":null,"preText":null,"styleAttributes":{},"topLabel":false,"translateable":1,"type":"var","uid":7,"variable":"scripts/test/surveys/offline/environment.interviewerId","where":{"data":false,"execute":false,"none":false,"notdp":false,"report":true,"summary":false,"survey":true}}],"hash":"896a711ba747996b0b1f8679d9d26e2e","messages":[{"id":"scripts/test/surveys/offline/environment.english.noAnswerSelected","lang":"english","name":"sys_noAnswerSelected","survey":"scripts/test/surveys/offline/environment","text":"Please select an answer."},{"id":"scripts/test/surveys/offline/environment.english.extraInfo","lang":"english","name":"sys_extraInfo","survey":"scripts/test/surveys/offline/environment","text":"Please specify the required extra information."},{"id":"scripts/test/surveys/offline/environment.english.notWhole","lang":"english","name":"sys_notWhole","survey":"scripts/test/surveys/offline/environment","text":"Please specify a whole number."},{"id":"scripts/test/surveys/offline/environment.english.notWholeNumber","lang":"english","name":"sys_notWholeNumber","survey":"scripts/test/surveys/offline/environment","text":"Please specify a whole number."},{"id":"scripts/test/surveys/offline/environment.english.notPositive","lang":"english","name":"sys_notPositive","survey":"scripts/test/surveys/offline/environment","text":"Please specify a positive number."},{"id":"scripts/test/surveys/offline/environment.english.valueAtLeast","lang":"english","name":"sys_valueAtLeast","survey":"scripts/test/surveys/offline/environment","text":"Sorry, but the value must be at least $(min)."},{"id":"scripts/test/surveys/offline/environment.english.valueAtMost","lang":"english","name":"sys_valueAtMost","survey":"scripts/test/surveys/offline/environment","text":"Sorry, but the value must be no more than $(max)."},{"id":"scripts/test/surveys/offline/environment.english.notValidDecimalNumber","lang":"english","name":"sys_notValidDecimalNumber","survey":"scripts/test/surveys/offline/environment","text":"Please specify a valid decimal number."},{"id":"scripts/test/surveys/offline/environment.english.textNoAnswerSelected","lang":"english","name":"sys_textNoAnswerSelected","survey":"scripts/test/surveys/offline/environment","text":"Please provide an answer"},{"id":"scripts/test/surveys/offline/environment.english.extraSelect","lang":"english","name":"sys_extraSelect","survey":"scripts/test/surveys/offline/environment","text":"Since you specified extra information, please also select a corresponding answer."},{"id":"scripts/test/surveys/offline/environment.english.check-error-atLeast-plur-column","lang":"english","name":"sys_check-error-atLeast-plur-column","survey":"scripts/test/surveys/offline/environment","text":"Please check at least $(count) boxes in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/environment.english.check-error-atLeast-plur-row","lang":"english","name":"sys_check-error-atLeast-plur-row","survey":"scripts/test/surveys/offline/environment","text":"Please check at least $(count) boxes in this row (you checked $(actual))."},{"id":"scripts/test/surveys/offline/environment.english.check-error-atMost-plur-column","lang":"english","name":"sys_check-error-atMost-plur-column","survey":"scripts/test/surveys/offline/environment","text":"Please check at most $(count) boxes in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/environment.english.check-error-atMost-plur-row","lang":"english","name":"sys_check-error-atMost-plur-row","survey":"scripts/test/surveys/offline/environment","text":"Please check at most $(count) boxes in this row (you checked $(actual))."},{"id":"scripts/test/surveys/offline/environment.english.check-error-exactly-plur-column","lang":"english","name":"sys_check-error-exactly-plur-column","survey":"scripts/test/surveys/offline/environment","text":"Please check exactly $(count) boxes in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/environment.english.check-error-exactly-plur-row","lang":"english","name":"sys_check-error-exactly-plur-row","survey":"scripts/test/surveys/offline/environment","text":"Please check exactly $(count) boxes in this row (you checked $(actual))."},{"id":"scripts/test/surveys/offline/environment.english.check-error-atLeast-sing-column","lang":"english","name":"sys_check-error-atLeast-sing-column","survey":"scripts/test/surveys/offline/environment","text":"Please check at least 1 box in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/environment.english.check-error-atLeast-sing-row","lang":"english","name":"sys_check-error-atLeast-sing-row","survey":"scripts/test/surveys/offline/environment","text":"Please check at least 1 box in this row (you checked $(actual))."},{"id":"scripts/test/surveys/offline/environment.english.check-error-atMost-sing-column","lang":"english","name":"sys_check-error-atMost-sing-column","survey":"scripts/test/surveys/offline/environment","text":"Please check at most 1 box in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/environment.english.check-error-atMost-sing-row","lang":"english","name":"sys_check-error-atMost-sing-row","survey":"scripts/test/surveys/offline/environment","text":"Please check at most 1 box in this row (you checked $(actual))."},{"id":"scripts/test/surveys/offline/environment.english.check-error-exactly-sing-column","lang":"english","name":"sys_check-error-exactly-sing-column","survey":"scripts/test/surveys/offline/environment","text":"Please check exactly 1 box in this column (you checked $(actual))."},{"id":"scripts/test/surveys/offline/environment.english.check-error-exactly-sing-row","lang":"english","name":"sys_check-error-exactly-sing-row","survey":"scripts/test/surveys/offline/environment","text":"Please check exactly 1 box in this row (you checked $(actual))."}],"path":"scripts/test/surveys/offline/environment","respondentsTitle":"Survey","state":"testing","variables":[{"col":null,"id":"scripts/test/surveys/offline/environment.q3b","label":"q3b","oe":false,"qlabel":"q3b","row":null,"survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2,3,4],"vgroup":"q3b"},{"col":null,"id":"scripts/test/surveys/offline/environment.interviewerId","label":"interviewerId","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"interviewerId"},{"col":null,"id":"scripts/test/surveys/offline/environment.q1b","label":"q1b","oe":false,"qlabel":"q1b","row":null,"survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2,3,4],"vgroup":"q1b"},{"col":null,"id":"scripts/test/surveys/offline/environment.qtime","label":"qtime","oe":false,"qlabel":"qtime","row":null,"survey":"scripts/test/surveys/offline/environment","type":"float","vgroup":"qtime"},{"col":null,"id":"scripts/test/surveys/offline/environment.session","label":"session","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"session"},{"col":null,"id":"scripts/test/surveys/offline/environment.userAgent","label":"userAgent","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"userAgent"},{"col":null,"id":"scripts/test/surveys/offline/environment.q1r1","label":"q1r1","oe":false,"qlabel":"q1","row":"r1","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2],"vgroup":"q1"},{"col":null,"id":"scripts/test/surveys/offline/environment.q1r2","label":"q1r2","oe":false,"qlabel":"q1","row":"r2","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2],"vgroup":"q1"},{"col":null,"id":"scripts/test/surveys/offline/environment.q1r3","label":"q1r3","oe":false,"qlabel":"q1","row":"r3","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2],"vgroup":"q1"},{"col":null,"id":"scripts/test/surveys/offline/environment.q1r4","label":"q1r4","oe":false,"qlabel":"q1","row":"r4","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2],"vgroup":"q1"},{"col":"c1","id":"scripts/test/surveys/offline/environment.q3r1c1","label":"q3r1c1","oe":false,"qlabel":"q3","row":"r1","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2,3,4],"vgroup":"q3r1"},{"col":"c2","id":"scripts/test/surveys/offline/environment.q3r1c2","label":"q3r1c2","oe":false,"qlabel":"q3","row":"r1","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2,3,4],"vgroup":"q3r1"},{"col":"c1","id":"scripts/test/surveys/offline/environment.q3r2c1","label":"q3r2c1","oe":false,"qlabel":"q3","row":"r2","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2,3,4],"vgroup":"q3r2"},{"col":"c2","id":"scripts/test/surveys/offline/environment.q3r2c2","label":"q3r2c2","oe":false,"qlabel":"q3","row":"r2","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2,3,4],"vgroup":"q3r2"},{"col":"c1","id":"scripts/test/surveys/offline/environment.q3r3c1","label":"q3r3c1","oe":false,"qlabel":"q3","row":"r3","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2,3,4],"vgroup":"q3r3"},{"col":"c2","id":"scripts/test/surveys/offline/environment.q3r3c2","label":"q3r3c2","oe":false,"qlabel":"q3","row":"r3","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2,3,4],"vgroup":"q3r3"},{"col":"c1","id":"scripts/test/surveys/offline/environment.q3r4c1","label":"q3r4c1","oe":false,"qlabel":"q3","row":"r4","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2,3,4],"vgroup":"q3r4"},{"col":"c2","id":"scripts/test/surveys/offline/environment.q3r4c2","label":"q3r4c2","oe":false,"qlabel":"q3","row":"r4","survey":"scripts/test/surveys/offline/environment","type":"single","values":[1,2,3,4],"vgroup":"q3r4"},{"col":"c1","id":"scripts/test/surveys/offline/environment.q2r1c1","label":"q2r1c1","oe":false,"qlabel":"q2","row":"r1","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2r1"},{"col":"c2","id":"scripts/test/surveys/offline/environment.q2r1c2","label":"q2r1c2","oe":false,"qlabel":"q2","row":"r1","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2r1"},{"col":"c1","id":"scripts/test/surveys/offline/environment.q2r2c1","label":"q2r2c1","oe":false,"qlabel":"q2","row":"r2","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2r2"},{"col":"c2","id":"scripts/test/surveys/offline/environment.q2r2c2","label":"q2r2c2","oe":false,"qlabel":"q2","row":"r2","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2r2"},{"col":"c1","id":"scripts/test/surveys/offline/environment.q2r3c1","label":"q2r3c1","oe":false,"qlabel":"q2","row":"r3","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2r3"},{"col":"c2","id":"scripts/test/surveys/offline/environment.q2r3c2","label":"q2r3c2","oe":false,"qlabel":"q2","row":"r3","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2r3"},{"col":"c1","id":"scripts/test/surveys/offline/environment.q2r4c1","label":"q2r4c1","oe":false,"qlabel":"q2","row":"r4","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2r4"},{"col":"c2","id":"scripts/test/surveys/offline/environment.q2r4c2","label":"q2r4c2","oe":false,"qlabel":"q2","row":"r4","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2r4"},{"col":"c1","id":"scripts/test/surveys/offline/environment.q2r5c1","label":"q2r5c1","oe":false,"qlabel":"q2","row":"r5","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2r5"},{"col":"c2","id":"scripts/test/surveys/offline/environment.q2r5c2","label":"q2r5c2","oe":false,"qlabel":"q2","row":"r5","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2r5"},{"col":null,"id":"scripts/test/surveys/offline/environment.q5","label":"q5","oe":false,"qlabel":"q5","row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"q5"},{"col":"c1","id":"scripts/test/surveys/offline/environment.q4r1c1","label":"q4r1c1","oe":false,"qlabel":"q4","row":"r1","survey":"scripts/test/surveys/offline/environment","type":"number","vgroup":"q4r1"},{"col":"c2","id":"scripts/test/surveys/offline/environment.q4r1c2","label":"q4r1c2","oe":false,"qlabel":"q4","row":"r1","survey":"scripts/test/surveys/offline/environment","type":"number","vgroup":"q4r1"},{"col":"c3","id":"scripts/test/surveys/offline/environment.q4r1c3","label":"q4r1c3","oe":false,"qlabel":"q4","row":"r1","survey":"scripts/test/surveys/offline/environment","type":"number","vgroup":"q4r1"},{"col":"c1","id":"scripts/test/surveys/offline/environment.q4r2c1","label":"q4r2c1","oe":false,"qlabel":"q4","row":"r2","survey":"scripts/test/surveys/offline/environment","type":"number","vgroup":"q4r2"},{"col":"c2","id":"scripts/test/surveys/offline/environment.q4r2c2","label":"q4r2c2","oe":false,"qlabel":"q4","row":"r2","survey":"scripts/test/surveys/offline/environment","type":"number","vgroup":"q4r2"},{"col":"c3","id":"scripts/test/surveys/offline/environment.q4r2c3","label":"q4r2c3","oe":false,"qlabel":"q4","row":"r2","survey":"scripts/test/surveys/offline/environment","type":"number","vgroup":"q4r2"},{"col":null,"id":"scripts/test/surveys/offline/environment.source","label":"source","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"source"},{"col":null,"id":"scripts/test/surveys/offline/environment.decLang","label":"decLang","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"decLang"},{"col":null,"id":"scripts/test/surveys/offline/environment.start_date","label":"start_date","oe":false,"qlabel":"start_date","row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"start_date"},{"col":null,"id":"scripts/test/surveys/offline/environment.q2br1","label":"q2br1","oe":false,"qlabel":"q2b","row":"r1","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2b"},{"col":null,"id":"scripts/test/surveys/offline/environment.q2br2","label":"q2br2","oe":false,"qlabel":"q2b","row":"r2","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2b"},{"col":null,"id":"scripts/test/surveys/offline/environment.q2br3","label":"q2br3","oe":false,"qlabel":"q2b","row":"r3","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2b"},{"col":null,"id":"scripts/test/surveys/offline/environment.q2br4","label":"q2br4","oe":false,"qlabel":"q2b","row":"r4","survey":"scripts/test/surveys/offline/environment","type":"multiple","vgroup":"q2b"},{"col":null,"id":"scripts/test/surveys/offline/environment.date","label":"date","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"date"},{"col":null,"id":"scripts/test/surveys/offline/environment.markers","label":"markers","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"markers"},{"col":null,"id":"scripts/test/surveys/offline/environment.q4b","label":"q4b","oe":false,"qlabel":"q4b","row":null,"survey":"scripts/test/surveys/offline/environment","type":"float","vgroup":"q4b"},{"col":null,"id":"scripts/test/surveys/offline/environment.url","label":"url","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"url"},{"col":null,"id":"scripts/test/surveys/offline/environment.list","label":"list","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"list"},{"col":null,"id":"scripts/test/surveys/offline/environment.dcua","label":"dcua","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"dcua"},{"col":null,"id":"scripts/test/surveys/offline/environment.record","label":"record","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"record"},{"col":null,"id":"scripts/test/surveys/offline/environment.ipAddress","label":"ipAddress","oe":false,"qlabel":null,"row":null,"survey":"scripts/test/surveys/offline/environment","type":"text","vgroup":"ipAddress"}],"version":1}};exports['default'] = _lodashLodash['default'].values(surveys);});
define('offline/mirage/models/asset', ['exports', 'ember-cli-mirage'], function (exports, _emberCliMirage) {
    exports['default'] = _emberCliMirage.Model.extend({
        survey: (0, _emberCliMirage.belongsTo)()
    });
});
define('offline/mirage/models/element', ['exports', 'ember-cli-mirage'], function (exports, _emberCliMirage) {
    exports['default'] = _emberCliMirage.Model.extend({
        survey: (0, _emberCliMirage.belongsTo)(),
        elements: (0, _emberCliMirage.hasMany)()
    });
});
define('offline/mirage/models/exitpage', ['exports', 'ember-cli-mirage'], function (exports, _emberCliMirage) {
  exports['default'] = _emberCliMirage.Model.extend({});
});
define('offline/mirage/models/interviewer', ['exports', 'ember-cli-mirage'], function (exports, _emberCliMirage) {
  exports['default'] = _emberCliMirage.Model.extend({});
});
define('offline/mirage/models/message', ['exports', 'ember-cli-mirage'], function (exports, _emberCliMirage) {
    exports['default'] = _emberCliMirage.Model.extend({
        survey: (0, _emberCliMirage.belongsTo)()
    });
});
define('offline/mirage/models/respondent', ['exports', 'ember-cli-mirage'], function (exports, _emberCliMirage) {
  exports['default'] = _emberCliMirage.Model.extend({});
});
define('offline/mirage/models/survey', ['exports', 'ember-cli-mirage'], function (exports, _emberCliMirage) {
    exports['default'] = _emberCliMirage.Model.extend({
        assets: (0, _emberCliMirage.hasMany)('asset'),
        messages: (0, _emberCliMirage.hasMany)('message'),
        elements: (0, _emberCliMirage.hasMany)('element'),
        variables: (0, _emberCliMirage.hasMany)('variable'),
        exitpages: (0, _emberCliMirage.hasMany)('exitpage'),
        //practices:    hasMany('practice'),
        respondents: (0, _emberCliMirage.hasMany)('respondent'),
        interviewers: (0, _emberCliMirage.hasMany)('interviewer')
    });
});
define("offline/mirage/scenarios/default", ["exports"], function (exports) {
  exports["default"] = function () /* server */{

    /*
      Seed your development database using your factories.
      This data will not be loaded in your tests.
       Make sure to define a factory for each model you want to create.
    */

    // server.createList('post', 10);
  };
});
define('offline/mirage/serializers/application', ['exports', 'ember-cli-mirage'], function (exports, _emberCliMirage) {
  exports['default'] = _emberCliMirage.RestSerializer;
});
define('offline/mixins/question-input', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Mixin.create({
        open: false,
        row: null,
        col: null,
        question: null,
        variable: null,
        attributeBindings: ['name', 'value'],
        didReceiveAttrs: function didReceiveAttrs() {
            this._super.apply(this, arguments);

            var _getProperties = this.getProperties('question', 'row', 'col', 'open');

            var question = _getProperties.question;
            var row = _getProperties.row;
            var col = _getProperties.col;
            var open = _getProperties.open;

            // an openend text field will only be passed a row or col, not both
            if (Boolean(open)) {
                this.variable = question.oeVariableFor((row || col).data);
            } else {
                row = row ? row.data : null;
                col = col ? col.data : null;
                this.variable = question.variableFor(row, col);
            }
            this.set('name', this.variable.get('label'));
        }
    });
});
define('offline/models/asset', ['exports', 'ember-data'], function (exports, _emberData) {
    exports['default'] = _emberData['default'].Model.extend({
        version: _emberData['default'].attr('string'),
        media_type: _emberData['default'].attr('string'),
        survey: _emberData['default'].belongsTo('survey', { inverse: 'assets' })
    });
});
define('offline/models/block', ['exports', 'ember-data', 'offline/models/if', 'offline/utils/survey/preorder', 'lodash/lodash'], function (exports, _emberData, _offlineModelsIf, _offlineUtilsSurveyPreorder, _lodashLodash) {
    exports['default'] = _offlineModelsIf['default'].extend({
        randomizeChildren: _emberData['default'].attr('boolean', { defaultValue: false }),
        /**
         * TODO: This doesn't support dynamic reordering of things
         * @param {Array.<Number>|Undefined} order
         */
        shuffleItems: function shuffleItems() {
            if (!Boolean(this.get('randomizeChildren'))) {
                return this.get('elements');
            }
            return (0, _offlineUtilsSurveyPreorder.randomize)(this.get('elements').toArray());
        }
    });
});
define('offline/models/cell', ['exports', 'ember', 'ember-data', 'model-fragments', 'model-fragments/array/fragment'], function (exports, _ember, _emberData, _modelFragments, _modelFragmentsArrayFragment) {

    _modelFragmentsArrayFragment['default'].reopen({
        order: _ember['default'].computed('@each.orderedIndex', {
            get: function get(_) {
                return this.sortBy('orderedIndex');
            },
            set: function set(_, indices) {
                var last = indices.length;
                this.forEach(function (item, index) {
                    var orderedIndex = indices.indexOf(index);
                    if (orderedIndex === -1) {
                        orderedIndex = last++;
                    }
                    item.set('orderedIndex', orderedIndex);
                });
                return this.sortBy('orderedIndex');
            }
        })
    });

    exports['default'] = _modelFragments['default'].Fragment.extend({
        project: _ember['default'].inject.service('current-project'),
        alt: _emberData['default'].attr('string'),
        cdata: _emberData['default'].attr('string'),
        cond: _emberData['default'].attr('string'),
        index: _emberData['default'].attr('number'),
        label: _emberData['default'].attr('string'),
        open: _emberData['default'].attr('boolean'),
        openOptional: _emberData['default'].attr('boolean'),
        openSize: _emberData['default'].attr('number'),
        optional: _emberData['default'].attr('boolean'),
        py_cond: _emberData['default'].attr('string'),
        preText: _emberData['default'].attr('string'),
        postText: _emberData['default'].attr('string'),
        randomize: _emberData['default'].attr('boolean'),
        range: _emberData['default'].attr('string'),
        size: _emberData['default'].attr('number', { defaultValue: -1 }),
        type: _emberData['default'].attr('string'),
        verify: _emberData['default'].attr('string'),
        where: _emberData['default'].attr(),
        orderedIndex: _emberData['default'].attr('number'),
        parent: _emberData['default'].attr('string'), // model fragments don't support belongsTo / hasMany relationships
        environment: _ember['default'].computed.alias('project.ssession.environment'),
        disabled: _ember['default'].computed('environment.p._disabled', function () {
            var disabledCells = _ember['default'].get(this, 'environment.p._disabled.' + _ember['default'].get(this, 'parent')) || {};
            return !!disabledCells[_ember['default'].get(this, 'label')];
        }).readOnly(),
        /**
         * This is the source of truth for whether the cell should be (and was) displayed or not
         */
        displayed: _ember['default'].computed('disabled', function () {
            if (this.get('disabled')) {
                return false;
            }
            var _data = this.data;
            var label = _data.label;
            var where = _data.where;
            var cond = _data.cond;
            var type = _data.type;

            cond = cond || this.get(type + 'Cond');
            if (label && where.survey) {
                if (cond) {
                    var reference = {};
                    reference[type] = this.data;
                    return this.get('environment').evaluate(cond, reference);
                }
                return true;
            }
            return false;
        }).readOnly(),
        displayCdata: _ember['default'].computed('cdata', function () {
            return this.get('project.ssession').replaceVariables(this.get('cdata'));
        }).readOnly()
    });
});
define('offline/models/checkbox', ['exports', 'ember', 'ember-data', 'offline/models/question'], function (exports, _ember, _emberData, _offlineModelsQuestion) {

    // constants we may need to change
    var NONE = null;
    var TRUE = true;
    var FALSE = false;

    function form(count) {
        return count === 1 ? 'sing' : 'plur';
    }

    exports['default'] = _offlineModelsQuestion['default'].extend({
        atleast: _emberData['default'].attr('number', { defaultValue: 0 }),
        atmost: _emberData['default'].attr('number', { defaultValue: Infinity }),
        exactly: _emberData['default'].attr('number', { defaultValue: 0 }),

        /**
         * @description
         * Provide a list of text, chaining together the text for each selected row or col
         * @returns {{valid: Boolean, text: String}}
         */
        pipeText: function pipeText() {
            var _getProperties = this.getProperties('rows', 'cols', 'grouping');

            var rows = _getProperties.rows;
            var cols = _getProperties.cols;
            var grouping = _getProperties.grouping;

            if (rows.get('length') > 1 && cols.get('length') > 1) {
                return { valid: false, text: 'too many dimensions' };
            }
            var values = this.dataFor();
            var valueaxis = grouping === 'rows' ? cols : rows;
            var strings = [];
            valueaxis.forEach(function (item) {
                if (values[item.get('index')]) {
                    strings.push(item.get('cdata'));
                }
            });
            return { valid: true, text: this.andJoin(strings) };
        },

        /**
         * Autopopulate this question using the "autofill" attribute
         * Only rows in Checkbox elements support this attribute.
         * The evaluation is done sequentially (i.e., from the top row to the bottom row)
         */
        executeAutofill: function executeAutofill() {
            var _this = this;

            var hasAutofill = false;
            var deferred = [];
            this.get('rows').forEach(function (row) {
                var autofill = row.getWithDefault('autofill', null);
                if (autofill) {
                    hasAutofill = true;
                }
                deferred.push({
                    variable: _this.variableFor(row.data),
                    condition: autofill
                });
            });
            if (hasAutofill) {
                deferred.forEach(function (item) {
                    var variable = item.variable;
                    var condition = item.condition;

                    // save this data to the survey state to be used for logic
                    // e.g., the next row autopopulates only if this row is TRUE
                    if (_this.get('environment').evaluate(condition)) {
                        variable.set('value', TRUE);
                    } else {
                        variable.set('value', NONE);
                    }
                });
            }
        },

        /**
         * @description
         * Attempt to auto-populate this question element (after stripping out hidden rows / cols)
         * If atleast=N or exactly=N, and N or less row / cols, then autopopulate all.
         * @returns {boolean}
         */
        tryAutoPopulate: function tryAutoPopulate() {
            var inferred = {};

            var _get = this.get('enabled');

            var rows = _get.rows;
            var cols = _get.cols;

            var _getProperties2 = this.getProperties('atleast', 'atmost', 'exactly');

            var atleast = _getProperties2.atleast;
            var atmost = _getProperties2.atmost;
            var exactly = _getProperties2.exactly;

            var values = this.get('grouping') === 'rows' ? cols : rows;
            if (values.length <= atleast || values.length <= exactly) {
                for (var r = 0; r < rows.length; r++) {
                    for (var c = 0; c < cols.length; c++) {
                        this.variableFor(rows.objectAt(r).data, cols.objectAt(c).data).set('value', TRUE);
                    }
                }
                return true;
            }
            return false;
        },

        /**
         * Hold onto the results of validating our variables.
         * For example, if we're grouped by rows, we shouldn't need to check
         * if more than one column was selected for every row.
         * Returns a fresh cache when some value changes
         */
        _errorsForGroup: _ember['default'].computed('variables.@each.value', function () {
            return {/* r1: [], r2: ['Please select at least one'] */};
        }),

        /**
         * Verify that the number of selected cells within a grouped set is within
         * the range defined by atleast, atmost, and exactly
         * @param {DS.Model} variable
         * @returns {Array}
         */
        verifyCell: function verifyCell(variable) {
            var _this2 = this;

            // only verify against the visible rows and cols
            var enabled = this.get('enabled');

            var _getProperties3 = this.getProperties('atleast', 'atmost', 'exactly');

            var atleast = _getProperties3.atleast;
            var atmost = _getProperties3.atmost;
            var exactly = _getProperties3.exactly;

            var values,
                group,
                grouping = this.get('grouping');
            if (grouping === 'rows') {
                values = enabled.cols;
                group = { type: 'row', label: variable.get('row') };
            } else {
                values = enabled.rows;
                group = { type: 'col', label: variable.get('col') };
            }

            // skip if this variable isn't displayed (i.e., it belongs to a real [non-transient] row or col that's hidden)
            if (group.label && !enabled[grouping].findBy('label', group.label)) {
                return [];
            }

            var errors = this.get('_errorsForGroup')[group.label];
            if (errors) {
                return errors;
            }
            errors = this.get('_errorsForGroup')[group.label] = [];
            // all inputs in a group will be in the same state ('valid' or 'invalid')
            // the check is done once and cached to be used by the other cells in the group
            // first, are any noanswers selected?
            var counter = 0;
            values.some(function (value) {
                counter = counter + Number(_this2.variableFor(group, value.data).get('value') === TRUE);
                if (counter > atmost || exactly > 0 && counter > exactly) {
                    return true;
                }
            });

            var where = grouping === 'rows' ? 'row' : 'column';
            if (!(atleast < 1 || counter >= atleast)) {
                errors.push(this.lget('check-error-atLeast-' + form(atleast) + '-' + where, { count: atleast, actual: counter }));
            }
            if (!(exactly < 1 || counter === exactly)) {
                errors.push(this.lget('check-error-exactly-' + form(exactly) + '-' + where, { count: exactly, actual: counter }));
            }
            if (!(atmost < 1 || counter <= atmost)) {
                errors.push(this.lget('check-error-atMost-' + form(atmost) + '-' + where, { count: atmost, actual: counter }));
            }
            return errors;
        },

        /**
         * Determine if the value is truthy.
         * @param value
         * @returns {boolean}
         */
        isValue: function isValue(value) {
            return value !== NONE && value !== FALSE;
        },

        /**
         * The atmost, atleast, exactly attributes can be ignored with optional="1"
         * @returns {boolean}
         */
        isMandatory: function isMandatory(variable) {
            var _getProperties4 = this.getProperties('grouping', 'optional');

            var grouping = _getProperties4.grouping;
            var optional = _getProperties4.optional;

            return !this.get(grouping).findBy('label', variable.get(grouping === 'rows' ? 'row' : 'col')).get('optional') && !optional;
        }

    });
    exports.TRUE = TRUE;
    exports.FALSE = FALSE;
    exports.NONE = NONE;
});
/* global andJoin */
define('offline/models/element', ['exports', 'ember', 'ember-data'], function (exports, _ember, _emberData) {

    /**
     * @name ElementModel
     * @extends {DS.Model}
     */
    var ElementModel = _emberData['default'].Model.extend({
        project: _ember['default'].inject.service('current-project'),
        environment: _ember['default'].computed.alias('project.ssession.environment'),
        uid: _emberData['default'].attr('number'),
        type: _emberData['default'].attr('string', { defaultValue: function defaultValue(record) {
                return record._internalModel.modelName;
            } }),
        style: _emberData['default'].attr('string'),
        cond: _emberData['default'].attr('string'),
        py_cond: _emberData['default'].attr('string'),
        where: _emberData['default'].attr({ defaultValue: function defaultValue() {
                return { survey: false, execute: false };
            } }),
        topLabel: _emberData['default'].attr('boolean', { defaultValue: false }),
        randomize: _emberData['default'].attr('boolean'),
        orderedIndex: _emberData['default'].attr('number'),
        // our relations
        survey: _emberData['default'].belongsTo('survey', { async: false }),
        parent: _emberData['default'].belongsTo('element', { inverse: 'elements', polymorphic: true, async: false }),
        elements: _emberData['default'].hasMany('element', { inverse: 'parent', polymorphic: true, async: false }),
        nextElement: _emberData['default'].belongsTo('element', { inverse: 'prevElement', polymorphic: true, async: false }),
        prevElement: _emberData['default'].belongsTo('element', { inverse: 'nextElement', polymorphic: true, async: false }),

        disabled: _ember['default'].computed('parent.order', 'environment.ctx.p._disabled', {
            get: function get(_) {
                var parent = this.get('parent');
                if (parent && parent.getWithDefault('elements', []).indexOf(this) >= parent.get('count')) {
                    return true;
                }
                return this.getWithDefault('environment.ctx.p._disabled.' + this.uid, false);
            },
            set: function set(_, value) {
                return this.set('environment.ctx.p._disabled.' + this.uid, value);
            }
        }),
        isHiddenQuestion: _ember['default'].computed('style', 'where.execute', function () {
            return this.get('style') === 'dev' || this.get('where.execute');
        }),
        displayTitle: _ember['default'].computed('title', function () {
            return this.get('project.ssession').replaceVariables(this.get('title'));
        }),
        displayComment: _ember['default'].computed('comment', function () {
            return this.get('project.ssession').replaceVariables(this.get('comment'));
        }),
        displayCdata: _ember['default'].computed('cdata', function () {
            return this.get('project.ssession').replaceVariables(this.get('cdata'));
        }),

        /**
         * Forcibly clear some computed properties displayed on the view.
         * These may have dependencies which cannot be defined statically,
         * e.g., the title "Was your selection '[pipe: q1]' correct?"
         */
        render: function render() {
            this.notifyPropertyChange('displayTitle');
            this.notifyPropertyChange('displayComment');
            this.notifyPropertyChange('displayCdata');
        },

        /**
         * This question / element is still considered as part of the survey.
         * i.e., their data is submitted upon survey completion, and they can be referenced by other questions
         * @returns {Boolean}
         */
        isDisplayableInSurvey: function isDisplayableInSurvey() {
            var state = this.get('project.survey.state');
            if (this.get('style') === 'dev' && !(state === 'dev' || state === 'testing')) {
                return false;
            }
            return this.get('where.survey') && this.conditionIsTrue();
        },
        /**
         * Check if this question is conditionally shown and right now is false
         */
        conditionIsTrue: function conditionIsTrue() {
            return !this.get('disabled') && (!this.get('cond') || !!this.get('environment').evaluate(this.get('cond')));
        },
        /**
         * By default, all elements are displayed. If it shouldn't be displayed,
         * then it should implement this method itself.
         * @returns {Boolean|Object}
         */
        display: function display() {
            return true;
        },
        /**
         * Return the next element that could be displayed
         * @returns {Element|null}
         */
        getNextElement: function getNextElement() {
            var _getProperties = this.getProperties('nextElement', 'parent');

            var nextElement = _getProperties.nextElement;
            var parent = _getProperties.parent;

            if (nextElement) {
                return nextElement;
            }
            if (parent) {
                return parent.getNextElement();
            }
            return null;
        }
    });

    /**
     * Our application's store will look for this attribute if it cannot find the
     * adapter or serializer explictly defined for a model.
     * This allows all models that inherit from ElementModel to fallback to
     * adapter:element or serializer:element
     */
    ElementModel.reopenClass({
        fallbackChain: ['element']
    });

    exports['default'] = ElementModel;
});
define('offline/models/exec', ['exports', 'ember-data', 'offline/models/element'], function (exports, _emberData, _offlineModelsElement) {

    // TODO: extend from a control element?
    exports['default'] = _offlineModelsElement['default'].extend({
        cdata: _emberData['default'].attr('string'),
        when: _emberData['default'].attr('set', {
            defaultValue: {
                survey: true,
                virtualInit: false,
                finished: false,
                finishedPost: false,
                init: false,
                sqlTransfer: false,
                sqlTransferInit: false,
                flow: false,
                load: false,
                postKeywordCoder: false,
                started: false,
                returning: false,
                verified: false,
                virtual: false,
                submit: false
            }
        })
    });
});
define('offline/models/exit', ['exports', 'ember-data', 'offline/models/element'], function (exports, _emberData, _offlineModelsElement) {
    exports['default'] = _offlineModelsElement['default'].extend({
        cdata: _emberData['default'].attr('string')
    });
});
define('offline/models/exitpage', ['exports', 'ember-data'], function (exports, _emberData) {
    exports['default'] = _emberData['default'].Model.extend({
        survey: _emberData['default'].belongsTo('survey')
    });
});
define('offline/models/finish', ['exports', 'ember-data', 'offline/models/element', 'offline/utils/survey/session'], function (exports, _emberData, _offlineModelsElement, _offlineUtilsSurveySession) {
    exports['default'] = _offlineModelsElement['default'].extend({
        now: _emberData['default'].attr('boolean', { defaultValue: false }),
        getNextElement: function getNextElement() {
            return null;
        },
        display: function display() {
            if (this.get('now')) {
                this.get('project.ssession').finish();
                return _offlineUtilsSurveySession.HALT;
            }
            return _offlineUtilsSurveySession.SKIP;
        }
    });
});
define('offline/models/float', ['exports', 'ember-data', 'offline/models/number'], function (exports, _emberData, _offlineModelsNumber) {

    var VERIFY_RX = /(.+),(.+)/;

    exports['default'] = _offlineModelsNumber['default'].extend({

        size: _emberData['default'].attr('number', { defaultValue: 6 }),

        convertDataType: function convertDataType(value) {
            return value === null ? null : parseFloat(value);
        },

        /**
         * Parse the <float range="x,y"> value
         * @returns {{min: Number, max: Number}}
         */
        getRange: function getRange() {
            if (this._range) {
                return this._range;
            }
            if (VERIFY_RX.test(this.get('range'))) {
                return this._range = {
                    min: (0, _offlineModelsNumber['default'])(this.get('range').split(',')[0]),
                    max: (0, _offlineModelsNumber['default'])(this.get('range').split(',')[1])
                };
            }
            return this._range = { min: 0, max: Infinity };
        },

        /**
         * Verify that the answer provided can be cast to a number
         * @param {DS.Model} variable
         * @returns {Array.<String>}
         */
        verifyCell: function verifyCell(variable) {
            var errors = [];
            var answer = variable.get('value');
            if (answer === null || isNaN(this.convertDataType(answer))) {
                // if the answer is truthy, then it's just invalid, otherwise prompt for answer needed
                errors.push(this.lget(answer ? 'notValidDecimalNumber' : 'noAnswerSelected'));
            }
            return errors;
        }
    });
});
define('offline/models/goto', ['exports', 'ember-data', 'offline/models/element', 'offline/utils/survey/session'], function (exports, _emberData, _offlineModelsElement, _offlineUtilsSurveySession) {
    exports['default'] = _offlineModelsElement['default'].extend({
        target: _emberData['default'].attr('string'),

        display: function display() {
            this.get('project.ssession').gotoTarget(this.get('target'));
            return _offlineUtilsSurveySession.HALT;
        }
    });
});
define('offline/models/hash', ['exports', 'ember-data'], function (exports, _emberData) {

  /**
   * Observers and computed properties on relations are odd,
   * so just store the survey hash info here and manually diff.
   */
  exports['default'] = _emberData['default'].Model.extend({
    value: _emberData['default'].attr('string'),
    timestamp: _emberData['default'].attr('date', { defaultValue: function defaultValue() {
        return new Date();
      } }),
    survey: _emberData['default'].belongsTo('survey')
  });
});
define('offline/models/html', ['exports', 'ember-data', 'offline/models/element', 'offline/utils/survey/session'], function (exports, _emberData, _offlineModelsElement, _offlineUtilsSurveySession) {
    exports['default'] = _offlineModelsElement['default'].extend({
        label: _emberData['default'].attr('string'),
        cdata: _emberData['default'].attr('string'),
        final: _emberData['default'].attr('boolean'),

        display: function display() {
            if (this.conditionIsTrue()) {
                if (this.get('final')) {
                    return _offlineUtilsSurveySession.HALT; // TODO
                }
                return true;
            }
            return false;
        },
        getContents: function getContents() {
            return this.get('cdata');
        }
    });
});
define('offline/models/if', ['exports', 'ember-data', 'offline/models/element', 'offline/utils/survey/session'], function (exports, _emberData, _offlineModelsElement, _offlineUtilsSurveySession) {
    exports['default'] = _offlineModelsElement['default'].extend({
        cond: _emberData['default'].attr('string'),
        label: _emberData['default'].attr('string'),
        count: _emberData['default'].attr('number'),
        /**
         * Always true so we can defer the display of children to display()
         * @returns {Boolean}
         */
        conditionIsTrue: function conditionIsTrue() {
            var parent = arguments.length <= 0 || arguments[0] === undefined ? false : arguments[0];

            if (parent) {
                return this._super();
            }
            return true;
        },
        /**
         * If the condition is true, then display children. Skip to sibling otherwise.
         * @returns {Boolean|Object}
         */
        display: function display() {
            // call Element.conditionIsTrue()
            if (!this.conditionIsTrue(true)) {
                return _offlineUtilsSurveySession.SKIP;
            }
            return false;
        }
    });
});
define('offline/models/image', ['exports', 'ember-data', 'offline/models/text'], function (exports, _emberData, _offlineModelsText) {
  exports['default'] = _offlineModelsText['default'].extend({});
});
define('offline/models/interviewer', ['exports', 'ember-data'], function (exports, _emberData) {
    exports['default'] = _emberData['default'].Model.extend({
        interviewer_id: _emberData['default'].attr('string'),
        description: _emberData['default'].attr('string', { defaultValue: '' }),
        active: _emberData['default'].attr('boolean', { defaultValue: false }),
        has_seen: _emberData['default'].attr('boolean', { defaultValue: true }),
        total_completes: _emberData['default'].attr('number', { defaultValue: 0 }),
        valid_completes: _emberData['default'].attr('number', { defaultValue: 0 }),
        last_respondent_upload_on: _emberData['default'].attr('date'),
        survey: _emberData['default'].belongsTo('survey')
    });
});
define('offline/models/label', ['exports', 'ember-data', 'offline/models/element'], function (exports, _emberData, _offlineModelsElement) {
    exports['default'] = _offlineModelsElement['default'].extend({
        label: _emberData['default'].attr('string'),
        display: function display() {
            return false;
        }
    });
});
define('offline/models/marker', ['exports', 'ember-data', 'offline/utils/survey/session'], function (exports, _emberData, _offlineUtilsSurveySession) {
    exports['default'] = _emberData['default'].Model.extend({
        name: _emberData['default'].attr('string'),
        // Display is called only if this.isDisplayableInSurvey() evaulates to true.
        // During that call the condition, if any, is evaluated on the marker.
        display: function display() {
            this.get('environment').ctx.setMarker(this.get('name'));
            return _offlineUtilsSurveySession.SKIP;
        }
    });
});
define('offline/models/message', ['exports', 'ember-data'], function (exports, _emberData) {
    exports['default'] = _emberData['default'].Model.extend({
        name: _emberData['default'].attr('string'),
        text: _emberData['default'].attr('string'),
        lang: _emberData['default'].attr('string'), // TODO: belongsTo('language')?
        survey: _emberData['default'].belongsTo('survey', { inverse: 'messages' })
    });
});
define('offline/models/number', ['exports', 'ember-data', 'offline/models/question', 'offline/utils/survey/logic', 'lodash/lodash'], function (exports, _emberData, _offlineModelsQuestion, _offlineUtilsSurveyLogic, _lodashLodash) {

    var VERIFY_RX = /range\(([0-9]+),([0-9]+)\)/;

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger#Polyfill
    //noinspection JSPrimitiveTypeWrapperUsage
    Number.isInteger = Number.isInteger || function isInteger(value) {
        return typeof value === "number" && isFinite(value) && Math.floor(value) === value;
    };

    exports['default'] = _offlineModelsQuestion['default'].extend({

        size: _emberData['default'].attr('number', { defaultValue: 0 }),
        range: _emberData['default'].attr('string'),
        optional: _emberData['default'].attr('boolean', { defaultValue: true }),

        convertDataType: function convertDataType(value) {
            return value === null ? null : parseInt(value);
        },

        /**
         * @description
         * Cast values to the configured dataType (integer or float)
         * @param row
         * @param col
         * @returns {Number|Array}
         */
        dataFor: function dataFor(row, col) {
            var _this = this;

            var data = this._super(row, col);
            if (_lodashLodash['default'].isArray(data)) {
                return _lodashLodash['default'].map(data, function (value) {
                    return _this.convertDataType(value);
                });
            }
            return this.convertDataType(data);
        },

        /**
         * @description
         * Provide the value given for this Number question with a single data-point
         * @returns {{valid: Boolean, text: String}}
         */
        pipeText: function pipeText() {
            if (this.get('variables.length') > 1) {
                return { valid: false, text: 'too many dimensions' };
            }
            var value = this.dataFor();
            if (!_lodashLodash['default'].isNumber(value)) {
                return { valid: true, text: '' };
            }
            return { valid: true, text: value };
        },

        /**
         * @description
         * Attempt to auto-populate this question element (after stripping out hidden rows / cols)
         */
        tryAutoPopulate: function tryAutoPopulate() {
            var _get = this.get('enabled');

            var rows = _get.rows;
            var cols = _get.cols;

            var values = this.get('grouping') === 'rows' ? cols : rows;
            if (this.get('points') && values.length === 1) {
                for (var r = 0; r < rows.length; r++) {
                    for (var c = 0; c < cols.length; c++) {
                        this.variableFor(rows[r], cols[c]).set('value', parseInt(this.get('points')));
                    }
                }
                return true;
            }
            return false;
        },

        /**
         * Verify that the answer provided can be cast to a number
         * The answer is invalid ONLY IF it is a number
         * @param {DS.Model} variable
         * @returns {Array.<String>}
         */
        verifyCell: function verifyCell(variable) {
            var errors = [];
            var range = this.getRange();
            var answer = variable.get('value');
            if (answer === null || !Number.isInteger(answer) || isNaN(this.convertDataType(answer))) {
                // Number elements prioritize the Range verifier,
                // which checks for an answer and displays "notWhole" if none
                errors.push(this.lget(answer ? 'notWholeNumber' : 'notWhole'));
            }
            if (_lodashLodash['default'].isNumber(answer)) {
                if (answer < range.min) {
                    errors.push(this.lget('valueAtLeast', { min: range.min }));
                }
                if (answer > range.max) {
                    errors.push(this.lget('valueAtMost', { max: range.max }));
                }
            }
            return errors;
        },

        /**
         * @description
         * The value must be a number or null
         * @params value
         * @returns {null|String}
         */
        validateValue: function validateValue(value) {
            if (value === null) {
                return null;
            }
            if (!_lodashLodash['default'].isNumber(value)) {
                throw new _offlineUtilsSurveyLogic.RuntimeError('Value must be a number, not ' + typeof value);
            }
            return value;
        },

        /**
         * Parse the <number verify="range(x,y)"> value
         * @returns {{min: Number, max: Number}}
         */
        getRange: function getRange() {
            if (this._range) {
                return this._range;
            }
            if (VERIFY_RX.test(this.get('verify'))) {
                return this._range = {
                    min: Number(VERIFY_RX.exec(this.get('verify'))[1]),
                    max: Number(VERIFY_RX.exec(this.get('verify'))[2])
                };
            }
            return this._range = { min: 0, max: Infinity };
        },

        /**
         * Determine if the value is a number.
         * @param value
         * @returns {boolean}
         */
        isValue: function isValue(value) {
            return _lodashLodash['default'].isNumber(value);
        }
    });
});
define('offline/models/practice', ['exports', 'ember-data'], function (exports, _emberData) {
    exports['default'] = _emberData['default'].Model.extend({
        hash: _emberData['default'].attr('string'), // the survey hash at the time of testing
        survey: _emberData['default'].belongsTo('survey', { inverse: 'practices' }),
        timestamp: _emberData['default'].attr('date', { defaultValue: function defaultValue() {
                return new Date();
            } })
    });
});
define('offline/models/question', ['exports', 'ember', 'ember-data', 'offline/models/element', 'offline/utils/survey/preorder', 'model-fragments/attributes', 'lodash/lodash'], function (exports, _ember, _emberData, _offlineModelsElement, _offlineUtilsSurveyPreorder, _modelFragmentsAttributes, _lodashLodash) {

    var QuestionModel = _offlineModelsElement['default'].extend({
        label: _emberData['default'].attr('string'),
        title: _emberData['default'].attr('string'),
        comment: _emberData['default'].attr('string'),
        exec: _emberData['default'].attr('string'),
        optional: _emberData['default'].attr('boolean'),
        grouping: _emberData['default'].attr('string'),
        shuffle: _emberData['default'].attr({ defaultValue: function defaultValue() {
                return { rows: false, cols: false, choices: false };
            } }),
        shuffleBy: _emberData['default'].attr('string'),
        uses: _emberData['default'].attr('slist'),
        styleAttributes: _emberData['default'].attr({ defaultValue: function defaultValue() {
                return {};
            } }),
        rows: (0, _modelFragmentsAttributes.fragmentArray)('cell'),
        cols: (0, _modelFragmentsAttributes.fragmentArray)('cell'),
        choices: (0, _modelFragmentsAttributes.fragmentArray)('cell'),
        noanswer: (0, _modelFragmentsAttributes.fragmentArray)('cell'),
        variables: _emberData['default'].hasMany('variable', { inverse: 'question', polymorphic: false, async: false }),
        /**
         * A table to lookup the variable for each cell (for performance reasons)
         */
        variableTable: _ember['default'].computed('variables.[]', function () {
            return this.get('variables').reduce(function (t, v) {
                var _v$data = v.data;
                var row = _v$data.row;
                var col = _v$data.col;
                var oe = _v$data.oe;

                if (oe) {
                    return t;
                }
                t[row] = t[row] || {};
                t[row][col] = v;
                return t;
            }, {});
        }),
        oeVariableTable: _ember['default'].computed('variables.[]', function () {
            return this.get('variables').reduce(function (t, v) {
                var _v$data2 = v.data;
                var row = _v$data2.row;
                var col = _v$data2.col;
                var oe = _v$data2.oe;

                if (!oe) {
                    return t;
                }
                t[row || col] = v;
                return t;
            }, {});
        }),

        enabled: _ember['default'].computed('rows.@each.displayed', 'cols.@each.displayed', 'choices.@each.displayed', function () {
            return {
                rows: this.get('rows').filter(function (row) {
                    return row.get('displayed');
                }),
                cols: this.get('cols').filter(function (col) {
                    return col.get('displayed');
                }),
                choices: this.get('choices').filter(function (choice) {
                    return choice.get('displayed');
                })
            };
        }),

        /**
         * Shuffle the displayed order of a list of cells using an ordered list
         * where each item is the cell or the cell's index
         * @param { 'rows', 'cols', 'choices' } listName
         * @param {Array.<Number>|Array.<DS.Model>|Undefined} order
         */
        shuffleItems: function shuffleItems(listName, order) {
            var _getProperties = this.getProperties('shuffle', 'shuffleBy');

            var shuffle = _getProperties.shuffle;
            var shuffleBy = _getProperties.shuffleBy;

            if (!shuffle[listName]) {
                throw new Error('Being asked to reorder something where shuffle= is not set!');
            }
            var itemList = this.get(listName);
            if (!_lodashLodash['default'].isArray(order)) {
                if (shuffleBy) {
                    order = [];
                    // TODO: shuffleBy may not have been shuffled yet
                    this.get('survey').findElement(shuffleBy).get(listName).forEach(function (item, index) {
                        return order[item.get('orderedIndex')] = index;
                    });
                } else {
                    order = (0, _offlineUtilsSurveyPreorder.randomize)(itemList.toArray());
                }
            }
            if (order.length && !_lodashLodash['default'].isNumber(order[0])) {
                order = order.map(function (item) {
                    return itemList.indexOf(item);
                });
            }
            itemList.set('order', order);
        },

        /**
         * Get some resource text
         * @param [name] The name of the resource (e.g., <res label="NAME"/>)
         * @param [args] What should the args expand to (e.g., $(some_arg))
         * @returns {String}
         */
        lget: function lget(name, args) {
            return this.get('project.ssession').getResource(name, this.get('label'), args);
        },

        andJoin: function andJoin(strings) {
            var last = strings.pop(),
                text = strings.join(', ');
            if (!text) {
                return last;
            }
            if (strings.length > 1) {
                return text + ', and ' + last;
            }
            return text + ' and ' + last;
        },

        /**
         * Return some value for some (row, col, choice) coordinate
         * - Boolean if number of cells matches the dimensions of the question
         * - Number if number of cells is one less than the dimensions
         * - List if number of cells is two or more less
         * This function assumes every cell has a unique data point
         * i.e., do not pass this transient cells
         * @param {{ label, index }} row
         * @param {{ label, index }} col
         * @param {{ label, index }} choice
         * @returns {Boolean|Number|Array}
         */
        dataFor: function dataFor(row, col, choice) {
            var _data = this.data;
            var rows = _data.rows;
            var cols = _data.cols;

            var data = [];
            if (row && col) {
                data = this.variableFor(row, col).get('value');
            } else {
                var variableTable = this.get('variableTable');
                if (col) {
                    rows.forEach(function (r) {
                        data[r.get('index')] = variableTable[r.get('label')][col.label].get('value');
                    });
                } else if (row) {
                    cols.forEach(function (c) {
                        data[c.get('index')] = variableTable[row.label][c.get('label')].get('value');
                    });
                } else {
                    cols.forEach(function (c) {
                        var _c$data = c.data;
                        var index = _c$data.index;
                        var label = _c$data.label;

                        data[index] = [];
                        rows.forEach(function (r) {
                            data[index][r.get('index')] = variableTable[r.get('label')][label].get('value');
                        });
                    });
                    // ensure the value we return has no more dimensions than the question
                    // e.g., return a string for a text question with no rows or cols
                    if (rows.get('length') < 2) {
                        data = data.map(function (rowvalues) {
                            return rowvalues[0];
                        });
                    }
                    if (cols.get('length') < 2) {
                        data = data[0];
                    }
                }
            }
            // if we have a single transient row / col, the value for that row / col is the value we return
            if (col && !rows.objectAt(0).get('label') || row && !cols.objectAt(0).get('label')) {
                data = data[0];
            }
            if (choice) {
                return choice.index === data;
            }
            return data;
        },

        /**
         * Return a list of values for the question or one of it's rows / cols
         * @param {{ label, index }} row
         * @param {{ label, index }} col
         * @param {{ label, index }} choice
         * @returns {Array}
         */
        valuesFor: function valuesFor(row, col, choice) {
            var values = [];
            var cols = this.get('cols').toArray().filter(function (item) {
                return !col || col.label === item.get('label');
            });
            var rows = this.get('rows').toArray().filter(function (item) {
                return !row || row.label === item.get('label');
            });
            for (var ci = 0; ci < cols.length; ci++) {
                for (var ri = 0; ri < rows.length; ri++) {
                    var value = this.variableFor(rows[ri].data, cols[ci].data).get('value');
                    if (choice) {
                        values.push(choice.index === value);
                    } else {
                        values.push(value);
                    }
                }
            }
            return values;
        },

        /**
         * Return the expanded title
         */
        getTitle: function getTitle() {
            return this.get('project.ssession').replaceVariables(this.title);
        },

        /**
         * Provide the cdata for the selected cell (implemented on every question type)
         * @returns {{valid: Boolean, text: String}}
         */
        pipeText: function pipeText() {
            _ember['default'].Logger.warn('Unimplemented "pipeText" called on a "' + this.get('type') + '" question', arguments);
        },

        /**
         * Attempt to auto-populate this question (implemented on every question type)
         * @returns {Boolean}
         */
        tryAutoPopulate: function tryAutoPopulate() {
            _ember['default'].Logger.warn('Unimplemented "tryAutoPopulate" called on a "' + this.get('type') + '" question', arguments);
        },

        /**
         * This is implemented for the Radio and Select questions only
         * @param row
         * @param col
         */
        selected: function selected(row, col) {
            _ember['default'].Logger.warn('Unimplemented "selected" called on a "' + this.get('type') + '" question', arguments);
        },

        /**
         * This is implemented separately for each question-type
         * @param row
         * @param col
         * @param choice
         */
        valueFor: function valueFor(row, col, choice) {
            _ember['default'].Logger.warn('Unimplemented "valueFor" called on a "' + this.get('type') + '" question', arguments);
        },

        /**
         * @description
         * Get the data-label pertaining to a cell at the intersection of (elA, elB)
         * e.g., variableFor(q1.r1, q1.c2) => 'q1r1c2'
         * This is implemented separately for Radios
         * @param row
         * @param col
         */
        variableFor: function variableFor(row, col) {
            var labels = { row: null, col: null };
            if (row) {
                labels[row.type] = row.label;
            }
            if (col) {
                labels[col.type] = col.label;
            }
            return this.get('variableTable')[labels.row][labels.col];
        },

        /**
         * Get the label for this cell's OE data
         * @param {{open, type, label}} cell
         * @returns {DS.Model}
         */
        oeVariableFor: function oeVariableFor(cell) {
            if (cell.open) {
                return this.get('oeVariableTable')[cell.label];
            }
        },

        /**
         * Get the label for the data-point of this <noanswer> row
         * @param {{type, label}} row
         * @returns {DS.Model}
         */
        naVariableFor: function naVariableFor(row) {
            if (row.type !== 'noanswer') {
                _ember['default'].Logger.warn('A noanswer must be of type "noanswer"');
            }
            var qLabel = this.get('label');
            return this.get('survey.elements').findBy('label', 'noanswer').get('variables').find(function (variable) {
                // noanswers can only be rows
                return variable.get('noanswerParent') === qLabel && variable.get('row') === row.label;
            });
        },

        /**
         * Retrieve the text for the cell selected (see pipeText for Radio and Select)
         * @returns {String}
         */
        getSelectedText: function getSelectedText() {
            var selected = this.selected();
            if (selected) {
                if (selected.get('open')) {
                    // return the response rather than the row / col text
                    return this.oeVariableFor(selected.data).get('value');
                }
                return selected.get('cdata');
            }
            return undefined;
        },

        isOEValue: function isOEValue(value) {
            if (!this._isOEValue) {
                this._isOEValue = this.store.modelFor('text').prototype.isValue.bind(this);
            }
            return this._isOEValue(value);
        },

        isNAValue: function isNAValue(value) {
            if (!this._isNAValue) {
                this._isNAValue = this.store.modelFor('checkbox').prototype.isValue.bind(this);
            }
            return this._isNAValue(value);
        },

        /**
         * Cache for noanswer variables associated with this question
         * @type {Array.<DS.Model>}
         */
        naVariables: _ember['default'].computed('survey.variables.@each.noanswerParent', function () {
            var label = this.get('label');
            // noanswer variables are not part of this question, they belongTo the noanswer checkbox
            return this.get('survey.variables').filter(function (v) {
                return v.get('noanswerParent') === label;
            });
        }).readOnly(),

        /**
         * Cache for at least one noanswer row being selected
         * @type {Boolean}
         */
        naSelected: _ember['default'].computed('naVariables.@each.value', function () {
            var _this = this;

            return this.get('naVariables').any(function (v) {
                return _this.isNAValue(v.get('value'));
            });
        }).readOnly(),

        verifyInput: function verifyInput(variable) {
            if (variable.get('oe')) {
                return this.verifyOECell(variable);
            }
            if (this.get('naSelected')) {
                return false;
            }
            if (this.isMandatory(variable)) {
                return this.verifyCell(variable);
            }
        },

        /**
         * Verify that the cell belongs to a set with at least one selected cell
         * @param {DS.Model} variable
         * @returns {Array}
         */
        verifyCell: function verifyCell(variable) {
            if (!this.isValue(variable.get('value'))) {
                return [this.lget('noAnswerSelected')];
            }
        },

        verifyOECell: function verifyOECell(variable) {
            var _this2 = this;

            var _variable$data = variable.data;
            var row = _variable$data.row;
            var col = _variable$data.col;

            row = row && this.get('rows').findBy('label', row).data || null;
            col = col && this.get('cols').findBy('label', col).data || null;

            var data = this.dataFor(row, col);
            // use the text model class to validate our input
            var hasData = this.isOEValue(variable.get('value'));
            // e.g., either this row was selected, or at least one col / choice in this row was selected
            var wasSelected = _lodashLodash['default'].isBoolean(data) && data || _lodashLodash['default'].isNumber(data) && this.isValue(data) || _lodashLodash['default'].isArray(data) && data.some(function (d) {
                return _this2.isValue(d);
            });

            if (wasSelected && !hasData && !(row || col).openOptional) {
                return [this.lget('extraInfo')];
            }
            if (!wasSelected && hasData && !(row || col).extraError) {
                return [this.lget('extraSelect')];
            }
        },

        /**
         * Determine if the value is valid for this question type.
         * This is implemented separately for checkbox, number, text, and textarea question types.
         * @param value
         * @returns {boolean}
         */
        isValue: function isValue(value) {
            // NaN is a number...
            return value !== null && !isNaN(value);
        },

        /**
         * A variable is optional IFF its row OR column OR question are optional
         * This is implemented separately for radios which may have one variable belonging to a group of cells
         * and for checkboxes which allow "optional" to override "atmost", "atleast", and "exactly"
         * @param {DS.Model} variable
         * @returns {boolean}
         */
        isMandatory: function isMandatory(variable) {
            if (this.get('optional')) {
                return false;
            }

            var _getProperties2 = this.getProperties('rows', 'cols');

            var rows = _getProperties2.rows;
            var cols = _getProperties2.cols;

            var _variable$getProperties = variable.getProperties('row', 'col');

            var row = _variable$getProperties.row;
            var col = _variable$getProperties.col;

            // nb: we cannot always assume variables belong to a row and col (e.g., a 1D number element)
            if (row && rows.findBy('label', row).get('optional')) {
                return false;
            }
            if (col && cols.findBy('label', col).get('optional')) {
                return false;
            }
            return true;
        },

        /**
         * Implemented on the Radio, Select, Text and Number question types
         * Are we setting the value to null, or something that casts to a number?
         * @param value {Number|String|Boolean}
         */
        validateValue: function validateValue(value) {
            if (value === null) {
                return value;
            }
            if (isNaN(Number(value))) {
                throw new TypeError('A number is required, not ' + typeof value);
            }
            return Number(value);
        },

        /**
         * Implemented only on the Checkbox question
         */
        executeAutofill: function executeAutofill() {},

        /**
         * Execute some exec block of code
         */
        executeCode: function executeCode() {
            if (!this.get('exec')) {
                return null;
            }
            var captured = { __stdout__: [] };
            return this.get('environment').evaluate(this.get('exec'), captured);
        },

        /**
         * Prepare this question's data for display
         * Variables are shown with previously submitted data
         * Hidden variables are set to null
         * TODO: how to handle noanswers (variable for label 'noanswer') and hidden values (e.g., disabled choice on 3d select)
         */
        restoreOldData: function restoreOldData() {
            var _get = this.get('enabled');

            var rows = _get.rows;
            var cols = _get.cols;

            this.get('variables').forEach(function (variable) {
                var _variable$getProperties2 = variable.getProperties('row', 'col', 'value');

                var row = _variable$getProperties2.row;
                var col = _variable$getProperties2.col;
                var value = _variable$getProperties2.value;

                if (value !== null) {
                    // nb: if the variable is shown, hidden, then shown again, it will lose any data in between
                    // to keep things sync, we avoid calling reload() to fetch from the most recent 'answers' snapshot,
                    // but this is also the behavior of online surveys
                    if (row && !rows.findBy('label', row) || col && !cols.findBy('label', col)) {
                        variable.set('value', null);
                    }
                }
            });
        },

        findEnabledCells: function findEnabledCells() {
            var _data2 = this.data;
            var rows = _data2.rows;
            var cols = _data2.cols;
            var choices = _data2.choices;

            rows.forEach(function (row) {
                return row.notifyPropertyChange('displayed');
            });
            cols.forEach(function (col) {
                return col.notifyPropertyChange('displayed');
            });
            choices.forEach(function (choice) {
                return choice.notifyPropertyChange('displayed');
            });
        },

        /**
         * Tell the survey service whether or not to display this question
         * Any autofill or exec attributes will be run starting here.
         * @returns {Boolean}
         */
        _display: function _display() {
            this.findEnabledCells();
            this.restoreOldData();
            // this is by default 'false' for all Elements
            this.executeAutofill();
            this.executeCode();
            var state = this.get('project.ssession.state');
            if (this.get('where.execute') && !(state === 'dev' || state === 'testing')) {
                return false;
            }
            if (!this.conditionIsTrue()) {
                return false;
            }
            // if we can infer the data values, then auto-populate (do not show)
            return !this.tryAutoPopulate();
        },

        /**
         * Wrap the _display method with setup / teardown of variables
         * that reference the current question
         * @returns {Boolean}
         */
        display: function display() {
            var result,
                env = this.get('environment').ctx;
            env._this = this.data;
            env.thisQuestion = env[env._this.label];
            result = this._display();
            delete env._this;
            delete env.thisQuestion;
            return result;
        }
    });

    QuestionModel.reopenClass({
        fallbackChain: ['question'].concat(_offlineModelsElement['default'].fallbackChain)
    });

    exports['default'] = QuestionModel;
});
define('offline/models/radio', ['exports', 'ember-data', 'offline/models/question', 'offline/utils/survey/logic', 'lodash/lodash'], function (exports, _emberData, _offlineModelsQuestion, _offlineUtilsSurveyLogic, _lodashLodash) {
    exports['default'] = _offlineModelsQuestion['default'].extend({
        /**
         * @description
         * Get the selected cell for this row or col
         * @returns {Object|Undefined}
         */
        selected: function selected(row, col) {
            var value = this.dataFor(row, col);
            if (value === null) {
                return null;
            }
            if (_lodashLodash['default'].isBoolean(value)) {
                throw new _offlineUtilsSurveyLogic.RuntimeError('Cannot call "selected" on a value');
            }
            if (_lodashLodash['default'].isArray(value)) {
                throw new _offlineUtilsSurveyLogic.RuntimeError('Cannot call "selected" on a list of ' + this.get('grouping'));
            }
            return this.get(this.get('grouping') === 'rows' ? 'cols' : 'rows').findBy('index', value);
        },

        /**
         * @description
         * Attempt to auto-populate this question element (after stripping out hidden rows / cols)
         * TODO: index or value?
         */
        tryAutoPopulate: function tryAutoPopulate() {
            var inferred = {};

            var _getProperties = this.getProperties('enabled', 'grouping', 'variables');

            var enabled = _getProperties.enabled;
            var grouping = _getProperties.grouping;
            var variables = _getProperties.variables;

            var groups = enabled[grouping];
            var values = enabled[grouping === 'rows' ? 'cols' : 'rows'];
            if (values.length === 1) {
                for (var i = 0; i < groups.length; i++) {
                    this.variableFor(groups[i], values[0]).set('value', values[0].get('index'));
                }
                return true;
            }
            return false;
        },

        /**
         * Provide the cdata for the selected cell (Radio and Select question types)
         * @returns {{valid: Boolean, text: String}}
         */
        pipeText: function pipeText() {
            if (this.get('rows.length') > 1 && this.get('cols.length') > 1) {
                return { valid: false, text: 'too many dimensions' };
            }
            return { valid: true, text: this.getSelectedText() };
        },

        /**
         * @description
         * Get the data-label pertaining to a cell at intersection (elA, elB)
         * e.g., variableFor(q1.r1, q1.c2) => 'q1r1', if q1 is grouped by rows
         * @param elA
         * @param elB
         * @returns {DS.Model}
         */
        variableFor: function variableFor(elA, elB) {
            var labels = { row: null, col: null };
            var variableTable = this.get('variableTable');
            if (elA) {
                labels[elA.type] = elA.label;
            }
            if (elB) {
                labels[elB.type] = elB.label;
            }
            if (this.get('grouping') === 'rows') {
                return variableTable[labels.row][null];
            }
            return variableTable[null][labels.col];
        },

        /**
         * @description
         * Value for e.g., q1r1, which would be the value for the selected column
         * @param elA
         * @param elB
         */
        valueFor: function valueFor(elA, elB) {
            var values = { col: null, row: null };
            if (elA) {
                values[elA.type] = elA.index;
            }
            if (elB) {
                values[elB.type] = elB.index;
            }
            return this.get('grouping') === 'rows' ? values.col : values.row;
        },

        /**
         * A cell is optional IFF its group OR the question is optional
         * @param {DS.Model} variable
         * @returns {boolean}
         */
        isMandatory: function isMandatory(variable) {
            var _getProperties2 = this.getProperties('grouping', 'optional');

            var grouping = _getProperties2.grouping;
            var optional = _getProperties2.optional;

            return !this.get(grouping).findBy('label', variable.get(grouping === 'rows' ? 'row' : 'col')).get('optional') && !optional;
        },

        /**
         * @description
         * Is the value null, or can it be cast to a number within the range of allowed values
         * (a question grouped by rows, with 3 cols has valid values [0, 1, 2])
         * @param value
         * @returns {null|Number}
         */
        validateValue: function validateValue(value) {
            if (value === null) {
                return null;
            }
            var _value = Number(value);
            if (isNaN(_value)) {
                throw new TypeError('A number is required, not ' + typeof value);
            }
            var minvalue = 0,
                maxvalue = this.get(this.get('grouping') === 'rows' ? 'cols' : 'rows').get('length') - 1;
            if (_value < minvalue || _value > maxvalue) {
                throw new _offlineUtilsSurveyLogic.RuntimeError('Value ' + _value + ' not within ' + minvalue + '..' + maxvalue);
            }
            return _value;
        },

        /**
         * Return the value for this (row, col) coordinate (or the list of values)
         * @param {{label, index}} row
         * @param {{label, index}} col
         * @returns {Number|Boolean|Array}
         */
        dataFor: function dataFor(row, col) {
            var _this = this;

            var group, value, groups;
            if (this.get('grouping') === 'rows') {
                group = row;
                value = col;
                groups = this.get('rows').toArray();
            } else {
                group = col;
                value = row;
                groups = this.get('cols').toArray();
            }
            if (group && value) {
                return this.variableFor(group).get('value') === value.index;
            } else if (group) {
                return this.variableFor(group).get('value');
            } else if (value) {
                return this.get('variables').any(function (v) {
                    return v.get('value') === value.index;
                });
            } else {
                var data = [];
                groups.forEach(function (g) {
                    data[g.get('index')] = _this.variableFor(g.data).get('value');
                });
                if (groups.length < 2) {
                    return data[0];
                }
                return data;
            }
        },

        /**
         * Return a list of values for the question or one of it's rows / cols
         * @param {{ label, index }} row
         * @param {{ label, index }} col
         * @returns {Array}
         */
        valuesFor: function valuesFor(row, col) {
            var values = [],
                groupCells = undefined,
                valueCell = undefined;
            if (this.get('grouping') === 'cols') {
                valueCell = row;
                groupCells = this.get('cols').toArray().filter(function (item) {
                    return !col || col.label === item.get('label');
                });
            } else {
                valueCell = col;
                groupCells = this.get('rows').toArray().filter(function (item) {
                    return !row || row.label === item.get('label');
                });
            }
            for (var i = 0; i < groupCells.length; i++) {
                var value = this.variableFor(groupCells[i].data).get('value');
                if (valueCell) {
                    values.push(valueCell.index === value);
                } else {
                    values.push(value);
                }
            }
            return values;
        },

        /**
         * Restore shown data-points for radios which are special
         * For example, if we're grouped by rows, restore a variable only if the row it belongs to
         * is shown and it's value is the index of a displayed column
         */
        restoreOldData: function restoreOldData() {
            var _getProperties3 = this.getProperties('enabled', 'grouping', 'variables');

            var enabled = _getProperties3.enabled;
            var grouping = _getProperties3.grouping;
            var variables = _getProperties3.variables;

            var values = enabled[grouping === 'rows' ? 'cols' : 'rows'].map(function (v) {
                return v.get('index');
            });
            variables.forEach(function (variable) {
                var value = variable.get('value');
                if (value !== null) {
                    var owner;
                    if (variable.get('oe')) {
                        // has to belong to either row or col, so was this openend shown?
                        if ((owner = variable.get('row')) && !enabled.rows.findBy('label', owner)) {
                            variable.set('value', null);
                        } else if ((owner = variable.get('col')) && !enabled.cols.findBy('label', owner)) {
                            variable.set('value', null);
                        }
                    } else {
                        owner = variable.get(grouping === 'rows' ? 'row' : 'col');
                        // if the variable is hidden, or it's value no longer shown, reset to None
                        if (owner && !enabled[grouping].findBy('label', owner) || values.indexOf(value) === -1) {
                            variable.set('value', null);
                        }
                    }
                }
            });
        }
    });
});
define('offline/models/res', ['exports', 'ember-data', 'offline/models/element'], function (exports, _emberData, _offlineModelsElement) {
    exports['default'] = _offlineModelsElement['default'].extend({
        cdata: _emberData['default'].attr('string'),
        label: _emberData['default'].attr('string'),

        display: function display() {
            return false;
        },
        getContents: function getContents() {
            return this.get('cdata');
        }
    });
});
define('offline/models/respondent', ['exports', 'ember', 'ember-data'], function (exports, _ember, _emberData) {

    /**
     * A serializable scratchpad available for editing by in-survey logic
     * @constructor
     */
    function Persistent() {
        this.markers = [/* "qualified" */];
        this._disabled = {/* q1: {r1: true, r2: false} */};
        this.__start_time__ = 0;
        this.__total_time__ = 0;
    }

    exports['default'] = _emberData['default'].Model.extend({
        status: _emberData['default'].attr('number', { defaultValue: 4 }),
        position: _emberData['default'].attr('number', { defaultValue: 0 }),
        created_on: _emberData['default'].attr('date', { defaultValue: function defaultValue() {
                return new Date();
            } }),
        lastupdated_on: _emberData['default'].attr('date'),
        completed_on: _emberData['default'].attr('date'),
        history: _emberData['default'].attr({ defaultValue: function defaultValue() {
                return [];
            } }),
        answers: _emberData['default'].attr({ defaultValue: function defaultValue() {
                return {};
            } }),
        persistent: _emberData['default'].attr({ defaultValue: function defaultValue() {
                return new Persistent();
            } }),
        description: _emberData['default'].attr('string', {
            defaultValue: function defaultValue(model) {
                // nb: the store only knows of respondents for the current interviewer
                return 'Respondent ' + model.get('store').peekAll('respondent').get('length');
            }
        }),

        survey: _emberData['default'].belongsTo('survey', { inverse: 'respondents' }),
        survey_hash: _ember['default'].computed.reads('survey.hash'),

        partial: _ember['default'].computed('status', function () {
            return this.get('status') === 4;
        }),

        completed: _ember['default'].computed('status', function () {
            // (1) term (2) OQ (3) qual
            return this.get('status') < 4;
        }),

        /**
         * Are we resuming the survey?
         * This is reset to false when saving / leaving the survey
         * TODO: may want to change with currentState.stateName
         * @type {Boolean}
         */
        isResuming: _ember['default'].computed({
            get: function get() {
                return this._isResuming || (this._isResuming = false);
            },
            set: function set(_, value) {
                return this._isResuming = value;
            }
        }),

        statusChanged: _ember['default'].observer('status', function () {
            if (this.get('completed')) {
                this.set('completed_on', new Date());
            }
        }),

        restart: function restart() {
            this.setProperties({
                isResuming: false,
                status: 4,
                position: 0,
                history: [],
                answers: {},
                persistent: new Persistent()
            });
        },

        /**
         * Update the lastupdated_on attr when committing, and rollback when commit fails
         * @param options
         * @returns {Promise}
         */
        save: function save(options) {
            var _this = this;

            var properties = { isResuming: false };
            // calling save() on deleted records does not count as updating
            if (!this.get('isDeleted')) {
                properties.lastupdated_on = new Date();
            }
            this.setProperties(properties);
            return this._super(options)['catch'](function (error) {
                var changedAttributes = _this.changedAttributes();
                if (changedAttributes.lastupdated_on) {
                    // reset lastupdated_on to the old value
                    // hasDirtyAttributes will return to false if nothing else changed
                    // but errors and isValid will be preserved
                    _this.set('lastupdated_on', changedAttributes.lastupdated_on[0]);
                }
                throw error;
            });
        }
    });
});
define('offline/models/select', ['exports', 'ember-data', 'offline/models/question', 'offline/utils/survey/logic'], function (exports, _emberData, _offlineModelsQuestion, _offlineUtilsSurveyLogic) {
    exports['default'] = _offlineModelsQuestion['default'].extend({
        /**
         * @description
         * Get the choice selected for this cell (row, col)
         * @returns {Object}
         */
        selected: function selected(row, col) {
            return this.get('choices').findBy('index', this.dataFor(row, col));
        },

        /**
         * @description
         * Attempt to auto-populate this question element (after stripping out hidden rows / cols)
         * @returns {boolean}
         */
        tryAutoPopulate: function tryAutoPopulate() {
            var _get = this.get('enabled');

            var rows = _get.rows;
            var cols = _get.cols;
            var choices = _get.choices;

            if (choices.length === 1) {
                var index = choices[0].get('index');
                for (var r = 0; r < rows.length; r++) {
                    for (var c = 0; c < cols.length; c++) {
                        this.variableFor(rows[r], cols[c]).set('value', index);
                    }
                }
                return true;
            }
            return false;
        },

        /**
         * Provide the cdata for the selected cell
         * @returns {{valid: Boolean, text: String}}
         */
        pipeText: function pipeText() {
            if (this.get('choices').length > 1 && (this.get('rows').length > 1 || this.get('cols').length > 1)) {
                return { valid: false, text: 'too many dimensions' };
            }
            return { valid: true, text: this.getSelectedText() };
        },

        /**
         * @description
         * Is the value null, or can it be cast to a number which is the index of some choice
         * @param value
         * @returns {null|Number}
         */
        validateValue: function validateValue(value) {
            if (value === null) {
                return null;
            }
            var _value = Number(value);
            if (isNaN(_value)) {
                throw new TypeError('A number is required, not ' + typeof value);
            }
            var minvalue = 0,
                maxvalue = this.get('choices').get('length') - 1;
            if (_value < minvalue || _value > maxvalue) {
                throw new _offlineUtilsSurveyLogic.RuntimeError('Value ' + _value + ' not within ' + minvalue + '..' + maxvalue);
            }
            return _value;
        }
    });
});
define('offline/models/survey', ['exports', 'ember', 'ember-data'], function (exports, _ember, _emberData) {
    exports['default'] = _emberData['default'].Model.extend({
        compat: _emberData['default'].attr('number'),
        state: _emberData['default'].attr('string'),
        hash: _emberData['default'].attr('string'), // TODO: didDefineProperty?
        latestHash: _emberData['default'].belongsTo('hash'),
        version: _emberData['default'].attr('string'),
        path: _emberData['default'].attr('string'),
        adminTitle: _emberData['default'].attr('string'),
        respondentsTitle: _emberData['default'].attr('string'),
        errors: _emberData['default'].attr({ 'default': [] }), // TODO: handle survey errors the Ember way
        assets: _emberData['default'].hasMany('assets', { inverse: 'survey', async: true }),
        messages: _emberData['default'].hasMany('message', { inverse: 'survey', async: false }),
        elements: _emberData['default'].hasMany('element', { inverse: 'survey', polymorphic: true, async: false }),
        variables: _emberData['default'].hasMany('variable', { inverse: 'survey', async: false }),
        exitpages: _emberData['default'].hasMany('exitpage', { inverse: 'survey', polymorphic: true, async: false }),
        practices: _emberData['default'].hasMany('practice', { inverse: 'survey', async: false }),
        respondents: _emberData['default'].hasMany('respondent', { inverse: 'survey', async: true }),
        interviewers: _emberData['default'].hasMany('interviewer'),

        downloaded: _ember['default'].computed('{elements,messages,exitpages,variables}.[]', function () {
            var _this = this;

            return ['elements', 'messages', 'exitpages', 'variables'].every(function (el, i, arr) {
                return _this.get(el).length > 0;
            });
        }),

        needsUpdate: _ember['default'].computed('hash', 'latestHash', 'downloaded', function () {
            var hash = this.get('hash');
            var latestHash = this.get('latestHash');
            var downloaded = this.get('downloaded');
            return downloaded && latestHash && latestHash.get('value') !== hash;
        }),

        /**
         * @description
         * Remove survey's hasMany relationships from device storage and store cache.
         * @returns {Ember.RSVP.Promise}
         */
        removeRelationships: function removeRelationships() {
            var _this2 = this;

            var protectedRelations = ['interviewers', 'respondents']; // relationships on the survey that won't be destroyed
            var resolving = [];

            this.eachRelationship(function (name, relationship) {
                var kind = relationship.kind;

                // only delete hasMany relationships on the survey that are not protected
                if (kind === 'hasMany' && protectedRelations.indexOf(name) === -1) {
                    var records = _this2.get(name);
                    if (_ember['default'].isArray(records)) {
                        resolving.concat(records.invoke('destroyRecord', { adapterOptions: { device: true } }));
                    }
                }
            });

            return _ember['default'].RSVP.all(resolving).then(function () {
                var adapter = _this2.store.adapterFor('survey');
                var data = _this2.serialize({ includeId: true });

                // Go directly to this instead of update record, beacause we need to override data.
                // If we use updateRecord it will try to get the survey from storage first including
                // related models which no longer exist.
                adapter.setIntoStorage(_this2.store.modelFor('survey'), data, true);
            });
        },

        /**
         * Iterative depth-first search for element with label
         * @param {String} label
         * @returns {DS.Model}
         */
        findElement: function findElement(label) {
            var stack = [this];
            while (stack.length) {
                var item = stack.pop();
                if (item.get('label') === label) {
                    return item;
                }
                stack.push.apply(stack, item.get('elements').toArray() || []);
            }
        }
    });
});
define('offline/models/suspend', ['exports', 'ember-data', 'offline/models/element'], function (exports, _emberData, _offlineModelsElement) {
  exports['default'] = _offlineModelsElement['default'].extend({});
});
define('offline/models/term', ['exports', 'ember-data', 'offline/models/element', 'offline/utils/survey/session'], function (exports, _emberData, _offlineModelsElement, _offlineUtilsSurveySession) {

    var MARKER_PREFIX = 'term';

    exports['default'] = _offlineModelsElement['default'].extend({
        py_cond: _emberData['default'].attr('string'), // python condition
        markers: _emberData['default'].attr('string'),

        /**
         * Display is run ONLY if this term is "shown" (i.e., it's condition is TRUE)
         * Prefix our markers with 'term:' then jump to the end of the survey
         * @returns {Object}
         */
        display: function display() {
            var _this = this;

            var markers = this.get('markers') && this.get('markers').split(',') || [];
            markers.concat(MARKER_PREFIX + ':' + (this.get('py_cond') || '')).forEach(function (marker) {
                _this.get('environment').ctx.setMarker(marker);
            });
            this.get('project.ssession').finish();
            return _offlineUtilsSurveySession.HALT;
        }
    });
});
define('offline/models/text', ['exports', 'ember-data', 'offline/models/question', 'offline/utils/survey/logic', 'lodash/lodash'], function (exports, _emberData, _offlineModelsQuestion, _offlineUtilsSurveyLogic, _lodashLodash) {

    var EMPTY_OR_WHITESPACE_ONLY_RX = /^\s*$/;

    exports['default'] = _offlineModelsQuestion['default'].extend({

        size: _emberData['default'].attr('number', { defaultValue: 20 }),
        optional: _emberData['default'].attr('boolean', { defaultValue: true }), // overrides the question model defaultValue

        /**
         * @description
         * Provide the text value for this Text question with a single data-point
         * @returns {{valid: Boolean, text: String}}
         */
        pipeText: function pipeText() {
            if (this.get('variables.length') > 1) {
                return { valid: false, text: 'too many dimensions' };
            }
            var value = this.dataFor();
            if (!_lodashLodash['default'].isString(value)) {
                return { valid: true, text: "" };
            }
            return { valid: true, text: value };
        },

        /**
         * @description
         * Attempt to auto-populate this question element (after stripping out hidden rows / cols)
         */
        tryAutoPopulate: function tryAutoPopulate() {
            return false;
        },

        /**
         * Verify that the cell has some non-whitespace characters
         * @param {DS.Model} variable
         * @returns {Array.<String>}
         */
        verifyCell: function verifyCell(variable) {
            var errors = [];
            var answer = variable.get('value');
            if (!this.isValue(answer)) {
                errors.push(this.lget('textNoAnswerSelected'));
            }
            return errors;
        },

        /**
         * @description
         * The value must be text or null
         * @params value
         * @returns {null|String}
         */
        validateValue: function validateValue(value) {
            if (value === null) {
                return null;
            }
            if (!_lodashLodash['default'].isString(value)) {
                throw new _offlineUtilsSurveyLogic.RuntimeError('Value must be text, not ' + typeof value);
            }
            return value;
        },

        /**
         * Determine if value is a non-empty string with at least one non-whitespace character.
         * @param value
         * @returns {boolean}
         */
        isValue: function isValue(value) {
            return _lodashLodash['default'].isString(value) && !value.match(EMPTY_OR_WHITESPACE_ONLY_RX);
        }

    });
});
define('offline/models/textarea', ['exports', 'ember-data', 'offline/models/text'], function (exports, _emberData, _offlineModelsText) {
    exports['default'] = _offlineModelsText['default'].extend({
        width: _emberData['default'].attr('number', { defaultValue: 40 }),
        height: _emberData['default'].attr('number', { defaultValue: 6 })
    });
});
define('offline/models/user', ['exports', 'ember-data'], function (exports, _emberData) {
    exports['default'] = _emberData['default'].Model.extend({
        display_name: _emberData['default'].attr('string'),
        locale: _emberData['default'].attr('string'),
        surveys: _emberData['default'].hasMany('survey'),
        interviewers: _emberData['default'].hasMany('interviewer')
    });
});
define('offline/models/var', ['exports', 'ember-data', 'offline/models/element'], function (exports, _emberData, _offlineModelsElement) {
    exports['default'] = _offlineModelsElement['default'].extend({
        name: _emberData['default'].attr('string'),
        variable: _emberData['default'].belongsTo('variable')
    });
});
define('offline/models/variable', ['exports', 'ember', 'ember-data'], function (exports, _ember, _emberData) {
    exports['default'] = _emberData['default'].Model.extend({
        value: _emberData['default'].attr({ defaultValue: null }), // the value committed after form submit
        label: _emberData['default'].attr('string'), // datapoint
        row: _emberData['default'].attr('string'), // row label
        col: _emberData['default'].attr('string'), // col label
        type: _emberData['default'].attr('string'), // single, multiple, float, etc
        oe: _emberData['default'].attr('boolean'), // is this an openend
        noanswerParent: _emberData['default'].attr('string'),
        values: _emberData['default'].attr({ defaultValue: null }), // list of custom assigned values for single types
        survey: _emberData['default'].belongsTo('survey', { inverse: 'variables' }),
        question: _emberData['default'].belongsTo('question', { inverse: 'variables', polymorphic: true }),
        isSelected: _ember['default'].computed('value', function () {
            return this.get('value') !== null;
        }),
        errorMessages: _ember['default'].computed('errors.@each.message', function () {
            return this.get('errors').map(function (error) {
                return error.message;
            });
        }),
        /**
         * This is a method instead of a computed property because we may want to call
         * variable.valueFor() on a value restored from localStorage without setting 'value'
         * @param value
         * @returns {*}
         */
        valueFor: function valueFor(value) {
            if (this.get('type') === 'single') {
                return this.get('values')[value];
            }
            return value;
        }
    });
});
define('offline/resolver', ['exports', 'ember-resolver'], function (exports, _emberResolver) {
  exports['default'] = _emberResolver['default'];
});
define('offline/router', ['exports', 'ember', 'offline/config/environment'], function (exports, _ember, _offlineConfigEnvironment) {

    var Router = _ember['default'].Router.extend({
        location: _offlineConfigEnvironment['default'].locationType,
        rootURL: _offlineConfigEnvironment['default'].rootURL
    });

    Router.map(function () {
        this.route('authenticated', { path: '/' }, function () {
            this.route('admin', { path: '/admin/:project' });
            this.route('respview', function () {
                this.route('survey', { path: '/survey/:respId' }, function () {
                    this.route('page');
                });
                this.route('exitpage');
            });
            this.route('projects');
        });
        this.route('login', function () {
            this.route('forgot');
        });
        this.route('solo', { path: '/solo/:uuid' });
        this.route('404', { path: '*path' });
    });

    exports['default'] = Router;
});
define('offline/routes/application', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Route.extend({
        session: _ember['default'].inject.service(),
        beforeModel: function beforeModel() {
            return this.get('intl').setLocale('en-us');
        }
    });
});
define('offline/routes/authenticated', ['exports', 'ember', 'ember-simple-auth/mixins/authenticated-route-mixin'], function (exports, _ember, _emberSimpleAuthMixinsAuthenticatedRouteMixin) {
    exports['default'] = _ember['default'].Route.extend(_emberSimpleAuthMixinsAuthenticatedRouteMixin['default'], {
        session: _ember['default'].inject.service(),
        currentUser: _ember['default'].inject.service('current-user'),
        model: function model() {
            var _this = this;

            return this.get('currentUser').load()['catch'](function () {
                _this.get('session').invalidate();
            });
        }
    });
});
define('offline/routes/authenticated/admin', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Route.extend({
        project: _ember['default'].inject.service('current-project'),
        network: _ember['default'].inject.service(),
        /**
         * Fetch the project, side loading the element tree, (translated) messages, and exit pages
         * When entering this route, we want to resolve the interviewer to the current user's interviewer
         * for this project
         * @param params
         * @returns {Promise}
         */
        model: function model(params) {
            var _this = this;

            this.set('project.path', params.project);
            return _ember['default'].RSVP.hash({
                // the goal here is to update only when we know we have stale data
                survey: this.store.findRecord('hash', params.project).then(function () {
                    return _this.store.findRecord('survey', params.project, { include: 'elements,messages,exitpages,variables,assets' }).then(function (survey) {
                        return _this.set('project.survey', survey);
                    });
                }),
                // (re)fetch interviewer counts and active status
                interviewer: this.get('project.interviewer').reload()
            });
        },
        /**
         * TODO: allow interviewer one subsequent upload after they're removed
         * @param {{ survey, interviewer }} model
         * @param {Transition} transition
         */
        afterModel: function afterModel(model, transition) {
            var _this2 = this;

            if (!model.interviewer.get('has_seen')) {
                model.interviewer.set('has_seen', true);
                model.interviewer.save({ adapterOptions: { device: true } });
            }
            if (!model.interviewer.get('active')) {
                return this.intermediateTransitionTo('authenticated.admin.locked');
            }
            // this loads all respondents on the device into memory, then uploads completes
            return this.store.findAll('respondent').then(function (respondents) {
                if (_this2.get('network.online')) {
                    (function () {
                        var options = { adapterOptions: { device: false } };
                        respondents.forEach(function (respondent) {
                            if (respondent.get('completed') && !respondent.get('isSaving')) {
                                respondent.save(options);
                            }
                        });
                    })();
                }
            });
        }
    });
});
define('offline/routes/authenticated/index', ['exports', 'ember', 'ember-simple-auth/mixins/authenticated-route-mixin'], function (exports, _ember, _emberSimpleAuthMixinsAuthenticatedRouteMixin) {
    exports['default'] = _ember['default'].Route.extend({
        beforeModel: function beforeModel() {
            this.transitionTo('authenticated.projects');
        }
    });
});
define('offline/routes/authenticated/projects', ['exports', 'ember', 'ember-simple-auth/mixins/application-route-mixin'], function (exports, _ember, _emberSimpleAuthMixinsApplicationRouteMixin) {

    /*
     */
    exports['default'] = _ember['default'].Route.extend(_emberSimpleAuthMixinsApplicationRouteMixin['default'], {
        poller: _ember['default'].inject.service(),
        model: function model() {
            var _this = this;

            var options = { adapterOptions: { device: true } };

            return _ember['default'].RSVP.hash({
                // Whats happening here:
                // 1. We fetch a shallow copy of all surveys a user has from network. We set all the results
                //    into storage (overwrite=false). We then resolve with the payload from network.
                // 2. If a survey was downloaded it is still in local storage and we don't know about it yet
                //    (because step 1 resolves with the network payload even though it sets into storage). So
                //    attempt to get each survey from storage. This will also get all embedded records out with it.
                // 3. When we are getting each survey from storage it doesn't get the hash. This is because hash
                //    isn't an embedded record, instead it is just an id mapping. So we also fetch the hash if
                //    the survey is downloaded (has elements, messages, etc) because thats how we compute needsUpdate.
                surveys: this.store.findAll('survey', { reload: true }).then(function (surveys) {

                    var resolving = [];
                    surveys.forEach(function (survey) {
                        var surveyProm = _this.store.findRecord('survey', survey.get('path'), options).then(function (s) {
                            if (s.get('downloaded')) {
                                return _this.store.findRecord('hash', s.get('path'), options);
                            }
                        });
                        resolving.push(surveyProm);
                    });
                    return _ember['default'].RSVP.all(resolving);
                }),
                interviewers: this.modelFor('authenticated').get('interviewers')
            });
        },
        actions: {
            /**
             * Clean up any polling when transitioning to somewhere like authenticated.admin.
             * Ember triggers this event.
             * @param {Transition} transition
             */
            willTransition: function willTransition(transition) {
                if (transition.targetName !== this.routeName) {
                    this.get('poller').stopPoll('projects-list');
                }
            }
        }
    });
});
define('offline/routes/authenticated/respview', ['exports', 'ember'], function (exports, _ember) {

    // https://guides.emberjs.com/v2.9.0/routing/preventing-and-retrying-transitions/#toc_storing-and-retrying-a-transition
    exports['default'] = _ember['default'].Route.extend({
        project: _ember['default'].inject.service('current-project'),
        /**
         * Avoid jumping directly into respview
         * TODO: can we restore the last project from session storage
         * @param transition
         */
        beforeModel: function beforeModel(transition) {
            if (!this.get('project.survey')) {
                this.transitionTo('authenticated.projects');
            }
        },
        handleTransition: function handleTransition(transition) {
            transition.abort();
            if (window.history) {
                window.history.forward();
            }
        },
        actions: {
            willTransition: function willTransition(transition) {
                // TODO: save respondent?
                //return this.handleTransition(transition);
            }
        }
    });
});
define('offline/routes/authenticated/respview/error', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('offline/routes/authenticated/respview/exitpage', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Route.extend({
        network: _ember['default'].inject.service(),
        project: _ember['default'].inject.service('current-project'),
        survey: _ember['default'].computed.alias('project.ssession'),
        model: function model() {
            var _this = this;

            return this.get('survey.exitpages').find(function (exitpage) {
                return _this.get('survey').environment.evaluate(exitpage.get('cond'));
            });
        },
        afterModel: function afterModel() {
            var _this2 = this;

            if (this.paramsFor('authenticated.respview').practice) {
                this.get('project.respondent').rollbackAttributes();
            } else {
                // save to the device, then upload in the background
                return this.get('project.respondent').save({ adapterOptions: { device: true } }).then(function () {
                    _this2.store.findAll('respondent').then(function (respondents) {
                        if (_this2.get('network.online')) {
                            (function () {
                                var options = { adapterOptions: { device: false } };
                                respondents.forEach(function (r) {
                                    if (r.get('completed') && !r.get('isSaving')) {
                                        r.save(options);
                                    }
                                });
                            })();
                        }
                    });
                });
            }
        }
    });
});
define('offline/routes/authenticated/respview/survey', ['exports', 'ember', 'offline/utils/survey/session'], function (exports, _ember, _offlineUtilsSurveySession) {

    /**
     * Create a new respondent when entering the survey view, if one isn't already supplied
     */
    exports['default'] = _ember['default'].Route.extend({
        project: _ember['default'].inject.service('current-project'),
        model: function model(params) {
            var respondent = this.get('project.survey.respondents').findBy('id', params.respId);
            if (respondent) {
                return this.set('project.respondent', respondent);
            } else {
                this.transitionTo('authenticated.admin', this.get('project.path'));
            }
        },
        afterModel: function afterModel(respondent) {
            var _this = this;

            this.set('project.ssession', _offlineUtilsSurveySession['default'].create({
                content: this.get('project.survey'),
                respondent: this.get('project.respondent'),
                interviewer: this.get('project.interviewer')
            }).on('finished', function (exitpage) {
                _this.transitionTo('authenticated.respview.exitpage');
            }).on('exception', function (error) {
                _this.intermediateTransitionTo('authenticated.respview.error', error);
            }));
            this.get('project.ssession').start();
        }
    });
});
define('offline/routes/authenticated/respview/survey/page', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('offline/routes/login', ['exports', 'ember', 'ember-simple-auth/mixins/application-route-mixin', 'ember-simple-auth/mixins/unauthenticated-route-mixin'], function (exports, _ember, _emberSimpleAuthMixinsApplicationRouteMixin, _emberSimpleAuthMixinsUnauthenticatedRouteMixin) {
  exports['default'] = _ember['default'].Route.extend(_emberSimpleAuthMixinsUnauthenticatedRouteMixin['default'], _emberSimpleAuthMixinsApplicationRouteMixin['default']);
});
define('offline/routes/login/forgot', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('offline/routes/login/index', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('offline/routes/solo', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Route.extend({
        model: function model(params) {}
    });
});
define('offline/serializers/application', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].RESTSerializer.extend({});
});
define('offline/serializers/cell', ['exports', 'ember-data/serializers/json'], function (exports, _emberDataSerializersJson) {
    exports['default'] = _emberDataSerializersJson['default'].extend({
        /**
         * convert empty labels to a standard null, so we don't have to OR against null everytime
         * @param model The model constructor
         * @param cell The payload
         * @param prop
         */
        normalize: function normalize(model, cell, prop) {
            cell.label = cell.label || null;
            return this._super(model, cell, prop);
        }
    });
});
define('offline/serializers/element', ['exports', 'ember-data', 'offline/serializers/application'], function (exports, _emberData, _offlineSerializersApplication) {
    exports['default'] = _offlineSerializersApplication['default'].extend(_emberData['default'].EmbeddedRecordsMixin, {
        attrs: {
            elements: { deserialize: 'records' }
        },
        extractId: function extractId(_, element) {
            return element.survey + '.' + element.uid;
        }
    });
});
define('offline/serializers/hash', ['exports', 'ember-data', 'offline/serializers/application'], function (exports, _emberData, _offlineSerializersApplication) {
    exports['default'] = _offlineSerializersApplication['default'].extend(_emberData['default'].EmbeddedRecordsMixin, {
        attrs: {
            survey: 'path'
        }
    });
});
define('offline/serializers/interviewer', ['exports', 'ember-data', 'offline/serializers/application'], function (exports, _emberData, _offlineSerializersApplication) {
    exports['default'] = _offlineSerializersApplication['default'].extend(_emberData['default'].EmbeddedRecordsMixin, {
        primaryKey: 'interviewer_id',
        attrs: {
            survey: 'path'
        }
    });
});
define('offline/serializers/message', ['exports', 'ember-data', 'offline/serializers/application'], function (exports, _emberData, _offlineSerializersApplication) {
  exports['default'] = _offlineSerializersApplication['default'].extend({});
});
define('offline/serializers/question', ['exports', 'offline/serializers/element'], function (exports, _offlineSerializersElement) {
    exports['default'] = _offlineSerializersElement['default'].extend({
        attrs: {
            elements: { deserialize: 'records' },
            variables: { deserialize: 'ids' }
        }
    });
});
define('offline/serializers/respondent', ['exports', 'ember', 'offline/serializers/application', 'lodash/lodash'], function (exports, _ember, _offlineSerializersApplication, _lodashLodash) {

    var PARTIAL_MARKER = 'autorecover';

    exports['default'] = _offlineSerializersApplication['default'].extend({
        extractErrors: function extractErrors(store, typeClass, payload, id) {
            var out = {};
            if (payload && typeof payload === 'object' && payload.errors) {
                for (var i = 0; i < payload.errors.length; i++) {
                    for (var key in payload.errors[i]) {
                        if (!payload.errors[i].hasOwnProperty(key)) {
                            continue;
                        }
                        (out[key] = out[key] || []).push(payload.errors[i][key]);
                    }
                }
            }
            return out;
        },

        /**
         * Fix up respondent 'answers' into values the backend expects
         * Always include the id
         * @param snapshot
         * @param options
         * @returns {Object}
         */
        serialize: function serialize(snapshot) {
            var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

            // save everything to the device
            if (snapshot.adapterOptions && snapshot.adapterOptions.device) {
                options.includeId = true;
                return this._super(snapshot, options);
            }

            var _snapshot$attributes = snapshot.attributes();

            var answers = _snapshot$attributes.answers;
            var persistent = _snapshot$attributes.persistent;
            var history = _snapshot$attributes.history;
            var serialized = { answers: _lodashLodash['default'].clone(answers), id: snapshot.id };
            var variableTable = {};

            delete serialized.answers._uncommitted; // ugh

            // if we're not saving to the device, go ahead and recover this partially completed respondent
            if (snapshot.record.get('partial')) {
                serialized.answers.date = new Date().toISOString();
                serialized.answers.markers = persistent.markers.concat(PARTIAL_MARKER, 'last_' + _lodashLodash['default'].last(history)).join(',');
            }

            snapshot.record.get('survey.variables').forEach(function (v) {
                variableTable[v.get('label')] = v;
            });

            _lodashLodash['default'].forEach(serialized.answers, function (value, key) {
                if (variableTable[key] && value !== null) {
                    serialized.answers[key] = variableTable[key].valueFor(value);
                } else {
                    delete serialized.answers[key];
                }
            });

            return serialized;
        }
    });
});
define('offline/serializers/survey', ['exports', 'ember-data', 'offline/serializers/application'], function (exports, _emberData, _offlineSerializersApplication) {

    function addRelations(elements, parent, projectID) {
        var prevElement = null;
        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            element.id = projectID + '.' + element.uid;
            element.survey = projectID;
            if (parent) {
                element.parent = parent;
            } else {
                element.parent = null;
            }
            if (prevElement) {
                element.prevElement = prevElement;
                prevElement.nextElement = element;
            } else {
                element.prevElement = null;
            }
            if (element.elements) {
                addRelations(element.elements, element, projectID);
            }
            prevElement = element;
        }
    }

    exports['default'] = _offlineSerializersApplication['default'].extend(_emberData['default'].EmbeddedRecordsMixin, {
        primaryKey: 'path',
        attrs: {
            assets: { deserialize: 'records' },
            elements: { deserialize: 'records' },
            exitpages: { deserialize: 'records' },
            messages: { deserialize: 'records' },
            variables: { deserialize: 'records' }
        },
        /**
         * Elements are side-loaded with the survey (when fetching a single survey)
         * Here we setup unique ids for each element, and preserve the tree structure
         * with parent, elements, prevElement, and nextElement relational attributes
         * @param {DS.Model} model
         * @param {Object} survey
         * @param {String} prop
         * @return {Object}
         */
        normalize: function normalize(model, survey, prop) {
            if (survey.elements) {
                addRelations(survey.elements, null, survey.path);
            }
            if (survey.exitpages) {
                addRelations(survey.exitpages, null, survey.path);
            }
            return this._super.apply(this, arguments);
        }
    });
});
define('offline/serializers/user', ['exports', 'ember-data', 'offline/serializers/application'], function (exports, _emberData, _offlineSerializersApplication) {
    exports['default'] = _offlineSerializersApplication['default'].extend(_emberData['default'].EmbeddedRecordsMixin, {
        attrs: {
            surveys: { deserialize: 'records' },
            interviewers: { deserialize: 'records' }
        }
    });
});
define('offline/serializers/var', ['exports', 'offline/serializers/element'], function (exports, _offlineSerializersElement) {
    exports['default'] = _offlineSerializersElement['default'].extend({
        attrs: {
            variable: { deserialize: 'ids' }
        }
    });
});
define('offline/serializers/variable', ['exports', 'offline/serializers/application'], function (exports, _offlineSerializersApplication) {
    exports['default'] = _offlineSerializersApplication['default'].extend({
        extractErrors: function extractErrors(store, typeClass, payload, id) {
            return payload.errors[0];
        }
    });
});
define('offline/services/ajax', ['exports', 'ember-ajax/services/ajax'], function (exports, _emberAjaxServicesAjax) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberAjaxServicesAjax['default'];
    }
  });
});
define('offline/services/capacity', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Service.extend({
        isLow: _ember['default'].computed(function () {
            return false;
        })
    });
});
define('offline/services/cordova', ['exports', 'ember-cordova/services/cordova'], function (exports, _emberCordovaServicesCordova) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberCordovaServicesCordova['default'];
    }
  });
});
define('offline/services/current-project', ['exports', 'ember'], function (exports, _ember) {

    /**
     * An object that is updated across our application as we
     * upload data and display this aggregate information to the user
     * 
     * completes - completed respondents uploaded
     * partials - partial respondents uploaded
     * survey - the current survey
     */
    var Status = _ember['default'].Object.extend({
        completes: null,
        partials: null,
        survey: null,
        init: function init() {
            this.completes = _ember['default'].Object.create({
                count: 0, // the number uploaded since the user was last shown
                success: false, // all uploads were 200
                failure: false, // at least one upload was 422
                errcode: false // all records failed with something not 422
            });
            this.partials = _ember['default'].Object.create({
                count: 0,
                success: false,
                failure: false,
                errcode: false
            });
            this.survey = _ember['default'].Object.create({
                changed: false
            });
            this._super.apply(this, arguments);
        }
    });

    /**
     * Set on the admin route (entry-point into the project)
     */
    exports['default'] = _ember['default'].Service.extend({
        user: _ember['default'].inject.service('current-user'),
        path: null,
        survey: null,
        ssession: null,
        respondent: null,
        interviewer: _ember['default'].computed('path', function () {
            var _this = this;

            return this.get('user.interviewers').find(function (interviewer) {
                return interviewer.belongsTo('survey').id() === _this.get('path');
            });
        }),
        status: _ember['default'].computed('path', function () {
            return Status.create();
        })
    });
});
define('offline/services/device/platform', ['exports', 'ember-cordova/services/device/platform'], function (exports, _emberCordovaServicesDevicePlatform) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberCordovaServicesDevicePlatform['default'];
    }
  });
});
define('offline/services/device/splashscreen', ['exports', 'ember-cordova/services/device/splashscreen'], function (exports, _emberCordovaServicesDeviceSplashscreen) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberCordovaServicesDeviceSplashscreen['default'];
    }
  });
});
define('offline/services/intl', ['exports', 'ember-intl/services/intl'], function (exports, _emberIntlServicesIntl) {
  /**
   * Copyright 2015, Yahoo! Inc.
   * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
   */

  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberIntlServicesIntl['default'];
    }
  });
});
define('offline/services/network', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Service.extend({
        online: false,
        watchNetwork: _ember['default'].on('init', function () {
            var _this = this;

            this.set('online', window.navigator.onLine);
            // FIXME: this may give us false positives
            window.addEventListener('online', function () {
                return _this.set('online', true);
            });
            window.addEventListener('offline', function () {
                return _this.set('online', false);
            });
        })
    });
});
define('offline/services/poller', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Service.extend({
        network: _ember['default'].inject.service(),
        _timers: {},
        // holds information about a polling action so we can suspend/resume it
        _meta: {},
        // An observer for the online option
        _watchNetwork: _ember['default'].observer('network.online', function () {
            if (this.get('network.online')) {
                this._resume('online');
            } else {
                this._suspend('online');
            }
        }).on('init'),

        /**
         * Suspend all the timers where a option is true
         * @param {String} option
         * @private
         */
        _suspend: function _suspend(option) {
            var meta = this.get('_meta');
            for (var _name in meta) {
                if (meta.hasOwnProperty(_name) && meta[_name]['options'][option]) {
                    var timer = this.get('_timers.' + _name);
                    _ember['default'].run.cancel(timer);
                    delete this.get('_timers')[_name];
                    meta[_name]['status'] = 'suspended';
                }
            }
        },

        /**
         * Resume all the timers where a option is true
         * @param {String} option
         * @private
         */
        _resume: function _resume(option) {
            var meta = this.get('_meta');
            for (var _name2 in meta) {
                if (meta.hasOwnProperty(_name2) && meta[_name2]['options'][option] && meta[_name2]['status'] === 'suspended') {
                    this.set('_timers.' + _name2, this._schedule(_name2, meta[_name2].context, meta[_name2].action, meta[_name2].interval));
                    meta[_name2]['status'] = 'running';
                }
            }
        },

        _schedule: function _schedule(name, context, action, interval) {
            return _ember['default'].run.later(this, function () {
                action.apply(context);
                this.set('_timers.' + name, this._schedule(name, context, action, interval));
            }, interval);
        },

        /**
         * Start a poll that will continually run every interval period until stopped
         * with stopPoll. This also takes an optional options hash. Currently the only
         * supported option is `online` which is false by default. If `online` is true
         * then we only poll when we are online.
         * @param {String} name The name for the poll, must be unique
         * @param {Object} context The `this` to be applied to the action
         * @param {Function} action A callback to be run
         * @param {Integer} interval How often the callback should be run
         * @param {Object} Options hash
         * @returns {String} The name for the poll
         */
        startPoll: function startPoll(name, context, action, interval, options) {
            _ember['default'].assert('There is already a poll running for ' + name, !this.get('_timers.' + name) || !this.get('_meta.' + name));

            options = _ember['default'].assign({ online: false }, options || {});
            // some meta information so we can suspend a polling function and resume it
            // later based on some option (ex. { online: true }
            this.set('_meta.' + name, {
                action: action,
                context: context,
                interval: interval,
                status: 'running',
                options: options
            });
            this.set('_timers.' + name, this._schedule(name, context, action, interval));
            return name;
        },

        /**
         * Cancels the running poll for `name` if exists.
         * @param {String} name The unique id for the poll to be canceled
         * @returns {Boolean} True if cancled else False
         */
        stopPoll: function stopPoll(name) {
            var timer = this.get('_timers.' + name);
            var meta = this.get('_meta.' + name);

            if (timer || meta) {
                _ember['default'].run.cancel(timer);
                delete this.get('_timers')[name];
                delete this.get('_meta')[name];
                return true;
            }
            return false;
        },

        /**
         * Get the status for a polling action
         * @param {String} name The unique id for the poll to status
         * @returns {String} 'running', 'suspended' or '' if no such name
         */
        getStatus: function getStatus(name) {
            var meta = this.get('_meta.' + name);
            if (!meta) {
                return '';
            }
            return meta.status;
        }
    });
});
define('offline/services/session', ['exports', 'ember', 'ember-simple-auth/services/session'], function (exports, _ember, _emberSimpleAuthServicesSession) {
    exports['default'] = _emberSimpleAuthServicesSession['default'].extend({
        forgotPassword: function forgotPassword(email) {
            var authenticator = _ember['default'].getOwner(this).lookup('authenticator:authenticator');
            return authenticator.forgotPassword(email);
        }
    });
});
define('offline/services/simple-locks', ['exports', 'ember', 'lodash/lodash'], function (exports, _ember, _lodashLodash) {

    var Lock = _ember['default'].Object.extend({

        init: function init() {
            this._super.apply(this, arguments);
            this.set('_locked', false);
            this._current = null;
            this._queue = [];
            this._id = 1;
        },

        isLocked: _ember['default'].computed('_locked', function () {
            return this.get('_locked');
        }),

        release: function release() {
            _ember['default'].run.later(this, function () {
                this.set('_locked', false);
                this._runNext();
            }, 0);
        },

        enqueue: function enqueue(callback) {
            _ember['default'].assert('Must pass a function. You passed ' + typeof callback, _lodashLodash['default'].isFunction(callback));
            if (callback === this._current) {
                return;
            }

            for (var i = 0; i < this._queue.length; i++) {
                if (callback === this._queue[i]) {
                    return;
                }
            }

            var id = this._id++;
            this._queue.push({ id: id, callback: callback });
            if (!this.get('isLocked')) {
                this._runNext();
            }
            return id;
        },

        remove: function remove(id) {
            var startLen = this._queue.length;

            this._queue = this._queue.filter(function (item) {
                return item.id !== id;
            });
            return startLen !== this._queue.length;
        },

        _runNext: function _runNext() {
            if (this._queue.length > 0) {
                var _queue$shift = this._queue.shift();

                var callback = _queue$shift.callback;

                this.set('_locked', true);
                this._current = callback;
                // its up to a callback to release a lock
                callback(this.release.bind(this));
            } else {
                this._current = null;
            }
        }

    });

    exports['default'] = _ember['default'].Service.extend({
        _locks: {},

        /**
         * @description
         * Acquire a lock name and a callback to be run while it has the lock. No
         * other callbacks can be run for lock `lockname` while the currently running
         * callback has the lock. Callbacks are queued and run in order after the lock
         * been released.
         *
         * NB: It is the callbacks responsiblity to release a lock when it is finished
         * with it.
         *
         * Simple Example:
         * ```js
         * acquire('foolock', lockRelease => {
         *     // do stuff
         *     // stuff done
         *     lockRelease();
         * });
         * ```
         *
         * @param {String} lockname
         * @param {Function} callback - this will be passed a function to release the
         *     lock when it's done with it.
         * @returns {Integer} An id that can be used to remove a request for the lock if
         *    still pending
         */
        acquire: function acquire(lockname, callback) {
            var lock = this._locks[lockname];

            if (!lock) {
                lock = this._locks[lockname] = Lock.create();
            }

            return lock.enqueue(callback);
        },

        /**
         * @description
         * Remove a request for a lock from the locks queue by id.
         * @param {String} lockname
         * @param {Integer} id
         * @returns {Boolean} True if the request was removed from the locks queue.
         *     Returning False may mean it currently has the lock, has already run,
         *     or the lock no longer exists (destroyed).
         */
        remove: function remove(lockname, id) {
            var lock = this._locks[lockname];

            if (!lock) {
                return false;
            }

            return lock.remove(id);
        },

        /**
         * @description
         * Destroy a lock. Any callbacks that are still waiting to acquire a lock
         * will never get excuted.
         * @param {String} lockname
         */
        destroy: function destroy(lockname) {
            if (this._locks[lockname]) {
                delete this._locks[lockname];
            }
        },

        /**
         * @description
         * Find out if a lock is locked or not.
         * @param {String} lockname
         * @returns {Boolean}
         */
        isLocked: function isLocked(lockname) {
            return this._locks[lockname] ? this._locks[lockname].get('isLocked') : false;
        }
    });
});
define('offline/services/storage', ['exports', 'ember', 'offline/config/environment', 'lodash/lodash'], function (exports, _ember, _offlineConfigEnvironment, _lodashLodash) {

    /**
     * A storage exception
     * @constructor
     */
    function StorageError(message, type, id) {
        this.id = id;
        this.type = type;
        this.message = message;
    }
    StorageError.prototype = new Error();
    StorageError.prototype.constructor = StorageError;

    /**
     * @name Storage
     * @description
     * Some abstraction layer on top of a persistent key-value store
     * @type {{select: ((type, id)), delete((type, id)), insert: ((type, id, value))}}
     */
    var Storage = _ember['default'].Service.extend({
        project: _ember['default'].inject.service('current-project'),
        _indexKey: 'resourceIndex',
        index: _ember['default'].computed({
            get: function get(_) {
                var _this = this;

                return this._getItem(this._indexKey)['catch'](function () {
                    return _this._setItem(_this._indexKey, {});
                });
            },
            set: function set(_, value) {
                return this._setItem(this._indexKey, value);
            }
        }),
        /**
         * Retreive all of some type or one if the id is provided
         * @returns {Promise.<Array.<Object>> | Promise.<Object>}
         */
        select: function select(type, id) {
            var _this2 = this;

            if (!id) {
                return this.get('index').then(function (index) {
                    return _ember['default'].RSVP.all((index[type] || []).map(function (id) {
                        return _this2.select(type, id);
                    }));
                });
            }
            return this._getItem(type + ':' + id)['catch'](function (error) {
                if (error instanceof StorageError) {
                    error = new StorageError('record not found', type, id);
                }
                throw error;
            });
        },
        /**
         * Remove an item with this id from this table (type)
         * Returns the record of the item that was removed
         * @returns {Promise.<String>}
         */
        'delete': function _delete(type, id) {
            var _this3 = this;

            return this.get('index').then(function (index) {
                index[type] = index[type].filter(function (_id) {
                    return _id !== id;
                });
                return _this3.set('index', index);
            }).then(function () {
                return _this3._getItem(type + ':' + id)['finally'](function (item) {
                    return _this3._removeItem(type + ':' + id).then(function () {
                        return item;
                    });
                });
            });
        },
        /**
         * Write or overwrite an item with this id into this table (type)
         * Returns the record that was inserted
         * @returns {Promise.<Object>}
         */
        insert: function insert(type, id, value) {
            var _this4 = this;

            return this.get('index').then(function (index) {
                index[type] = index[type] || [];
                if (index[type].indexOf(id) === -1) {
                    index[type].push(id);
                }
                return _this4.set('index', index);
            }).then(function () {
                return _this4._setItem(type + ':' + id, value);
            });
        },
        /**
         * Extend or update attributes of a current record
         * @returns {Promise.<Object>}
         */
        update: function update(type, id, value) {
            var _this5 = this;

            return this.get('index').then(function (index) {
                index[type] = index[type] || [];
                if (index[type].indexOf(id) === -1) {
                    return _ember['default'].RSVP.reject(new StorageError('record not found', type, id));
                }
            }).then(function () {
                return _this5._getItem(type + ':' + id).then(function (record) {
                    return _this5._setItem(type + ':' + id, _lodashLodash['default'].extend(record, value, function (v1, v2) {
                        // reuse the existing value if the new value is null or undefined
                        return v2 === null || v2 === undefined ? v1 : v2;
                    }));
                });
            });
        }
    });

    if (_offlineConfigEnvironment['default'].APP.LOCAL_STORAGE) {
        (function () {
            var LS = window.localStorage;
            // Get and set from localStorage asynchronously
            Storage.reopen({
                _getItem: function _getItem(key) {
                    try {
                        return _ember['default'].RSVP.resolve(JSON.parse(LS.getItem(key) || ''));
                    } catch (error) {
                        return _ember['default'].RSVP.reject(new StorageError('item for key ' + key + ' does not exist'));
                    }
                },
                _removeItem: function _removeItem(key) {
                    LS.removeItem(key);
                    return _ember['default'].RSVP.resolve(key);
                },
                _setItem: function _setItem(key, value) {
                    LS.setItem(key, JSON.stringify(value));
                    return _ember['default'].RSVP.resolve(value);
                }
            });
        })();
    } else if (_offlineConfigEnvironment['default'].APP.MEMORY_STORAGE) {
        _ember['default'].assert('Memory storage is only available during testing', _ember['default'].testing);
        Storage.reopen({
            __dict__: {},
            _getItem: function _getItem(key) {
                if (key in this.__dict__) {
                    return _ember['default'].RSVP.resolve(this.__dict__[key]);
                } else {
                    return _ember['default'].RSVP.reject(new StorageError('item for key ' + key + ' does not exist'));
                }
            },
            _removeItem: function _removeItem(key) {
                delete this.__dict__[key];
                return _ember['default'].RSVP.resolve(key);
            },
            _setItem: function _setItem(key, value) {
                return _ember['default'].RSVP.resolve(this.__dict__[key] = value);
            }
        });
    }

    exports['default'] = Storage;
});
define('offline/services/store', ['exports', 'ember-data', 'ember'], function (exports, _emberData, _ember) {
    function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

    exports['default'] = _emberData['default'].Store.extend({
        storage: _ember['default'].inject.service(),
        /**
         * Push our own fallbacks into the chain to prioritize the inherited model adapter over e.g., 
         * the application adapter
         * @param {String} type Object to lookup (serializer or adapter)
         * @param {String} modelName the object modelName
         * @param {Array} fallbacks the fallback objects to lookup if the lookup for modelName or 'application' fails
         * @return {Ember.Object}
         */
        retrieveManagedInstance: function retrieveManagedInstance(type, modelName, fallbacks) {
            var modelFor = this.modelFor(modelName);
            if (modelFor.fallbackChain) {
                fallbacks.unshift.apply(fallbacks, _toConsumableArray(modelFor.fallbackChain));
            }
            return this._super.apply(this, arguments);
        }
    });
});
define('offline/services/ui-state', ['exports', 'ember'], function (exports, _ember) {

    /**
     * Service to hold ui state that needs to persist across the offline application.
     * This is useful if a component needs to maintain some state between being destroyed
     * and rerendered later.
     *
     * Good rule of thumb is to namespace anything by the component that owns it.
     */
    exports['default'] = _ember['default'].Service.extend({
        projectList: {
            testing: false
        }
    });
});
define('offline/services/upload-status', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Service.extend({});
});
define('offline/session-stores/application', ['exports', 'ember-simple-auth/session-stores/adaptive'], function (exports, _emberSimpleAuthSessionStoresAdaptive) {
  exports['default'] = _emberSimpleAuthSessionStoresAdaptive['default'].extend();
});
define("offline/templates/application", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "offline/templates/application.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "outlet", ["loc", [null, [1, 0], [1, 10]]], 0, 0, 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/authenticated", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "offline/templates/authenticated.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "outlet", ["loc", [null, [1, 0], [1, 10]]], 0, 0, 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/authenticated/admin-loading", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 10,
            "column": 0
          }
        },
        "moduleName": "offline/templates/authenticated/admin-loading.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "loading-screen");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("h1");
        dom.setAttribute(el2, "class", "loading-header");
        var el3 = dom.createTextNode("\n        PROJECT_TITLE\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "loading-footer");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "fa fa-spinner fa-pulse");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "loading-action");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(dom.childAt(fragment, [0, 3, 3]), 0, 0);
        return morphs;
      },
      statements: [["inline", "t", ["Downloading Survey"], [], ["loc", [null, [7, 36], [7, 62]]], 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/authenticated/admin", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 7,
              "column": 8
            },
            "end": {
              "line": 14,
              "column": 8
            }
          },
          "moduleName": "offline/templates/authenticated/admin.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "user-menu-languages");
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "user-menu-footer");
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("button");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element2 = dom.childAt(fragment, [3, 1]);
          var morphs = new Array(3);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
          morphs[1] = dom.createElementMorph(element2);
          morphs[2] = dom.createMorphAt(element2, 0, 0);
          return morphs;
        },
        statements: [["content", "language-menu", ["loc", [null, [9, 16], [9, 33]]], 0, 0, 0, 0], ["element", "action", ["removeSurvey"], [], ["loc", [null, [12, 24], [12, 49]]], 0, 0], ["inline", "t", ["Remove from Device"], [], ["loc", [null, [12, 50], [12, 76]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    var child1 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 16,
              "column": 4
            },
            "end": {
              "line": 20,
              "column": 4
            }
          },
          "moduleName": "offline/templates/authenticated/admin.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "message-low-storage");
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          dom.setAttribute(el2, "class", "message-bold");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode(":");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode(" ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element1 = dom.childAt(fragment, [1]);
          var morphs = new Array(2);
          morphs[0] = dom.createMorphAt(dom.childAt(element1, [1]), 0, 0);
          morphs[1] = dom.createMorphAt(element1, 3, 3);
          return morphs;
        },
        statements: [["inline", "t", ["Storage Warning"], [], ["loc", [null, [18, 39], [18, 62]]], 0, 0], ["inline", "t", ["You are down to 5% on your local storage capacity&#46; Please upload completed surveys&#46;"], [], ["loc", [null, [18, 71], [18, 170]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    var child2 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 23,
              "column": 8
            },
            "end": {
              "line": 33,
              "column": 8
            }
          },
          "moduleName": "offline/templates/authenticated/admin.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "id", "survey-change");
          dom.setAttribute(el1, "class", "status-message");
          dom.setAttribute(el1, "ng-show", "status.survey.changed && !respondents.partials.length");
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          var el3 = dom.createTextNode("\n                ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("b");
          var el4 = dom.createElement("i");
          dom.setAttribute(el4, "class", "fa fa-exclamation");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("p");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n            ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("button");
          dom.setAttribute(el2, "ng-click", "status.survey.changed = false");
          dom.setAttribute(el2, "class", "button-icon");
          var el3 = dom.createTextNode("\n                    ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("i");
          dom.setAttribute(el3, "class", "fa fa-times");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1, 1]);
          var morphs = new Array(2);
          morphs[0] = dom.createMorphAt(dom.childAt(element0, [1]), 1, 1);
          morphs[1] = dom.createMorphAt(dom.childAt(element0, [3]), 0, 0);
          return morphs;
        },
        statements: [["inline", "t", ["FYI, the survey has changed&#46;"], [], ["loc", [null, [26, 52], [26, 92]]], 0, 0], ["inline", "t", ["You might want to practice it again and get familiar with the changes&#46; All partial completes are no longer valid and have been removed&#46;"], [], ["loc", [null, [27, 19], [27, 170]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 52,
            "column": 0
          }
        },
        "moduleName": "offline/templates/authenticated/admin.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "outer-container");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment(" row::Navigation Bar Section ");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("header");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "header-action");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("button");
        var el5 = dom.createElement("i");
        dom.setAttribute(el5, "class", "fa fa-chevron-left");
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode(" ");
        dom.appendChild(el4, el5);
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment(" row::Start/Practice Survey Section ");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "panel-full");
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("h1");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "panel-inner-controls");
        var el4 = dom.createTextNode("\n\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("button");
        dom.setAttribute(el4, "id", "practice-survey");
        dom.setAttribute(el4, "class", "btn btn-default");
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("button");
        dom.setAttribute(el4, "id", "start-survey");
        dom.setAttribute(el4, "class", "btn btn-special");
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "summary-container");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("p");
        dom.setAttribute(el3, "class", "project-version");
        var el4 = dom.createTextNode("Project Version: ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element3 = dom.childAt(fragment, [0]);
        var element4 = dom.childAt(element3, [3]);
        var element5 = dom.childAt(element4, [1, 1]);
        var element6 = dom.childAt(element3, [9]);
        var element7 = dom.childAt(element6, [5]);
        var element8 = dom.childAt(element7, [1]);
        var element9 = dom.childAt(element7, [3]);
        var element10 = dom.childAt(element3, [11]);
        var morphs = new Array(13);
        morphs[0] = dom.createElementMorph(element5);
        morphs[1] = dom.createMorphAt(element5, 2, 2);
        morphs[2] = dom.createMorphAt(element4, 3, 3);
        morphs[3] = dom.createMorphAt(element3, 5, 5);
        morphs[4] = dom.createMorphAt(element6, 1, 1);
        morphs[5] = dom.createMorphAt(dom.childAt(element6, [3]), 0, 0);
        morphs[6] = dom.createElementMorph(element8);
        morphs[7] = dom.createMorphAt(element8, 0, 0);
        morphs[8] = dom.createElementMorph(element9);
        morphs[9] = dom.createMorphAt(element9, 0, 0);
        morphs[10] = dom.createMorphAt(element10, 1, 1);
        morphs[11] = dom.createMorphAt(element10, 3, 3);
        morphs[12] = dom.createMorphAt(dom.childAt(element10, [5]), 1, 1);
        return morphs;
      },
      statements: [["element", "action", ["backToProjects"], [], ["loc", [null, [5, 20], [5, 47]]], 0, 0], ["inline", "t", ["Project List"], [], ["loc", [null, [5, 83], [5, 103]]], 0, 0], ["block", "user-menu", [], [], 0, null, ["loc", [null, [7, 8], [14, 22]]]], ["block", "if", [["get", "capacity.isLow", ["loc", [null, [16, 10], [16, 24]]], 0, 0, 0, 0]], [], 1, null, ["loc", [null, [16, 4], [20, 11]]]], ["block", "if", [["get", "surveyChanged", ["loc", [null, [23, 14], [23, 27]]], 0, 0, 0, 0]], [], 2, null, ["loc", [null, [23, 8], [33, 15]]]], ["content", "project.survey.adminTitle", ["loc", [null, [35, 12], [35, 41]]], 0, 0, 0, 0], ["element", "action", ["practiceSurvey"], [], ["loc", [null, [39, 65], [39, 92]]], 0, 0], ["inline", "t", ["Practice Survey"], [], ["loc", [null, [39, 93], [39, 116]]], 0, 0], ["element", "action", ["startSurvey"], [], ["loc", [null, [41, 62], [41, 86]]], 0, 0], ["inline", "t", ["Start Survey"], [], ["loc", [null, [41, 87], [41, 107]]], 0, 0], ["content", "completes-summary", ["loc", [null, [46, 8], [46, 29]]], 0, 0, 0, 0], ["inline", "partials-summary", [], ["resumeSurvey", ["subexpr", "action", ["resumeSurvey"], [], ["loc", [null, [47, 40], [47, 63]]], 0, 0]], ["loc", [null, [47, 8], [47, 65]]], 0, 0], ["content", "project.survey.version", ["loc", [null, [48, 52], [48, 78]]], 0, 0, 0, 0]],
      locals: [],
      templates: [child0, child1, child2]
    };
  })());
});
define("offline/templates/authenticated/projects", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "offline/templates/authenticated/projects.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["inline", "projects-list", [], ["interviewers", ["subexpr", "@mut", [["get", "model.interviewers", ["loc", [null, [1, 29], [1, 47]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [1, 0], [1, 49]]], 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/authenticated/respview", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        var child0 = (function () {
          return {
            meta: {
              "revision": "Ember@2.9.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 8,
                  "column": 20
                },
                "end": {
                  "line": 12,
                  "column": 20
                }
              },
              "moduleName": "offline/templates/authenticated/respview.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("                        ");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("button");
              dom.setAttribute(el1, "class", "btn btn-primary");
              var el2 = dom.createTextNode("\n                            ");
              dom.appendChild(el1, el2);
              var el2 = dom.createComment("");
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n                        ");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var element3 = dom.childAt(fragment, [1]);
              var morphs = new Array(2);
              morphs[0] = dom.createElementMorph(element3);
              morphs[1] = dom.createMorphAt(element3, 1, 1);
              return morphs;
            },
            statements: [["element", "action", ["endPractice"], [], ["loc", [null, [9, 56], [9, 80]]], 0, 0], ["inline", "t", ["End Practice Mode"], [], ["loc", [null, [10, 28], [10, 53]]], 0, 0]],
            locals: [],
            templates: []
          };
        })();
        var child1 = (function () {
          return {
            meta: {
              "revision": "Ember@2.9.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 12,
                  "column": 20
                },
                "end": {
                  "line": 19,
                  "column": 20
                }
              },
              "moduleName": "offline/templates/authenticated/respview.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("                        ");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("button");
              dom.setAttribute(el1, "id", "exit-survey");
              dom.setAttribute(el1, "class", "btn btn-primary");
              var el2 = dom.createTextNode("\n                            ");
              dom.appendChild(el1, el2);
              var el2 = dom.createComment("");
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n                        ");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n                        ");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("button");
              dom.setAttribute(el1, "id", "save-survey");
              dom.setAttribute(el1, "class", "btn btn-action");
              var el2 = dom.createTextNode("\n                            ");
              dom.appendChild(el1, el2);
              var el2 = dom.createComment("");
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n                        ");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var element1 = dom.childAt(fragment, [1]);
              var element2 = dom.childAt(fragment, [3]);
              var morphs = new Array(4);
              morphs[0] = dom.createElementMorph(element1);
              morphs[1] = dom.createMorphAt(element1, 1, 1);
              morphs[2] = dom.createElementMorph(element2);
              morphs[3] = dom.createMorphAt(element2, 1, 1);
              return morphs;
            },
            statements: [["element", "action", ["endSurvey"], [], ["loc", [null, [13, 73], [13, 95]]], 0, 0], ["inline", "t", ["End Survey"], [], ["loc", [null, [14, 28], [14, 46]]], 0, 0], ["element", "action", ["saveSurvey"], [], ["loc", [null, [16, 72], [16, 95]]], 0, 0], ["inline", "t", ["Save for Later"], [], ["loc", [null, [17, 28], [17, 50]]], 0, 0]],
            locals: [],
            templates: []
          };
        })();
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 6,
                "column": 12
              },
              "end": {
                "line": 21,
                "column": 12
              }
            },
            "moduleName": "offline/templates/authenticated/respview.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "class", "user-menu-body");
            var el2 = dom.createTextNode("\n");
            dom.appendChild(el1, el2);
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("                ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
            return morphs;
          },
          statements: [["block", "if", [["get", "practice", ["loc", [null, [8, 26], [8, 34]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [8, 20], [19, 27]]]]],
          locals: [],
          templates: [child0, child1]
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 5,
              "column": 8
            },
            "end": {
              "line": 22,
              "column": 8
            }
          },
          "moduleName": "offline/templates/authenticated/respview.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["block", "user-menu", [], [], 0, null, ["loc", [null, [6, 12], [21, 26]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child1 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 25,
              "column": 4
            },
            "end": {
              "line": 42,
              "column": 4
            }
          },
          "moduleName": "offline/templates/authenticated/respview.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "ng-if", "!childview.survey");
          dom.setAttribute(el1, "ng-messages", "status.upload");
          dom.setAttribute(el1, "ng-show", "status.online");
          dom.setAttribute(el1, "class", "header-controls");
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          dom.setAttribute(el2, "ng-message", "pending");
          dom.setAttribute(el2, "id", "online-message");
          dom.setAttribute(el2, "class", "status uploading");
          var el3 = dom.createTextNode("\n                ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("i");
          dom.setAttribute(el3, "class", "fa fa-spinner fa-spin");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode(" ");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n            ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          dom.setAttribute(el2, "ng-message", "success");
          dom.setAttribute(el2, "id", "success-message");
          dom.setAttribute(el2, "class", "status uploaded");
          var el3 = dom.createTextNode("\n                ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("i");
          dom.setAttribute(el3, "class", "fa fa-check");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode(" ");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n            ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          dom.setAttribute(el2, "ng-message", "failure");
          dom.setAttribute(el2, "id", "failure-message");
          dom.setAttribute(el2, "class", "status failed");
          var el3 = dom.createTextNode("\n                ");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment(" silently fail ");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n            ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "ng-hide", "status.online");
          var el3 = dom.createTextNode("\n                ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("div");
          dom.setAttribute(el3, "id", "saved");
          dom.setAttribute(el3, "class", "status saved");
          var el4 = dom.createTextNode("\n                    ");
          dom.appendChild(el3, el4);
          var el4 = dom.createElement("i");
          dom.setAttribute(el4, "class", "fa fa-check");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode(" ");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode("\n                ");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n            ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1]);
          var morphs = new Array(3);
          morphs[0] = dom.createMorphAt(dom.childAt(element0, [1]), 3, 3);
          morphs[1] = dom.createMorphAt(dom.childAt(element0, [3]), 3, 3);
          morphs[2] = dom.createMorphAt(dom.childAt(element0, [7, 1]), 3, 3);
          return morphs;
        },
        statements: [["inline", "t", ["Uploading"], [], ["loc", [null, [28, 54], [28, 71]]], 0, 0], ["inline", "t", ["Survey Uploaded"], [], ["loc", [null, [31, 44], [31, 67]]], 0, 0], ["inline", "t", ["Survey Saved"], [], ["loc", [null, [38, 48], [38, 68]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 48,
            "column": 0
          }
        },
        "moduleName": "offline/templates/authenticated/respview.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "outer-container");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment(" navbar section ");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("header");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("h1");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment(" navbar section ");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment(" the survey, errorpage, or exitpage ");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "id", "survey");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element4 = dom.childAt(fragment, [0]);
        var element5 = dom.childAt(element4, [3]);
        var morphs = new Array(4);
        morphs[0] = dom.createMorphAt(dom.childAt(element5, [1]), 0, 0);
        morphs[1] = dom.createMorphAt(element5, 3, 3);
        morphs[2] = dom.createMorphAt(element4, 7, 7);
        morphs[3] = dom.createMorphAt(dom.childAt(element4, [11]), 1, 1);
        return morphs;
      },
      statements: [["content", "project.survey.adminTitle", ["loc", [null, [4, 12], [4, 41]]], 0, 0, 0, 0], ["block", "unless", [["get", "project.respondent.completed", ["loc", [null, [5, 18], [5, 46]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [5, 8], [22, 19]]]], ["block", "if", [["get", "network.online", ["loc", [null, [25, 10], [25, 24]]], 0, 0, 0, 0]], [], 1, null, ["loc", [null, [25, 4], [42, 11]]]], ["content", "outlet", ["loc", [null, [45, 8], [45, 18]]], 0, 0, 0, 0]],
      locals: [],
      templates: [child0, child1]
    };
  })());
});
define("offline/templates/authenticated/respview/error", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 7,
              "column": 4
            },
            "end": {
              "line": 15,
              "column": 4
            }
          },
          "moduleName": "offline/templates/authenticated/respview/error.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "id", "code");
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("code");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          dom.setAttribute(el2, "id", "name");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode(":");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          dom.setAttribute(el2, "id", "message");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [3]);
          var morphs = new Array(3);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1, 1]), 0, 0);
          morphs[1] = dom.createMorphAt(dom.childAt(element0, [1]), 0, 0);
          morphs[2] = dom.createMorphAt(dom.childAt(element0, [3]), 0, 0);
          return morphs;
        },
        statements: [["content", "model.code", ["loc", [null, [9, 18], [9, 34]]], 0, 0, 0, 0], ["content", "model.name", ["loc", [null, [12, 28], [12, 44]]], 0, 0, 0, 0], ["content", "model.message", ["loc", [null, [13, 31], [13, 50]]], 0, 0, 0, 0]],
        locals: [],
        templates: []
      };
    })();
    var child1 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 15,
              "column": 4
            },
            "end": {
              "line": 19,
              "column": 4
            }
          },
          "moduleName": "offline/templates/authenticated/respview/error.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "id", "code");
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("pre");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1, 1]), 0, 0);
          return morphs;
        },
        statements: [["content", "model.stack", ["loc", [null, [17, 17], [17, 34]]], 0, 0, 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 22,
            "column": 0
          }
        },
        "moduleName": "offline/templates/authenticated/respview/error.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "errorpage");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("h1");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("p");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element1 = dom.childAt(fragment, [0]);
        var element2 = dom.childAt(element1, [1]);
        var morphs = new Array(3);
        morphs[0] = dom.createMorphAt(dom.childAt(element2, [1]), 0, 0);
        morphs[1] = dom.createMorphAt(dom.childAt(element2, [3]), 0, 0);
        morphs[2] = dom.createMorphAt(dom.childAt(element1, [3]), 1, 1);
        return morphs;
      },
      statements: [["inline", "t", ["Hmm, there seems to be a problem&#8230;"], [], ["loc", [null, [3, 12], [3, 59]]], 0, 0], ["inline", "t", ["An error occurred when executing code from the survey&#46;"], [], ["loc", [null, [4, 11], [4, 77]]], 0, 0], ["block", "if", [["get", "model.code", ["loc", [null, [7, 10], [7, 20]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [7, 4], [19, 11]]]]],
      locals: [],
      templates: [child0, child1]
    };
  })());
});
define("offline/templates/authenticated/respview/exitpage", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 1,
            "column": 94
          }
        },
        "moduleName": "offline/templates/authenticated/respview/exitpage.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        dom.insertBoundary(fragment, null);
        return morphs;
      },
      statements: [["inline", "exit-page", [], ["exitpage", ["subexpr", "@mut", [["get", "model", ["loc", [null, [1, 21], [1, 26]]], 0, 0, 0, 0]], [], [], 0, 0], "goHome", ["subexpr", "action", ["goHome"], [], ["loc", [null, [1, 34], [1, 51]]], 0, 0], "startNewSurvey", ["subexpr", "action", ["startNewSurvey"], [], ["loc", [null, [1, 67], [1, 92]]], 0, 0]], ["loc", [null, [1, 0], [1, 94]]], 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/authenticated/respview/survey", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 7,
              "column": 12
            },
            "end": {
              "line": 11,
              "column": 12
            }
          },
          "moduleName": "offline/templates/authenticated/respview/survey.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("                ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("button");
          dom.setAttribute(el1, "class", "btn-transparent");
          dom.setAttribute(el1, "type", "button");
          var el2 = dom.createTextNode("\n                    ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1]);
          var morphs = new Array(2);
          morphs[0] = dom.createElementMorph(element0);
          morphs[1] = dom.createMorphAt(element0, 1, 1);
          return morphs;
        },
        statements: [["element", "action", ["goBack"], [], ["loc", [null, [8, 62], [8, 81]]], 0, 0], ["inline", "t", ["Previous"], [], ["loc", [null, [9, 20], [9, 36]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 18,
            "column": 0
          }
        },
        "moduleName": "offline/templates/authenticated/respview/survey.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("form");
        dom.setAttribute(el1, "name", "surveyForm");
        dom.setAttribute(el1, "id", "surveyFormId");
        dom.setAttribute(el1, "novalidate", "");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "survey-page");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("footer");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "footer_button-wrapper");
        var el4 = dom.createTextNode("\n");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("button");
        dom.setAttribute(el4, "class", "button-footer");
        dom.setAttribute(el4, "type", "submit");
        var el5 = dom.createTextNode("\n                ");
        dom.appendChild(el4, el5);
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n            ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element1 = dom.childAt(fragment, [0]);
        var element2 = dom.childAt(element1, [3, 1]);
        var element3 = dom.childAt(element2, [3]);
        var morphs = new Array(4);
        morphs[0] = dom.createMorphAt(dom.childAt(element1, [1]), 1, 1);
        morphs[1] = dom.createMorphAt(element2, 1, 1);
        morphs[2] = dom.createElementMorph(element3);
        morphs[3] = dom.createMorphAt(element3, 1, 1);
        return morphs;
      },
      statements: [["content", "outlet", ["loc", [null, [3, 8], [3, 18]]], 0, 0, 0, 0], ["block", "if", [["get", "survey.canGoPrev", ["loc", [null, [7, 18], [7, 34]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [7, 12], [11, 19]]]], ["element", "action", ["goNext"], [], ["loc", [null, [12, 56], [12, 75]]], 0, 0], ["inline", "if", [["get", "survey.canGoNext", ["loc", [null, [13, 21], [13, 37]]], 0, 0, 0, 0], ["subexpr", "t", ["Next"], [], ["loc", [null, [13, 38], [13, 48]]], 0, 0], ["subexpr", "t", ["Finish"], [], ["loc", [null, [13, 49], [13, 61]]], 0, 0]], [], ["loc", [null, [13, 16], [13, 63]]], 0, 0]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("offline/templates/authenticated/respview/survey/page", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 2,
              "column": 4
            },
            "end": {
              "line": 5,
              "column": 4
            }
          },
          "moduleName": "offline/templates/authenticated/respview/survey/page.hbs"
        },
        isEmpty: false,
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [["inline", "component", [["subexpr", "question-component-name", [["get", "question", ["loc", [null, [4, 45], [4, 53]]], 0, 0, 0, 0]], [], ["loc", [null, [4, 20], [4, 54]]], 0, 0]], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [4, 64], [4, 72]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [4, 8], [4, 74]]], 0, 0]],
        locals: ["question"],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 7,
            "column": 0
          }
        },
        "moduleName": "offline/templates/authenticated/respview/survey/page.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "question-container");
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(dom.childAt(fragment, [0]), 1, 1);
        return morphs;
      },
      statements: [["block", "each", [["get", "currentpage.elements", ["loc", [null, [2, 12], [2, 32]]], 0, 0, 0, 0]], ["key", "label"], 0, null, ["loc", [null, [2, 4], [5, 13]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("offline/templates/components/atm1d-element", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        var child0 = (function () {
          return {
            meta: {
              "revision": "Ember@2.9.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 15,
                  "column": 28
                },
                "end": {
                  "line": 19,
                  "column": 28
                }
              },
              "moduleName": "offline/templates/components/atm1d-element.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("                                ");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("div");
              dom.setAttribute(el1, "class", "sq-atm1d-openEnd");
              var el2 = dom.createTextNode("\n                                    ");
              dom.appendChild(el1, el2);
              var el2 = dom.createComment("");
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n                                ");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var morphs = new Array(1);
              morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
              return morphs;
            },
            statements: [["inline", "text-input", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [17, 58], [17, 66]]], 0, 0, 0, 0]], [], [], 0, 0], "row", ["subexpr", "@mut", [["get", "row", ["loc", [null, [17, 71], [17, 74]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [17, 86], [17, 100]]], 0, 0, 0, 0], ["get", "row.label", ["loc", [null, [17, 101], [17, 110]]], 0, 0, 0, 0], "oe"], [], ["loc", [null, [17, 78], [17, 116]]], 0, 0], "open", true], ["loc", [null, [17, 36], [17, 128]]], 0, 0]],
            locals: [],
            templates: []
          };
        })();
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 5,
                "column": 8
              },
              "end": {
                "line": 28,
                "column": 8
              }
            },
            "moduleName": "offline/templates/components/atm1d-element.hbs"
          },
          isEmpty: false,
          arity: 1,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("            ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("li");
            dom.setAttribute(el1, "class", "sq-atm1d-row");
            var el2 = dom.createTextNode("\n                ");
            dom.appendChild(el1, el2);
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("label");
            var el3 = dom.createTextNode("\n                    ");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("div");
            dom.setAttribute(el3, "class", "sq-atm1d-table");
            var el4 = dom.createTextNode("\n                        ");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("div");
            dom.setAttribute(el4, "class", "sq-atm1d-tr sq-atm1d-tr-top");
            var el5 = dom.createTextNode("\n                            ");
            dom.appendChild(el4, el5);
            var el5 = dom.createElement("div");
            dom.setAttribute(el5, "class", "sq-atm1d-td sq-atm1d-button-icon");
            dom.appendChild(el4, el5);
            var el5 = dom.createTextNode("\n                            ");
            dom.appendChild(el4, el5);
            var el5 = dom.createElement("div");
            var el6 = dom.createTextNode("\n                                ");
            dom.appendChild(el5, el6);
            var el6 = dom.createElement("div");
            dom.setAttribute(el6, "class", "cdata sq-atm1d-legend");
            var el7 = dom.createComment("");
            dom.appendChild(el6, el7);
            dom.appendChild(el5, el6);
            var el6 = dom.createTextNode("\n");
            dom.appendChild(el5, el6);
            var el6 = dom.createComment("");
            dom.appendChild(el5, el6);
            var el6 = dom.createTextNode("                            ");
            dom.appendChild(el5, el6);
            dom.appendChild(el4, el5);
            var el5 = dom.createTextNode("\n                        ");
            dom.appendChild(el4, el5);
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n                        ");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("div");
            dom.setAttribute(el4, "class", "sq-atm1d-tr sq-atm1d-tr-bottom");
            var el5 = dom.createTextNode("\n                            ");
            dom.appendChild(el4, el5);
            var el5 = dom.createElement("div");
            dom.setAttribute(el5, "class", "sq-atm1d-td sq-atm1d-button-icon");
            dom.appendChild(el4, el5);
            var el5 = dom.createTextNode("\n                        ");
            dom.appendChild(el4, el5);
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n                    ");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n                ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element0 = dom.childAt(fragment, [1]);
            var element1 = dom.childAt(element0, [3]);
            var element2 = dom.childAt(element1, [1, 1, 3]);
            var morphs = new Array(6);
            morphs[0] = dom.createMorphAt(element0, 1, 1);
            morphs[1] = dom.createAttrMorph(element1, 'for');
            morphs[2] = dom.createAttrMorph(element1, 'class');
            morphs[3] = dom.createAttrMorph(element2, 'class');
            morphs[4] = dom.createUnsafeMorphAt(dom.childAt(element2, [1]), 0, 0);
            morphs[5] = dom.createMorphAt(element2, 3, 3);
            return morphs;
          },
          statements: [["inline", "component", [["subexpr", "concat", [["get", "question.type", ["loc", [null, [7, 36], [7, 49]]], 0, 0, 0, 0], "-input"], [], ["loc", [null, [7, 28], [7, 59]]], 0, 0]], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [7, 69], [7, 77]]], 0, 0, 0, 0]], [], [], 0, 0], "row", ["subexpr", "@mut", [["get", "row", ["loc", [null, [7, 82], [7, 85]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [7, 97], [7, 111]]], 0, 0, 0, 0], ["get", "row.label", ["loc", [null, [7, 112], [7, 121]]], 0, 0, 0, 0]], [], ["loc", [null, [7, 89], [7, 122]]], 0, 0]], ["loc", [null, [7, 16], [7, 124]]], 0, 0], ["attribute", "for", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [8, 36], [8, 50]]], 0, 0, 0, 0], ["get", "row.label", ["loc", [null, [8, 51], [8, 60]]], 0, 0, 0, 0]], [], ["loc", [null, [null, null], [8, 62]]], 0, 0], 0, 0, 0, 0], ["attribute", "class", ["concat", ["sq-atm1d-button clickable ", ["subexpr", "if", [["get", "row.open", ["loc", [null, [9, 61], [9, 69]]], 0, 0, 0, 0], "sq-atm1d-open"], [], ["loc", [null, [9, 56], [9, 87]]], 0, 0], " ", ["subexpr", "if", [["get", "row.openOptional", ["loc", [null, [9, 93], [9, 109]]], 0, 0, 0, 0], "sq-atm1d-openOptional"], [], ["loc", [null, [9, 88], [9, 135]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["attribute", "class", ["concat", ["sq-atm1d-td sq-atm1d-content sq-atm1d-content-align-", ["get", "rendered.classes.contentAlign", ["loc", [null, [13, 94], [13, 123]]], 0, 0, 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["content", "row.displayCdata", ["loc", [null, [14, 67], [14, 89]]], 0, 0, 0, 0], ["block", "if", [["get", "row.open", ["loc", [null, [15, 34], [15, 42]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [15, 28], [19, 35]]]]],
          locals: ["row"],
          templates: [child0]
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 1,
              "column": 0
            },
            "end": {
              "line": 31,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/atm1d-element.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "sq-atm1d-block");
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("ul");
          var el3 = dom.createTextNode("\n");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("        ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element3 = dom.childAt(fragment, [1]);
          var element4 = dom.childAt(element3, [3]);
          var morphs = new Array(3);
          morphs[0] = dom.createMorphAt(element3, 1, 1);
          morphs[1] = dom.createAttrMorph(element4, 'class');
          morphs[2] = dom.createMorphAt(element4, 1, 1);
          return morphs;
        },
        statements: [["inline", "question-error", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [3, 34], [3, 42]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [3, 8], [3, 44]]], 0, 0], ["attribute", "class", ["concat", ["sq-atm1d-buttons sq-atm1d-", ["get", "rendered.classes.viewMode", ["loc", [null, [4, 47], [4, 72]]], 0, 0, 0, 0], " sq-atm1d-button-align-", ["get", "rendered.classes.buttonAlign", ["loc", [null, [4, 99], [4, 127]]], 0, 0, 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["block", "each", [["get", "question.enabled.rows", ["loc", [null, [5, 16], [5, 37]]], 0, 0, 0, 0]], ["key", "label"], 0, null, ["loc", [null, [5, 8], [28, 17]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 32,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/atm1d-element.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        dom.insertBoundary(fragment, null);
        return morphs;
      },
      statements: [["block", "survey-element", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [1, 27], [1, 35]]], 0, 0, 0, 0]], [], [], 0, 0]], 0, null, ["loc", [null, [1, 0], [31, 19]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("offline/templates/components/completes-summary", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 3,
                "column": 4
              },
              "end": {
                "line": 9,
                "column": 4
              }
            },
            "moduleName": "offline/templates/components/completes-summary.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "id", "online-message");
            dom.setAttribute(el1, "class", "status-message");
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("span");
            var el3 = dom.createTextNode("\n                ");
            dom.appendChild(el2, el3);
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n            ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n        ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1, 1]), 1, 1);
            return morphs;
          },
          statements: [["inline", "t", ["It looks like you're online&#46; We'll go ahead and upload your completed surveys now&#46;"], [], ["loc", [null, [6, 16], [6, 114]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      var child1 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 10,
                "column": 4
              },
              "end": {
                "line": 19,
                "column": 4
              }
            },
            "moduleName": "offline/templates/components/completes-summary.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "id", "success-message");
            dom.setAttribute(el1, "class", "status-message");
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("span");
            var el3 = dom.createTextNode("\n                ");
            dom.appendChild(el2, el3);
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n            ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("button");
            dom.setAttribute(el2, "class", "btn-transparent");
            var el3 = dom.createTextNode("\n                ");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("i");
            dom.setAttribute(el3, "class", "fa fa-times");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n            ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n        ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element10 = dom.childAt(fragment, [1]);
            var element11 = dom.childAt(element10, [3]);
            var morphs = new Array(2);
            morphs[0] = dom.createUnsafeMorphAt(dom.childAt(element10, [1]), 1, 1);
            morphs[1] = dom.createElementMorph(element11);
            return morphs;
          },
          statements: [["inline", "t", ["<b>Success!</b> Your completed surveys have been uploaded&#46;"], [], ["loc", [null, [13, 16], [13, 88]]], 0, 0], ["element", "action", ["clearStatus", "success"], [], ["loc", [null, [15, 44], [15, 78]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      var child2 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 20,
                "column": 4
              },
              "end": {
                "line": 29,
                "column": 4
              }
            },
            "moduleName": "offline/templates/components/completes-summary.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "id", "failure-message");
            dom.setAttribute(el1, "class", "status-message");
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("span");
            var el3 = dom.createTextNode("\n                ");
            dom.appendChild(el2, el3);
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n            ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("button");
            dom.setAttribute(el2, "class", "btn-transparent");
            var el3 = dom.createTextNode("\n                ");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("i");
            dom.setAttribute(el3, "class", "fa fa-times");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n            ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n        ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element8 = dom.childAt(fragment, [1]);
            var element9 = dom.childAt(element8, [3]);
            var morphs = new Array(2);
            morphs[0] = dom.createMorphAt(dom.childAt(element8, [1]), 1, 1);
            morphs[1] = dom.createElementMorph(element9);
            return morphs;
          },
          statements: [["inline", "t", ["Uh Oh! Upload failed&#46;<br/> Please try again later when you are online&#46;"], [], ["loc", [null, [23, 16], [23, 102]]], 0, 0], ["element", "action", ["clearStatus", "failure"], [], ["loc", [null, [25, 44], [25, 78]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      var child3 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 30,
                "column": 4
              },
              "end": {
                "line": 39,
                "column": 4
              }
            },
            "moduleName": "offline/templates/components/completes-summary.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "id", "errcode-message");
            dom.setAttribute(el1, "class", "status-message");
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("span");
            var el3 = dom.createTextNode("\n                ");
            dom.appendChild(el2, el3);
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n            ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("button");
            dom.setAttribute(el2, "class", "btn-transparent");
            var el3 = dom.createTextNode("\n                ");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("i");
            dom.setAttribute(el3, "class", "fa fa-times");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n            ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n        ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element6 = dom.childAt(fragment, [1]);
            var element7 = dom.childAt(element6, [3]);
            var morphs = new Array(2);
            morphs[0] = dom.createMorphAt(dom.childAt(element6, [1]), 1, 1);
            morphs[1] = dom.createElementMorph(element7);
            return morphs;
          },
          statements: [["inline", "t", ["Uh Oh! An error has occurred when attempting to upload the data&#46;"], [], ["loc", [null, [33, 16], [33, 92]]], 0, 0], ["element", "action", ["clearStatus", "errcode"], [], ["loc", [null, [35, 44], [35, 78]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 1,
              "column": 0
            },
            "end": {
              "line": 41,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/completes-summary.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "role", "alert");
          var el2 = dom.createTextNode("\n");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element12 = dom.childAt(fragment, [1]);
          var morphs = new Array(4);
          morphs[0] = dom.createMorphAt(element12, 1, 1);
          morphs[1] = dom.createMorphAt(element12, 2, 2);
          morphs[2] = dom.createMorphAt(element12, 3, 3);
          morphs[3] = dom.createMorphAt(element12, 4, 4);
          return morphs;
        },
        statements: [["block", "if", [["get", "anyUploading", ["loc", [null, [3, 10], [3, 22]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [3, 4], [9, 11]]]], ["block", "if", [["get", "status.completes.success", ["loc", [null, [10, 10], [10, 34]]], 0, 0, 0, 0]], [], 1, null, ["loc", [null, [10, 4], [19, 11]]]], ["block", "if", [["get", "status.completes.failure", ["loc", [null, [20, 10], [20, 34]]], 0, 0, 0, 0]], [], 2, null, ["loc", [null, [20, 4], [29, 11]]]], ["block", "if", [["get", "status.completes.errcode", ["loc", [null, [30, 10], [30, 34]]], 0, 0, 0, 0]], [], 3, null, ["loc", [null, [30, 4], [39, 11]]]]],
        locals: [],
        templates: [child0, child1, child2, child3]
      };
    })();
    var child1 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 44,
                "column": 4
              },
              "end": {
                "line": 49,
                "column": 4
              }
            },
            "moduleName": "offline/templates/components/completes-summary.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "id", "offline-message");
            dom.setAttribute(el1, "class", "status-message");
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("span");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n        ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element4 = dom.childAt(fragment, [1]);
            var element5 = dom.childAt(element4, [3]);
            var morphs = new Array(3);
            morphs[0] = dom.createMorphAt(element4, 1, 1);
            morphs[1] = dom.createElementMorph(element5);
            morphs[2] = dom.createMorphAt(element5, 0, 0);
            return morphs;
          },
          statements: [["inline", "t", ["It looks like you're offline right now&#46; We'll automatically upload your responses when you get online&#46;"], [], ["loc", [null, [46, 12], [46, 130]]], 0, 0], ["element", "action", ["attemptUpload"], [], ["loc", [null, [47, 18], [47, 44]]], 0, 0], ["inline", "t", ["Click here to try uploading now&#46;"], [], ["loc", [null, [47, 45], [47, 89]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      var child1 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 49,
                "column": 4
              },
              "end": {
                "line": 54,
                "column": 4
              }
            },
            "moduleName": "offline/templates/components/completes-summary.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "id", "offline-message-2");
            dom.setAttribute(el1, "class", "status-message");
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("span");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n        ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element2 = dom.childAt(fragment, [1]);
            var element3 = dom.childAt(element2, [3]);
            var morphs = new Array(3);
            morphs[0] = dom.createUnsafeMorphAt(element2, 1, 1);
            morphs[1] = dom.createElementMorph(element3);
            morphs[2] = dom.createMorphAt(element3, 0, 0);
            return morphs;
          },
          statements: [["inline", "t", ["<b>Nope&#46; Still offline&#46;</b> We'll automatically upload your responses when you get online&#46;"], [], ["loc", [null, [51, 12], [51, 124]]], 0, 0], ["element", "action", ["attemptUpload"], [], ["loc", [null, [52, 18], [52, 44]]], 0, 0], ["inline", "t", ["Click here to try again&#46;"], [], ["loc", [null, [52, 45], [52, 81]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 42,
              "column": 0
            },
            "end": {
              "line": 56,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/completes-summary.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "role", "alert");
          var el2 = dom.createTextNode("\n");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
          return morphs;
        },
        statements: [["block", "unless", [["get", "uploadAttempted", ["loc", [null, [44, 14], [44, 29]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [44, 4], [54, 15]]]]],
        locals: [],
        templates: [child0, child1]
      };
    })();
    var child2 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 65,
              "column": 8
            },
            "end": {
              "line": 72,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/completes-summary.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "upload-status-row");
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "id", "waiting");
          dom.setAttribute(el2, "class", "upload-status-cell");
          var el3 = dom.createTextNode("\n                    ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("span");
          dom.setAttribute(el3, "class", "status-bold");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                    ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("span");
          dom.setAttribute(el3, "class", "status-action");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element1 = dom.childAt(fragment, [1, 1]);
          var morphs = new Array(2);
          morphs[0] = dom.createMorphAt(dom.childAt(element1, [1]), 0, 0);
          morphs[1] = dom.createMorphAt(dom.childAt(element1, [3]), 0, 0);
          return morphs;
        },
        statements: [["content", "completes.length", ["loc", [null, [68, 46], [68, 68]]], 0, 0, 0, 0], ["inline", "t", ["Waiting to be uploaded&#8230;"], [], ["loc", [null, [69, 48], [69, 85]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    var child3 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 73,
              "column": 8
            },
            "end": {
              "line": 80,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/completes-summary.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "upload-status-row");
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "id", "uploading");
          dom.setAttribute(el2, "class", "upload-status-cell");
          var el3 = dom.createTextNode("\n                    ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("i");
          dom.setAttribute(el3, "class", "fa fa-spinner fa-spin");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                    ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("span");
          dom.setAttribute(el3, "class", "status-action");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1, 1, 3]), 0, 0);
          return morphs;
        },
        statements: [["inline", "t", ["Uploading"], [], ["loc", [null, [77, 48], [77, 65]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    var child4 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 81,
              "column": 8
            },
            "end": {
              "line": 88,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/completes-summary.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "upload-status-row fadeout");
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "id", "uploaded");
          dom.setAttribute(el2, "class", "upload-status-cell");
          var el3 = dom.createTextNode("\n                    ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("span");
          dom.setAttribute(el3, "class", "status-bold");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                    ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("span");
          dom.setAttribute(el3, "class", "status-action");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1, 1]);
          var morphs = new Array(2);
          morphs[0] = dom.createMorphAt(dom.childAt(element0, [1]), 0, 0);
          morphs[1] = dom.createMorphAt(dom.childAt(element0, [3]), 0, 0);
          return morphs;
        },
        statements: [["content", "status.completes.count", ["loc", [null, [84, 46], [84, 74]]], 0, 0, 0, 0], ["inline", "t", ["Uploaded to server"], [], ["loc", [null, [85, 48], [85, 74]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 91,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/completes-summary.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("    ");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "panel-inner");
        var el2 = dom.createTextNode("\n        ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("span");
        dom.setAttribute(el2, "class", "status-count");
        var el3 = dom.createTextNode("\n            ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n        ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("span");
        dom.setAttribute(el2, "class", "status-type");
        var el3 = dom.createTextNode("\n            ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n        ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "upload-status-table");
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("        ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element13 = dom.childAt(fragment, [3]);
        var element14 = dom.childAt(element13, [5]);
        var morphs = new Array(7);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        morphs[1] = dom.createMorphAt(fragment, 1, 1, contextualElement);
        morphs[2] = dom.createMorphAt(dom.childAt(element13, [1]), 1, 1);
        morphs[3] = dom.createMorphAt(dom.childAt(element13, [3]), 1, 1);
        morphs[4] = dom.createMorphAt(element14, 1, 1);
        morphs[5] = dom.createMorphAt(element14, 2, 2);
        morphs[6] = dom.createMorphAt(element14, 3, 3);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["block", "if", [["get", "network.online", ["loc", [null, [1, 6], [1, 20]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [1, 0], [41, 7]]]], ["block", "if", [["get", "willAttemptLater", ["loc", [null, [42, 6], [42, 22]]], 0, 0, 0, 0]], [], 1, null, ["loc", [null, [42, 0], [56, 7]]]], ["content", "project.interviewer.total_completes", ["loc", [null, [59, 12], [59, 53]]], 0, 0, 0, 0], ["inline", "t", ["Completed surveys"], [], ["loc", [null, [62, 12], [62, 37]]], 0, 0], ["block", "if", [["get", "completesInQueue", ["loc", [null, [65, 14], [65, 30]]], 0, 0, 0, 0]], [], 2, null, ["loc", [null, [65, 8], [72, 15]]]], ["block", "if", [["get", "anyUploading", ["loc", [null, [73, 14], [73, 26]]], 0, 0, 0, 0]], [], 3, null, ["loc", [null, [73, 8], [80, 15]]]], ["block", "if", [["get", "anyUploaded", ["loc", [null, [81, 14], [81, 25]]], 0, 0, 0, 0]], [], 4, null, ["loc", [null, [81, 8], [88, 15]]]]],
      locals: [],
      templates: [child0, child1, child2, child3, child4]
    };
  })());
});
define("offline/templates/components/exit-page", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 9,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/exit-page.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "survey-complete-body");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("h1");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("p");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "survey-complete-footer");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("button");
        dom.setAttribute(el2, "class", "btn btn-link");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("button");
        dom.setAttribute(el2, "class", "btn btn-special");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0]);
        var element1 = dom.childAt(fragment, [2]);
        var element2 = dom.childAt(element1, [1]);
        var element3 = dom.childAt(element1, [3]);
        var morphs = new Array(6);
        morphs[0] = dom.createMorphAt(dom.childAt(element0, [1]), 0, 0);
        morphs[1] = dom.createMorphAt(dom.childAt(element0, [3]), 0, 0);
        morphs[2] = dom.createElementMorph(element2);
        morphs[3] = dom.createMorphAt(element2, 0, 0);
        morphs[4] = dom.createElementMorph(element3);
        morphs[5] = dom.createMorphAt(element3, 0, 0);
        return morphs;
      },
      statements: [["inline", "t", ["Survey Completed - Thank You"], [], ["loc", [null, [2, 8], [2, 44]]], 0, 0], ["content", "surveyCompleteMessage", ["loc", [null, [3, 7], [3, 32]]], 0, 0, 0, 0], ["element", "action", ["goHome"], [], ["loc", [null, [6, 33], [6, 52]]], 0, 0], ["inline", "t", ["Home"], [], ["loc", [null, [6, 53], [6, 65]]], 0, 0], ["element", "action", ["startNewSurvey"], [], ["loc", [null, [7, 36], [7, 63]]], 0, 0], ["inline", "t", ["Start New Survey"], [], ["loc", [null, [7, 64], [7, 88]]], 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/components/forgot-form", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 8,
              "column": 4
            },
            "end": {
              "line": 12,
              "column": 4
            }
          },
          "moduleName": "offline/templates/components/forgot-form.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("p");
          dom.setAttribute(el2, "class", "success-message");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1, 1]), 0, 0);
          return morphs;
        },
        statements: [["content", "successMsg", ["loc", [null, [10, 39], [10, 53]]], 0, 0, 0, 0]],
        locals: [],
        templates: []
      };
    })();
    var child1 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 16,
                "column": 12
              },
              "end": {
                "line": 18,
                "column": 12
              }
            },
            "moduleName": "offline/templates/components/forgot-form.hbs"
          },
          isEmpty: false,
          arity: 1,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("p");
            dom.setAttribute(el1, "class", "error-message");
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 0, 0);
            return morphs;
          },
          statements: [["content", "errorMsg", ["loc", [null, [17, 41], [17, 53]]], 0, 0, 0, 0]],
          locals: ["errorMsg"],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 14,
              "column": 4
            },
            "end": {
              "line": 20,
              "column": 4
            }
          },
          "moduleName": "offline/templates/components/forgot-form.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          var el2 = dom.createTextNode("\n");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
          return morphs;
        },
        statements: [["block", "each", [["get", "errorMsgs", ["loc", [null, [16, 20], [16, 29]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [16, 12], [18, 21]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child2 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 28,
              "column": 8
            },
            "end": {
              "line": 28,
              "column": 76
            }
          },
          "moduleName": "offline/templates/components/forgot-form.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["inline", "t", ["Back to Login"], [], ["loc", [null, [28, 55], [28, 76]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 31,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/forgot-form.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "login-form");
        var el2 = dom.createTextNode("\n\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "forgot-form-wrapper");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("h2");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("p");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("form");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "form-group");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("label");
        dom.setAttribute(el4, "for", "email");
        dom.setAttribute(el4, "class", "login-label");
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("button");
        dom.setAttribute(el3, "class", "btn btn-login");
        dom.setAttribute(el3, "type", "submit");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0]);
        var element1 = dom.childAt(element0, [1]);
        var element2 = dom.childAt(element0, [7]);
        var element3 = dom.childAt(element2, [1]);
        var morphs = new Array(9);
        morphs[0] = dom.createMorphAt(dom.childAt(element1, [1]), 0, 0);
        morphs[1] = dom.createMorphAt(dom.childAt(element1, [3]), 0, 0);
        morphs[2] = dom.createMorphAt(element0, 3, 3);
        morphs[3] = dom.createMorphAt(element0, 5, 5);
        morphs[4] = dom.createElementMorph(element2);
        morphs[5] = dom.createMorphAt(dom.childAt(element3, [1]), 0, 0);
        morphs[6] = dom.createMorphAt(element3, 3, 3);
        morphs[7] = dom.createMorphAt(dom.childAt(element2, [3]), 0, 0);
        morphs[8] = dom.createMorphAt(element2, 5, 5);
        return morphs;
      },
      statements: [["inline", "t", ["Forgotten Password"], [], ["loc", [null, [4, 12], [4, 38]]], 0, 0], ["inline", "t", ["Enter the email address you registered your account with below&#46; You will be sent an email containing information on how to reset your password&#46;"], [], ["loc", [null, [5, 11], [5, 170]]], 0, 0], ["block", "if", [["get", "successMsg", ["loc", [null, [8, 10], [8, 20]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [8, 4], [12, 11]]]], ["block", "if", [["get", "errorMsgs", ["loc", [null, [14, 10], [14, 19]]], 0, 0, 0, 0]], [], 1, null, ["loc", [null, [14, 4], [20, 11]]]], ["element", "action", ["validate"], ["on", "submit"], ["loc", [null, [22, 10], [22, 43]]], 0, 0], ["inline", "t", ["Email"], [], ["loc", [null, [24, 51], [24, 64]]], 0, 0], ["inline", "input", [], ["id", "email", "class", "form-control login-input", "value", ["subexpr", "@mut", [["get", "email", ["loc", [null, [25, 70], [25, 75]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [25, 12], [25, 77]]], 0, 0], ["inline", "t", ["Reset"], [], ["loc", [null, [27, 52], [27, 65]]], 0, 0], ["block", "link-to", ["login"], ["class", "btn btn-link block"], 2, null, ["loc", [null, [28, 8], [28, 88]]]]],
      locals: [],
      templates: [child0, child1, child2]
    };
  })());
});
define("offline/templates/components/html-element", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/html-element.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("h2");
        dom.setAttribute(el1, "class", "comment-text");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createUnsafeMorphAt(dom.childAt(fragment, [0]), 0, 0);
        return morphs;
      },
      statements: [["content", "question.displayCdata", ["loc", [null, [1, 25], [1, 52]]], 0, 0, 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/components/image-element", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/image-element.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("h2");
        dom.setAttribute(el1, "class", "image-text");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(dom.childAt(fragment, [0]), 0, 0);
        return morphs;
      },
      statements: [["inline", "Hello", [["get", "World", ["loc", [null, [1, 31], [1, 36]]], 0, 0, 0, 0]], [], ["loc", [null, [1, 23], [1, 38]]], 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/components/imgupload-element", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 1,
            "column": 20
          }
        },
        "moduleName": "offline/templates/components/imgupload-element.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createTextNode("Image Upload Element");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes() {
        return [];
      },
      statements: [],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/components/language-menu", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 2,
              "column": 0
            },
            "end": {
              "line": 4,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/language-menu.hbs"
        },
        isEmpty: false,
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("li");
          var el2 = dom.createElement("button");
          dom.setAttribute(el2, "class", "button-text");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1, 0]);
          var morphs = new Array(2);
          morphs[0] = dom.createElementMorph(element0);
          morphs[1] = dom.createMorphAt(element0, 0, 0);
          return morphs;
        },
        statements: [["element", "action", ["changeLanguage", ["get", "languageChoice.code", ["loc", [null, [3, 62], [3, 81]]], 0, 0, 0, 0]], [], ["loc", [null, [3, 36], [3, 83]]], 0, 0], ["content", "languageChoice.name", ["loc", [null, [3, 84], [3, 107]]], 0, 0, 0, 0]],
        locals: ["languageChoice"],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 5,
            "column": 5
          }
        },
        "moduleName": "offline/templates/components/language-menu.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("ul");
        dom.setAttribute(el1, "class", "languages");
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(dom.childAt(fragment, [0]), 1, 1);
        return morphs;
      },
      statements: [["block", "each", [["get", "languages", ["loc", [null, [2, 8], [2, 17]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [2, 0], [4, 9]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("offline/templates/components/login-form", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 14,
                "column": 16
              },
              "end": {
                "line": 16,
                "column": 16
              }
            },
            "moduleName": "offline/templates/components/login-form.hbs"
          },
          isEmpty: false,
          arity: 1,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                    ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("p");
            dom.setAttribute(el1, "class", "error-message");
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createUnsafeMorphAt(dom.childAt(fragment, [1]), 0, 0);
            return morphs;
          },
          statements: [["content", "errorMsg", ["loc", [null, [15, 45], [15, 59]]], 0, 0, 0, 0]],
          locals: ["errorMsg"],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 12,
              "column": 8
            },
            "end": {
              "line": 18,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/login-form.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          var el2 = dom.createTextNode("\n");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
          return morphs;
        },
        statements: [["block", "each", [["get", "errorMsgs", ["loc", [null, [14, 24], [14, 33]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [14, 16], [16, 25]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child1 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 28,
                "column": 16
              },
              "end": {
                "line": 28,
                "column": 67
              }
            },
            "moduleName": "offline/templates/components/login-form.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
            dom.insertBoundary(fragment, 0);
            dom.insertBoundary(fragment, null);
            return morphs;
          },
          statements: [["inline", "t", ["Forgot Password?"], [], ["loc", [null, [28, 43], [28, 67]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 21,
              "column": 12
            },
            "end": {
              "line": 32,
              "column": 12
            }
          },
          "moduleName": "offline/templates/components/login-form.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "form-group");
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("label");
          dom.setAttribute(el2, "for", "email");
          dom.setAttribute(el2, "class", "login-label");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "form-group");
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("label");
          dom.setAttribute(el2, "for", "password");
          dom.setAttribute(el2, "class", "login-label");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("button");
          dom.setAttribute(el2, "class", "login-icon-overlay");
          var el3 = dom.createElement("i");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element1 = dom.childAt(fragment, [1]);
          var element2 = dom.childAt(fragment, [3]);
          var element3 = dom.childAt(element2, [7]);
          var element4 = dom.childAt(element3, [0]);
          var morphs = new Array(7);
          morphs[0] = dom.createMorphAt(dom.childAt(element1, [1]), 0, 0);
          morphs[1] = dom.createMorphAt(element1, 3, 3);
          morphs[2] = dom.createMorphAt(dom.childAt(element2, [1]), 0, 0);
          morphs[3] = dom.createMorphAt(element2, 3, 3);
          morphs[4] = dom.createMorphAt(element2, 5, 5);
          morphs[5] = dom.createElementMorph(element3);
          morphs[6] = dom.createAttrMorph(element4, 'class');
          return morphs;
        },
        statements: [["inline", "t", ["Username"], [], ["loc", [null, [23, 55], [23, 71]]], 0, 0], ["inline", "input", [], ["id", "email", "class", "form-control login-input", "value", ["subexpr", "@mut", [["get", "email", ["loc", [null, [24, 74], [24, 79]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [24, 16], [24, 81]]], 0, 0], ["inline", "t", ["Password"], [], ["loc", [null, [27, 58], [27, 74]]], 0, 0], ["block", "link-to", ["login.forgot"], [], 0, null, ["loc", [null, [28, 16], [28, 79]]]], ["inline", "input", [], ["id", "password", "class", "form-control login-input", "type", ["subexpr", "@mut", [["get", "inputType", ["loc", [null, [29, 76], [29, 85]]], 0, 0, 0, 0]], [], [], 0, 0], "value", ["subexpr", "@mut", [["get", "password", ["loc", [null, [29, 92], [29, 100]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [29, 16], [29, 102]]], 0, 0], ["element", "action", ["toggleMask"], [], ["loc", [null, [30, 24], [30, 47]]], 0, 0], ["attribute", "class", ["subexpr", "if", [["get", "masked", ["loc", [null, [30, 89], [30, 95]]], 0, 0, 0, 0], "fa fa-eye", "fa fa-eye-slash"], [], ["loc", [null, [null, null], [30, 127]]], 0, 0], 0, 0, 0, 0]],
        locals: [],
        templates: [child0]
      };
    })();
    var child2 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 32,
              "column": 12
            },
            "end": {
              "line": 37,
              "column": 12
            }
          },
          "moduleName": "offline/templates/components/login-form.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "form-group");
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("label");
          dom.setAttribute(el2, "for", "token");
          dom.setAttribute(el2, "class", "login-label");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1]);
          var morphs = new Array(2);
          morphs[0] = dom.createMorphAt(dom.childAt(element0, [1]), 0, 0);
          morphs[1] = dom.createMorphAt(element0, 3, 3);
          return morphs;
        },
        statements: [["inline", "t", ["Code"], [], ["loc", [null, [34, 55], [34, 67]]], 0, 0], ["inline", "input", [], ["id", "token", "class", "form-control login-input", "type", "text", "value", ["subexpr", "@mut", [["get", "token", ["loc", [null, [35, 86], [35, 91]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [35, 16], [35, 93]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 45,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/login-form.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "login-header");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "login-body");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "login-form");
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("form");
        var el4 = dom.createTextNode("\n");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("button");
        dom.setAttribute(el4, "class", "btn btn-login");
        dom.setAttribute(el4, "type", "submit");
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "login-footer");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element5 = dom.childAt(fragment, [0]);
        var element6 = dom.childAt(element5, [1]);
        var element7 = dom.childAt(element5, [3]);
        var element8 = dom.childAt(fragment, [2, 1]);
        var element9 = dom.childAt(element8, [3]);
        var morphs = new Array(11);
        morphs[0] = dom.createAttrMorph(element6, 'class');
        morphs[1] = dom.createElementMorph(element6);
        morphs[2] = dom.createMorphAt(dom.childAt(element6, [1]), 0, 0);
        morphs[3] = dom.createAttrMorph(element7, 'class');
        morphs[4] = dom.createElementMorph(element7);
        morphs[5] = dom.createMorphAt(dom.childAt(element7, [1]), 0, 0);
        morphs[6] = dom.createMorphAt(element8, 1, 1);
        morphs[7] = dom.createElementMorph(element9);
        morphs[8] = dom.createMorphAt(element9, 1, 1);
        morphs[9] = dom.createMorphAt(dom.childAt(element9, [3]), 0, 0);
        morphs[10] = dom.createMorphAt(dom.childAt(element8, [5]), 1, 1);
        return morphs;
      },
      statements: [["attribute", "class", ["concat", ["login-tab ", ["subexpr", "if", [["get", "standardLogin", ["loc", [null, [2, 58], [2, 71]]], 0, 0, 0, 0], "active"], [], ["loc", [null, [2, 53], [2, 82]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["element", "action", ["standardLogin"], [], ["loc", [null, [2, 9], [2, 35]]], 0, 0], ["inline", "t", ["Account Login"], [], ["loc", [null, [3, 14], [3, 35]]], 0, 0], ["attribute", "class", ["concat", ["login-tab ", ["subexpr", "if", [["get", "standardLogin", ["loc", [null, [5, 54], [5, 67]]], 0, 0, 0, 0], "", "active"], [], ["loc", [null, [5, 49], [5, 81]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["element", "action", ["codeLogin"], [], ["loc", [null, [5, 9], [5, 31]]], 0, 0], ["inline", "t", ["Access Code"], [], ["loc", [null, [6, 14], [6, 33]]], 0, 0], ["block", "if", [["get", "errorMsgs", ["loc", [null, [12, 14], [12, 23]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [12, 8], [18, 15]]]], ["element", "action", ["validate"], ["on", "submit"], ["loc", [null, [20, 14], [20, 47]]], 0, 0], ["block", "if", [["get", "standardLogin", ["loc", [null, [21, 18], [21, 31]]], 0, 0, 0, 0]], [], 1, 2, ["loc", [null, [21, 12], [37, 19]]]], ["inline", "t", ["Login"], [], ["loc", [null, [38, 56], [38, 69]]], 0, 0], ["content", "language-menu", ["loc", [null, [41, 12], [41, 29]]], 0, 0, 0, 0]],
      locals: [],
      templates: [child0, child1, child2]
    };
  })());
});
define("offline/templates/components/logout-button", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 1,
              "column": 0
            },
            "end": {
              "line": 3,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/logout-button.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("button");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1]);
          var morphs = new Array(2);
          morphs[0] = dom.createElementMorph(element0);
          morphs[1] = dom.createMorphAt(element0, 0, 0);
          return morphs;
        },
        statements: [["element", "action", ["invalidateSession"], [], ["loc", [null, [2, 12], [2, 42]]], 0, 0], ["inline", "t", ["Logout"], [], ["loc", [null, [2, 43], [2, 57]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 4,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/logout-button.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        dom.insertBoundary(fragment, null);
        return morphs;
      },
      statements: [["block", "if", [["get", "session.isAuthenticated", ["loc", [null, [1, 6], [1, 29]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [1, 0], [3, 7]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("offline/templates/components/partials-summary", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 3,
                "column": 4
              },
              "end": {
                "line": 12,
                "column": 4
              }
            },
            "moduleName": "offline/templates/components/partials-summary.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "id", "success-message");
            dom.setAttribute(el1, "class", "status-message");
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("span");
            var el3 = dom.createTextNode("\n                ");
            dom.appendChild(el2, el3);
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n            ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("button");
            dom.setAttribute(el2, "class", "btn-transparent");
            var el3 = dom.createTextNode("\n                ");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("i");
            dom.setAttribute(el3, "class", "fa fa-times");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n            ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n        ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element6 = dom.childAt(fragment, [1]);
            var element7 = dom.childAt(element6, [3]);
            var morphs = new Array(2);
            morphs[0] = dom.createUnsafeMorphAt(dom.childAt(element6, [1]), 1, 1);
            morphs[1] = dom.createElementMorph(element7);
            return morphs;
          },
          statements: [["inline", "t", ["<b>Success!</b> Your partial survey have been uploaded&#46;"], [], ["loc", [null, [6, 16], [6, 85]]], 0, 0], ["element", "action", ["clearStatus", "success"], [], ["loc", [null, [8, 44], [8, 78]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 1,
              "column": 0
            },
            "end": {
              "line": 14,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/partials-summary.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "role", "alert");
          var el2 = dom.createTextNode("\n");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
          return morphs;
        },
        statements: [["block", "if", [["get", "status.partials.success", ["loc", [null, [3, 10], [3, 33]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [3, 4], [12, 11]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child1 = (function () {
      var child0 = (function () {
        var child0 = (function () {
          var child0 = (function () {
            return {
              meta: {
                "revision": "Ember@2.9.0",
                "loc": {
                  "source": null,
                  "start": {
                    "line": 38,
                    "column": 24
                  },
                  "end": {
                    "line": 42,
                    "column": 24
                  }
                },
                "moduleName": "offline/templates/components/partials-summary.hbs"
              },
              isEmpty: false,
              arity: 0,
              cachedFragment: null,
              hasRendered: false,
              buildFragment: function buildFragment(dom) {
                var el0 = dom.createDocumentFragment();
                var el1 = dom.createTextNode("                            ");
                dom.appendChild(el0, el1);
                var el1 = dom.createElement("button");
                dom.setAttribute(el1, "class", "btn-transparent partial-upload");
                var el2 = dom.createTextNode("\n                                ");
                dom.appendChild(el1, el2);
                var el2 = dom.createElement("i");
                dom.appendChild(el1, el2);
                var el2 = dom.createTextNode("\n                            ");
                dom.appendChild(el1, el2);
                dom.appendChild(el0, el1);
                var el1 = dom.createTextNode("\n");
                dom.appendChild(el0, el1);
                return el0;
              },
              buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
                var element0 = dom.childAt(fragment, [1]);
                var element1 = dom.childAt(element0, [1]);
                var morphs = new Array(2);
                morphs[0] = dom.createElementMorph(element0);
                morphs[1] = dom.createAttrMorph(element1, 'class');
                return morphs;
              },
              statements: [["element", "action", ["uploadPartial", ["get", "resp", ["loc", [null, [39, 100], [39, 104]]], 0, 0, 0, 0]], [], ["loc", [null, [39, 75], [39, 106]]], 0, 0], ["attribute", "class", ["concat", ["fa ", ["subexpr", "if", [["get", "resp.isSaving", ["loc", [null, [40, 50], [40, 63]]], 0, 0, 0, 0], "fa-spinner fa-spin", "fa-upload"], [], ["loc", [null, [40, 45], [40, 98]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0]],
              locals: [],
              templates: []
            };
          })();
          return {
            meta: {
              "revision": "Ember@2.9.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 26,
                  "column": 20
                },
                "end": {
                  "line": 44,
                  "column": 20
                }
              },
              "moduleName": "offline/templates/components/partials-summary.hbs"
            },
            isEmpty: false,
            arity: 1,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("                        ");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("div");
              dom.setAttribute(el1, "class", "container-respondent");
              var el2 = dom.createTextNode("\n                            ");
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("a");
              var el3 = dom.createTextNode("\n                                ");
              dom.appendChild(el2, el3);
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              var el3 = dom.createElement("br");
              dom.appendChild(el2, el3);
              var el3 = dom.createTextNode("\n                            ");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n                            ");
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("span");
              dom.setAttribute(el2, "class", "status-info");
              var el3 = dom.createTextNode("\n                                ");
              dom.appendChild(el2, el3);
              var el3 = dom.createElement("i");
              dom.setAttribute(el3, "class", "fa fa-clock-o");
              dom.appendChild(el2, el3);
              var el3 = dom.createTextNode("\n                                ");
              dom.appendChild(el2, el3);
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              var el3 = dom.createTextNode(" | ");
              dom.appendChild(el2, el3);
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              var el3 = dom.createTextNode("\n                            ");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n                            ");
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("button");
              dom.setAttribute(el2, "class", "btn-transparent partial-remove");
              var el3 = dom.createTextNode("\n                                ");
              dom.appendChild(el2, el3);
              var el3 = dom.createElement("i");
              dom.setAttribute(el3, "class", "fa fa-times-circle");
              dom.appendChild(el2, el3);
              var el3 = dom.createTextNode("\n                            ");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n");
              dom.appendChild(el1, el2);
              var el2 = dom.createComment("");
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("                        ");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var element2 = dom.childAt(fragment, [1]);
              var element3 = dom.childAt(element2, [1]);
              var element4 = dom.childAt(element2, [3]);
              var element5 = dom.childAt(element2, [5]);
              var morphs = new Array(6);
              morphs[0] = dom.createElementMorph(element3);
              morphs[1] = dom.createMorphAt(element3, 1, 1);
              morphs[2] = dom.createMorphAt(element4, 3, 3);
              morphs[3] = dom.createMorphAt(element4, 5, 5);
              morphs[4] = dom.createElementMorph(element5);
              morphs[5] = dom.createMorphAt(element2, 7, 7);
              return morphs;
            },
            statements: [["element", "action", ["resumeSurvey", ["get", "resp", ["loc", [null, [28, 55], [28, 59]]], 0, 0, 0, 0]], [], ["loc", [null, [28, 31], [28, 61]]], 0, 0], ["inline", "if", [["get", "resp.description", ["loc", [null, [29, 37], [29, 53]]], 0, 0, 0, 0], ["get", "resp.description", ["loc", [null, [29, 54], [29, 70]]], 0, 0, 0, 0], ["subexpr", "t", ["No description"], [], ["loc", [null, [29, 71], [29, 91]]], 0, 0]], [], ["loc", [null, [29, 32], [29, 93]]], 0, 0], ["inline", "format-date", [["get", "resp.lastupdated_on", ["loc", [null, [33, 46], [33, 65]]], 0, 0, 0, 0], "h:mm A"], [], ["loc", [null, [33, 32], [33, 76]]], 0, 0], ["inline", "format-date", [["get", "resp.lastupdated_on", ["loc", [null, [33, 93], [33, 112]]], 0, 0, 0, 0], "dddd, MMMM D"], [], ["loc", [null, [33, 79], [33, 129]]], 0, 0], ["element", "action", ["removePartial", ["get", "resp", ["loc", [null, [35, 100], [35, 104]]], 0, 0, 0, 0]], [], ["loc", [null, [35, 75], [35, 106]]], 0, 0], ["block", "if", [["get", "network.online", ["loc", [null, [38, 30], [38, 44]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [38, 24], [42, 31]]]]],
            locals: ["resp"],
            templates: [child0]
          };
        })();
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 23,
                "column": 8
              },
              "end": {
                "line": 47,
                "column": 8
              }
            },
            "moduleName": "offline/templates/components/partials-summary.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("            ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "class", "container-partial");
            var el2 = dom.createTextNode("\n                ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("div");
            dom.setAttribute(el2, "class", "partials-row");
            var el3 = dom.createTextNode("\n");
            dom.appendChild(el2, el3);
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("                ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1, 1]), 1, 1);
            return morphs;
          },
          statements: [["block", "each", [["get", "partials", ["loc", [null, [26, 28], [26, 36]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [26, 20], [44, 29]]]]],
          locals: [],
          templates: [child0]
        };
      })();
      var child1 = (function () {
        var child0 = (function () {
          return {
            meta: {
              "revision": "Ember@2.9.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 48,
                  "column": 12
                },
                "end": {
                  "line": 52,
                  "column": 12
                }
              },
              "moduleName": "offline/templates/components/partials-summary.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("                ");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("div");
              var el2 = dom.createTextNode("\n                    ");
              dom.appendChild(el1, el2);
              var el2 = dom.createComment("");
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n                ");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var morphs = new Array(1);
              morphs[0] = dom.createUnsafeMorphAt(dom.childAt(fragment, [1]), 1, 1);
              return morphs;
            },
            statements: [["inline", "t", ["Survey has changed&#46;<br>All stored partial surveys were removed&#46;"], [], ["loc", [null, [50, 20], [50, 101]]], 0, 0]],
            locals: [],
            templates: []
          };
        })();
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 47,
                "column": 8
              },
              "end": {
                "line": 53,
                "column": 8
              }
            },
            "moduleName": "offline/templates/components/partials-summary.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
            dom.insertBoundary(fragment, 0);
            dom.insertBoundary(fragment, null);
            return morphs;
          },
          statements: [["block", "if", [["get", "status.survey.changed", ["loc", [null, [48, 18], [48, 39]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [48, 12], [52, 19]]]]],
          locals: [],
          templates: [child0]
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 21,
              "column": 4
            },
            "end": {
              "line": 55,
              "column": 4
            }
          },
          "moduleName": "offline/templates/components/partials-summary.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          var el2 = dom.createTextNode("\n");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
          return morphs;
        },
        statements: [["block", "if", [["get", "partials.length", ["loc", [null, [23, 14], [23, 29]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [23, 8], [53, 15]]]]],
        locals: [],
        templates: [child0, child1]
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 57,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/partials-summary.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("    ");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "panel-inner");
        var el2 = dom.createTextNode("\n        ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("span");
        dom.setAttribute(el2, "class", "status-count");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n        ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("button");
        dom.setAttribute(el2, "class", "btn-link");
        var el3 = dom.createTextNode("\n            ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("i");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n            ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("    ");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element8 = dom.childAt(fragment, [2]);
        var element9 = dom.childAt(element8, [3]);
        var element10 = dom.childAt(element9, [1]);
        var morphs = new Array(6);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        morphs[1] = dom.createMorphAt(dom.childAt(element8, [1]), 0, 0);
        morphs[2] = dom.createElementMorph(element9);
        morphs[3] = dom.createAttrMorph(element10, 'class');
        morphs[4] = dom.createMorphAt(element9, 3, 3);
        morphs[5] = dom.createMorphAt(element8, 5, 5);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["block", "if", [["get", "network.online", ["loc", [null, [1, 6], [1, 20]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [1, 0], [14, 7]]]], ["content", "partials.length", ["loc", [null, [16, 35], [16, 56]]], 0, 0, 0, 0], ["element", "action", ["toggleCollapsed"], [], ["loc", [null, [17, 33], [17, 61]]], 0, 0], ["attribute", "class", ["concat", ["fa ", ["subexpr", "if", [["get", "isCollapsed", ["loc", [null, [18, 30], [18, 41]]], 0, 0, 0, 0], "fa-angle-right", "fa-angle-down"], [], ["loc", [null, [18, 25], [18, 76]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["inline", "t", ["Partial Surveys (on device)"], [], ["loc", [null, [19, 12], [19, 47]]], 0, 0], ["block", "unless", [["get", "isCollapsed", ["loc", [null, [21, 14], [21, 25]]], 0, 0, 0, 0]], [], 1, null, ["loc", [null, [21, 4], [55, 15]]]]],
      locals: [],
      templates: [child0, child1]
    };
  })());
});
define("offline/templates/components/projects-list-item", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 4,
                "column": 12
              },
              "end": {
                "line": 6,
                "column": 12
              }
            },
            "moduleName": "offline/templates/components/projects-list-item.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                ");
            dom.appendChild(el0, el1);
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
            return morphs;
          },
          statements: [["content", "interviewer.survey.adminTitle", ["loc", [null, [5, 16], [5, 49]]], 0, 0, 0, 0]],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 3,
              "column": 8
            },
            "end": {
              "line": 7,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/projects-list-item.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["block", "link-to", ["authenticated.admin", ["get", "interviewer.survey.path", ["loc", [null, [4, 45], [4, 68]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [4, 12], [6, 24]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child1 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 7,
                "column": 8
              },
              "end": {
                "line": 9,
                "column": 8
              }
            },
            "moduleName": "offline/templates/components/projects-list-item.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("            ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("a");
            dom.setAttribute(el1, "href", "#");
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element7 = dom.childAt(fragment, [1]);
            var morphs = new Array(2);
            morphs[0] = dom.createElementMorph(element7);
            morphs[1] = dom.createMorphAt(element7, 0, 0);
            return morphs;
          },
          statements: [["element", "action", ["warning", "closed"], [], ["loc", [null, [8, 15], [8, 44]]], 0, 0], ["content", "interviewer.survey.adminTitle", ["loc", [null, [8, 54], [8, 87]]], 0, 0, 0, 0]],
          locals: [],
          templates: []
        };
      })();
      var child1 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 9,
                "column": 8
              },
              "end": {
                "line": 11,
                "column": 8
              }
            },
            "moduleName": "offline/templates/components/projects-list-item.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("            ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("a");
            dom.setAttribute(el1, "href", "#");
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n        ");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element6 = dom.childAt(fragment, [1]);
            var morphs = new Array(2);
            morphs[0] = dom.createElementMorph(element6);
            morphs[1] = dom.createMorphAt(element6, 0, 0);
            return morphs;
          },
          statements: [["element", "action", ["warning", "download"], [], ["loc", [null, [10, 15], [10, 46]]], 0, 0], ["content", "interviewer.survey.adminTitle", ["loc", [null, [10, 56], [10, 89]]], 0, 0, 0, 0]],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 7,
              "column": 8
            },
            "end": {
              "line": 11,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/projects-list-item.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["block", "if", [["get", "surveyClosed", ["loc", [null, [7, 18], [7, 30]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [7, 8], [11, 8]]]]],
        locals: [],
        templates: [child0, child1]
      };
    })();
    var child2 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 12,
              "column": 8
            },
            "end": {
              "line": 14,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/projects-list-item.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("span");
          dom.setAttribute(el1, "class", "new-badge");
          var el2 = dom.createTextNode("NEW");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child3 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 21,
                "column": 8
              },
              "end": {
                "line": 23,
                "column": 8
              }
            },
            "moduleName": "offline/templates/components/projects-list-item.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("            ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("span");
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode(" ");
            dom.appendChild(el1, el2);
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element2 = dom.childAt(fragment, [1]);
            var morphs = new Array(2);
            morphs[0] = dom.createMorphAt(element2, 0, 0);
            morphs[1] = dom.createMorphAt(element2, 2, 2);
            return morphs;
          },
          statements: [["content", "readyToUpload", ["loc", [null, [22, 18], [22, 35]]], 0, 0, 0, 0], ["inline", "t", ["Ready To Upload"], [], ["loc", [null, [22, 36], [22, 59]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 17,
              "column": 4
            },
            "end": {
              "line": 25,
              "column": 4
            }
          },
          "moduleName": "offline/templates/components/projects-list-item.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "projects-stats");
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode(" ");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode(" ");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element3 = dom.childAt(fragment, [1]);
          var element4 = dom.childAt(element3, [1]);
          var element5 = dom.childAt(element3, [3]);
          var morphs = new Array(5);
          morphs[0] = dom.createMorphAt(element4, 0, 0);
          morphs[1] = dom.createMorphAt(element4, 2, 2);
          morphs[2] = dom.createMorphAt(element5, 0, 0);
          morphs[3] = dom.createMorphAt(element5, 2, 2);
          morphs[4] = dom.createMorphAt(element3, 5, 5);
          return morphs;
        },
        statements: [["content", "interviewer.total_completes", ["loc", [null, [19, 14], [19, 45]]], 0, 0, 0, 0], ["inline", "t", ["Completes"], [], ["loc", [null, [19, 46], [19, 63]]], 0, 0], ["content", "partials", ["loc", [null, [20, 14], [20, 26]]], 0, 0, 0, 0], ["inline", "t", ["Partials"], [], ["loc", [null, [20, 27], [20, 43]]], 0, 0], ["block", "if", [["get", "readyToUpload", ["loc", [null, [21, 14], [21, 27]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [21, 8], [23, 15]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child4 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 30,
                "column": 12
              },
              "end": {
                "line": 35,
                "column": 12
              }
            },
            "moduleName": "offline/templates/components/projects-list-item.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            var el2 = dom.createTextNode("\n                    ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("i");
            dom.setAttribute(el2, "class", "fa fa-spinner fa-pulse");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                    ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("br");
            dom.appendChild(el1, el2);
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 4, 4);
            return morphs;
          },
          statements: [["inline", "t", ["Downloading"], [], ["loc", [null, [33, 25], [33, 44]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      var child1 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 35,
                "column": 12
              },
              "end": {
                "line": 40,
                "column": 12
              }
            },
            "moduleName": "offline/templates/components/projects-list-item.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("button");
            var el2 = dom.createTextNode("\n                    ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("i");
            dom.setAttribute(el2, "class", "fa fa-cloud-download");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                    ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("br");
            dom.appendChild(el1, el2);
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element1 = dom.childAt(fragment, [1]);
            var morphs = new Array(2);
            morphs[0] = dom.createElementMorph(element1);
            morphs[1] = dom.createMorphAt(element1, 4, 4);
            return morphs;
          },
          statements: [["element", "action", ["download"], [], ["loc", [null, [36, 24], [36, 45]]], 0, 0], ["inline", "t", ["Download"], [], ["loc", [null, [38, 25], [38, 41]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 29,
              "column": 8
            },
            "end": {
              "line": 41,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/projects-list-item.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["block", "if", [["get", "downloading", ["loc", [null, [30, 18], [30, 29]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [30, 12], [40, 19]]]]],
        locals: [],
        templates: [child0, child1]
      };
    })();
    var child5 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 42,
                "column": 12
              },
              "end": {
                "line": 47,
                "column": 12
              }
            },
            "moduleName": "offline/templates/components/projects-list-item.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            var el2 = dom.createTextNode("\n                    ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("i");
            dom.setAttribute(el2, "class", "fa fa-spinner fa-pulse");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                    ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("br");
            dom.appendChild(el1, el2);
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 4, 4);
            return morphs;
          },
          statements: [["inline", "t", ["Uploading"], [], ["loc", [null, [45, 25], [45, 42]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      var child1 = (function () {
        var child0 = (function () {
          return {
            meta: {
              "revision": "Ember@2.9.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 47,
                  "column": 12
                },
                "end": {
                  "line": 52,
                  "column": 12
                }
              },
              "moduleName": "offline/templates/components/projects-list-item.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("                ");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("div");
              var el2 = dom.createTextNode("\n                    ");
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("i");
              dom.setAttribute(el2, "class", "fa fa-spinner fa-pulse");
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n                    ");
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("br");
              dom.appendChild(el1, el2);
              var el2 = dom.createComment("");
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n                ");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var morphs = new Array(1);
              morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 4, 4);
              return morphs;
            },
            statements: [["inline", "t", ["Updating"], [], ["loc", [null, [50, 25], [50, 41]]], 0, 0]],
            locals: [],
            templates: []
          };
        })();
        var child1 = (function () {
          var child0 = (function () {
            return {
              meta: {
                "revision": "Ember@2.9.0",
                "loc": {
                  "source": null,
                  "start": {
                    "line": 52,
                    "column": 12
                  },
                  "end": {
                    "line": 57,
                    "column": 12
                  }
                },
                "moduleName": "offline/templates/components/projects-list-item.hbs"
              },
              isEmpty: false,
              arity: 0,
              cachedFragment: null,
              hasRendered: false,
              buildFragment: function buildFragment(dom) {
                var el0 = dom.createDocumentFragment();
                var el1 = dom.createTextNode("                ");
                dom.appendChild(el0, el1);
                var el1 = dom.createElement("button");
                var el2 = dom.createTextNode("\n                    ");
                dom.appendChild(el1, el2);
                var el2 = dom.createElement("i");
                dom.setAttribute(el2, "class", "fa fa-times-circle");
                dom.appendChild(el1, el2);
                var el2 = dom.createTextNode("\n                    ");
                dom.appendChild(el1, el2);
                var el2 = dom.createElement("br");
                dom.appendChild(el1, el2);
                var el2 = dom.createComment("");
                dom.appendChild(el1, el2);
                var el2 = dom.createTextNode("\n                ");
                dom.appendChild(el1, el2);
                dom.appendChild(el0, el1);
                var el1 = dom.createTextNode("\n            ");
                dom.appendChild(el0, el1);
                return el0;
              },
              buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
                var element0 = dom.childAt(fragment, [1]);
                var morphs = new Array(2);
                morphs[0] = dom.createElementMorph(element0);
                morphs[1] = dom.createMorphAt(element0, 4, 4);
                return morphs;
              },
              statements: [["element", "action", ["remove"], [], ["loc", [null, [53, 24], [53, 43]]], 0, 0], ["inline", "t", ["Remove"], [], ["loc", [null, [55, 25], [55, 39]]], 0, 0]],
              locals: [],
              templates: []
            };
          })();
          return {
            meta: {
              "revision": "Ember@2.9.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 52,
                  "column": 12
                },
                "end": {
                  "line": 57,
                  "column": 12
                }
              },
              "moduleName": "offline/templates/components/projects-list-item.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createComment("");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var morphs = new Array(1);
              morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
              dom.insertBoundary(fragment, 0);
              dom.insertBoundary(fragment, null);
              return morphs;
            },
            statements: [["block", "if", [["get", "surveyClosed", ["loc", [null, [52, 22], [52, 34]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [52, 12], [57, 12]]]]],
            locals: [],
            templates: [child0]
          };
        })();
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 47,
                "column": 12
              },
              "end": {
                "line": 57,
                "column": 12
              }
            },
            "moduleName": "offline/templates/components/projects-list-item.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
            dom.insertBoundary(fragment, 0);
            dom.insertBoundary(fragment, null);
            return morphs;
          },
          statements: [["block", "if", [["get", "updating", ["loc", [null, [47, 22], [47, 30]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [47, 12], [57, 12]]]]],
          locals: [],
          templates: [child0, child1]
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 41,
              "column": 8
            },
            "end": {
              "line": 58,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/projects-list-item.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["block", "if", [["get", "uploading", ["loc", [null, [42, 18], [42, 27]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [42, 12], [57, 19]]]]],
        locals: [],
        templates: [child0, child1]
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 62,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/projects-list-item.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "projects-info");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "projects-name");
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("p");
        dom.setAttribute(el3, "class", "projects-warning");
        var el4 = dom.createElement("i");
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "projects-action");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "projects-icon");
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element8 = dom.childAt(fragment, [0]);
        var element9 = dom.childAt(element8, [1]);
        var morphs = new Array(5);
        morphs[0] = dom.createMorphAt(element9, 1, 1);
        morphs[1] = dom.createMorphAt(element9, 2, 2);
        morphs[2] = dom.createMorphAt(dom.childAt(element9, [4, 0]), 0, 0);
        morphs[3] = dom.createMorphAt(element8, 3, 3);
        morphs[4] = dom.createMorphAt(dom.childAt(fragment, [2, 1]), 1, 1);
        return morphs;
      },
      statements: [["block", "if", [["get", "canNavigate", ["loc", [null, [3, 14], [3, 25]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [3, 8], [11, 15]]]], ["block", "unless", [["get", "hasSeen", ["loc", [null, [12, 18], [12, 25]]], 0, 0, 0, 0]], [], 2, null, ["loc", [null, [12, 8], [14, 19]]]], ["content", "warning", ["loc", [null, [15, 39], [15, 52]]], 0, 0, 0, 0], ["block", "if", [["get", "downloaded", ["loc", [null, [17, 10], [17, 20]]], 0, 0, 0, 0]], [], 3, null, ["loc", [null, [17, 4], [25, 11]]]], ["block", "unless", [["get", "downloaded", ["loc", [null, [29, 18], [29, 28]]], 0, 0, 0, 0]], [], 4, 5, ["loc", [null, [29, 8], [58, 19]]]]],
      locals: [],
      templates: [child0, child1, child2, child3, child4, child5]
    };
  })());
});
define("offline/templates/components/projects-list", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 2,
                "column": 4
              },
              "end": {
                "line": 4,
                "column": 4
              }
            },
            "moduleName": "offline/templates/components/projects-list.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("p");
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 0, 0);
            return morphs;
          },
          statements: [["inline", "t", ["Offline"], [], ["loc", [null, [3, 11], [3, 26]]], 0, 0]],
          locals: [],
          templates: []
        };
      })();
      var child1 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 5,
                "column": 4
              },
              "end": {
                "line": 9,
                "column": 4
              }
            },
            "moduleName": "offline/templates/components/projects-list.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("i");
            dom.setAttribute(el2, "class", "fa fa-spinner fa-pulse");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n        ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 1,
              "column": 0
            },
            "end": {
              "line": 10,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/projects-list.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(2);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          morphs[1] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["block", "unless", [["get", "online", ["loc", [null, [2, 14], [2, 20]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [2, 4], [4, 15]]]], ["block", "if", [["get", "refreshing", ["loc", [null, [5, 10], [5, 20]]], 0, 0, 0, 0]], [], 1, null, ["loc", [null, [5, 4], [9, 11]]]]],
        locals: [],
        templates: [child0, child1]
      };
    })();
    var child1 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 14,
              "column": 8
            },
            "end": {
              "line": 19,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/projects-list.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("           ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "projects-list-item projects-testing-info");
          var el2 = dom.createTextNode("\n               ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("h2");
          var el3 = dom.createTextNode("These projects are in testing.");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n               ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("p");
          var el3 = dom.createTextNode("Any data collected will be deleted once the survey is launched.");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n           ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child2 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 20,
              "column": 8
            },
            "end": {
              "line": 22,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/projects-list.hbs"
        },
        isEmpty: false,
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [["inline", "projects-list-item", [], ["interviewer", ["subexpr", "@mut", [["get", "i", ["loc", [null, [21, 45], [21, 46]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [21, 12], [21, 48]]], 0, 0]],
        locals: ["i"],
        templates: []
      };
    })();
    var child3 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 22,
              "column": 8
            },
            "end": {
              "line": 24,
              "column": 8
            }
          },
          "moduleName": "offline/templates/components/projects-list.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "projects-list-null");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 0, 0);
          return morphs;
        },
        statements: [["inline", "t", ["You have no surveys to administer&#46;"], [], ["loc", [null, [23, 44], [23, 90]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 27,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/projects-list.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "projects");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "id", "projects-refresh");
        dom.setAttribute(el2, "class", "projects-refresh-hidden");
        var el3 = dom.createElement("i");
        dom.setAttribute(el3, "class", "fa fa-spinner fa-2x");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "id", "projects-list-pulldown");
        dom.setAttribute(el2, "class", "projects-list");
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [1, 3]);
        var morphs = new Array(3);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        morphs[1] = dom.createMorphAt(element0, 1, 1);
        morphs[2] = dom.createMorphAt(element0, 2, 2);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["block", "projects-nav", [], ["title", "Projects", "testing", ["subexpr", "@mut", [["get", "testing", ["loc", [null, [1, 41], [1, 48]]], 0, 0, 0, 0]], [], [], 0, 0]], 0, null, ["loc", [null, [1, 0], [10, 17]]]], ["block", "if", [["get", "testing", ["loc", [null, [14, 14], [14, 21]]], 0, 0, 0, 0]], [], 1, null, ["loc", [null, [14, 8], [19, 15]]]], ["block", "each", [["get", "interviewersSorted", ["loc", [null, [20, 16], [20, 34]]], 0, 0, 0, 0]], [], 2, 3, ["loc", [null, [20, 8], [24, 17]]]]],
      locals: [],
      templates: [child0, child1, child2, child3]
    };
  })());
});
define("offline/templates/components/projects-nav", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 4,
              "column": 12
            },
            "end": {
              "line": 6,
              "column": 12
            }
          },
          "moduleName": "offline/templates/components/projects-nav.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("                ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode(" ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("i");
          dom.setAttribute(el1, "class", "fa fa-chevron-down");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n            ");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [["inline", "t", ["Testing Projects"], [], ["loc", [null, [5, 16], [5, 40]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    var child1 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 6,
              "column": 12
            },
            "end": {
              "line": 8,
              "column": 12
            }
          },
          "moduleName": "offline/templates/components/projects-nav.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("  ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment(" Show live by default ");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode(" ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("i");
          dom.setAttribute(el1, "class", "fa fa-chevron-down");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 3, 3, contextualElement);
          return morphs;
        },
        statements: [["inline", "t", ["Live Projects"], [], ["loc", [null, [7, 16], [7, 37]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    var child2 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 14,
              "column": 4
            },
            "end": {
              "line": 18,
              "column": 4
            }
          },
          "moduleName": "offline/templates/components/projects-nav.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "projects-nav-user-menu");
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
          return morphs;
        },
        statements: [["content", "language-menu", ["loc", [null, [16, 8], [16, 25]]], 0, 0, 0, 0]],
        locals: [],
        templates: []
      };
    })();
    var child3 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 20,
              "column": 0
            },
            "end": {
              "line": 24,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/projects-nav.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "projects-nav-options");
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("button");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1, 1]);
          var morphs = new Array(2);
          morphs[0] = dom.createElementMorph(element0);
          morphs[1] = dom.createMorphAt(element0, 0, 0);
          return morphs;
        },
        statements: [["element", "action", ["toggleTesting"], [], ["loc", [null, [22, 16], [22, 42]]], 0, 0], ["inline", "if", [["get", "testing", ["loc", [null, [22, 48], [22, 55]]], 0, 0, 0, 0], ["subexpr", "t", ["Live Projects"], [], ["loc", [null, [22, 56], [22, 75]]], 0, 0], ["subexpr", "t", ["Testing Projects"], [], ["loc", [null, [22, 76], [22, 98]]], 0, 0]], [], ["loc", [null, [22, 43], [22, 100]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 25,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/projects-nav.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("header");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "header-action");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("button");
        var el4 = dom.createTextNode("\n");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "projects-nav-yield");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element1 = dom.childAt(fragment, [0]);
        var element2 = dom.childAt(element1, [1, 1]);
        var morphs = new Array(5);
        morphs[0] = dom.createElementMorph(element2);
        morphs[1] = dom.createMorphAt(element2, 1, 1);
        morphs[2] = dom.createMorphAt(dom.childAt(element1, [3]), 1, 1);
        morphs[3] = dom.createMorphAt(element1, 5, 5);
        morphs[4] = dom.createMorphAt(fragment, 2, 2, contextualElement);
        dom.insertBoundary(fragment, null);
        return morphs;
      },
      statements: [["element", "action", ["toggleDropdown"], [], ["loc", [null, [3, 16], [3, 43]]], 0, 0], ["block", "if", [["get", "testing", ["loc", [null, [4, 18], [4, 25]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [4, 12], [8, 19]]]], ["content", "yield", ["loc", [null, [12, 8], [12, 17]]], 0, 0, 0, 0], ["block", "user-menu", [], [], 2, null, ["loc", [null, [14, 4], [18, 18]]]], ["block", "if", [["get", "dropdown", ["loc", [null, [20, 6], [20, 14]]], 0, 0, 0, 0]], [], 3, null, ["loc", [null, [20, 0], [24, 7]]]]],
      locals: [],
      templates: [child0, child1, child2, child3]
    };
  })());
});
define("offline/templates/components/question-1d-list-cell/basic-cell", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 7,
              "column": 4
            },
            "end": {
              "line": 11,
              "column": 4
            }
          },
          "moduleName": "offline/templates/components/question-1d-list-cell/basic-cell.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "open");
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
          return morphs;
        },
        statements: [["inline", "text-input", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [9, 34], [9, 42]]], 0, 0, 0, 0]], [], [], 0, 0], "row", ["subexpr", "@mut", [["get", "cell", ["loc", [null, [9, 47], [9, 51]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [9, 63], [9, 77]]], 0, 0, 0, 0], ["get", "cell.label", ["loc", [null, [9, 78], [9, 88]]], 0, 0, 0, 0], "oe"], [], ["loc", [null, [9, 55], [9, 94]]], 0, 0], "open", true], ["loc", [null, [9, 12], [9, 106]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 14,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/question-1d-list-cell/basic-cell.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "cell list");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("label");
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        dom.setAttribute(el3, "class", "input-element");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        dom.setAttribute(el3, "class", "cdata list-answer");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0, 1]);
        var morphs = new Array(5);
        morphs[0] = dom.createAttrMorph(element0, 'for');
        morphs[1] = dom.createAttrMorph(element0, 'class');
        morphs[2] = dom.createMorphAt(element0, 2, 2);
        morphs[3] = dom.createUnsafeMorphAt(dom.childAt(element0, [6]), 0, 0);
        morphs[4] = dom.createMorphAt(element0, 8, 8);
        return morphs;
      },
      statements: [["attribute", "for", ["concat", [["subexpr", "concat", [["get", "question.label", ["loc", [null, [2, 25], [2, 39]]], 0, 0, 0, 0], ["get", "cell.label", ["loc", [null, [2, 40], [2, 50]]], 0, 0, 0, 0]], [], ["loc", [null, [2, 16], [2, 52]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["attribute", "class", ["concat", [["get", "question.type", ["loc", [null, [2, 63], [2, 76]]], 0, 0, 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["inline", "component", [["subexpr", "concat", [["get", "question.type", ["loc", [null, [4, 28], [4, 41]]], 0, 0, 0, 0], "-input"], [], ["loc", [null, [4, 20], [4, 51]]], 0, 0]], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [4, 61], [4, 69]]], 0, 0, 0, 0]], [], [], 0, 0], "row", ["subexpr", "@mut", [["get", "cell", ["loc", [null, [4, 74], [4, 78]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [4, 90], [4, 104]]], 0, 0, 0, 0], ["get", "cell.label", ["loc", [null, [4, 105], [4, 115]]], 0, 0, 0, 0]], [], ["loc", [null, [4, 82], [4, 116]]], 0, 0]], ["loc", [null, [4, 8], [4, 118]]], 0, 0], ["content", "cell.displayCdata", ["loc", [null, [6, 40], [6, 65]]], 0, 0, 0, 0], ["block", "if", [["get", "cell.open", ["loc", [null, [7, 10], [7, 19]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [7, 4], [11, 11]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("offline/templates/components/question-1d-list-cell/float-cell", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 7,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/question-1d-list-cell/float-cell.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "cell list");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("label");
        dom.setAttribute(el2, "class", "number");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        dom.setAttribute(el3, "class", "cdata block-row");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0]);
        var element1 = dom.childAt(element0, [1]);
        var morphs = new Array(3);
        morphs[0] = dom.createAttrMorph(element1, 'for');
        morphs[1] = dom.createUnsafeMorphAt(dom.childAt(element1, [1]), 0, 0);
        morphs[2] = dom.createMorphAt(element0, 3, 3);
        return morphs;
      },
      statements: [["attribute", "for", ["concat", [["subexpr", "concat", [["get", "question.label", ["loc", [null, [2, 25], [2, 39]]], 0, 0, 0, 0], ["get", "cell.label", ["loc", [null, [2, 40], [2, 50]]], 0, 0, 0, 0]], [], ["loc", [null, [2, 16], [2, 52]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["content", "cell.displayCdata", ["loc", [null, [3, 38], [3, 63]]], 0, 0, 0, 0], ["inline", "float-input", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [5, 27], [5, 35]]], 0, 0, 0, 0]], [], [], 0, 0], "row", ["subexpr", "@mut", [["get", "cell", ["loc", [null, [5, 40], [5, 44]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [5, 56], [5, 70]]], 0, 0, 0, 0], ["get", "cell.label", ["loc", [null, [5, 71], [5, 81]]], 0, 0, 0, 0]], [], ["loc", [null, [5, 48], [5, 82]]], 0, 0]], ["loc", [null, [5, 4], [5, 84]]], 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/components/question-1d-list-cell/number-cell", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 7,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/question-1d-list-cell/number-cell.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "cell list");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("label");
        dom.setAttribute(el2, "class", "number");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        dom.setAttribute(el3, "class", "cdata block-row");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0]);
        var element1 = dom.childAt(element0, [1]);
        var morphs = new Array(3);
        morphs[0] = dom.createAttrMorph(element1, 'for');
        morphs[1] = dom.createUnsafeMorphAt(dom.childAt(element1, [1]), 0, 0);
        morphs[2] = dom.createMorphAt(element0, 3, 3);
        return morphs;
      },
      statements: [["attribute", "for", ["concat", [["subexpr", "concat", [["get", "question.label", ["loc", [null, [2, 25], [2, 39]]], 0, 0, 0, 0], ["get", "cell.label", ["loc", [null, [2, 40], [2, 50]]], 0, 0, 0, 0]], [], ["loc", [null, [2, 16], [2, 52]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["content", "cell.displayCdata", ["loc", [null, [3, 38], [3, 63]]], 0, 0, 0, 0], ["inline", "number-input", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [5, 28], [5, 36]]], 0, 0, 0, 0]], [], [], 0, 0], "row", ["subexpr", "@mut", [["get", "cell", ["loc", [null, [5, 41], [5, 45]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [5, 57], [5, 71]]], 0, 0, 0, 0], ["get", "cell.label", ["loc", [null, [5, 72], [5, 82]]], 0, 0, 0, 0]], [], ["loc", [null, [5, 49], [5, 83]]], 0, 0]], ["loc", [null, [5, 4], [5, 85]]], 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/components/question-1d-list-cell/select-cell", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 7,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/question-1d-list-cell/select-cell.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "cell list");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("label");
        dom.setAttribute(el2, "class", "select");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        dom.setAttribute(el3, "class", "cdata");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0]);
        var element1 = dom.childAt(element0, [1]);
        var morphs = new Array(3);
        morphs[0] = dom.createAttrMorph(element1, 'for');
        morphs[1] = dom.createUnsafeMorphAt(dom.childAt(element1, [1]), 0, 0);
        morphs[2] = dom.createMorphAt(element0, 3, 3);
        return morphs;
      },
      statements: [["attribute", "for", ["concat", [["subexpr", "concat", [["get", "question.label", ["loc", [null, [2, 25], [2, 39]]], 0, 0, 0, 0], ["get", "cell.label", ["loc", [null, [2, 40], [2, 50]]], 0, 0, 0, 0]], [], ["loc", [null, [2, 16], [2, 52]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["content", "cell.displayCdata", ["loc", [null, [3, 28], [3, 53]]], 0, 0, 0, 0], ["inline", "select-input", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [5, 28], [5, 36]]], 0, 0, 0, 0]], [], [], 0, 0], "row", ["subexpr", "@mut", [["get", "cell", ["loc", [null, [5, 41], [5, 45]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [5, 57], [5, 71]]], 0, 0, 0, 0], ["get", "cell.label", ["loc", [null, [5, 72], [5, 82]]], 0, 0, 0, 0]], [], ["loc", [null, [5, 49], [5, 83]]], 0, 0]], ["loc", [null, [5, 4], [5, 85]]], 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/components/question-1d-list-cell/text-cell", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 2,
              "column": 0
            },
            "end": {
              "line": 6,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/question-1d-list-cell/text-cell.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "pre-text");
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          dom.setAttribute(el2, "class", "cdata");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createUnsafeMorphAt(dom.childAt(fragment, [1, 1]), 0, 0);
          return morphs;
        },
        statements: [["inline", "if", [["get", "question.preText", ["loc", [null, [4, 34], [4, 50]]], 0, 0, 0, 0], ["get", "question.preText", ["loc", [null, [4, 51], [4, 67]]], 0, 0, 0, 0], ["get", "cell.preText", ["loc", [null, [4, 68], [4, 80]]], 0, 0, 0, 0]], [], ["loc", [null, [4, 28], [4, 83]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    var child1 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 12,
              "column": 0
            },
            "end": {
              "line": 16,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/question-1d-list-cell/text-cell.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "post-text");
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("span");
          dom.setAttribute(el2, "class", "cdata");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createUnsafeMorphAt(dom.childAt(fragment, [1, 1]), 0, 0);
          return morphs;
        },
        statements: [["inline", "if", [["get", "question.postText", ["loc", [null, [14, 34], [14, 51]]], 0, 0, 0, 0], ["get", "question.postText", ["loc", [null, [14, 52], [14, 69]]], 0, 0, 0, 0], ["get", "cell.postText", ["loc", [null, [14, 70], [14, 83]]], 0, 0, 0, 0]], [], ["loc", [null, [14, 28], [14, 86]]], 0, 0]],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 18,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/question-1d-list-cell/text-cell.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "cell list");
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("label");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        dom.setAttribute(el3, "class", "cdata block-row");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0]);
        var element1 = dom.childAt(element0, [3]);
        var morphs = new Array(5);
        morphs[0] = dom.createMorphAt(element0, 1, 1);
        morphs[1] = dom.createAttrMorph(element1, 'for');
        morphs[2] = dom.createUnsafeMorphAt(dom.childAt(element1, [1]), 0, 0);
        morphs[3] = dom.createMorphAt(element0, 6, 6);
        morphs[4] = dom.createMorphAt(element0, 8, 8);
        return morphs;
      },
      statements: [["block", "if", [["get", "preText", ["loc", [null, [2, 6], [2, 13]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [2, 0], [6, 7]]]], ["attribute", "for", ["concat", [["subexpr", "concat", [["get", "question.label", ["loc", [null, [7, 25], [7, 39]]], 0, 0, 0, 0], ["get", "cell.label", ["loc", [null, [7, 40], [7, 50]]], 0, 0, 0, 0]], [], ["loc", [null, [7, 16], [7, 52]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["content", "cell.displayCdata", ["loc", [null, [8, 38], [8, 63]]], 0, 0, 0, 0], ["inline", "component", [["subexpr", "concat", [["get", "question.type", ["loc", [null, [11, 24], [11, 37]]], 0, 0, 0, 0], "-input"], [], ["loc", [null, [11, 16], [11, 47]]], 0, 0]], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [11, 57], [11, 65]]], 0, 0, 0, 0]], [], [], 0, 0], "row", ["subexpr", "@mut", [["get", "cell", ["loc", [null, [11, 70], [11, 74]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [11, 86], [11, 100]]], 0, 0, 0, 0], ["get", "cell.label", ["loc", [null, [11, 101], [11, 111]]], 0, 0, 0, 0]], [], ["loc", [null, [11, 78], [11, 112]]], 0, 0]], ["loc", [null, [11, 4], [11, 114]]], 0, 0], ["block", "if", [["get", "postText", ["loc", [null, [12, 6], [12, 14]]], 0, 0, 0, 0]], [], 1, null, ["loc", [null, [12, 0], [16, 7]]]]],
      locals: [],
      templates: [child0, child1]
    };
  })());
});
define("offline/templates/components/question-element/1d-list", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 5,
                "column": 8
              },
              "end": {
                "line": 7,
                "column": 8
              }
            },
            "moduleName": "offline/templates/components/question-element/1d-list.hbs"
          },
          isEmpty: false,
          arity: 1,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("            ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("li");
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 0, 0);
            return morphs;
          },
          statements: [["inline", "partial", [["get", "listCell", ["loc", [null, [6, 26], [6, 34]]], 0, 0, 0, 0]], [], ["loc", [null, [6, 16], [6, 36]]], 0, 0]],
          locals: ["cell"],
          templates: []
        };
      })();
      var child1 = (function () {
        var child0 = (function () {
          return {
            meta: {
              "revision": "Ember@2.9.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 8,
                  "column": 12
                },
                "end": {
                  "line": 10,
                  "column": 12
                }
              },
              "moduleName": "offline/templates/components/question-element/1d-list.hbs"
            },
            isEmpty: false,
            arity: 1,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("                ");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("li");
              var el2 = dom.createComment("");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var morphs = new Array(1);
              morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 0, 0);
              return morphs;
            },
            statements: [["inline", "partial", [["get", "listCell", ["loc", [null, [9, 30], [9, 38]]], 0, 0, 0, 0]], [], ["loc", [null, [9, 20], [9, 40]]], 0, 0]],
            locals: ["cell"],
            templates: []
          };
        })();
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 7,
                "column": 8
              },
              "end": {
                "line": 11,
                "column": 8
              }
            },
            "moduleName": "offline/templates/components/question-element/1d-list.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
            dom.insertBoundary(fragment, 0);
            dom.insertBoundary(fragment, null);
            return morphs;
          },
          statements: [["block", "with", [["get", "question.rows.firstObject", ["loc", [null, [8, 20], [8, 45]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [8, 12], [10, 21]]]]],
          locals: [],
          templates: [child0]
        };
      })();
      var child2 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 12,
                "column": 8
              },
              "end": {
                "line": 22,
                "column": 8
              }
            },
            "moduleName": "offline/templates/components/question-element/1d-list.hbs"
          },
          isEmpty: false,
          arity: 1,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("            ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("li");
            var el2 = dom.createTextNode("\n                ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("div");
            dom.setAttribute(el2, "class", "cell list");
            var el3 = dom.createTextNode("\n                    ");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("label");
            dom.setAttribute(el3, "class", "checkbox");
            var el4 = dom.createTextNode("\n                        ");
            dom.appendChild(el3, el4);
            var el4 = dom.createComment("");
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n                        ");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("span");
            dom.setAttribute(el4, "class", "input-element");
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n                        ");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("span");
            dom.setAttribute(el4, "class", "cdata");
            var el5 = dom.createComment("");
            dom.appendChild(el4, el5);
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n                    ");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n                ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n            ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element0 = dom.childAt(fragment, [1, 1, 1]);
            var morphs = new Array(3);
            morphs[0] = dom.createAttrMorph(element0, 'for');
            morphs[1] = dom.createMorphAt(element0, 1, 1);
            morphs[2] = dom.createUnsafeMorphAt(dom.childAt(element0, [5]), 0, 0);
            return morphs;
          },
          statements: [["attribute", "for", ["concat", [["subexpr", "concat", [["get", "question.label", ["loc", [null, [15, 41], [15, 55]]], 0, 0, 0, 0], ["get", "noanswer.label", ["loc", [null, [15, 56], [15, 70]]], 0, 0, 0, 0], "na"], [], ["loc", [null, [15, 32], [15, 77]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["inline", "noanswer-input", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [16, 50], [16, 58]]], 0, 0, 0, 0]], [], [], 0, 0], "noanswer", ["subexpr", "@mut", [["get", "noanswer", ["loc", [null, [16, 68], [16, 76]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [16, 88], [16, 102]]], 0, 0, 0, 0], ["get", "noanswer.label", ["loc", [null, [16, 103], [16, 117]]], 0, 0, 0, 0], "na"], [], ["loc", [null, [16, 80], [16, 123]]], 0, 0]], ["loc", [null, [16, 24], [16, 125]]], 0, 0], ["content", "noanswer.displayCdata", ["loc", [null, [18, 44], [18, 73]]], 0, 0, 0, 0]],
          locals: ["noanswer"],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 1,
              "column": 0
            },
            "end": {
              "line": 25,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/question-element/1d-list.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "question-1d");
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("ul");
          dom.setAttribute(el2, "class", "answers answers-1d-horizontal");
          var el3 = dom.createTextNode("\n");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("        ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element1 = dom.childAt(fragment, [1]);
          var element2 = dom.childAt(element1, [3]);
          var morphs = new Array(3);
          morphs[0] = dom.createMorphAt(element1, 1, 1);
          morphs[1] = dom.createMorphAt(element2, 1, 1);
          morphs[2] = dom.createMorphAt(element2, 2, 2);
          return morphs;
        },
        statements: [["inline", "question-error", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [3, 34], [3, 42]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [3, 8], [3, 44]]], 0, 0], ["block", "each", [["get", "cells", ["loc", [null, [5, 16], [5, 21]]], 0, 0, 0, 0]], [], 0, 1, ["loc", [null, [5, 8], [11, 17]]]], ["block", "each", [["get", "question.noanswer", ["loc", [null, [12, 16], [12, 33]]], 0, 0, 0, 0]], [], 2, null, ["loc", [null, [12, 8], [22, 17]]]]],
        locals: [],
        templates: [child0, child1, child2]
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 26,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/question-element/1d-list.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        dom.insertBoundary(fragment, null);
        return morphs;
      },
      statements: [["block", "survey-element", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [1, 27], [1, 35]]], 0, 0, 0, 0]], [], [], 0, 0]], 0, null, ["loc", [null, [1, 0], [25, 19]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("offline/templates/components/question-element/2d-grid", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        var child0 = (function () {
          return {
            meta: {
              "revision": "Ember@2.9.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 11,
                  "column": 28
                },
                "end": {
                  "line": 13,
                  "column": 28
                }
              },
              "moduleName": "offline/templates/components/question-element/2d-grid.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("                                ");
              dom.appendChild(el0, el1);
              var el1 = dom.createComment("");
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var morphs = new Array(1);
              morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
              return morphs;
            },
            statements: [["inline", "text-input", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [12, 54], [12, 62]]], 0, 0, 0, 0]], [], [], 0, 0], "col", ["subexpr", "@mut", [["get", "col", ["loc", [null, [12, 67], [12, 70]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [12, 82], [12, 96]]], 0, 0, 0, 0], ["get", "col.label", ["loc", [null, [12, 97], [12, 106]]], 0, 0, 0, 0], "oe"], [], ["loc", [null, [12, 74], [12, 112]]], 0, 0], "open", true], ["loc", [null, [12, 32], [12, 124]]], 0, 0]],
            locals: [],
            templates: []
          };
        })();
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 8,
                "column": 20
              },
              "end": {
                "line": 15,
                "column": 20
              }
            },
            "moduleName": "offline/templates/components/question-element/2d-grid.hbs"
          },
          isEmpty: false,
          arity: 1,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                        ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("th");
            dom.setAttribute(el1, "class", "col-legend");
            var el2 = dom.createTextNode("\n                            ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("span");
            dom.setAttribute(el2, "class", "cdata grid-2d-label");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n");
            dom.appendChild(el1, el2);
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("                        ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element6 = dom.childAt(fragment, [1]);
            var morphs = new Array(2);
            morphs[0] = dom.createUnsafeMorphAt(dom.childAt(element6, [1]), 0, 0);
            morphs[1] = dom.createMorphAt(element6, 3, 3);
            return morphs;
          },
          statements: [["content", "col.displayCdata", ["loc", [null, [10, 62], [10, 86]]], 0, 0, 0, 0], ["block", "if", [["get", "col.open", ["loc", [null, [11, 34], [11, 42]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [11, 28], [13, 35]]]]],
          locals: ["col"],
          templates: [child0]
        };
      })();
      var child1 = (function () {
        var child0 = (function () {
          return {
            meta: {
              "revision": "Ember@2.9.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 21,
                  "column": 28
                },
                "end": {
                  "line": 23,
                  "column": 28
                }
              },
              "moduleName": "offline/templates/components/question-element/2d-grid.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("                                ");
              dom.appendChild(el0, el1);
              var el1 = dom.createComment("");
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var morphs = new Array(1);
              morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
              return morphs;
            },
            statements: [["inline", "text-input", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [22, 54], [22, 62]]], 0, 0, 0, 0]], [], [], 0, 0], "row", ["subexpr", "@mut", [["get", "row", ["loc", [null, [22, 67], [22, 70]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [22, 82], [22, 96]]], 0, 0, 0, 0], ["get", "row.label", ["loc", [null, [22, 97], [22, 106]]], 0, 0, 0, 0], "oe"], [], ["loc", [null, [22, 74], [22, 112]]], 0, 0], "open", true], ["loc", [null, [22, 32], [22, 124]]], 0, 0]],
            locals: [],
            templates: []
          };
        })();
        var child1 = (function () {
          return {
            meta: {
              "revision": "Ember@2.9.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 25,
                  "column": 24
                },
                "end": {
                  "line": 35,
                  "column": 24
                }
              },
              "moduleName": "offline/templates/components/question-element/2d-grid.hbs"
            },
            isEmpty: false,
            arity: 1,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("                            ");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("td");
              dom.setAttribute(el1, "class", "question-2d-inputs");
              var el2 = dom.createTextNode("\n                                ");
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("label");
              var el3 = dom.createTextNode("\n                                    ");
              dom.appendChild(el2, el3);
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              var el3 = dom.createTextNode("\n                                    ");
              dom.appendChild(el2, el3);
              var el3 = dom.createElement("span");
              dom.setAttribute(el3, "class", "input-element");
              dom.appendChild(el2, el3);
              var el3 = dom.createTextNode("\n                                    ");
              dom.appendChild(el2, el3);
              var el3 = dom.createElement("span");
              dom.setAttribute(el3, "class", "cdata list-2d-label");
              var el4 = dom.createComment("");
              dom.appendChild(el3, el4);
              dom.appendChild(el2, el3);
              var el3 = dom.createTextNode("\n                                ");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n                            ");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var element3 = dom.childAt(fragment, [1, 1]);
              var morphs = new Array(4);
              morphs[0] = dom.createAttrMorph(element3, 'for');
              morphs[1] = dom.createAttrMorph(element3, 'class');
              morphs[2] = dom.createMorphAt(element3, 1, 1);
              morphs[3] = dom.createUnsafeMorphAt(dom.childAt(element3, [5]), 0, 0);
              return morphs;
            },
            statements: [["attribute", "for", ["concat", [["subexpr", "concat", [["get", "question.label", ["loc", [null, [27, 53], [27, 67]]], 0, 0, 0, 0], ["get", "row.label", ["loc", [null, [27, 68], [27, 77]]], 0, 0, 0, 0], ["get", "col.label", ["loc", [null, [27, 78], [27, 87]]], 0, 0, 0, 0]], [], ["loc", [null, [27, 44], [27, 89]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["attribute", "class", ["concat", [["get", "question.type", ["loc", [null, [27, 100], [27, 113]]], 0, 0, 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["inline", "component", [["subexpr", "concat", [["get", "question.type", ["loc", [null, [28, 56], [28, 69]]], 0, 0, 0, 0], "-input"], [], ["loc", [null, [28, 48], [28, 79]]], 0, 0]], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [29, 49], [29, 57]]], 0, 0, 0, 0]], [], [], 0, 0], "row", ["subexpr", "@mut", [["get", "row", ["loc", [null, [29, 62], [29, 65]]], 0, 0, 0, 0]], [], [], 0, 0], "col", ["subexpr", "@mut", [["get", "col", ["loc", [null, [29, 70], [29, 73]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [30, 51], [30, 65]]], 0, 0, 0, 0], ["get", "row.label", ["loc", [null, [30, 66], [30, 75]]], 0, 0, 0, 0], ["get", "col.label", ["loc", [null, [30, 76], [30, 85]]], 0, 0, 0, 0]], [], ["loc", [null, [30, 43], [30, 86]]], 0, 0]], ["loc", [null, [28, 36], [30, 88]]], 0, 0], ["content", "col.displayCdata", ["loc", [null, [32, 70], [32, 94]]], 0, 0, 0, 0]],
            locals: ["col"],
            templates: []
          };
        })();
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 17,
                "column": 16
              },
              "end": {
                "line": 37,
                "column": 16
              }
            },
            "moduleName": "offline/templates/components/question-element/2d-grid.hbs"
          },
          isEmpty: false,
          arity: 1,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                    ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("tr");
            dom.setAttribute(el1, "class", "answers");
            var el2 = dom.createTextNode("\n                        ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2, "class", "row-answer row-answer-list");
            var el3 = dom.createTextNode("\n                            ");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("span");
            dom.setAttribute(el3, "class", "cdata");
            var el4 = dom.createComment("");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n");
            dom.appendChild(el2, el3);
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("                        ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n");
            dom.appendChild(el1, el2);
            var el2 = dom.createComment("");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("                      ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element4 = dom.childAt(fragment, [1]);
            var element5 = dom.childAt(element4, [1]);
            var morphs = new Array(3);
            morphs[0] = dom.createUnsafeMorphAt(dom.childAt(element5, [1]), 0, 0);
            morphs[1] = dom.createMorphAt(element5, 3, 3);
            morphs[2] = dom.createMorphAt(element4, 3, 3);
            return morphs;
          },
          statements: [["content", "row.displayCdata", ["loc", [null, [20, 48], [20, 72]]], 0, 0, 0, 0], ["block", "if", [["get", "row.open", ["loc", [null, [21, 34], [21, 42]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [21, 28], [23, 35]]]], ["block", "each", [["get", "question.enabled.cols", ["loc", [null, [25, 32], [25, 53]]], 0, 0, 0, 0]], [], 1, null, ["loc", [null, [25, 24], [35, 33]]]]],
          locals: ["row"],
          templates: [child0, child1]
        };
      })();
      var child2 = (function () {
        return {
          meta: {
            "revision": "Ember@2.9.0",
            "loc": {
              "source": null,
              "start": {
                "line": 38,
                "column": 16
              },
              "end": {
                "line": 54,
                "column": 16
              }
            },
            "moduleName": "offline/templates/components/question-element/2d-grid.hbs"
          },
          isEmpty: false,
          arity: 1,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                    ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("tr");
            dom.setAttribute(el1, "class", "answers noanswers");
            var el2 = dom.createTextNode("\n                        ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2, "class", "row-answer row-answer-list");
            var el3 = dom.createTextNode("\n                            ");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("span");
            dom.setAttribute(el3, "class", "cdata");
            var el4 = dom.createComment("");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n                        ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                        ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("td");
            dom.setAttribute(el2, "class", "noanswer-row");
            var el3 = dom.createTextNode("\n                            ");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("label");
            dom.setAttribute(el3, "class", "checkbox");
            var el4 = dom.createTextNode("\n                                ");
            dom.appendChild(el3, el4);
            var el4 = dom.createComment("");
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n                                ");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("span");
            dom.setAttribute(el4, "class", "input-element");
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n                                ");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("span");
            dom.setAttribute(el4, "class", "cdata");
            var el5 = dom.createComment("");
            dom.appendChild(el4, el5);
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n                            ");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n                        ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                    ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element0 = dom.childAt(fragment, [1]);
            var element1 = dom.childAt(element0, [3]);
            var element2 = dom.childAt(element1, [1]);
            var morphs = new Array(5);
            morphs[0] = dom.createUnsafeMorphAt(dom.childAt(element0, [1, 1]), 0, 0);
            morphs[1] = dom.createAttrMorph(element1, 'colspan');
            morphs[2] = dom.createAttrMorph(element2, 'for');
            morphs[3] = dom.createMorphAt(element2, 1, 1);
            morphs[4] = dom.createUnsafeMorphAt(dom.childAt(element2, [5]), 0, 0);
            return morphs;
          },
          statements: [["content", "noanswer.displayCdata", ["loc", [null, [41, 48], [41, 77]]], 0, 0, 0, 0], ["attribute", "colspan", ["concat", [["get", "question.enabled.cols.length", ["loc", [null, [43, 60], [43, 88]]], 0, 0, 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["attribute", "for", ["concat", [["subexpr", "concat", [["get", "question.label", ["loc", [null, [44, 49], [44, 63]]], 0, 0, 0, 0], ["get", "noanswer.label", ["loc", [null, [44, 64], [44, 78]]], 0, 0, 0, 0], "na"], [], ["loc", [null, [44, 40], [44, 85]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["inline", "noanswer-input", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [46, 45], [46, 53]]], 0, 0, 0, 0]], [], [], 0, 0], "noanswer", ["subexpr", "@mut", [["get", "noanswer", ["loc", [null, [47, 45], [47, 53]]], 0, 0, 0, 0]], [], [], 0, 0], "id", ["subexpr", "concat", [["get", "question.label", ["loc", [null, [48, 47], [48, 61]]], 0, 0, 0, 0], ["get", "noanswer.label", ["loc", [null, [48, 62], [48, 76]]], 0, 0, 0, 0], "na"], [], ["loc", [null, [48, 39], [48, 82]]], 0, 0]], ["loc", [null, [45, 32], [48, 84]]], 0, 0], ["content", "noanswer.displayCdata", ["loc", [null, [50, 52], [50, 81]]], 0, 0, 0, 0]],
          locals: ["noanswer"],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 1,
              "column": 0
            },
            "end": {
              "line": 58,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/question-element/2d-grid.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("table");
          dom.setAttribute(el2, "class", "answer-wrapper grid-mode");
          var el3 = dom.createTextNode("\n            ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("tbody");
          var el4 = dom.createTextNode("\n                ");
          dom.appendChild(el3, el4);
          var el4 = dom.createElement("tr");
          dom.setAttribute(el4, "class", "header-row");
          var el5 = dom.createTextNode("\n                    ");
          dom.appendChild(el4, el5);
          var el5 = dom.createElement("th");
          dom.setAttribute(el5, "class", "empty-cell");
          dom.appendChild(el4, el5);
          var el5 = dom.createTextNode("\n");
          dom.appendChild(el4, el5);
          var el5 = dom.createComment("");
          dom.appendChild(el4, el5);
          var el5 = dom.createTextNode("                ");
          dom.appendChild(el4, el5);
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode("\n");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode("            ");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n        ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element7 = dom.childAt(fragment, [1]);
          var element8 = dom.childAt(element7, [3, 1]);
          var morphs = new Array(5);
          morphs[0] = dom.createAttrMorph(element7, 'class');
          morphs[1] = dom.createMorphAt(element7, 1, 1);
          morphs[2] = dom.createMorphAt(dom.childAt(element8, [1]), 3, 3);
          morphs[3] = dom.createMorphAt(element8, 3, 3);
          morphs[4] = dom.createMorphAt(element8, 4, 4);
          return morphs;
        },
        statements: [["attribute", "class", ["concat", ["question-2d ", ["subexpr", "concat", [["get", "question.type", ["loc", [null, [2, 37], [2, 50]]], 0, 0, 0, 0], "-grid"], [], ["loc", [null, [2, 28], [2, 60]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["inline", "question-error", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [3, 34], [3, 42]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [3, 8], [3, 44]]], 0, 0], ["block", "each", [["get", "question.enabled.cols", ["loc", [null, [8, 28], [8, 49]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [8, 20], [15, 29]]]], ["block", "each", [["get", "question.enabled.rows", ["loc", [null, [17, 24], [17, 45]]], 0, 0, 0, 0]], [], 1, null, ["loc", [null, [17, 16], [37, 25]]]], ["block", "each", [["get", "question.noanswer", ["loc", [null, [38, 24], [38, 41]]], 0, 0, 0, 0]], [], 2, null, ["loc", [null, [38, 16], [54, 25]]]]],
        locals: [],
        templates: [child0, child1, child2]
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 59,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/question-element/2d-grid.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        dom.insertBoundary(fragment, null);
        return morphs;
      },
      statements: [["block", "survey-element", [], ["question", ["subexpr", "@mut", [["get", "question", ["loc", [null, [1, 27], [1, 35]]], 0, 0, 0, 0]], [], [], 0, 0]], 0, null, ["loc", [null, [1, 0], [58, 19]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("offline/templates/components/question-error", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 1,
              "column": 0
            },
            "end": {
              "line": 3,
              "column": 0
            }
          },
          "moduleName": "offline/templates/components/question-error.hbs"
        },
        isEmpty: false,
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 0, 0);
          return morphs;
        },
        statements: [["content", "error", ["loc", [null, [2, 9], [2, 20]]], 0, 0, 0, 0]],
        locals: ["error"],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 4,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/question-error.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        dom.insertBoundary(fragment, null);
        return morphs;
      },
      statements: [["block", "each", [["get", "errors", ["loc", [null, [1, 8], [1, 14]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [1, 0], [3, 9]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("offline/templates/components/select-input", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@2.9.0",
          "loc": {
            "source": null,
            "start": {
              "line": 3,
              "column": 4
            },
            "end": {
              "line": 7,
              "column": 4
            }
          },
          "moduleName": "offline/templates/components/select-input.hbs"
        },
        isEmpty: false,
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("option");
          var el2 = dom.createTextNode("\n            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1]);
          var morphs = new Array(2);
          morphs[0] = dom.createAttrMorph(element0, 'value');
          morphs[1] = dom.createUnsafeMorphAt(element0, 1, 1);
          return morphs;
        },
        statements: [["attribute", "value", ["concat", [["get", "choice.index", ["loc", [null, [4, 25], [4, 37]]], 0, 0, 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["content", "choice.displayCdata", ["loc", [null, [5, 12], [5, 39]]], 0, 0, 0, 0]],
        locals: ["choice"],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 9,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/select-input.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("select");
        dom.setAttribute(el1, "class", "select-dropdown");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("option");
        dom.setAttribute(el2, "value", "");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element1 = dom.childAt(fragment, [0]);
        var morphs = new Array(3);
        morphs[0] = dom.createAttrMorph(element1, 'name');
        morphs[1] = dom.createMorphAt(dom.childAt(element1, [1]), 0, 0);
        morphs[2] = dom.createMorphAt(element1, 3, 3);
        return morphs;
      },
      statements: [["attribute", "name", ["concat", [["get", "name", ["loc", [null, [1, 16], [1, 20]]], 0, 0, 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["inline", "t", ["Select one"], [], ["loc", [null, [2, 21], [2, 39]]], 0, 0], ["block", "each", [["get", "question.enabled.choices", ["loc", [null, [3, 12], [3, 36]]], 0, 0, 0, 0]], [], 0, null, ["loc", [null, [3, 4], [7, 13]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("offline/templates/components/survey-element", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 8,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/survey-element.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "question-element");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("h2");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("h3");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0]);
        var element1 = dom.childAt(element0, [1]);
        var morphs = new Array(5);
        morphs[0] = dom.createAttrMorph(element0, 'id');
        morphs[1] = dom.createAttrMorph(element1, 'class');
        morphs[2] = dom.createUnsafeMorphAt(dom.childAt(element1, [1]), 0, 0);
        morphs[3] = dom.createUnsafeMorphAt(dom.childAt(element1, [3]), 0, 0);
        morphs[4] = dom.createMorphAt(element1, 5, 5);
        return morphs;
      },
      statements: [["attribute", "id", ["concat", ["question_", ["get", "question.label", ["loc", [null, [1, 45], [1, 59]]], 0, 0, 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["attribute", "class", ["concat", ["question ", ["subexpr", "if", [["get", "question.isHiddenQuestion", ["loc", [null, [2, 30], [2, 55]]], 0, 0, 0, 0], "dev"], [], ["loc", [null, [2, 25], [2, 63]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["content", "question.displayTitle", ["loc", [null, [3, 12], [3, 41]]], 0, 0, 0, 0], ["content", "question.displayComment", ["loc", [null, [4, 12], [4, 43]]], 0, 0, 0, 0], ["content", "yield", ["loc", [null, [5, 8], [5, 17]]], 0, 0, 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/components/user-menu", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 9,
            "column": 0
          }
        },
        "moduleName": "offline/templates/components/user-menu.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("button");
        dom.setAttribute(el1, "class", "menu-button");
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "user-menu-header");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        dom.setAttribute(el3, "class", "user-type");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0]);
        var element1 = dom.childAt(fragment, [2]);
        var element2 = dom.childAt(element1, [1]);
        var morphs = new Array(6);
        morphs[0] = dom.createElementMorph(element0);
        morphs[1] = dom.createMorphAt(element0, 0, 0);
        morphs[2] = dom.createAttrMorph(element1, 'class');
        morphs[3] = dom.createMorphAt(dom.childAt(element2, [1]), 0, 0);
        morphs[4] = dom.createMorphAt(element2, 3, 3);
        morphs[5] = dom.createMorphAt(element1, 3, 3);
        return morphs;
      },
      statements: [["element", "action", ["toggleMenu"], [], ["loc", [null, [1, 28], [1, 51]]], 0, 0], ["inline", "t", ["Menu"], [], ["loc", [null, [1, 52], [1, 64]]], 0, 0], ["attribute", "class", ["concat", ["user-menu ", ["subexpr", "if", [["get", "isOpened", ["loc", [null, [2, 27], [2, 35]]], 0, 0, 0, 0], "isShown"], [], ["loc", [null, [2, 22], [2, 47]]], 0, 0]], 0, 0, 0, 0, 0], 0, 0, 0, 0], ["content", "userName", ["loc", [null, [4, 32], [4, 44]]], 0, 0, 0, 0], ["content", "logout-button", ["loc", [null, [5, 8], [5, 25]]], 0, 0, 0, 0], ["content", "yield", ["loc", [null, [7, 4], [7, 13]]], 0, 0, 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/components/videocapture-element", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 1,
            "column": 21
          }
        },
        "moduleName": "offline/templates/components/videocapture-element.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createTextNode("Video Capture Element");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes() {
        return [];
      },
      statements: [],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/login", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 5,
            "column": 0
          }
        },
        "moduleName": "offline/templates/login.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("header");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("h1");
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(2);
        morphs[0] = dom.createMorphAt(dom.childAt(fragment, [0, 1]), 0, 0);
        morphs[1] = dom.createMorphAt(fragment, 2, 2, contextualElement);
        return morphs;
      },
      statements: [["inline", "t", ["Offline App"], [], ["loc", [null, [2, 8], [2, 27]]], 0, 0], ["content", "outlet", ["loc", [null, [4, 0], [4, 10]]], 0, 0, 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/login/forgot", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 1,
            "column": 76
          }
        },
        "moduleName": "offline/templates/login/forgot.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        dom.insertBoundary(fragment, null);
        return morphs;
      },
      statements: [["inline", "forgot-form", [], ["forgotPassword", ["subexpr", "action", ["forgotPassword"], [], ["loc", [null, [1, 29], [1, 54]]], 0, 0], "errorMsgs", ["subexpr", "@mut", [["get", "errorMsgs", ["loc", [null, [1, 65], [1, 74]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [1, 0], [1, 76]]], 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/login/index", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "offline/templates/login/index.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["inline", "login-form", [], ["authenticate", ["subexpr", "action", ["authenticate"], [], ["loc", [null, [1, 26], [1, 49]]], 0, 0], "errorMsgs", ["subexpr", "@mut", [["get", "errorMsgs", ["loc", [null, [1, 60], [1, 69]]], 0, 0, 0, 0]], [], [], 0, 0]], ["loc", [null, [1, 0], [1, 71]]], 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define("offline/templates/solo", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@2.9.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "offline/templates/solo.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "outlet", ["loc", [null, [1, 0], [1, 10]]], 0, 0, 0, 0]],
      locals: [],
      templates: []
    };
  })());
});
define('offline/tests/mirage/mirage/config.jshint', ['exports'], function (exports) {
  describe('JSHint | mirage/config.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/mirage/mirage/fixtures/surveys.jshint', ['exports'], function (exports) {
  describe('JSHint | mirage/fixtures/surveys.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/mirage/mirage/models/asset.jshint', ['exports'], function (exports) {
  describe('JSHint | mirage/models/asset.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/mirage/mirage/models/element.jshint', ['exports'], function (exports) {
  describe('JSHint | mirage/models/element.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/mirage/mirage/models/exitpage.jshint', ['exports'], function (exports) {
  describe('JSHint | mirage/models/exitpage.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/mirage/mirage/models/interviewer.jshint', ['exports'], function (exports) {
  describe('JSHint | mirage/models/interviewer.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/mirage/mirage/models/message.jshint', ['exports'], function (exports) {
  describe('JSHint | mirage/models/message.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/mirage/mirage/models/respondent.jshint', ['exports'], function (exports) {
  describe('JSHint | mirage/models/respondent.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/mirage/mirage/models/survey.jshint', ['exports'], function (exports) {
  describe('JSHint | mirage/models/survey.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/mirage/mirage/scenarios/default.jshint', ['exports'], function (exports) {
  describe('JSHint | mirage/scenarios/default.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/mirage/mirage/serializers/application.jshint', ['exports'], function (exports) {
  describe('JSHint | mirage/serializers/application.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/transforms/array', ['exports', 'model-fragments/transforms/array'], function (exports, _modelFragmentsTransformsArray) {
  exports['default'] = _modelFragmentsTransformsArray['default'];
});
define('offline/transforms/fragment-array', ['exports', 'model-fragments/transforms/fragment-array'], function (exports, _modelFragmentsTransformsFragmentArray) {
  exports['default'] = _modelFragmentsTransformsFragmentArray['default'];
});
define('offline/transforms/fragment', ['exports', 'model-fragments/transforms/fragment'], function (exports, _modelFragmentsTransformsFragment) {
  exports['default'] = _modelFragmentsTransformsFragment['default'];
});
define('offline/transforms/object', ['exports', 'ember', 'ember-data'], function (exports, _ember, _emberData) {
  exports['default'] = _emberData['default'].Transform.extend({
    deserialize: function deserialize(serialized) {
      return _ember['default'].Object.create(serialized);
    },

    serialize: function serialize(deserialized) {
      return deserialized;
    }
  });
});
define('offline/transforms/set', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].Transform.extend({
    deserialize: function deserialize(value, options) {
      var out = {};
      var empty = value && Object.keys(value).length > 0;
      for (var key in options.defaultValue) {
        if (options.defaultValue.hasOwnProperty(key)) {
          if (!empty) {
            out[key] = value.hasOwnProperty(key) ? Boolean(value[key]) : false;
          } else {
            out[key] = options.defaultValue[key];
          }
        }
      }
      return out;
    },

    serialize: function serialize(deserialized) {
      return deserialized;
    }
  });
});
define('offline/transforms/slist', ['exports', 'ember-data', 'lodash/lodash'], function (exports, _emberData, _lodashLodash) {
    var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

    exports['default'] = _emberData['default'].Transform.extend({
        /**
         * ["atm1d.8"] => [{name: "atm1d", version: 8}]
         * @param {Array.<String> | null} serialized
         * @returns {Array.<Object> | null}
         */
        deserialize: function deserialize(serialized) {
            if (_lodashLodash['default'].isArray(serialized)) {
                return serialized.map(function (s) {
                    var _s$split = s.split('.');

                    var _s$split2 = _slicedToArray(_s$split, 2);

                    var name = _s$split2[0];
                    var version = _s$split2[1];

                    return {
                        name: name,
                        version: parseInt(version)
                    };
                });
            }
            return null;
        },
        /**
         * [{name: "atm1d", version: 8}] => ["atm1d.8"]
         * @param {Array.<Object> | null} deserialized
         * @returns {Array.<String> | null}
         */
        serialize: function serialize(deserialized) {
            if (_lodashLodash['default'].isArray(deserialized)) {
                return deserialized.map(function (s) {
                    return [s.name, s.version].join('.');
                });
            }
            return null;
        }
    });
});
define("offline/translations/en-us", ["exports"], function (exports) {
  exports["default"] = { "<b>Nope&#46; Still offline&#46;</b> We'll automaticaly upload your responses when you get online&#46;": "<b>Nope. Still offline.</b> We'll automaticaly upload your responses when you get online.", "<b>Success!</b> Your completed surveys have been uploaded&#46;": "<b>Success!</b> Your completed surveys have been uploaded.", "<b>Success!</b> Your partial survey have been uploaded&#46;": "<b>Success!</b> Your partial survey have been uploaded.", "Access Code": "Access Code", "Account Login": "Account Login", "An email has been sent to the email address that you supplied": "An email has been sent to the email address that you supplied", "An error occurred when executing code from the survey&#46;": "An error occurred when executing code from the survey.", "Back to Login": "Back to Login", "Click here to try again&#46;": "Click here to try again.", "Click here to try uploading now&#46;": "Click here to try uploading now.", "Code": "Code", "Completed surveys": "Completed surveys", "Completes": "Completes", "Download": "Download", "Downloading": "Downloading", "Downloading Survey": "Downloading Survey", "Email": "Email", "End Practice Mode": "End Practice Mode", "End Survey": "End Survey", "Enter the email address you registered your account with below&#46; You will be sent an email containing information on how to reset your password&#46;": "Enter the email address you registered your account with below. You will be sent an email containing information on how to reset your password.", "FYI, the survey has changed&#46;": "FYI, the survey has changed.", "Failed to Authenticate": "Failed to Authenticate", "Finish": "Finish", "Forgot Password?": "Forgot Password?", "Forgotten Password": "Forgotten Password", "Hmm, there seems to be a problem&#8230;": "Hmm, there seems to be a problem...", "Home": "Home", "It looks like you're offline right now&#46; We'll automatically upload your responses when you get online&#46;": "It looks like you're offline right now. We'll automatically upload your responses when you get online.", "It looks like you're online&#46; We'll go ahead and upload your completed surveys now&#46;": "It looks like you're online. We'll go ahead and upload your completed surveys now.", "Live Projects": "Live Projects", "Login": "Login", "Logout": "Logout", "Menu": "Menu", "Next": "Next", "No description": "No description", "Offline": "Offline", "Offline App": "Offline App", "Partial Surveys (on device)": "Partial Surveys (on device)", "Partials": "Partials", "Password": "Password", "Please provide a access code": "Please provide a access code", "Please provide a email address or user name": "Please provide a email address or user name", "Please provide a password": "Please provide a password", "Please provide a valid email address": "Please provide a valid email address", "Practice Survey": "Practice Survey", "Previous": "Previous", "Project List": "Project List", "Remove from Device": "Remove from Device", "Reset": "Reset", "Save for later": "Save for later", "Select one": "Select One", "Start New Survey": "Start New Survey", "Start Survey": "Start Survey", "Storage Warning": "Storage Warning", "Survey Completed - Thank You": "Survey Completed - Thank You", "Survey Saved": "Survey Saved", "Survey Uploaded": "Survey Uploaded", "Survey has changed&#46;<br>All stored partial surveys were removed&#46;": "Survey has changed.<br>All stored partial surveys were removed.", "Testing Projects": "Testing Projects", "Uh Oh! An error has occurred when attempting to upload the data&#46;": "Uh Oh! An error has occurred when attempting to upload the data.", "Uh Oh! Upload failed&#46;<br/> Please try again later when you are online&#46;": "Uh Oh! Upload failed.<br/> Please try again later when you are online.", "Unable to log in while offline": "Unable to log in while offline", "Unable to send password reset request while offline": "Unable to send password reset request while offline", "Updating": "Updating", "Uploaded to server": "Uploaded to server", "Uploading": "Uploading", "Username": "Username", "Waiting to be uploaded&#46;&#46;&#46;": "Waiting to be uploaded...", "You are down to 5% on your local storage capacity&#46; Please upload completed surveys&#46;": "You are down to 5% on your local storage capacity. Please upload completed surveys.", "You have no surveys to administer&#46;": "You have no surveys to administer.", "You might want to practice it again and get familiar with the changes&#46; All partial completes are no longer valid and have been removed&#46;": "You might want to practice it again and get familiar with the changes. All partial completes are no longer valid and have been removed." };
});
define("offline/translations/es-mx", ["exports"], function (exports) {
  exports["default"] = { "<b>Nope&#46; Still offline&#46;</b> We'll automaticaly upload your responses when you get online&#46;": "<b>Nope. Still offline.</b> We'll automaticaly upload your responses when you get online.", "<b>Success!</b> Your completed surveys have been uploaded&#46;": "<b>Success!</b> Your completed surveys have been uploaded.", "<b>Success!</b> Your partial survey have been uploaded&#46;": "<b>Success!</b> Your partial survey have been uploaded.", "Access Code": "Access Code", "Account Login": "Account Login", "An email has been sent to the email address that you supplied": "An email has been sent to the email address that you supplied", "An error occurred when executing code from the survey&#46;": "An error occurred when executing code from the survey.", "Back to Login": "Back to Login", "Click here to try again&#46;": "Click here to try again.", "Click here to try uploading now&#46;": "Click here to try uploading now.", "Code": "Code", "Completed surveys": "Completed surveys", "Completes": "Completes", "Download": "Download", "Downloading": "Downloading", "Downloading Survey": "Downloading Survey", "Email": "Email", "End Practice Mode": "End Practice Mode", "End Survey": "End Survey", "Enter the email address you registered your account with below&#46; You will be sent an email containing information on how to reset your password&#46;": "Enter the email address you registered your account with below. You will be sent an email containing information on how to reset your password.", "FYI, the survey has changed&#46;": "FYI, the survey has changed.", "Failed to Authenticate": "Failed to Authenticate", "Finish": "Finish", "Forgot Password?": "Forgot Password?", "Forgotten Password": "Forgotten Password", "Hmm, there seems to be a problem&#8230;": "Hmm, there seems to be a problem...", "Home": "Home", "It looks like you're offline right now&#46; We'll automatically upload your responses when you get online&#46;": "It looks like you're offline right now. We'll automatically upload your responses when you get online.", "It looks like you're online&#46; We'll go ahead and upload your completed surveys now&#46;": "It looks like you're online. We'll go ahead and upload your completed surveys now.", "Live Projects": "Live Projects", "Login": "Login", "Logout": "Logout", "Menu": "Menu", "Next": "Next", "No description": "No description", "Offline": "Offline", "Offline App": "Offline App", "Partial Surveys (on device)": "Partial Surveys (on device)", "Partials": "Partials", "Password": "Password", "Please provide a access code": "Please provide a access code", "Please provide a email address or user name": "Please provide a email address or user name", "Please provide a password": "Please provide a password", "Please provide a valid email address": "Please provide a valid email address", "Practice Survey": "Practice Survey", "Previous": "Previous", "Project List": "Project List", "Remove from Device": "Remove from Device", "Reset": "Reset", "Save for later": "Save for later", "Select one": "Select One", "Start New Survey": "Start New Survey", "Start Survey": "Start Survey", "Storage Warning": "Storage Warning", "Survey Completed - Thank You": "Survey Completed - Thank You", "Survey Saved": "Survey Saved", "Survey Uploaded": "Survey Uploaded", "Survey has changed&#46;<br>All stored partial surveys were removed&#46;": "Survey has changed.<br>All stored partial surveys were removed.", "Testing Projects": "Testing Projects", "Uh Oh! An error has occurred when attempting to upload the data&#46;": "Uh Oh! An error has occurred when attempting to upload the data.", "Uh Oh! Upload failed&#46;<br/> Please try again later when you are online&#46;": "Uh Oh! Upload failed.<br/> Please try again later when you are online.", "Unable to log in while offline": "Unable to log in while offline", "Unable to send password reset request while offline": "Unable to send password reset request while offline", "Updating": "Updating", "Uploaded to server": "Uploaded to server", "Uploading": "Uploading", "Username": "Username", "Waiting to be uploaded&#46;&#46;&#46;": "Waiting to be uploaded...", "You are down to 5% on your local storage capacity&#46; Please upload completed surveys&#46;": "You are down to 5% on your local storage capacity. Please upload completed surveys.", "You have no surveys to administer&#46;": "You have no surveys to administer.", "You might want to practice it again and get familiar with the changes&#46; All partial completes are no longer valid and have been removed&#46;": "You might want to practice it again and get familiar with the changes. All partial completes are no longer valid and have been removed." };
});
define("offline/translations/fr-fr", ["exports"], function (exports) {
  exports["default"] = { "<b>Nope&#46; Still offline&#46;</b> We'll automaticaly upload your responses when you get online&#46;": "<b>Nope. Still offline.</b> We'll automaticaly upload your responses when you get online.", "<b>Success!</b> Your completed surveys have been uploaded&#46;": "<b>Success!</b> Your completed surveys have been uploaded.", "<b>Success!</b> Your partial survey have been uploaded&#46;": "<b>Success!</b> Your partial survey have been uploaded.", "Access Code": "Access Code", "Account Login": "Account Login", "An email has been sent to the email address that you supplied": "An email has been sent to the email address that you supplied", "An error occurred when executing code from the survey&#46;": "An error occurred when executing code from the survey.", "Back to Login": "Back to Login", "Click here to try again&#46;": "Click here to try again.", "Click here to try uploading now&#46;": "Click here to try uploading now.", "Code": "Code", "Completed surveys": "Completed surveys", "Completes": "Completes", "Download": "Download", "Downloading": "Downloading", "Downloading Survey": "Downloading Survey", "Email": "Email", "End Practice Mode": "End Practice Mode", "End Survey": "End Survey", "Enter the email address you registered your account with below&#46; You will be sent an email containing information on how to reset your password&#46;": "Enter the email address you registered your account with below. You will be sent an email containing information on how to reset your password.", "FYI, the survey has changed&#46;": "FYI, the survey has changed.", "Failed to Authenticate": "Failed to Authenticate", "Finish": "Finish", "Forgot Password?": "Forgot Password?", "Forgotten Password": "Forgotten Password", "Hmm, there seems to be a problem&#8230;": "Hmm, there seems to be a problem...", "Home": "Home", "It looks like you're offline right now&#46; We'll automatically upload your responses when you get online&#46;": "It looks like you're offline right now. We'll automatically upload your responses when you get online.", "It looks like you're online&#46; We'll go ahead and upload your completed surveys now&#46;": "It looks like you're online. We'll go ahead and upload your completed surveys now.", "Live Projects": "Live Projects", "Login": "Login", "Logout": "Logout", "Menu": "Menu", "Next": "Next", "No description": "No description", "Offline": "Offline", "Offline App": "Offline App", "Partial Surveys (on device)": "Partial Surveys (on device)", "Partials": "Partials", "Password": "Password", "Please provide a access code": "Please provide a access code", "Please provide a email address or user name": "Please provide a email address or user name", "Please provide a password": "Please provide a password", "Please provide a valid email address": "Please provide a valid email address", "Practice Survey": "Practice Survey", "Previous": "Previous", "Project List": "Project List", "Remove from Device": "Remove from Device", "Reset": "Reset", "Save for later": "Save for later", "Select one": "Select One", "Start New Survey": "Start New Survey", "Start Survey": "Start Survey", "Storage Warning": "Storage Warning", "Survey Completed - Thank You": "Survey Completed - Thank You", "Survey Saved": "Survey Saved", "Survey Uploaded": "Survey Uploaded", "Survey has changed&#46;<br>All stored partial surveys were removed&#46;": "Survey has changed.<br>All stored partial surveys were removed.", "Testing Projects": "Testing Projects", "Uh Oh! An error has occurred when attempting to upload the data&#46;": "Uh Oh! An error has occurred when attempting to upload the data.", "Uh Oh! Upload failed&#46;<br/> Please try again later when you are online&#46;": "Uh Oh! Upload failed.<br/> Please try again later when you are online.", "Unable to log in while offline": "Unable to log in while offline", "Unable to send password reset request while offline": "Unable to send password reset request while offline", "Updating": "Updating", "Uploaded to server": "Uploaded to server", "Uploading": "Uploading", "Username": "Username", "Waiting to be uploaded&#46;&#46;&#46;": "Waiting to be uploaded...", "You are down to 5% on your local storage capacity&#46; Please upload completed surveys&#46;": "You are down to 5% on your local storage capacity. Please upload completed surveys.", "You have no surveys to administer&#46;": "You have no surveys to administer.", "You might want to practice it again and get familiar with the changes&#46; All partial completes are no longer valid and have been removed&#46;": "You might want to practice it again and get familiar with the changes. All partial completes are no longer valid and have been removed." };
});
define("offline/translations/ja-jp", ["exports"], function (exports) {
  exports["default"] = { "<b>Nope&#46; Still offline&#46;</b> We'll automaticaly upload your responses when you get online&#46;": "<b>Nope. Still offline.</b> We'll automaticaly upload your responses when you get online.", "<b>Success!</b> Your completed surveys have been uploaded&#46;": "<b>Success!</b> Your completed surveys have been uploaded.", "<b>Success!</b> Your partial survey have been uploaded&#46;": "<b>Success!</b> Your partial survey have been uploaded.", "Access Code": "Access Code", "Account Login": "Account Login", "An email has been sent to the email address that you supplied": "An email has been sent to the email address that you supplied", "An error occurred when executing code from the survey&#46;": "An error occurred when executing code from the survey.", "Back to Login": "Back to Login", "Click here to try again&#46;": "Click here to try again.", "Click here to try uploading now&#46;": "Click here to try uploading now.", "Code": "Code", "Completed surveys": "Completed surveys", "Completes": "Completes", "Download": "Download", "Downloading": "Downloading", "Downloading Survey": "Downloading Survey", "Email": "Email", "End Practice Mode": "End Practice Mode", "End Survey": "End Survey", "Enter the email address you registered your account with below&#46; You will be sent an email containing information on how to reset your password&#46;": "Enter the email address you registered your account with below. You will be sent an email containing information on how to reset your password.", "FYI, the survey has changed&#46;": "FYI, the survey has changed.", "Failed to Authenticate": "Failed to Authenticate", "Finish": "Finish", "Forgot Password?": "Forgot Password?", "Forgotten Password": "Forgotten Password", "Hmm, there seems to be a problem&#8230;": "Hmm, there seems to be a problem...", "Home": "Home", "It looks like you're offline right now&#46; We'll automatically upload your responses when you get online&#46;": "It looks like you're offline right now. We'll automatically upload your responses when you get online.", "It looks like you're online&#46; We'll go ahead and upload your completed surveys now&#46;": "It looks like you're online. We'll go ahead and upload your completed surveys now.", "Live Projects": "Live Projects", "Login": "Login", "Logout": "Logout", "Menu": "Menu", "Next": "Next", "No description": "No description", "Offline": "Offline", "Offline App": "Offline App", "Partial Surveys (on device)": "Partial Surveys (on device)", "Partials": "Partials", "Password": "Password", "Please provide a access code": "Please provide a access code", "Please provide a email address or user name": "Please provide a email address or user name", "Please provide a password": "Please provide a password", "Please provide a valid email address": "Please provide a valid email address", "Practice Survey": "Practice Survey", "Previous": "Previous", "Project List": "Project List", "Remove from Device": "Remove from Device", "Reset": "Reset", "Save for later": "Save for later", "Select one": "Select One", "Start New Survey": "Start New Survey", "Start Survey": "Start Survey", "Storage Warning": "Storage Warning", "Survey Completed - Thank You": "Survey Completed - Thank You", "Survey Saved": "Survey Saved", "Survey Uploaded": "Survey Uploaded", "Survey has changed&#46;<br>All stored partial surveys were removed&#46;": "Survey has changed.<br>All stored partial surveys were removed.", "Testing Projects": "Testing Projects", "Uh Oh! An error has occurred when attempting to upload the data&#46;": "Uh Oh! An error has occurred when attempting to upload the data.", "Uh Oh! Upload failed&#46;<br/> Please try again later when you are online&#46;": "Uh Oh! Upload failed.<br/> Please try again later when you are online.", "Unable to log in while offline": "Unable to log in while offline", "Unable to send password reset request while offline": "Unable to send password reset request while offline", "Updating": "Updating", "Uploaded to server": "Uploaded to server", "Uploading": "Uploading", "Username": "Username", "Waiting to be uploaded&#46;&#46;&#46;": "Waiting to be uploaded...", "You are down to 5% on your local storage capacity&#46; Please upload completed surveys&#46;": "You are down to 5% on your local storage capacity. Please upload completed surveys.", "You have no surveys to administer&#46;": "You have no surveys to administer.", "You might want to practice it again and get familiar with the changes&#46; All partial completes are no longer valid and have been removed&#46;": "You might want to practice it again and get familiar with the changes. All partial completes are no longer valid and have been removed." };
});
define("offline/translations/pt-pt", ["exports"], function (exports) {
  exports["default"] = { "<b>Nope&#46; Still offline&#46;</b> We'll automaticaly upload your responses when you get online&#46;": "<b>Nope. Still offline.</b> We'll automaticaly upload your responses when you get online.", "<b>Success!</b> Your completed surveys have been uploaded&#46;": "<b>Success!</b> Your completed surveys have been uploaded.", "<b>Success!</b> Your partial survey have been uploaded&#46;": "<b>Success!</b> Your partial survey have been uploaded.", "Access Code": "Access Code", "Account Login": "Account Login", "An email has been sent to the email address that you supplied": "An email has been sent to the email address that you supplied", "An error occurred when executing code from the survey&#46;": "An error occurred when executing code from the survey.", "Back to Login": "Back to Login", "Click here to try again&#46;": "Click here to try again.", "Click here to try uploading now&#46;": "Click here to try uploading now.", "Code": "Code", "Completed surveys": "Completed surveys", "Completes": "Completes", "Download": "Download", "Downloading": "Downloading", "Downloading Survey": "Downloading Survey", "Email": "Email", "End Practice Mode": "End Practice Mode", "End Survey": "End Survey", "Enter the email address you registered your account with below&#46; You will be sent an email containing information on how to reset your password&#46;": "Enter the email address you registered your account with below. You will be sent an email containing information on how to reset your password.", "FYI, the survey has changed&#46;": "FYI, the survey has changed.", "Failed to Authenticate": "Failed to Authenticate", "Finish": "Finish", "Forgot Password?": "Forgot Password?", "Forgotten Password": "Forgotten Password", "Hmm, there seems to be a problem&#8230;": "Hmm, there seems to be a problem...", "Home": "Home", "It looks like you're offline right now&#46; We'll automatically upload your responses when you get online&#46;": "It looks like you're offline right now. We'll automatically upload your responses when you get online.", "It looks like you're online&#46; We'll go ahead and upload your completed surveys now&#46;": "It looks like you're online. We'll go ahead and upload your completed surveys now.", "Live Projects": "Live Projects", "Login": "Login", "Logout": "Logout", "Menu": "Menu", "Next": "Next", "No description": "No description", "Offline": "Offline", "Offline App": "Offline App", "Partial Surveys (on device)": "Partial Surveys (on device)", "Partials": "Partials", "Password": "Password", "Please provide a access code": "Please provide a access code", "Please provide a email address or user name": "Please provide a email address or user name", "Please provide a password": "Please provide a password", "Please provide a valid email address": "Please provide a valid email address", "Practice Survey": "Practice Survey", "Previous": "Previous", "Project List": "Project List", "Remove from Device": "Remove from Device", "Reset": "Reset", "Save for later": "Save for later", "Select one": "Select One", "Start New Survey": "Start New Survey", "Start Survey": "Start Survey", "Storage Warning": "Storage Warning", "Survey Completed - Thank You": "Survey Completed - Thank You", "Survey Saved": "Survey Saved", "Survey Uploaded": "Survey Uploaded", "Survey has changed&#46;<br>All stored partial surveys were removed&#46;": "Survey has changed.<br>All stored partial surveys were removed.", "Testing Projects": "Testing Projects", "Uh Oh! An error has occurred when attempting to upload the data&#46;": "Uh Oh! An error has occurred when attempting to upload the data.", "Uh Oh! Upload failed&#46;<br/> Please try again later when you are online&#46;": "Uh Oh! Upload failed.<br/> Please try again later when you are online.", "Unable to log in while offline": "Unable to log in while offline", "Unable to send password reset request while offline": "Unable to send password reset request while offline", "Updating": "Updating", "Uploaded to server": "Uploaded to server", "Uploading": "Uploading", "Username": "Username", "Waiting to be uploaded&#46;&#46;&#46;": "Waiting to be uploaded...", "You are down to 5% on your local storage capacity&#46; Please upload completed surveys&#46;": "You are down to 5% on your local storage capacity. Please upload completed surveys.", "You have no surveys to administer&#46;": "You have no surveys to administer.", "You might want to practice it again and get familiar with the changes&#46; All partial completes are no longer valid and have been removed&#46;": "You might want to practice it again and get familiar with the changes. All partial completes are no longer valid and have been removed." };
});
define('offline/utils/intl/missing-message', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = missingMessage;
    var logger = _ember['default'].Logger;

    function missingMessage(key, locales) {
        logger.warn('translation: ' + key + ' on locale: ' + locales.join(', ') + ' was not found.');

        return key;
    }
});
define('offline/utils/survey/embedded', ['exports', 'ember', 'offline/utils/survey/logic', 'lodash/lodash'], function (exports, _ember, _offlineUtilsSurveyLogic, _lodashLodash) {

    /**
     * Used by `check(input)` to parse inputs like `>-1` to separate the operator from the comparator (e.g. [>, -1]).
     * @type {RegExp}
     */
    var CHECK_RX = /^([!=<>]+)(.*)/;
    /**
     * Used by `check(input)` to evaluate expressions parsed by the CHECK_RX RegExp.
     */
    var OP_MAP = {
        '=': function _(left, right) {
            return left === right;
        },
        '>': function _(left, right) {
            return left > right;
        },
        '<': function _(left, right) {
            return left < right;
        },
        '>=': function _(left, right) {
            return left >= right;
        },
        '<=': function _(left, right) {
            return left <= right;
        },
        '==': function _(left, right) {
            return left === right;
        },
        '!=': function _(left, right) {
            return left !== right;
        }
    };
    /**
     * Used by `fixup` to convert html entities into operators.
     */
    var HTML_ENTITY_MAP = {
        '&gt;': '>',
        '&lt;': '<',
        '&ge;': '>=',
        '&le;': '<='
    };
    /**
     * Normalizes input strings from `check(input)` by stripping out extra whitespace and converting html entities
     * into operators.
     * @param input
     * @returns {*}
     */
    function fixup(input) {
        //  First remove excessive whitespace
        input = input.replace(/\s/g, '');

        //  Find any occurrences of html entities and switch with its counterpart
        //  (e.g.) `&ge;` --> `>=`
        var keys = Object.keys(HTML_ENTITY_MAP);
        var i, key;
        for (i = 0; i < keys.length; i++) {
            key = keys[i];
            if (input.indexOf(key) > -1) {
                input = input.replace(key, HTML_ENTITY_MAP[key]);
            }
        }
        return input;
    }

    /**
     * Functions available within the survey evaluation context
     * e.g., <exec>setMarker('foo')</exec>
     * This object is merged in with the entire calling context,
     * so "this" refers to anything in our environment (p, q1, whatever)
     * @type {Object.<Function>}
     */
    var EvalContext = {
        updateConditionTable: function updateConditionTable() {
            if (typeof this.conditions === 'undefined') {
                return;
            }
            var conditions = this.conditions.rows;
            for (var cindex = 0; cindex < conditions.length; cindex++) {
                if (conditions[cindex].conditionIsTrue()) {
                    conditions[cindex].val = 1;
                }
            }
        },
        /**
         * Get the aggregate time spent on each page
         * Excludes the time inbetween saving and resuming
         * @returns {Number}
         */
        timeSpent: function timeSpent() {
            var now = new Date() / 1000;
            var spentOnThisPage = now - (this.p.__start_time__ || now);
            return (this.p.__total_time__ || 0) + spentOnThisPage;
        },
        hasMarker: function hasMarker(marker) {
            return this.p.markers.indexOf(marker) > -1;
        },
        setMarker: function setMarker(marker) {
            if (!this.hasMarker(marker)) {
                this.p.markers.push(marker);
            }
        },
        any: function any(inval) {
            if (!_ember['default'].isArray(inval)) {
                throw new TypeError('any() expects an input type of (array). Instead received input of (' + inval + ')');
            }
            for (var i = 0; i < inval.length; i++) {
                if (inval[i]) {
                    return true;
                }
            }
            return false;
        },
        all: function all(inval) {
            if (!_ember['default'].isArray(inval)) {
                throw new TypeError('all() expects an input type of (array). Instead received input of (' + inval + ')');
            }
            for (var i = 0; i < inval.length; i++) {
                if (!inval[i]) {
                    return false;
                }
            }
            return true;
        },
        _in: function _in(item, list) {
            return list.indexOf(item) > -1;
        },
        _print: function _print(text) {
            this.__stdout__.push(text);
        }
    };

    /**
     * An eval within an eval
     * Used to delegate all evaluation to the caller
     * @param code
     * @constructor
     */
    function EmbeddedEval(code) {
        this.code = code;
    }

    /**
     * Wrapper on a generic Element (non-question objects)
     * @param q
     * @constructor
     */
    function E(q) {
        this._q = q;
        this.label = q.label;
    }

    /**
     * Dynamically defined attrs
     * @param {Q} parent
     * @param {DS.Model} cell
     */
    function defineCell(parent, cell) {
        Object.defineProperty(parent, cell.get('label'), {
            enumerable: true,
            get: function get() {
                return new C(parent, cell, parent._attrs);
            }
        });
    }

    /**
     * Wrapper on all questions exposed to the execcontext
     * e.g., the expression "q1.r1.c1" evaluates to the value of variable with label "q1r1c1"
     * @param {DS.Model} q
     * @constructor
     */
    function Q(q) {
        var _this = this;

        this._q = this.o = q;
        this._attrs = { row: null, col: null, choice: null, noanswer: null };
        this._variables = {};

        q.get('variables').forEach(function (variable) {
            _this._variables[variable.get('label')] = variable;
        });

        q.get('rows').forEach(function (row) {
            if (row.get('label')) {
                defineCell(_this, row);
            }
        });
        q.get('cols').forEach(function (col) {
            if (col.get('label')) {
                defineCell(_this, col);
            }
        });
        q.get('choices').forEach(function (choice) {
            if (choice.get('label')) {
                defineCell(_this, choice);
            }
        });
        q.get('noanswer').forEach(function (noanswer) {
            defineCell(_this, noanswer);
        });

        this.label = q.get('label');
    }

    Q.prototype = Object.defineProperties({
        valueOf: function valueOf() {
            return this.val;
        },
        toString: function toString() {
            return this.label;
        },
        getItem: function getItem(item) {
            switch (typeof item) {
                case 'object':
                    return this[item.label];
                case 'number':
                    if (this._q.get('cols.length') > 1) {
                        var col = this._q.get('cols').objectAt(item);
                        return col ? this[col.get('label')] : undefined;
                    } else if (this._q.get('rows.length') > 1) {
                        var row = this._q.get('rows').objectAt(item);
                        return row ? this[row.get('label')] : undefined;
                    }
            }
            throw new _offlineUtilsSurveyLogic.RuntimeError('Item cannot be indexed');
        },

        /**
         * Takes the input and evaluates against the current value.
         * For example: q1.check(">2") where q1.val is 5 evaluates to true.
         * @param input (e.g. "0-100", "0,1,2", ">-1", "1,3,>5", "!=2")
         * @returns {*}
         */
        check: function check(input) {
            //  Normalize input to a predictable state.
            input = fixup(input);

            //  Is this a comma separated list? (e.g.) (0, 4, >25)
            if (input.indexOf(',') > -1) {
                var conds = input.split(',');
                var i,
                    values = [];
                for (i = 0; i < conds.length; i++) {
                    values.push(this.check(conds[i]));
                }
                return EvalContext.any(values);
            }

            var value = this.values;
            if (!(_lodashLodash['default'].isArray(value) && value.length === 1)) {
                throw new _offlineUtilsSurveyLogic.RuntimeError('Unexpected input. Input should be of type Array with 1 value. Instead, received (' + input + ')');
            }

            //  Does the input contain an comparator? (e.g.) (> -1) (!= 2)
            var match = CHECK_RX.exec(input);
            if (match) {
                try {
                    var operator = OP_MAP[match[1]];
                } catch (error) {
                    throw new _offlineUtilsSurveyLogic.RuntimeError('Check() was passed an unknown operator in expression -> (' + input + ')');
                }
                var rightSide = match[2];
                return operator(Number(value), Number(rightSide)); // jshint ignore:line
            } else {
                    //  Is the input a range or does it contain a negative number? (e.g.) (1-100)
                    var negate = false,
                        index = input.indexOf('-');
                    if (index > -1) {
                        if (index === 0) {
                            //	Input is a negative number. Remove minus sign from input.
                            negate = true;
                            input = input.substr(1);
                        }
                        var sides = input.split('-');
                        var left = sides[0];
                        var right = sides[1];
                        if (negate) {
                            input = -input;
                        }
                        return Number(right) >= Number(value) && Number(value) >= Number(left);
                    } else {
                        return Number(value) === Number(input);
                    }
                }
        }
    }, {
        val: {
            /**
             * Return a Boolean if this cell is a value
             * Return the index of the cell selected if this cell is a group
             * @returns {Boolean|Number|Array}
             */

            get: function get() {
                if (this._attrs.noanswer) {
                    return this._q.naVariableFor(this._attrs.noanswer).get('value');
                }
                return this._q.dataFor(this._attrs.row, this._attrs.col, this._attrs.choice);
            },
            set: function set(value) {
                var variable,
                    q = this._q;
                _lodashLodash['default'].forEach(this._attrs, function (cell) {
                    if (cell && cell.type === 'noanswer') {
                        value = value === null ? null : Number(value);
                        if (value === null || value === 0 || value === 1) {
                            variable = q.naVariableFor(cell);
                        }
                        throw new _offlineUtilsSurveyLogic.RuntimeError('Value ' + value + ' not within 0..1');
                    }
                });
                // nb: choices are intentionally ignored
                if (typeof variable === 'undefined') {
                    value = q.validateValue(value);
                    variable = q.variableFor(this._attrs.row, this._attrs.col);
                }
                variable.set('value', value);
            },
            configurable: true,
            enumerable: true
        },
        open: {
            get: function get() {
                return this._variables[this._q.getOEDataLabel(this._c.data || this._q.data)].get('value');
            },
            configurable: true,
            enumerable: true
        },
        title: {
            get: function get() {
                return this._q.getTitle();
            },
            configurable: true,
            enumerable: true
        },
        rows: {
            get: function get() {
                var _this2 = this;

                return this._q.get('rows').map(function (row) {
                    return _this2[row.label];
                });
            },
            configurable: true,
            enumerable: true
        },
        cols: {
            get: function get() {
                var _this3 = this;

                return this._q.get('cols').map(function (col) {
                    return _this3[col.label];
                });
            },
            configurable: true,
            enumerable: true
        },
        choices: {
            get: function get() {
                var _this4 = this;

                return this._q.get('choices').map(function (choice) {
                    return _this4[choice.get('label')];
                });
            },
            configurable: true,
            enumerable: true
        },
        selected: {
            get: function get() {
                var selected = this._q.selected(this._attrs.row, this._attrs.col);
                return this[selected.get('label')];
            },
            configurable: true,
            enumerable: true
        },
        values: {
            get: function get() {
                return this._q.valuesFor(this._attrs.row, this._attrs.col, this._attrs.choice);
            },
            configurable: true,
            enumerable: true
        },
        count: {
            get: function get() {
                var q = this._q;
                return this.values.filter(function (value) {
                    return q.isValue(value);
                }).length;
            },
            configurable: true,
            enumerable: true
        },
        any: {
            get: function get() {
                return this.count > 0;
            },
            configurable: true,
            enumerable: true
        },
        all: {
            get: function get() {
                return this.values.length === this.count;
            },
            configurable: true,
            enumerable: true
        }
    });

    /**
     * Wrapper on all question attributes
     * These are built just-in-time
     * @param [q]     The parent question
     * @param [cell]  This cell
     * @param [attrs] The previously supplied attributes (e.g., 'q1.r1.c1' => {row: {label: 'r1', ..}}
     * @constructor
     */
    function C(parent, cell, attrs) {
        var _this5 = this;

        this._q = parent._q;
        this._c = cell;
        this._variables = parent._variables;
        this._attrs = { row: attrs.row, col: attrs.col, choice: attrs.choice, noanswer: attrs.noanswer };
        cell = cell.data;
        if (attrs[cell.type]) {
            throw new _offlineUtilsSurveyLogic.RuntimeError('This type of object was already supplied (' + cell.type + ' "' + cell.label + '")');
        }
        this._attrs[cell.type] = cell;
        this.index = cell.index;
        this.label = cell.label;
        this.text = cell.cdata;

        this._q.get('rows').forEach(function (row) {
            if (row.get('label')) {
                defineCell(_this5, row);
            }
        });
        this._q.get('cols').forEach(function (col) {
            if (col.get('label')) {
                defineCell(_this5, col);
            }
        });
        this._q.get('choices').forEach(function (choice) {
            if (choice.get('label')) {
                defineCell(_this5, choice);
            }
        });
        this._q.get('noanswer').forEach(function (noanswer) {
            defineCell(_this5, noanswer);
        });
    }

    C.prototype = Object.create(Q.prototype, {
        getItem: {
            value: function getItem(item) {
                switch (typeof item) {
                    case 'object':
                        return this[item.label];
                    case 'number':
                        if (this._attrs.col && !this._attrs.row && !this._attrs.choice) {
                            var row = this._q.get('rows').objectAt(item);
                            return row ? this[row.get('label')] : undefined;
                        } else if (this._attrs.row && !this._attrs.col && !this._attrs.choice) {
                            var col = this._q.get('cols').objectAt(item);
                            return col ? this[col.get('label')] : undefined;
                        }
                }
                throw new _offlineUtilsSurveyLogic.RuntimeError('Cannot further index this object');
            }
        },
        conditionIsTrue: {
            value: function conditionIsTrue() {
                return new EmbeddedEval('Boolean(' + this._c.get('cond') + ')');
            }
        }
    });

    /**
     * Reference to all <condition> tags, including
     * qualified, terminated, and overquota
     * @param {Array.<Object>} [conditions] List of all named-conditions
     * @constructor
     */
    function Condition(conditions) {
        var self = this;
        conditions.forEach(function (condition) {
            Object.defineProperty(self, condition.get('label'), {
                get: function get() {
                    return new EmbeddedEval('Boolean(' + condition.get('cond') + ')');
                }
            });
        });
    }

    Condition.prototype = Object.defineProperties({}, {
        qualified: {
            get: function get() {
                return new EmbeddedEval('hasMarker("qualified")');
            },
            configurable: true,
            enumerable: true
        },
        overquota: {
            get: function get() {
                return new EmbeddedEval('hasMarker("OQ")');
            },
            configurable: true,
            enumerable: true
        },
        terminated: {
            get: function get() {
                return new EmbeddedEval('!(hasMarker("qualified") || hasMarker("OQ"))');
            },
            configurable: true,
            enumerable: true
        }
    });

    /**
     * Reference to all <res> tags
     * @param {Array.<Res>} resources
     * @constructor
     */
    function ResourceWrapper(resources) {
        for (var i = 0; i < resources.length; i++) {
            this[resources[i].get('label')] = resources[i].getContents();
        }
    }

    exports.E = E;
    exports.Q = Q;
    exports.C = C;
    exports.Condition = Condition;
    exports.ResourceWrapper = ResourceWrapper;
    exports.EvalContext = EvalContext;
    exports.EmbeddedEval = EmbeddedEval;
});
define('offline/utils/survey/environment', ['exports', 'ember', 'offline/utils/survey/logic', 'offline/utils/survey/embedded', 'lodash/lang'], function (exports, _ember, _offlineUtilsSurveyLogic, _offlineUtilsSurveyEmbedded, _lodashLang) {

    /**
     * A Survey Session's Environment
     * @constructor
     * @extends {Ember.Object}
     */
    function Environment() {
        /**
         * A plain javascript object acting as our evaluating context (global variables)
         * @type {Object}
         */
        this.ctx = null;
        /**
         * A reference to the current survey object proxy
         * @type {SurveySession}
         */
        this.survey = null;

        this.init = function setUpEnvironment() {
            this.ctx = _lodashLang['default'].clone(_offlineUtilsSurveyEmbedded.EvalContext);
            var self = this;
            var conditions = [];
            var resources = [];
            /**
             * Recursively walk the element tree
             * Creates a reference in the `ctx` for each topLevel element
             * @param {Array.<DS.Model>} elements A QuestionElement, Suspend, Block with nested elements, Condition, etc..
             */
            function walk(elements) {
                elements.forEach(function (element) {
                    if (!element.get('label') || !element.get('topLabel')) {
                        return;
                    }

                    switch (element.get('type')) {
                        case 'res':
                            resources.push(element);
                            return;
                        case 'condition':
                            conditions.push(element);
                            return;
                        case 'label':
                            return;
                    }

                    // finally, create a reference to the question or element in our eval env
                    self.ctx[element.get('label')] = element.get('rows.length') ? new _offlineUtilsSurveyEmbedded.Q(element) : new _offlineUtilsSurveyEmbedded.E(element);
                    walk(element.get('elements'));
                });
            }

            walk(this.survey.get('elements'));

            // setup the persistent dict
            // the respondent's persistent attr references this object
            // so changes are carried over and saved with the respondent, but can be reverted on rollback
            // the rest of our environment lasts only as long as the session is alive
            // these attributes MUST be enumerable, otherwise they're ignored during stringification
            Object.defineProperty(this.ctx, 'p', {
                enumerable: true,
                writeable: false,
                value: _lodashLang['default'].cloneDeep(this.survey.respondent.get('persistent'))
            });
            this.survey.respondent.set('persistent', this.ctx.p);

            // misc hardcoded environment variables
            this.ctx.interviewerId = this.survey.interviewer.get('id');
            this.ctx.condition = new _offlineUtilsSurveyEmbedded.Condition(conditions);
            this.ctx.res = new _offlineUtilsSurveyEmbedded.ResourceWrapper(resources);
        };

        this.evaluate = function surveyEvaluate(code, context) {
            var result = (0, _offlineUtilsSurveyLogic.evaluate)(code, this.ctx, context);
            if (result instanceof _offlineUtilsSurveyEmbedded.EmbeddedEval) {
                return (0, _offlineUtilsSurveyLogic.evaluate)(result.code, this.ctx, context);
            }
            return result;
        };
    }

    exports['default'] = _ember['default'].Object.extend(new Environment());
});
define('offline/utils/survey/logic', ['exports'], function (exports) {
    /* jshint newcap: false */
    /* jshint evil: true */
    /* jshint forin: false */

    function extend(to, from) {
        for (var k in from) {
            //noinspection JSUnfilteredForInLoop
            if (!from.constructor || from.hasOwnProperty(k)) {
                to[k] = from[k];
            }
        }
    }

    var SAFE_ENV = (function safeEnv() {
        var env = Object.create(null);
        // Per MDN:
        // Functions created with the Function constructor do not create closures to their creation contexts;
        // they always are created in the global scope
        env.Function = undefined;
        for (var global in window) {
            //noinspection JSUnfilteredForInLoop
            env[global] = undefined;
        }
        return env;
    })();

    /**
     * Sandboxed eval
     * @param {String} [cond]      The condition
     * @param {Object} [surveyenv] The current survey evaluating context
     * @param {Object} [context]   Some additional variables we want exposed
     * @returns {*}
     */
    function safeEval(cond, surveyenv, context) {
        var env = Object.create(null);
        extend(env, SAFE_ENV);
        extend(env, surveyenv);
        extend(env, context);
        return window.Function('with (this) { return ' + cond + '; }').call(env);
    }

    /**
    * Error wrapping the thrown runtime error
    * with additional info on the code which threw the error.
    * @constructor
    */
    function UserCodeError(name, code, message) {
        this.name = name;
        this.code = code;
        this.message = message;
    }
    UserCodeError.prototype = new Error();
    UserCodeError.prototype.constructor = UserCodeError;

    /**
     * Some Error manually thrown when evaluating user code
     * @constructor
     */
    function RuntimeError(message) {
        this.message = message;
    }
    RuntimeError.prototype = new Error();
    RuntimeError.prototype.constructor = RuntimeError;
    RuntimeError.prototype.name = 'RuntimeError';

    function evaluate(cond, surveyenv, context) {
        try {
            return safeEval(cond, surveyenv, context);
        } catch (error) {
            throw new UserCodeError(error.name, cond, error.message);
        }
    }

    exports.UserCodeError = UserCodeError;
    exports.RuntimeError = RuntimeError;
    exports.evaluate = evaluate;
});
define('offline/utils/survey/preorder', ['exports', 'lodash/lodash'], function (exports, _lodashLodash) {
    exports['default'] = surveyPreorder;

    function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

    var prng;
    var ENTROPY = '*%^RY&CH&maYU29T';

    /**
     * TODO: set prng when called outside of surveyPreorder
     * Utility function
     * Randomization with respect to randomize="0"
     * @param {Array.<DS.Model>} elements
     */
    function randomize(elements) {
        elements = Array.apply(undefined, _toConsumableArray(elements));
        var from = elements.length,
            tmp,
            to;
        FROM: while (from) {
            to = Math.floor(prng() * from--);
            if (to === from || !Number(elements[from].get('randomize'))) {
                continue;
            }
            while (!Number(elements[to].get('randomize'))) {
                if (--to < 0) {
                    continue FROM;
                }
            }
            tmp = elements[from];
            elements[from] = elements[to];
            elements[to] = tmp;
        }
        return elements;
    }

    /**
     * @name surveyPreorder
     * @param {String} seed
     * @param {ArrayProxy.<DS.Model>} elements
     * @returns {Array.<DS.Model>}
     */

    function surveyPreorder(seed, elements) {

        function flatten(elements) {
            var parent = arguments.length <= 1 || arguments[1] === undefined ? null : arguments[1];

            return elements.reduce(function (out, element, index) {
                var _element$getProperties = element.getProperties('rows', 'cols', 'choices');

                var rows = _element$getProperties.rows;
                var cols = _element$getProperties.cols;
                var choices = _element$getProperties.choices;

                if (rows && cols && choices) {
                    var _element$get = element.get('shuffle');

                    var _rows = _element$get.rows;
                    var _cols = _element$get.cols;
                    var _choices = _element$get.choices;

                    if (_rows) {
                        element.shuffleItems('rows');
                    }
                    if (_cols) {
                        element.shuffleItems('cols');
                    }
                    if (_choices) {
                        element.shuffleItems('choices');
                    }
                }
                out.push(element);
                if (element.getWithDefault('elements', []).length) {
                    var order = element.getWithDefault('randomizeChildren', false) ? element.shuffleItems() : element.get('elements').toArray();
                    // randomization must occur before flattening
                    out.push.apply(out, _toConsumableArray(flatten(order, element)));
                }
                return out;
            }, []);
        }

        try {
            prng = new Math.seedrandom(seed + ENTROPY);
            return flatten(elements);
        } finally {
            prng = null;
        }
    }

    exports.randomize = randomize;
});
define('offline/utils/survey/session', ['exports', 'ember', 'offline/utils/survey/environment', 'offline/utils/survey/preorder', 'lodash/lodash'], function (exports, _ember, _offlineUtilsSurveyEnvironment, _offlineUtilsSurveyPreorder, _lodashLodash) {

    var VAR_RX = /\$\((.+?)\)/g;
    var EXPR_RX = /\${(.+?)}/g;
    var PIPE_RX = /\[([a-zA-Z]+?):?\s+(.+?)(?:\s*(lower|upper|title|capitalize))?]/g;
    var TRANSFORMATIONS = {
        lower: function lower(text) {
            return text.toLowerCase();
        },
        upper: function upper(text) {
            return text.toUpperCase();
        },
        title: function title(text) {
            return text.replace(/\S\w*/g, this.capitalize);
        },
        capitalize: function capitalize(text) {
            return text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
        }
    };

    function isDisqualifyingMarker(marker) {
        switch (marker) {
            case 'OQ':
                return true;
            case 'DUPE':
                return true;
            default:
                return (/^(term:|NQ )/.test(marker)
                );
        }
    }

    var Page = _ember['default'].Object.extend({
        index: -1,
        elements: null,
        init: function init() {
            this._super.apply(this, arguments);
            this.elements = [];
        }
    });

    var HALT = {}; // discontinue the current display procedure
    var SKIP = {}; // skip this element

    /**
     * A short-lived survey session
     * Wraps the current project's survey record and rolls back when the session is destroyed
     * Saves serialize and commit persistent things to the respondent record
     * During a session (interview transaction) transient elements and variables are created
     * @constructor
     * @implements {Ember.Object}
     * @extends {Ember.ObjectProxy}
     */
    function SurveySession() {

        this.practice = false;
        /**
         * The current respondent
         * @type {DS.Model}
         */
        this.respondent = null;
        /**
         * The current interviewer
         * @type {DS.Model}
         */
        this.interviewer = null;
        /**
         * The current page (elements, et al)
         * @type {Page}
         */
        this.currentpage = null;
        /**
         * @type Environment
         */
        this.environment = null;
        /**
         * @type {Array.<DS.Model>}
         */
        this.execs = null;

        /**
         * Run all exec code blocks
         * @param { 'finished', 'init', 'started', 'submit', 'survey', 'verified' } [when]
         */
        this.callExec = function callExec(when) {
            var _this = this;

            this.get('execs').filterBy('when.' + when, true).forEach(function (exec) {
                if (!exec.get('cond') || _this.environment.evaluate(exec.get('cond'))) {
                    _this.environment.evaluate(exec.get('cdata'));
                }
            });
        };

        /**
         * Create a fresh survey session and environment
         * Build a list of all our executable blocks of code
         * Order the list of survey elements and their nested elements / cells
         * Create a fresh evaluation environment
         * This is so expensive!
         */
        this.init = function init() {
            _ember['default'].assert('Attempting to load a modified survey', !this.get('isModified'));
            _ember['default'].assert('Attempting to resume a completed respondent', !(this.respondent.get('isResuming') && this.respondent.get('completed')));
            // Update survey messages with project-level overrides
            var message,
                messages = this.get('messages');
            this.get('elements').filterBy('type', 'res').forEach(function (res) {
                if (message = messages.findBy('name', res.get('label'))) {
                    message.set('text', res.get('cdata'));
                } else {
                    messages.createRecord({
                        name: res.get('label'),
                        text: res.get('cdata')
                    });
                }
            });

            // Offline surveys are incompatible with online transient execs
            // These are used to setup the start_date, populate the conditions table, etc
            this.execs = [];
            this.execs.pushObject(this.get('store').createRecord('exec', {
                survey: this.get('path'),
                cdata: 'start_date.val = new Date().toISOString()',
                when: { started: true }
            }));
            this.execs.pushObject(this.get('store').createRecord('exec', {
                survey: this.get('path'),
                cdata: 'qtime.val = timeSpent()',
                when: { finished: true }
            }));
            this.execs.pushObject(this.get('store').createRecord('exec', {
                survey: this.get('path'),
                cdata: 'updateConditionTable()',
                when: { finished: true }
            }));

            // nb: we cannot assume things about the respondent data yet because their data hasn't been loaded
            this.elementList = (0, _offlineUtilsSurveyPreorder['default'])(this.respondent.get('id'), this.get('elements'));
            this.environment = _offlineUtilsSurveyEnvironment['default'].create({ survey: this });
        };

        this.start = function startSurvey() {
            var _this2 = this;

            if (!this.respondent.get('isResuming') && !this.respondent.get('isNew')) {
                this.respondent.restart();
            }
            // reload() restores the current respondent's values from answers
            return this.get('variables').reload().then(function (variables) {
                variables.findBy('label', 'interviewerId').set('value', _this2.interviewer.get('id'));

                if (!_this2.respondent.get('isResuming')) {
                    _this2.callExec('started');
                }

                // do an initial save of some variables applied behind the scenes (e.g., start_date)
                var dontValidate = { adapterOptions: { validate: false } };
                return _ember['default'].RSVP.all(variables.map(function (v) {
                    return v.save(dontValidate);
                }));
            }).then(function () {
                // if we're returning to the survey, drop us where we left off
                if (_this2.respondent.get('isResuming')) {
                    var answers = _this2.respondent.get('answers');

                    if (answers._uncommitted) {
                        _lodashLodash['default'].forEach(answers._uncommitted, function (value, key) {
                            // nb these are dirty (unsaved)
                            _this2.get('variables').findBy('label', key).set('value', value);
                        });
                        delete answers._uncommitted;
                        _this2.respondent.set('answers', answers);
                    }

                    if (_this2.respondent.get('history.length')) {
                        return _this2.gotoTarget(_this2.respondent.get('history').popObject());
                    }
                }
                return _this2.displaySurvey({ startAtIndex: _this2.respondent.get('position') });
            });
        };

        /**
         * After Form Submit
         * The respondent has completed the final page of their survey.
         * Upon submit, an exit page is displayed based on the respondent's completion status.
         * Data is written to local storage and an attempted data-upload occurs. If successful,
         * all previous respondent data is uploaded and local storage is cleared.
         * Add qualifying marker -> call 'finished' exec -> prefix markers if terminated -> save variables
         * @returns {Promise}
         */
        this.finish = function finishSurvey() {
            var _this3 = this;

            var env = this.environment.ctx;

            if (!env.p.markers.some(isDisqualifyingMarker)) {
                env.setMarker('qualified');
                this.respondent.set('status', 3);
            }

            // must be called after assigning the 'qualified' marker
            this.callExec('finished');

            // globals needed for the evaluation of <exit> conditions
            env.qualified = env.overquota = env.terminated = false;
            if (env.hasMarker('qualified')) {
                env.qualified = true;
            } else {
                env.p.markers = env.p.markers.map(function (marker) {
                    if (marker.charAt(0) === '/') {
                        return 'bad:' + marker;
                    }
                    return marker;
                });
                if (env.overquota = env.hasMarker('OQ')) {
                    this.respondent.set('status', 2);
                } else {
                    env.terminated = true;
                    this.respondent.set('status', 1);
                }
            }

            // finally save our answers
            var o = { adapterOptions: { validate: false } };
            var variables = this.get('variables');
            variables.findBy('label', 'markers').set('value', this.environment.ctx.p.markers.join(','));
            variables.findBy('label', 'date').set('value', new Date().toISOString());
            return _ember['default'].RSVP.all(variables.map(function (v) {
                return v.save(o);
            })).then(function () {
                _this3.trigger('finished');
            });
        };

        /**
         * We're exiting a survey session and thus backing out
         * of the current respondent.answers editing transaction.
         * Anything not explicitly comitted (e.g., saveForLater()) will be tossed.
         */
        this.willDestroy = function willDestroy() {
            function rollback(r) {
                r.rollbackAttributes();
            }
            this.environment.destroy();
            this.respondent.rollbackAttributes();
            this.execs.forEach(function (e) {
                return e.destroyRecord();
            });
            // take a snapshot of each ManyArray to avoid mutating the same list of records we're iterating over
            this.get('messages').toArray().forEach(rollback);
            this.get('elements').toArray().forEach(rollback);
            this.get('variables').toArray().forEach(rollback);
        };

        /**
         * Submit our current page variables, saving their values to the current respondent's answers
         */
        this.saveFormData = function saveFormData() {
            return _ember['default'].RSVP.all(this.currentpage.elements.map(function (element) {
                return element.get('variables').save();
            }));
        };

        /**
         * Nest the respondent's uncommitted responses within their answers
         * TODO: don't we need to take a snapshot of their persistent attr?
         * @returns {Ember.RSVP.Promise}
         */
        this.saveForLater = function saveForLater() {
            this.recordTime();
            var answers = this.respondent.get('answers');
            answers._uncommitted = answers._uncommitted || {};
            this.get('variables').forEach(function (variable) {
                if (variable.get('hasDirtyAttributes')) {
                    answers._uncommitted[variable.get('label')] = variable.get('value');
                }
            });
            this.respondent.set('answers', answers);
            return this.respondent.save();
        };

        /**
         * Get some <res label="res1">some text</res> text
         * @param {String} [name]
         * @param {String} [element]
         * @param {Object} [args]
         * @returns {String}
         */
        this.getResource = function getResource(name, element, args) {
            if (!element) {
                return null;
            }
            var messages = this.get('messages');
            var text = messages.findBy('name', element + ',sys_' + name) || messages.findBy('name', 'sys_' + name);
            return this.replaceLanguageVariables(text && text.get('text') || null, args);
        };

        this.replaceLanguageVariables = function replaceLanguageVariables(text, args) {
            function varReplacer(text, arg) {
                if (args.hasOwnProperty(arg)) {
                    return args[arg];
                }
                return '[Var not found: ' + arg + ']';
            }
            return (text || "").replace(VAR_RX, varReplacer);
        };

        /**
         * Replace embedded ${expr}
         * @param text
         */
        this.replaceExpr = function replaceExpr(text) {
            var self = this;
            function exprReplacer(text, expr) {
                return String(self.environment.evaluate(expr)).toString();
            }
            return (text || "").replace(EXPR_RX, exprReplacer);
        };

        /**
         * Replace embedded [variables] and ${expr}
         * @param [text] The text containing some variables
         */
        this.replaceVariables = function replaceVariables(text) {
            var self = this;
            /**
             * Dispatch to the declared instruction, then transform the result
             *   [pipe: q1 lower] => q.pipeText(q1.val).toLowerCase()
             * @param text
             * @param instruction
             * @param label
             * @param transform
             */
            function pipeReplacer(text, instruction, label, transform) {
                var q = self.environment.ctx[label];
                // verify this is a question element and not one of its attributes
                if (!q || !q._attrs || q._attrs.row || q._attrs.col || q._attrs.choice) {
                    return '<span class="pipe-error">INVALID PIPE ' + label + ': not found</span>';
                }
                var func = instruction + 'Text';
                if (!q._q[func]) {
                    return '<span class="pipe-error">INVALID PIPE ' + instruction + '</span>';
                }
                var result = q._q[func]();
                if (!result.valid) {
                    return '<span class="pipe-error">INVALID PIPE ' + label + ': ' + result.text + '</span>';
                }
                if (PIPE_RX.test(result.text) || EXPR_RX.test(result.text)) {
                    return self.replaceVariables(result.text);
                }
                if (typeof result.text === 'undefined') {
                    return '*NO ANSWER*';
                }
                return transform ? TRANSFORMATIONS[transform](result.text) : result.text;
            }

            return this.replaceExpr(text).replace(PIPE_RX, pipeReplacer);
        };

        /**
         * [Re]render the current survey page
         * @param [kwargs]
         * @param [kwargs.nextElement]  Display the next element
         * @param [kwargs.startAtIndex] Skip to a specific location in the survey
         */
        this.displaySurvey = function displaySurvey(kwargs) {
            this.recordTime();
            // reset the page
            this.set('currentpage', Page.create());
            var position = kwargs.nextElement ? this.respondent.get('position') + 1 : kwargs.startAtIndex || 0;
            var element, next;
            try {
                for (; position > -1 && position < this.elementList.length; position++) {
                    /** @type {DS.Model} */
                    element = this.elementList[position];
                    if (element.get('type') === 'suspend') {
                        if (this.currentpage.elements.length) {
                            break;
                        }
                        continue;
                    }
                    if (!element.isDisplayableInSurvey()) {
                        continue;
                    }
                    switch (element.display()) {
                        case HALT:
                            // suspend the current display process (invoked by some term, goto, etc)
                            return; // TODO: return Promise?
                        case SKIP:
                            next = element.getNextElement();
                            position = (next ? this.elementList.indexOf(next) : this.elementList.length) - 1;
                            break;
                        case true:
                            this.currentpage.elements.pushObject(element);
                    }
                }
            } catch (error) {
                this.trigger('exception', error);
                return _ember['default'].RSVP.Promise.reject();
            }
            // if there is nothing to display, then the page is index -1
            if (!this.currentpage.elements.length) {
                return this.finish();
            }
            this.respondent.set('position', this.currentpage.set('index', position - 1));
            this.respondent.get('history').pushObject(this.currentpage.elements[0].get('label'));
            // fixup stuff right before rendering the page
            this.currentpage.elements.forEach(function (element) {
                return element.render();
            });
            this.startTimer();
            return _ember['default'].RSVP.Promise.resolve();
        };

        this.goPrev = function goPrev() {
            // TODO: do we want to reset the currentpage values to null?
            // Go to the first question shown on the last page
            var history = this.respondent.get('history');
            var pageNum = history.length - 1 - 1;
            var previouslyShown = history[pageNum];
            if (!previouslyShown) {
                throw new Error('Cannot go back');
            }
            history.splice(pageNum);
            history.enumerableContentDidChange();
            return this.gotoTarget(previouslyShown);
        };

        this.goNext = function goNext() {
            var _this4 = this;

            return this.saveFormData().then(function () {
                return _this4.displaySurvey({ nextElement: true });
            });
        };

        /**
         * Attempt to skip to some target question (forward or backward).
         * If we cannot display anything, then finish
         * @param {String} [target]
         */
        this.gotoTarget = function gotoTarget(target) {
            for (var index = 0; index < this.elementList.length; index++) {
                if (this.elementList[index].get('label') === target) {
                    break;
                }
            }
            return this.displaySurvey({ startAtIndex: index });
        };

        this.canGoPrev = _ember['default'].computed('respondent.history.[]', function () {
            return this.respondent.get('history.length') > 1;
        });

        this.canGoNext = _ember['default'].computed('respondent.position', function () {
            for (var i = this.respondent.get('position') + 1; i < this.elementList.length; i++) {
                if (this.elementList[i].get('type') === "suspend") {
                    return true;
                }
            }
            return false;
        });

        this.startTimer = function startTimer() {
            this.environment.ctx.p.__start_time__ = new Date() / 1000;
        };

        this.recordTime = function recordTime() {
            var p = this.environment.ctx.p;
            var now = new Date() / 1000;
            p.__total_time__ = (p.__total_time__ || 0) + now - (p.__start_time__ || now);
            p.__start_time__ = 0;
        };
    }

    exports['default'] = _ember['default'].ObjectProxy.extend(_ember['default'].Evented, new SurveySession());
    exports.SKIP = SKIP;
    exports.HALT = HALT;
});
/* jshint ignore:start */



/* jshint ignore:end */

/* jshint ignore:start */

define('offline/config/environment', ['ember'], function(Ember) {
  var exports = {'default': {"modulePrefix":"offline","environment":"mobile","rootURL":"","locationType":"hash","apiNameSpace":"/api/v1/offline","authNameSpace":"http://localhost:5000/admin/ajax/","EmberENV":{"FEATURES":{},"EXTEND_PROTOTYPES":{"Date":false}},"APP":{"rootElement":"body","LOCAL_STORAGE":true,"MEMORY_STORAGE":false,"name":"offline","version":"0.0.0+ea8e7c12"},"serviceWorker":{"register":true,"cacheName":"offline-cache","precache":["assets/vendor.js","assets/vendor.css","assets/offline.js","assets/offline.css","favicon.ico","fonts/OpenSans.ttf","fonts/OpenSans-Bold.ttf","fonts/OpenSans-Light.ttf","fonts/OpenSans-Semibold.ttf","fonts/fontawesome-webfont.woff?v=4.3.0","fonts/fontawesome-webfont.woff2?v=4.3.0"]},"ember-simple-auth":{"baseURL":"","authenticationRoute":"login","routeAfterAuthentication":"authenticated.projects","routeIfAlreadyAuthenticated":"authenticated.projects"},"ember-cli-mirage":{"usingProxy":false,"useDefaultPassthroughs":true},"exportApplicationGlobal":true}};Object.defineProperty(exports, '__esModule', {value: true});return exports;
});

/* jshint ignore:end */

/* jshint ignore:start */

if (!runningTests) {
  require("offline/app")["default"].create({"rootElement":"body","LOCAL_STORAGE":true,"MEMORY_STORAGE":false,"name":"offline","version":"0.0.0+ea8e7c12"});
}

/* jshint ignore:end */
//# sourceMappingURL=offline.map
