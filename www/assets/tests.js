'use strict';

define('offline/tests/acceptance/utils/survey/environment-test', ['exports', 'ember', 'lodash/lodash', 'chai', 'mocha', 'offline/tests/helpers/ember-simple-auth', 'offline/tests/helpers/start-survey', 'offline/tests/helpers/destroy-app'], function (exports, _ember, _lodashLodash, _chai, _mocha, _offlineTestsHelpersEmberSimpleAuth, _offlineTestsHelpersStartSurvey, _offlineTestsHelpersDestroyApp) {

    (0, _mocha.describe)('Survey Environment Test', function () {

        var SURVEY = 'scripts/test/surveys/offline/environment';

        var store = undefined,
            project = undefined,
            environment = undefined,
            application = undefined;

        (0, _mocha.before)(function () {
            return (0, _offlineTestsHelpersStartSurvey['default'])(SURVEY).then(function (app) {
                application = app;
                store = application.__container__.lookup('service:store');
                project = application.__container__.lookup('service:current-project');
                environment = project.get('ssession.environment');
            });
        });

        (0, _mocha.after)(function () {
            (0, _offlineTestsHelpersEmberSimpleAuth.invalidateSession)(application).then(function () {
                return (0, _offlineTestsHelpersDestroyApp['default'])(application);
            });
        });

        (0, _mocha.describe)('sandboxed eval', function () {

            (0, _mocha.it)('should not allow access to globals', function () {
                (0, _chai.expect)(environment.evaluate('window')).to.be.undefined;
                (0, _chai.expect)(environment.evaluate('open')).to.be.undefined;
            });
            (0, _mocha.it)('should allow access to builtins', function () {
                (0, _chai.expect)(environment.evaluate('Number()')).to.be.a('number');
                (0, _chai.expect)(environment.evaluate('Object()')).to.be.a('object');
                (0, _chai.expect)(environment.evaluate('String()')).to.be.a('string');
            });
            (0, _mocha.it)('should not allow access to the Function constructor', function () {
                // If a function is created using the Function constructor
                // then it's calling context is the global scope
                (0, _chai.expect)(environment.evaluate('Function')).to.be.undefined;
            });
        });

        (0, _mocha.describe)('beacon logic', function () {

            var data = {
                q1r1: 0, q1r2: 1, q1r3: 0, q1r4: 1,
                q2r1c1: null, q2r2c1: 1, q2r3c1: null, q2r4c1: 1, q2r5c1: null,
                q2r1c2: 1, q2r2c2: null, q2r3c2: 1, q2r4c2: null, q2r5c2: null,
                q3r1c1: 0, q3r2c1: 2, q3r3c1: 2, q3r4c1: 1,
                q3r1c2: 1, q3r2c2: 3, q3r3c2: 0, q3r4c2: 3,
                q1b: 2,
                q2br1: null, q2br2: 1, q2br3: null, q2br4: 1,
                q3b: 3,
                q4r1c1: 25, q4r1c2: 50, q4r1c3: 75,
                q4r2c1: 0, q4r2c2: 1, q4r2c3: -50,
                q4b: 2.71828,
                qtime: 0,
                start_date: '2015-09-16T17:50:48.438Z'
            };

            (0, _mocha.beforeEach)(function () {
                _ember['default'].run.begin();

                environment.ctx._this = environment.ctx.q1.o.data;
                environment.ctx.thisQuestion = environment.ctx.q1;

                var variables = store.peekRecord('survey', SURVEY).get('variables');
                _lodashLodash['default'].forEach(data, function (value, label) {
                    return variables.findBy('label', label).set('value', value);
                });
            });

            (0, _mocha.afterEach)(function () {
                _ember['default'].run.end();
            });

            (0, _mocha.describe)('embedded misc', function () {
                (0, _mocha.it)('should expose resource tags', function () {
                    (0, _chai.expect)(environment.evaluate('res.simple_resource')).to.equal('and here is some text...');
                    (0, _chai.expect)(environment.evaluate('res["simple_resource"]')).to.equal('and here is some text...');
                });
            });

            (0, _mocha.describe)('Q.val getter', function () {
                (0, _mocha.it)('should return a list if query matches many data points', function () {
                    (0, _chai.expect)(environment.evaluate('q1.val')).to.eql([0, 1, 0, 1]);
                    (0, _chai.expect)(environment.evaluate('q2.val')).to.eql([[null, 1, null, 1, null], [1, null, 1, null, null]]);
                    (0, _chai.expect)(environment.evaluate('q2.r1.val')).to.eql([null, 1]);
                    (0, _chai.expect)(environment.evaluate('q3.val')).to.eql([[0, 2, 2, 1], [1, 3, 0, 3]]);
                    (0, _chai.expect)(environment.evaluate('q3.r1.val')).to.eql([0, 1]);
                    // Radio and Select 1D questions only have one data-point
                    (0, _chai.expect)(environment.evaluate('q2b.val')).to.eql([null, 1, null, 1]);
                });
                (0, _mocha.it)('should return the value if query matches only one data point', function () {
                    (0, _chai.expect)(environment.evaluate('q1.r2.val')).to.equal(1);
                    (0, _chai.expect)(environment.evaluate('q2.r2.c1.val')).to.equal(1);
                    (0, _chai.expect)(environment.evaluate('q3.r2.c1.val')).to.equal(2);
                    (0, _chai.expect)(environment.evaluate('q1b.val')).to.equal(2);
                    (0, _chai.expect)(environment.evaluate('q2b.r2.val')).to.equal(1);
                    (0, _chai.expect)(environment.evaluate('q3b.val')).to.equal(3);
                });
                (0, _mocha.it)('should return a boolean if query matches a single value', function () {
                    (0, _chai.expect)(environment.evaluate('q1.r2.c1.val')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('q1.r2.c2.val')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q3.r2.c1.ch3.val')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q1b.r1.val')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('q1b.r3.val')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q3b.ch3.val')).to['false'];
                    (0, _chai.expect)(environment.evaluate('q3b.ch4.val')).to['true'];
                });
            });

            (0, _mocha.describe)('Q.val setter', function () {
                (0, _mocha.it)('should allow setting the data for a single cell', function () {
                    (0, _chai.expect)(environment.evaluate('q1.r1.c2.val')).to.be['false'];
                    environment.evaluate('q1.r1.val = q1.c2.index');
                    (0, _chai.expect)(environment.evaluate('q1.r1.c2.val')).to.be['true'];

                    (0, _chai.expect)(environment.evaluate('q1b.r1.val')).to.be['false'];
                    environment.evaluate('q1b.val = q1b.r1.index');
                    (0, _chai.expect)(environment.evaluate('q1b.val')).to.be.equal(0);
                });

                (0, _mocha.it)('should error when setting anything other than a single value', function () {
                    (0, _chai.expect)(function throws() {
                        environment.evaluate('q1.val = [0, 1, 0, 1]');
                    }).to['throw'](Error, 'A number is required, not object');
                });

                (0, _mocha.it)('should protect against bad data for numbers and floats', function () {
                    environment.evaluate('q4.r1.c1.val = 3.1415926538');
                    (0, _chai.expect)(environment.evaluate('q4.r1.c1.val')).to.be.equal(3);

                    environment.evaluate('q4b.val = 3.1415926538');
                    (0, _chai.expect)(environment.evaluate('q4b.val')).to.be.equal(3.1415926538);
                });

                (0, _mocha.it)('should ignore select options', function () {
                    environment.evaluate('q3.r1.c1.ch1.val = q3.r1.c1.ch2.index');
                    (0, _chai.expect)(environment.evaluate('q3.r1.c1.ch2.val')).to.be['true'];

                    environment.evaluate('q3b.ch3.val = q3.ch2.index');
                    (0, _chai.expect)(environment.evaluate('q3b.ch2.val')).to.be['true'];
                });

                (0, _mocha.it)('should protect against anything outside the range of allowed values', function () {
                    // radio and select questions only
                    (0, _chai.expect)(function () {
                        environment.evaluate('q1.r1.val = 99');
                    }).to['throw'](Error, 'Value 99 not within 0..1');

                    (0, _chai.expect)(function () {
                        environment.evaluate('q3.r1.c1.val = 99');
                    }).to['throw'](Error, 'Value 99 not within 0..3');
                });
            });

            (0, _mocha.describe)('Q.values', function () {
                (0, _mocha.it)('should return a list', function () {
                    (0, _chai.expect)(environment.evaluate('q1.values')).to.eql([0, 1, 0, 1]);
                    (0, _chai.expect)(environment.evaluate('q2.values')).to.eql([null, 1, null, 1, null, 1, null, 1, null, null]);
                    (0, _chai.expect)(environment.evaluate('q3.values')).to.eql([0, 2, 2, 1, 1, 3, 0, 3]);
                    (0, _chai.expect)(environment.evaluate('q4.values')).to.eql([25, 0, 50, 1, 75, -50]);
                    (0, _chai.expect)(environment.evaluate('q1b.values')).to.eql([2]);
                    (0, _chai.expect)(environment.evaluate('q2b.values')).to.eql([null, 1, null, 1]);
                    (0, _chai.expect)(environment.evaluate('q3b.values')).to.eql([3]);
                });
            });

            (0, _mocha.describe)('C.values', function () {
                (0, _mocha.it)('should return a list', function () {
                    (0, _chai.expect)(environment.evaluate('q1.r1.values')).to.eql([0]);
                    (0, _chai.expect)(environment.evaluate('q1.r2.values')).to.eql([1]);
                    (0, _chai.expect)(environment.evaluate('q2.r1.values')).to.eql([null, 1]);
                    (0, _chai.expect)(environment.evaluate('q2.c1.values')).to.eql([null, 1, null, 1, null]);
                    (0, _chai.expect)(environment.evaluate('q2.c2.values')).to.eql([1, null, 1, null, null]);
                    (0, _chai.expect)(environment.evaluate('q2.r3.c1.values')).to.eql([null]);
                    (0, _chai.expect)(environment.evaluate('q2.r3.c2.values')).to.eql([1]);
                    (0, _chai.expect)(environment.evaluate('q3.r2.values')).to.eql([2, 3]);
                    (0, _chai.expect)(environment.evaluate('q3.c1.values')).to.eql([0, 2, 2, 1]);
                    (0, _chai.expect)(environment.evaluate('q3.c2.values')).to.eql([1, 3, 0, 3]);
                    (0, _chai.expect)(environment.evaluate('q3.r4.c2.values')).to.eql([3]);
                    (0, _chai.expect)(environment.evaluate('q3.r2.c2.values')).to.eql([3]);
                    (0, _chai.expect)(environment.evaluate('q4.r1.values')).to.eql([25, 50, 75]);
                    (0, _chai.expect)(environment.evaluate('q4.r2.values')).to.eql([0, 1, -50]);
                    (0, _chai.expect)(environment.evaluate('q4.c1.values')).to.eql([25, 0]);
                    (0, _chai.expect)(environment.evaluate('q4.c3.values')).to.eql([75, -50]);
                    (0, _chai.expect)(environment.evaluate('q2b.r1.values')).to.eql([null]);
                    (0, _chai.expect)(environment.evaluate('q2b.r2.values')).to.eql([1]);
                });
            });

            (0, _mocha.describe)('C.text', function () {
                (0, _mocha.it)('should return the cell cdata', function () {
                    (0, _chai.expect)(environment.evaluate('q1.c1.text')).to.equal('Tomster');
                    (0, _chai.expect)(environment.evaluate('q1.text')).to.be.undefined;
                });
            });

            (0, _mocha.describe)('any()', function () {
                (0, _mocha.it)('should return true', function () {
                    (0, _chai.expect)(environment.evaluate('!any([0, false])')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('any([1, false, 15])')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('any([q1.r2.c2.val, q1.r2.c1.val])')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('any([q1.r2.c1.val, q1.r2.c2.val])')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('any([q1.r2.any])')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('any([q3.r2.any])')).to.be['true'];
                });
                (0, _mocha.it)('should return false', function () {
                    (0, _chai.expect)(environment.evaluate('!any([1, false])')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('any([0, false])')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('any([q1.r2.c1.val, q2.r1.c1.val])')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('any([q2.r2.all])')).to.be['false'];
                });
            });

            (0, _mocha.describe)('all()', function () {
                (0, _mocha.it)('should return true', function () {
                    (0, _chai.expect)(environment.evaluate('!all([0, false])')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('all([1, true, 15])')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('all([q2.r1.c2.val])')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('all([q2.r2.c1.val])')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('all([q2.r1.c2.val, q2.r2.c1.val])')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('all([q3.r2.any])')).to.be['true'];
                });
                (0, _mocha.it)('should return false', function () {
                    (0, _chai.expect)(environment.evaluate('!all([1, true])')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('all([1, false])')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('!all([q2.r4.c1.val])')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('all([q2.r1.c1.val])')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('all([q2.r3.c1.val])')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('all([q2.r3.c1.val, q2.r1.c1.val])')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('all([q2.r2.all])')).to.be['false'];
                });
            });

            (0, _mocha.describe)('any', function () {
                (0, _mocha.it)('should return true', function () {
                    (0, _chai.expect)(environment.evaluate('q1.any')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q2.any')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q3.any')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q2.r1.any')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q2.c1.any')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q2.r4.c1.any')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q3.r2.any')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q3.c1.any')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q2b.any')).to.be['true'];
                });
                (0, _mocha.it)('should return false', function () {
                    (0, _chai.expect)(environment.evaluate('q2.r5.any')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('q2b.r1.any')).to.be['false'];
                });
            });

            (0, _mocha.describe)('all', function () {
                (0, _mocha.it)('should return true', function () {
                    (0, _chai.expect)(environment.evaluate('q1.all')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q3.all')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q1.r1.all')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q3.c1.all')).to.be['true'];
                });
                (0, _mocha.it)('should return false', function () {
                    (0, _chai.expect)(environment.evaluate('q2.all')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('q2.r1.all')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('q2.c2.all')).to.be['false'];
                });
            });

            (0, _mocha.describe)('check()', function () {
                (0, _mocha.it)('should check range', function () {
                    (0, _chai.expect)(environment.evaluate('q4.r1.c1.check("0-100")')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q4.r1.c1.check("25-100")')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q4.r1.c1.check("26-100")')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('q4.r2.c1.check("0, 1, 2")')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q4.r1.c3.check("0, 1, 2")')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('q4.r2.c1.check("> -1")')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q4.r2.c3.check(">-1")')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('q4.r1.c2.check("1,3,>5")')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q4.r2.c3.check("1,3,>5")')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('q4.r1.c3.check("!= 2")')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q4.r2.c3.check("==2")')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('q4.r1.c3.check("&lt;= 75")')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q4.r1.c3.check("&le; 75")')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q4.r1.c3.check("&lt; 75")')).to.be['false'];
                    (0, _chai.expect)(environment.evaluate('q4.r1.c3.check("&gt;= 75")')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q4.r1.c3.check("&ge; 75")')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q4.r1.c3.check("&gt; 75")')).to.be['false'];
                });
            });

            (0, _mocha.describe)('getItem', function () {
                (0, _mocha.it)('should allow "indexing" by row / col / choice objects', function () {
                    (0, _chai.expect)(environment.evaluate('q1.getItem({type: "row", label: "r1"}).label')).to.be.equal('r1');
                    (0, _chai.expect)(environment.evaluate('q2.getItem({type: "row", label: "c1"}).label')).to.be.equal('c1');
                    (0, _chai.expect)(environment.evaluate('q3.getItem({type: "row", label: "ch1"}).label')).to.be.equal('ch1');
                    (0, _chai.expect)(environment.evaluate('q3.getItem({type: "row", label: "ch2"}).r1.c2.val')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q1.getItem({type: "row", label: "r100"})')).to.be.undefined;
                });
                (0, _mocha.it)('should allow "indexing" by row / col / choice indices', function () {
                    (0, _chai.expect)(environment.evaluate('q1.c1.getItem(q1.r1.index).label')).to.be.equal('r1');
                    (0, _chai.expect)(environment.evaluate('q2.getItem(q2.c1.index).label')).to.be.equal('c1');
                    (0, _chai.expect)(environment.evaluate('q2.r1.getItem(q2.c1.index).label')).to.be.equal('c1');
                    (0, _chai.expect)(environment.evaluate('q3.choices[q3.ch2.index].r1.c2.val')).to.be['true'];
                    (0, _chai.expect)(environment.evaluate('q1.getItem(100)')).to.be.undefined;
                });
            });

            (0, _mocha.describe)('self references', function () {
                (0, _mocha.it)('should support the "_this" object as the current question object', function () {
                    (0, _chai.expect)(environment.evaluate('_this.type')).to.be.equal('radio');
                });
                (0, _mocha.it)('should support the "thisQuestion" object as the current question wrapper', function () {
                    (0, _chai.expect)(environment.evaluate('thisQuestion.r1.c1.val')).to.be['true'];
                });
            });
        });
    });
});
define('offline/tests/acceptance/utils/survey/environment-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | acceptance/utils/survey/environment-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/acceptance/utils/survey/preorder-test', ['exports', 'ember', 'chai', 'mocha', 'offline/tests/helpers/ember-simple-auth', 'offline/tests/helpers/start-app', 'offline/tests/helpers/destroy-app', 'offline/utils/survey/preorder'], function (exports, _ember, _chai, _mocha, _offlineTestsHelpersEmberSimpleAuth, _offlineTestsHelpersStartApp, _offlineTestsHelpersDestroyApp, _offlineUtilsSurveyPreorder) {

    function labels(rows) {
        return rows.map(function (r) {
            return r.get('label');
        });
    }

    function listEquals(list1, list2) {
        if (list1.length !== list2.length) {
            return false;
        }
        for (var i = 0; i < list1.length; i++) {
            if (list1[i] !== list2[i]) {
                return false;
            }
        }
        return true;
    }

    function randomSeed() {
        var text = "",
            possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    (0, _mocha.describe)('Survey Tree Structure Tests', function () {

        var SURVEY = 'scripts/test/surveys/offline/elements';

        var SUSPEND_INDEX = 0;
        var BLOCK_INDEX = 16;

        var application = undefined,
            store = undefined,
            survey = undefined,
            elementlist = undefined;

        (0, _mocha.before)(function () {
            application = (0, _offlineTestsHelpersStartApp['default'])();
            document.cookie = 'HERMES_FKEY=FAKESESSION-' + parseInt(new Date().getTime() / 1000);
            (0, _offlineTestsHelpersEmberSimpleAuth.authenticateSession)(application);
            visit('/admin/' + encodeURIComponent(SURVEY));
            store = application.__container__.lookup('service:store');
        });

        (0, _mocha.after)(function () {
            (0, _offlineTestsHelpersEmberSimpleAuth.invalidateSession)(application).then(function () {
                return (0, _offlineTestsHelpersDestroyApp['default'])(application);
            });
        });

        (0, _mocha.beforeEach)(function () {
            _ember['default'].run.begin();
            survey = store.peekRecord('survey', SURVEY);
            elementlist = (0, _offlineUtilsSurveyPreorder['default'])(randomSeed(), survey.get('elements'));
        });

        (0, _mocha.afterEach)(function () {
            _ember['default'].run.end();
        });

        (0, _mocha.describe)('tree structure', function () {

            (0, _mocha.it)('should allow access to siblings', function () {
                var suspend1 = elementlist[SUSPEND_INDEX];
                (0, _chai.expect)(suspend1.get('prevElement')).to.be['null'];
                (0, _chai.expect)(suspend1.get('nextElement')).to.equal(elementlist[1]);
            });

            (0, _mocha.it)('should preserve hierarchy', function () {
                var block = elementlist[BLOCK_INDEX];
                (0, _chai.expect)(block.get('nextElement')).to.not.equal(elementlist[BLOCK_INDEX + 1]);
                (0, _chai.expect)(block.get('nextElement')).to.equal(elementlist[BLOCK_INDEX + block.get('elements.length') + 1]);
                var lastChild = elementlist[BLOCK_INDEX + block.get('elements.length')];
                (0, _chai.expect)(lastChild.get('nextElement')).to.be['null'];
            });
        });

        (0, _mocha.describe)('randomization', function () {

            (0, _mocha.it)('should randomize cells and child elements only if randomize="1"', function () {
                var attempt = 0,
                    randomized = false,
                    shuffledlabels = [],
                    rowlabels = labels(elementlist[BLOCK_INDEX + 1].get('rows'));
                while (attempt < 10) {
                    shuffledlabels = labels((0, _offlineUtilsSurveyPreorder['default'])(randomSeed(), survey.get('elements'))[BLOCK_INDEX + 1].get('rows.order'));
                    if (shuffledlabels[1] !== 'r2') {
                        _chai.assert.fail(shuffledlabels, rowlabels, 'Incorrectly moved row r2');
                        break;
                    }
                    if (!listEquals(shuffledlabels, rowlabels)) {
                        randomized = true;
                        break;
                    }
                    attempt = attempt + 1;
                }
                if (!randomized) {
                    _chai.assert.fail(shuffledlabels, rowlabels, 'Rows were not randomized');
                }
            });

            (0, _mocha.it)('should not mutate the original list of elements', function () {
                var q6 = survey.findElement('q6');
                (0, _chai.expect)(q6.get('shuffle.rows')).to.be['true'];
                (0, _chai.expect)(q6.get('rows')).to.not.equal(q6.get('rows.order'));
                (0, _chai.expect)(labels(q6.get('rows'))).to.eql(['r1', 'r2', 'r3', 'r4']);

                var q7 = survey.findElement('q7');
                (0, _chai.expect)(q7.get('shuffleBy')).to.equal('q6');
                (0, _chai.expect)(q7.get('rows')).to.not.equal(q7.get('rows.order'));
                (0, _chai.expect)(labels(q7.get('rows'))).to.eql(['r1', 'r2', 'r3', 'r4']);

                var b1 = survey.findElement('b1');
                (0, _chai.expect)(b1.get('elements')).to.not.equal(b1.get('elements.order'));
                (0, _chai.expect)(labels(b1.get('elements'))).to.eql(['q6', undefined, 'q7', 'q8']);
            });

            (0, _mocha.it)('should preserve random order for the same respondent', function () {
                var attempt = 0,
                    seed = randomSeed(),
                    shuffledlabels = [],
                    rowlabels = labels((0, _offlineUtilsSurveyPreorder['default'])(seed, survey.get('elements'))[BLOCK_INDEX + 1].get('rows.order'));
                while (attempt < 10) {
                    shuffledlabels = labels((0, _offlineUtilsSurveyPreorder['default'])(seed, survey.get('elements'))[BLOCK_INDEX + 1].get('rows.order'));
                    if (!listEquals(shuffledlabels, rowlabels)) {
                        _chai.assert.fail(shuffledlabels, rowlabels, 'Rows were incorrectly randomized for respondent' + seed);
                        break;
                    }
                    attempt = attempt + 1;
                }
            });
        });
    });
});
define('offline/tests/acceptance/utils/survey/preorder-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | acceptance/utils/survey/preorder-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/acceptance/utils/survey/session-test', ['exports', 'ember', 'chai', 'mocha', 'offline/tests/helpers/ember-simple-auth', 'offline/tests/helpers/start-app', 'offline/tests/helpers/destroy-app'], function (exports, _ember, _chai, _mocha, _offlineTestsHelpersEmberSimpleAuth, _offlineTestsHelpersStartApp, _offlineTestsHelpersDestroyApp) {

    (0, _mocha.describe)('Survey Session Test', function () {

        var SURVEY = 'scripts/test/surveys/offline/environment';

        var application = undefined;

        (0, _mocha.before)(function () {
            application = (0, _offlineTestsHelpersStartApp['default'])();
            document.cookie = 'HERMES_FKEY=FAKESESSION-' + parseInt(new Date().getTime() / 1000);
            return (0, _offlineTestsHelpersEmberSimpleAuth.authenticateSession)(application);
        });

        (0, _mocha.after)(function () {
            return (0, _offlineTestsHelpersEmberSimpleAuth.invalidateSession)(application).then(function () {
                return (0, _offlineTestsHelpersDestroyApp['default'])(application);
            });
        });

        (0, _mocha.describe)('Piping Text', function () {

            var store = undefined,
                survey = undefined,
                session = undefined;

            var data = {
                q1r1: 0, q1r2: 1, q1r3: 0, q1r4: 1,
                q2r1c1: 1, q2r2c1: 1, q2r3c1: 1,
                q1b: 1,
                q2br1: 1, q2br2: 1, q2br3: 1, q2br4: 1,
                q3b: 3,
                q5: 'here is my answer'
            };

            (0, _mocha.before)(function () {
                visit('/admin/' + encodeURIComponent(SURVEY));
                click('button#start-survey');
                return andThen(function () {
                    store = application.__container__.lookup('service:store');

                    var project = application.__container__.lookup('service:current-project');
                    survey = project.get('survey');
                    session = project.get('ssession');
                });
            });

            (0, _mocha.beforeEach)(function () {
                _ember['default'].run.begin();
                survey.get('variables').forEach(function (v) {
                    if (v.get('label') in data) {
                        v.set('value', data[v.get('label')]);
                    }
                });
            });

            (0, _mocha.afterEach)(function () {
                // reset our answers
                var reset = _ember['default'].RSVP.all(survey.get('variables').map(function (v) {
                    v.set('value', null);
                    return v.save({ adapterOptions: { validate: false } });
                }));
                _ember['default'].run.end();
                return reset;
            });

            (0, _mocha.it)('should pipe the text for the cell selected', function () {
                var renderedCdata = session.replaceVariables('[pipe: q1b]');
                (0, _chai.expect)(renderedCdata).to.equal('Ravenclaw');
            });

            (0, _mocha.it)('should pipe an error message when a single value cannot be resolved', function () {
                var renderedCdata = session.replaceVariables('[pipe: q1]');
                (0, _chai.expect)(renderedCdata).to.equal('<span class="pipe-error">INVALID PIPE q1: too many dimensions</span>');

                renderedCdata = session.replaceVariables('[pipe: q1.r1]');
                (0, _chai.expect)(renderedCdata).to.equal('<span class="pipe-error">INVALID PIPE q1.r1: not found</span>');

                renderedCdata = session.replaceVariables('[pipe: abc]');
                (0, _chai.expect)(renderedCdata).to.equal('<span class="pipe-error">INVALID PIPE abc: not found</span>');

                renderedCdata = session.replaceVariables('[foo: q1]');
                (0, _chai.expect)(renderedCdata).to.equal('<span class="pipe-error">INVALID PIPE foo</span>');
            });

            (0, _mocha.it)('should support nested piping (dynamically)', function () {
                var q3b = survey.findElement('q3b');
                var renderedCdata = session.replaceVariables('[pipe: q3b]');
                // q3b.ch4.cdata is [pipe: q1b]
                // on initial render of the survey: q1b has no data, meaning there is nothing to pipe into q3b.ch4
                // after setting q1b.r2 and q3b.ch4: [pipe: q3b] -> q3b.ch4.cdata -> q1b.r2.cdata
                (0, _chai.expect)(q3b.get('choices').findBy('label', 'ch4').get('displayCdata')).to.equal('*NO ANSWER*');
                (0, _chai.expect)(renderedCdata).to.equal('Ravenclaw');
            });

            (0, _mocha.it)('should support checkbox piping', function () {
                var variables = survey.get('variables');
                var renderedCdata = session.replaceVariables('[pipe: q2b]');
                (0, _chai.expect)(renderedCdata).to.equal('Java, Python, JavaScript, and Ruby');

                variables.findBy('label', 'q2br1').set('value', null);
                variables.findBy('label', 'q2br2').set('value', null);
                variables.findBy('label', 'q2br3').set('value', 1);
                variables.findBy('label', 'q2br4').set('value', null);
                renderedCdata = session.replaceVariables('[pipe: q2b]');
                (0, _chai.expect)(renderedCdata).to.equal('JavaScript');

                variables.findBy('label', 'q2br1').set('value', null);
                variables.findBy('label', 'q2br2').set('value', 1);
                variables.findBy('label', 'q2br3').set('value', null);
                variables.findBy('label', 'q2br4').set('value', 1);
                renderedCdata = session.replaceVariables('[pipe: q2b]');
                (0, _chai.expect)(renderedCdata).to.equal('Python and Ruby');
            });

            (0, _mocha.it)('should not rerender on change', function () {
                var q3bch4 = survey.findElement('q3b').get('choices').findBy('label', 'ch4');
                (0, _chai.expect)(q3bch4.get('cdata')).to.equal('[pipe: q1b]');
                (0, _chai.expect)(q3bch4.get('displayCdata')).to.equal('*NO ANSWER*');

                survey.get('variables').findBy('label', 'q1b').set('value', 3);
                (0, _chai.expect)(q3bch4.get('displayCdata')).to.equal('*NO ANSWER*');
            });

            (0, _mocha.it)('should support open-ended piping', function () {
                var renderedCdata = session.replaceVariables('[pipe: q5]');
                (0, _chai.expect)(renderedCdata).to.equal('here is my answer');
            });

            (0, _mocha.describe)('transformations', function () {
                (0, _mocha.it)('should lowercase', function () {
                    var renderedCdata = session.replaceVariables('[pipe: q2b lower]');
                    (0, _chai.expect)(renderedCdata).to.equal('java, python, javascript, and ruby');
                });
                (0, _mocha.it)('should uppercase', function () {
                    var renderedCdata = session.replaceVariables('[pipe: q2b upper]');
                    (0, _chai.expect)(renderedCdata).to.equal('JAVA, PYTHON, JAVASCRIPT, AND RUBY');
                });
                (0, _mocha.it)('should titlecase', function () {
                    var renderedCdata = session.replaceVariables('[pipe: q5 title]');
                    (0, _chai.expect)(renderedCdata).to.equal('Here Is My Answer');
                });
                (0, _mocha.it)('should capitalize', function () {
                    var renderedCdata = session.replaceVariables('[pipe: q5 capitalize]');
                    (0, _chai.expect)(renderedCdata).to.equal('Here is my answer');
                });
            });
        });

        (0, _mocha.describe)('Session Start / Finish', function () {

            var project = undefined,
                store = undefined,
                survey = undefined,
                session = undefined;

            (0, _mocha.beforeEach)(function () {
                return visit('/admin/' + encodeURIComponent(SURVEY)).then(function () {
                    project = application.__container__.lookup('service:current-project');
                    store = application.__container__.lookup('service:store');
                    survey = project.get('survey');
                });
            });

            (0, _mocha.afterEach)(function () {
                session = null;
                return _ember['default'].RSVP.all(store.peekAll('respondent').map(function (r) {
                    return r.rollbackAttributes();
                }));
            });

            (0, _mocha.it)('should start fresh', function () {
                var now = new Date();
                click('button#start-survey');
                return andThen(function () {
                    var variables = survey.get('variables');

                    variables.forEach(function (v) {
                        var qlabel = v.get('qlabel');
                        if (qlabel && survey.findElement(qlabel).get('where.survey')) {
                            (0, _chai.expect)(v.get('value')).to.be['null'];
                        }
                    });

                    var qtime = variables.findBy('label', 'qtime').get('value');
                    var start_date = variables.findBy('label', 'start_date').get('value');
                    (0, _chai.expect)(qtime).to.be['null'];
                    (0, _chai.expect)(new Date(start_date)).to.be.at.least(now);

                    (0, _chai.expect)(project.get('respondent.isResuming')).to.be['false'];
                });
            });

            (0, _mocha.it)('should repopulate answers when resuming', function () {
                var q5 = "this is my old answer",
                    start_date = '2016-12-31T23:59:59Z';
                project.get('survey.respondents').createRecord({
                    description: "Resume me",
                    answers: { q5: q5, start_date: start_date }
                });
                wait(); // wait for the run loop to process the render queue
                click('.container-respondent:last a');
                return andThen(function () {
                    var variables = project.get('survey.variables');
                    (0, _chai.expect)(variables.findBy('label', 'q5').get('value')).to.equal(q5);
                    (0, _chai.expect)(variables.findBy('label', 'start_date').get('value')).to.equal(start_date);
                });
            });

            (0, _mocha.it)('should save respondent answers (committed and uncommitted)', function () {
                var respondent = undefined;
                var q1b = 2;
                var q5 = "this is NOT committed";
                click('button#start-survey');
                andThen(function () {
                    session = project.get('ssession');
                    respondent = session.get('respondent');

                    var variables = session.get('variables');
                    var vq5 = variables.findBy('label', 'q5');
                    var vq1b = variables.findBy('label', 'q1b');
                    vq5.set('value', q5);
                    vq1b.set('value', q1b);

                    return vq1b.save({ adapterOptions: { validate: false } });
                });
                click('button.menu-button');
                click('button#save-survey');
                andThen(function () {
                    (0, _chai.expect)(respondent.get('answers.q1b')).to.equal(q1b);
                    (0, _chai.expect)(respondent.get('answers.q5')).to.be['null'];
                    (0, _chai.expect)(respondent.get('answers._uncommitted.q5')).to.equal(q5);
                });
                click('.container-respondent:last a');
                return andThen(function () {
                    session = project.get('ssession');
                    respondent = session.get('respondent');

                    var variables = session.get('variables');
                    var vq5 = variables.findBy('label', 'q5');
                    var vq1b = variables.findBy('label', 'q1b');

                    (0, _chai.expect)(vq5.get('value')).to.equal(q5);
                    (0, _chai.expect)(vq5.get('hasDirtyAttributes')).to.be['true'];

                    (0, _chai.expect)(vq1b.get('value')).to.equal(q1b);
                    (0, _chai.expect)(vq1b.get('hasDirtyAttributes')).to.be['false'];
                });
            });
        });
    });
});
define('offline/tests/acceptance/utils/survey/session-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | acceptance/utils/survey/session-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/adapters/application.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | adapters/application.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/application.js should pass jshint.');
  });
});
define('offline/tests/adapters/asset.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | adapters/asset.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/asset.js should pass jshint.');
  });
});
define('offline/tests/adapters/hash.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | adapters/hash.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/hash.js should pass jshint.');
  });
});
define('offline/tests/adapters/interviewer.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | adapters/interviewer.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/interviewer.js should pass jshint.');
  });
});
define('offline/tests/adapters/message.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | adapters/message.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/message.js should pass jshint.');
  });
});
define('offline/tests/adapters/respondent.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | adapters/respondent.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/respondent.js should pass jshint.');
  });
});
define('offline/tests/adapters/survey.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | adapters/survey.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/survey.js should pass jshint.');
  });
});
define('offline/tests/adapters/user.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | adapters/user.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/adapters/variable.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | adapters/variable.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/variable.js should pass jshint.');
  });
});
define('offline/tests/app.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | app.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass jshint.');
  });
});
define('offline/tests/authenticators/authenticator.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | authenticators/authenticator.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/authorizers/authorizer.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | authorizers/authorizer.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/components/atm1d-element.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | components/atm1d-element.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/components/checkbox-element.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/checkbox-element.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/checkbox-element.js should pass jshint.');
  });
});
define('offline/tests/components/checkbox-input.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/checkbox-input.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/checkbox-input.js should pass jshint.');
  });
});
define('offline/tests/components/comment-element.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/comment-element.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/comment-element.js should pass jshint.');
  });
});
define('offline/tests/components/completes-summary.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/completes-summary.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/completes-summary.js should pass jshint.');
  });
});
define('offline/tests/components/exit-page.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/exit-page.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/exit-page.js should pass jshint.');
  });
});
define('offline/tests/components/float-element.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/float-element.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/float-element.js should pass jshint.');
  });
});
define('offline/tests/components/float-input.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/float-input.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/float-input.js should pass jshint.');
  });
});
define('offline/tests/components/forgot-form.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/forgot-form.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/forgot-form.js should pass jshint.');
  });
});
define('offline/tests/components/html-element.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/html-element.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/html-element.js should pass jshint.');
  });
});
define('offline/tests/components/image-element.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | components/image-element.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/components/imgupload-element.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | components/imgupload-element.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/components/language-menu.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/language-menu.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/language-menu.js should pass jshint.');
  });
});
define('offline/tests/components/login-form.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | components/login-form.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/components/logout-button.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/logout-button.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/logout-button.js should pass jshint.');
  });
});
define('offline/tests/components/noanswer-input.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/noanswer-input.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/noanswer-input.js should pass jshint.');
  });
});
define('offline/tests/components/number-element.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/number-element.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/number-element.js should pass jshint.');
  });
});
define('offline/tests/components/number-input.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/number-input.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/number-input.js should pass jshint.');
  });
});
define('offline/tests/components/partials-summary.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/partials-summary.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/partials-summary.js should pass jshint.');
  });
});
define('offline/tests/components/projects-list-item.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/projects-list-item.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/projects-list-item.js should pass jshint.');
  });
});
define('offline/tests/components/projects-list.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/projects-list.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/projects-list.js should pass jshint.');
  });
});
define('offline/tests/components/projects-nav.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/projects-nav.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/projects-nav.js should pass jshint.');
  });
});
define('offline/tests/components/question-element.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | components/question-element.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/components/question-error.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/question-error.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/question-error.js should pass jshint.');
  });
});
define('offline/tests/components/radio-element.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/radio-element.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/radio-element.js should pass jshint.');
  });
});
define('offline/tests/components/radio-input.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/radio-input.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/radio-input.js should pass jshint.');
  });
});
define('offline/tests/components/select-element.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/select-element.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/select-element.js should pass jshint.');
  });
});
define('offline/tests/components/select-input.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/select-input.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/select-input.js should pass jshint.');
  });
});
define('offline/tests/components/text-element.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/text-element.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/text-element.js should pass jshint.');
  });
});
define('offline/tests/components/text-input.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/text-input.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/text-input.js should pass jshint.');
  });
});
define('offline/tests/components/textarea-element.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/textarea-element.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/textarea-element.js should pass jshint.');
  });
});
define('offline/tests/components/textarea-input.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/textarea-input.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/textarea-input.js should pass jshint.');
  });
});
define('offline/tests/components/user-menu.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/user-menu.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/user-menu.js should pass jshint.');
  });
});
define('offline/tests/components/videocapture-element.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | components/videocapture-element.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/controllers/authenticated/admin.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | controllers/authenticated/admin.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/authenticated/admin.js should pass jshint.');
  });
});
define('offline/tests/controllers/authenticated/respview.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | controllers/authenticated/respview.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/authenticated/respview.js should pass jshint.');
  });
});
define('offline/tests/controllers/authenticated/respview/error.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | controllers/authenticated/respview/error.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/authenticated/respview/error.js should pass jshint.');
  });
});
define('offline/tests/controllers/authenticated/respview/exitpage.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | controllers/authenticated/respview/exitpage.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/authenticated/respview/exitpage.js should pass jshint.');
  });
});
define('offline/tests/controllers/authenticated/respview/survey.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | controllers/authenticated/respview/survey.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/authenticated/respview/survey.js should pass jshint.');
  });
});
define('offline/tests/controllers/authenticated/respview/survey/page.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | controllers/authenticated/respview/survey/page.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/authenticated/respview/survey/page.js should pass jshint.');
  });
});
define('offline/tests/controllers/login/forgot.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | controllers/login/forgot.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/login/forgot.js should pass jshint.');
  });
});
define('offline/tests/controllers/login/index.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | controllers/login/index.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/login/index.js should pass jshint.');
  });
});
define('offline/tests/formats.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | formats.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'formats.js should pass jshint.');
  });
});
define('offline/tests/helpers/destroy-app', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = destroyApp;

  function destroyApp(application) {
    _ember['default'].run(application, 'destroy');
  }
});
define('offline/tests/helpers/destroy-app.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | helpers/destroy-app.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/helpers/ember-simple-auth', ['exports', 'ember-simple-auth/authenticators/test'], function (exports, _emberSimpleAuthAuthenticatorsTest) {
  exports.authenticateSession = authenticateSession;
  exports.currentSession = currentSession;
  exports.invalidateSession = invalidateSession;

  var TEST_CONTAINER_KEY = 'authenticator:test';

  function ensureAuthenticator(app, container) {
    var authenticator = container.lookup(TEST_CONTAINER_KEY);
    if (!authenticator) {
      app.register(TEST_CONTAINER_KEY, _emberSimpleAuthAuthenticatorsTest['default']);
    }
  }

  function authenticateSession(app, sessionData) {
    var container = app.__container__;

    var session = container.lookup('service:session');
    ensureAuthenticator(app, container);
    session.authenticate(TEST_CONTAINER_KEY, sessionData);
    return wait();
  }

  ;

  function currentSession(app) {
    return app.__container__.lookup('service:session');
  }

  ;

  function invalidateSession(app) {
    var session = app.__container__.lookup('service:session');
    if (session.get('isAuthenticated')) {
      session.invalidate();
    }
    return wait();
  }

  ;
});
define('offline/tests/helpers/format-date.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | helpers/format-date.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/format-date.js should pass jshint.');
  });
});
define('offline/tests/helpers/module-for-acceptance', ['exports', 'qunit', 'ember', 'offline/tests/helpers/start-app', 'offline/tests/helpers/destroy-app'], function (exports, _qunit, _ember, _offlineTestsHelpersStartApp, _offlineTestsHelpersDestroyApp) {
  var Promise = _ember['default'].RSVP.Promise;

  exports['default'] = function (name) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    (0, _qunit.module)(name, {
      beforeEach: function beforeEach() {
        this.application = (0, _offlineTestsHelpersStartApp['default'])();

        if (options.beforeEach) {
          return options.beforeEach.apply(this, arguments);
        }
      },

      afterEach: function afterEach() {
        var _this = this;

        var afterEach = options.afterEach && options.afterEach.apply(this, arguments);
        return Promise.resolve(afterEach).then(function () {
          return (0, _offlineTestsHelpersDestroyApp['default'])(_this.application);
        });
      }
    });
  };
});
define('offline/tests/helpers/module-for-acceptance.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | helpers/module-for-acceptance.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/helpers/question-component-name.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | helpers/question-component-name.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/question-component-name.js should pass jshint.');
  });
});
define('offline/tests/helpers/resolver', ['exports', 'offline/resolver', 'offline/config/environment'], function (exports, _offlineResolver, _offlineConfigEnvironment) {

  var resolver = _offlineResolver['default'].create();

  resolver.namespace = {
    modulePrefix: _offlineConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _offlineConfigEnvironment['default'].podModulePrefix
  };

  exports['default'] = resolver;
});
define('offline/tests/helpers/resolver.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | helpers/resolver.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/helpers/start-app', ['exports', 'ember', 'offline/app', 'offline/config/environment'], function (exports, _ember, _offlineApp, _offlineConfigEnvironment) {
  exports['default'] = startApp;

  function startApp(attrs) {
    var application = undefined;

    var attributes = _ember['default'].merge({}, _offlineConfigEnvironment['default'].APP);
    attributes = _ember['default'].merge(attributes, attrs); // use defaults, but you can override;

    _ember['default'].run(function () {
      application = _offlineApp['default'].create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
    });

    return application;
  }
});
define('offline/tests/helpers/start-app.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | helpers/start-app.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/helpers/start-survey', ['exports', 'offline/tests/helpers/ember-simple-auth', 'offline/tests/helpers/start-app'], function (exports, _offlineTestsHelpersEmberSimpleAuth, _offlineTestsHelpersStartApp) {
    exports['default'] = startSurvey;

    function startSurvey(survey) {
        var application = (0, _offlineTestsHelpersStartApp['default'])();
        document.cookie = 'HERMES_FKEY=FAKESESSION-' + parseInt(new Date().getTime() / 1000);
        (0, _offlineTestsHelpersEmberSimpleAuth.authenticateSession)(application);
        visit('/admin/' + encodeURIComponent(survey));
        click('button#start-survey');
        return wait().then(function () {
            return application;
        });
    }
});
define('offline/tests/helpers/start-survey.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | helpers/start-survey.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/helpers/survey-component-setup', ['exports', 'ember', 'offline/utils/survey/session', 'offline/mirage/fixtures/surveys'], function (exports, _ember, _offlineUtilsSurveySession, _offlineMirageFixturesSurveys) {
    exports['default'] = surveyComponentSetup;

    function surveyComponentSetup() {
        this.register('service:service-worker', _ember['default'].Object.extend());
        this.register('service:current-user', _ember['default'].Object.extend());
        this.inject.service('store');
        this.inject.service('current-project', { as: 'project' });
        this.store.pushPayload('survey', { 'survey': _offlineMirageFixturesSurveys['default'].find(function (s) {
                return s.path === 'scripts/test/surveys/offline/elements';
            }) });
        this.project.set('ssession', _offlineUtilsSurveySession['default'].create({
            content: this.store.peekRecord('survey', 'scripts/test/surveys/offline/elements'),
            respondent: this.store.createRecord('respondent'),
            interviewer: this.store.createRecord('interviewer')
        }));
    }
});
define('offline/tests/helpers/survey-component-setup.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | helpers/survey-component-setup.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/initializers/intl.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | initializers/intl.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'initializers/intl.js should pass jshint.');
  });
});
define('offline/tests/instance-initializers/current-user.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | instance-initializers/current-user.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/instance-initializers/service-worker.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | instance-initializers/service-worker.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'instance-initializers/service-worker.js should pass jshint.');
  });
});
define('offline/tests/integration/components/atm1d-element-test', ['exports', 'chai', 'mocha', 'ember-mocha', 'offline/tests/helpers/survey-component-setup'], function (exports, _chai, _mocha, _emberMocha, _offlineTestsHelpersSurveyComponentSetup) {

    (0, _mocha.describe)('Integration | Component | atm1d element', function () {

        (0, _emberMocha.setupComponentTest)('atm1d element', {
            integration: true,
            setup: function setup() {
                _offlineTestsHelpersSurveyComponentSetup['default'].call(this);
                this.set('question', this.project.get('ssession.elements').findBy('label', 'q1'));
            }
        });

        (0, _mocha.it)('renders', function () {
            // Handle any actions with this.on('myAction', function(val) { ... });
            this.render(Ember.HTMLBars.template((function () {
                return {
                    meta: {
                        'revision': 'Ember@2.9.0',
                        'loc': {
                            'source': null,
                            'start': {
                                'line': 1,
                                'column': 0
                            },
                            'end': {
                                'line': 1,
                                'column': 35
                            }
                        }
                    },
                    isEmpty: false,
                    arity: 0,
                    cachedFragment: null,
                    hasRendered: false,
                    buildFragment: function buildFragment(dom) {
                        var el0 = dom.createDocumentFragment();
                        var el1 = dom.createComment('');
                        dom.appendChild(el0, el1);
                        return el0;
                    },
                    buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
                        var morphs = new Array(1);
                        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
                        dom.insertBoundary(fragment, 0);
                        dom.insertBoundary(fragment, null);
                        return morphs;
                    },
                    statements: [['inline', 'atm1d-element', [], ['question', ['subexpr', '@mut', [['get', 'question', ['loc', [null, [1, 25], [1, 33]]], 0, 0, 0, 0]], [], [], 0, 0]], ['loc', [null, [1, 0], [1, 35]]], 0, 0]],
                    locals: [],
                    templates: []
                };
            })()));

            var content = this.$().text().split('\n').map(function (s) {
                return s.trim();
            }).filter(function (s) {
                return s.length;
            });

            (0, _chai.expect)(content).to.eql(['New Button Select (Multi-Select) Question example 1', 'Select all buttons that apply.', 'example 1', 'example 2', 'example 3', 'example 4', 'example 5 (skip to q4)']);
        });
    });
});
define('offline/tests/integration/components/atm1d-element-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | integration/components/atm1d-element-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/integration/components/checkbox-element-test', ['exports', 'chai', 'mocha', 'ember-mocha', 'offline/tests/helpers/survey-component-setup'], function (exports, _chai, _mocha, _emberMocha, _offlineTestsHelpersSurveyComponentSetup) {

    (0, _mocha.describe)('Integration | Component | checkbox element', function () {

        (0, _emberMocha.setupComponentTest)('checkbox element', {
            integration: true,
            setup: function setup() {
                _offlineTestsHelpersSurveyComponentSetup['default'].call(this);
                this.set('question', this.project.get('ssession.elements').findBy('label', 'q4'));
            }
        });

        (0, _mocha.it)('renders as a list', function () {
            this.set('isGrid', false);

            this.render(Ember.HTMLBars.template((function () {
                return {
                    meta: {
                        'revision': 'Ember@2.9.0',
                        'loc': {
                            'source': null,
                            'start': {
                                'line': 1,
                                'column': 0
                            },
                            'end': {
                                'line': 1,
                                'column': 38
                            }
                        }
                    },
                    isEmpty: false,
                    arity: 0,
                    cachedFragment: null,
                    hasRendered: false,
                    buildFragment: function buildFragment(dom) {
                        var el0 = dom.createDocumentFragment();
                        var el1 = dom.createComment('');
                        dom.appendChild(el0, el1);
                        return el0;
                    },
                    buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
                        var morphs = new Array(1);
                        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
                        dom.insertBoundary(fragment, 0);
                        dom.insertBoundary(fragment, null);
                        return morphs;
                    },
                    statements: [['inline', 'checkbox-element', [], ['question', ['subexpr', '@mut', [['get', 'question', ['loc', [null, [1, 28], [1, 36]]], 0, 0, 0, 0]], [], [], 0, 0]], ['loc', [null, [1, 0], [1, 38]]], 0, 0]],
                    locals: [],
                    templates: []
                };
            })()));

            var content = this.$('.cdata:visible').text();

            (0, _chai.expect)(content).to.equal('TermCol 1Col 2Don\'t TermCol 1Col 2');
        });

        (0, _mocha.it)('renders as a grid', function () {
            this.set('isGrid', true);

            this.render(Ember.HTMLBars.template((function () {
                return {
                    meta: {
                        'revision': 'Ember@2.9.0',
                        'loc': {
                            'source': null,
                            'start': {
                                'line': 1,
                                'column': 0
                            },
                            'end': {
                                'line': 1,
                                'column': 38
                            }
                        }
                    },
                    isEmpty: false,
                    arity: 0,
                    cachedFragment: null,
                    hasRendered: false,
                    buildFragment: function buildFragment(dom) {
                        var el0 = dom.createDocumentFragment();
                        var el1 = dom.createComment('');
                        dom.appendChild(el0, el1);
                        return el0;
                    },
                    buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
                        var morphs = new Array(1);
                        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
                        dom.insertBoundary(fragment, 0);
                        dom.insertBoundary(fragment, null);
                        return morphs;
                    },
                    statements: [['inline', 'checkbox-element', [], ['question', ['subexpr', '@mut', [['get', 'question', ['loc', [null, [1, 28], [1, 36]]], 0, 0, 0, 0]], [], [], 0, 0]], ['loc', [null, [1, 0], [1, 38]]], 0, 0]],
                    locals: [],
                    templates: []
                };
            })()));

            var content = this.$('.cdata:visible').text();

            (0, _chai.expect)(content).to.equal('Col 1Col 2TermDon\'t Term');
        });
    });
});
define('offline/tests/integration/components/checkbox-element-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | integration/components/checkbox-element-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/integration/components/comment-element-test', ['exports', 'mocha', 'ember-mocha'], function (exports, _mocha, _emberMocha) {

  // TODO: need a <html> and <comment> element
  (0, _mocha.describe)('Integration | Component | comment element', function () {

    (0, _emberMocha.setupComponentTest)('comment element', {
      integration: true,
      setup: function setup() {}
    });
  });
});
define('offline/tests/integration/components/comment-element-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | integration/components/comment-element-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/integration/components/completes-summary-test', ['exports', 'ember', 'chai', 'mocha', 'ember-mocha'], function (exports, _ember, _chai, _mocha, _emberMocha) {

    var O = _ember['default'].Object.create.bind(_ember['default'].Object);

    (0, _mocha.describe)('Integration | Component | completes summary', function () {

        (0, _emberMocha.setupComponentTest)('completes summary', {
            integration: true,
            setup: function setup() {
                // setup a mock project
                this.register('service:current-project', _ember['default'].Object.extend({
                    user: O({
                        id: 'me',
                        interviewers: []
                    }),
                    path: '/test/survey/path',
                    survey: O({
                        respondents: [O({ id: 1, completed: true }), O({ id: 2, completed: true })]
                    }),
                    ssession: O(),
                    interviewer: O({
                        total_completes: 500
                    }),
                    status: O({
                        completes: O({ counts: 50, success: true })
                    })
                }));
                this.inject.service('current-project', { as: 'project' });
            }
        });

        (0, _mocha.it)('displays the interviewer total completes', function () {
            this.render(_ember['default'].HTMLBars.template((function () {
                return {
                    meta: {
                        'revision': 'Ember@2.9.0',
                        'loc': {
                            'source': null,
                            'start': {
                                'line': 1,
                                'column': 0
                            },
                            'end': {
                                'line': 1,
                                'column': 21
                            }
                        }
                    },
                    isEmpty: false,
                    arity: 0,
                    cachedFragment: null,
                    hasRendered: false,
                    buildFragment: function buildFragment(dom) {
                        var el0 = dom.createDocumentFragment();
                        var el1 = dom.createComment('');
                        dom.appendChild(el0, el1);
                        return el0;
                    },
                    buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
                        var morphs = new Array(1);
                        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
                        dom.insertBoundary(fragment, 0);
                        dom.insertBoundary(fragment, null);
                        return morphs;
                    },
                    statements: [['content', 'completes-summary', ['loc', [null, [1, 0], [1, 21]]], 0, 0, 0, 0]],
                    locals: [],
                    templates: []
                };
            })()));
            (0, _chai.expect)(this.$('.status-count').text().trim()).to.equal('500');
        });

        (0, _mocha.it)('displays the amount waiting to upload', function () {
            this.render(_ember['default'].HTMLBars.template((function () {
                return {
                    meta: {
                        'revision': 'Ember@2.9.0',
                        'loc': {
                            'source': null,
                            'start': {
                                'line': 1,
                                'column': 0
                            },
                            'end': {
                                'line': 1,
                                'column': 21
                            }
                        }
                    },
                    isEmpty: false,
                    arity: 0,
                    cachedFragment: null,
                    hasRendered: false,
                    buildFragment: function buildFragment(dom) {
                        var el0 = dom.createDocumentFragment();
                        var el1 = dom.createComment('');
                        dom.appendChild(el0, el1);
                        return el0;
                    },
                    buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
                        var morphs = new Array(1);
                        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
                        dom.insertBoundary(fragment, 0);
                        dom.insertBoundary(fragment, null);
                        return morphs;
                    },
                    statements: [['content', 'completes-summary', ['loc', [null, [1, 0], [1, 21]]], 0, 0, 0, 0]],
                    locals: [],
                    templates: []
                };
            })()));
            (0, _chai.expect)(this.$('#waiting span:first').text().trim()).to.equal('2');
        });
    });
});
define('offline/tests/integration/components/completes-summary-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | integration/components/completes-summary-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/integration/components/html-element-test', ['exports', 'chai', 'mocha', 'ember-mocha'], function (exports, _chai, _mocha, _emberMocha) {

  (0, _mocha.describe)('Integration | Component | html element', function () {
    (0, _emberMocha.setupComponentTest)('html-element', {
      integration: true
    });

    (0, _mocha.it)('renders', function () {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.on('myAction', function(val) { ... });
      // Template block usage:
      // this.render(hbs`
      //   {{#html-element}}
      //     template content
      //   {{/html-element}}
      // `);

      this.render(Ember.HTMLBars.template((function () {
        return {
          meta: {
            'revision': 'Ember@2.9.1',
            'loc': {
              'source': null,
              'start': {
                'line': 1,
                'column': 0
              },
              'end': {
                'line': 1,
                'column': 16
              }
            }
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment('');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
            dom.insertBoundary(fragment, 0);
            dom.insertBoundary(fragment, null);
            return morphs;
          },
          statements: [['content', 'html-element', ['loc', [null, [1, 0], [1, 16]]], 0, 0, 0, 0]],
          locals: [],
          templates: []
        };
      })()));
      (0, _chai.expect)(this.$()).to.have.length(1);
    });
  });
});
define('offline/tests/integration/components/html-element-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | integration/components/html-element-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/integration/components/imgupload-element-test', ['exports', 'chai', 'mocha', 'ember-mocha'], function (exports, _chai, _mocha, _emberMocha) {

  (0, _mocha.describe)('Integration | Component | imgupload element', function () {
    (0, _emberMocha.setupComponentTest)('imgupload-element', {
      integration: true
    });

    (0, _mocha.it)('renders', function () {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.on('myAction', function(val) { ... });
      // Template block usage:
      // this.render(hbs`
      //   {{#imgupload-element}}
      //     template content
      //   {{/imgupload-element}}
      // `);

      this.render(Ember.HTMLBars.template((function () {
        return {
          meta: {
            'revision': 'Ember@2.9.0',
            'loc': {
              'source': null,
              'start': {
                'line': 1,
                'column': 0
              },
              'end': {
                'line': 1,
                'column': 21
              }
            }
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment('');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
            dom.insertBoundary(fragment, 0);
            dom.insertBoundary(fragment, null);
            return morphs;
          },
          statements: [['content', 'imgupload-element', ['loc', [null, [1, 0], [1, 21]]], 0, 0, 0, 0]],
          locals: [],
          templates: []
        };
      })()));
      (0, _chai.expect)(this.$()).to.have.length(1);
    });
  });
});
define('offline/tests/integration/components/imgupload-element-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | integration/components/imgupload-element-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/integration/components/partials-summary-test', ['exports', 'ember', 'chai', 'mocha', 'ember-mocha'], function (exports, _ember, _chai, _mocha, _emberMocha) {

    var O = _ember['default'].Object.create.bind(_ember['default'].Object);

    (0, _mocha.describe)('Integration | Component | partials summary', function () {

        (0, _emberMocha.setupComponentTest)('partials summary', {
            integration: true,
            setup: function setup() {
                // setup a mock project
                this.register('service:current-project', _ember['default'].Object.extend({
                    user: O({
                        id: 'me',
                        interviewers: []
                    }),
                    path: '/test/survey/path',
                    survey: O({
                        respondents: [O({ id: 1, partial: true }), O({ id: 2, partial: true }), O({ id: 3, partial: true })]
                    }),
                    ssession: O(),
                    interviewer: O({
                        total_completes: 99,
                        partial_completes: 600
                    }),
                    status: O({
                        partials: O({ counts: 10, success: true })
                    })
                }));
                this.inject.service('current-project', { as: 'project' });
            }
        });

        (0, _mocha.it)('displays all partials on the device', function () {
            this.render(_ember['default'].HTMLBars.template((function () {
                return {
                    meta: {
                        'revision': 'Ember@2.9.0',
                        'loc': {
                            'source': null,
                            'start': {
                                'line': 1,
                                'column': 0
                            },
                            'end': {
                                'line': 1,
                                'column': 20
                            }
                        }
                    },
                    isEmpty: false,
                    arity: 0,
                    cachedFragment: null,
                    hasRendered: false,
                    buildFragment: function buildFragment(dom) {
                        var el0 = dom.createDocumentFragment();
                        var el1 = dom.createComment('');
                        dom.appendChild(el0, el1);
                        return el0;
                    },
                    buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
                        var morphs = new Array(1);
                        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
                        dom.insertBoundary(fragment, 0);
                        dom.insertBoundary(fragment, null);
                        return morphs;
                    },
                    statements: [['content', 'partials-summary', ['loc', [null, [1, 0], [1, 20]]], 0, 0, 0, 0]],
                    locals: [],
                    templates: []
                };
            })()));
            (0, _chai.expect)(this.$('.status-count').text().trim()).to.equal('3');
            (0, _chai.expect)(this.$('.container-respondent').length).to.equal(3);
        });
    });
});
define('offline/tests/integration/components/partials-summary-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | integration/components/partials-summary-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/integration/components/videocapture-element-test', ['exports', 'chai', 'mocha', 'ember-mocha'], function (exports, _chai, _mocha, _emberMocha) {

  (0, _mocha.describe)('Integration | Component | videocapture element', function () {
    (0, _emberMocha.setupComponentTest)('videocapture-element', {
      integration: true
    });

    (0, _mocha.it)('renders', function () {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.on('myAction', function(val) { ... });
      // Template block usage:
      // this.render(hbs`
      //   {{#videocapture-element}}
      //     template content
      //   {{/videocapture-element}}
      // `);

      this.render(Ember.HTMLBars.template((function () {
        return {
          meta: {
            'revision': 'Ember@2.9.0',
            'loc': {
              'source': null,
              'start': {
                'line': 1,
                'column': 0
              },
              'end': {
                'line': 1,
                'column': 24
              }
            }
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment('');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
            dom.insertBoundary(fragment, 0);
            dom.insertBoundary(fragment, null);
            return morphs;
          },
          statements: [['content', 'videocapture-element', ['loc', [null, [1, 0], [1, 24]]], 0, 0, 0, 0]],
          locals: [],
          templates: []
        };
      })()));
      (0, _chai.expect)(this.$()).to.have.length(1);
    });
  });
});
define('offline/tests/integration/components/videocapture-element-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | integration/components/videocapture-element-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/mixins/question-input.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | mixins/question-input.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'mixins/question-input.js should pass jshint.');
  });
});
define('offline/tests/models/asset.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/asset.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/asset.js should pass jshint.');
  });
});
define('offline/tests/models/block.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/block.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/block.js should pass jshint.');
  });
});
define('offline/tests/models/cell.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/cell.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/cell.js should pass jshint.');
  });
});
define('offline/tests/models/checkbox.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | models/checkbox.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/models/element.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/element.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/element.js should pass jshint.');
  });
});
define('offline/tests/models/exec.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/exec.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/exec.js should pass jshint.');
  });
});
define('offline/tests/models/exit.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/exit.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/exit.js should pass jshint.');
  });
});
define('offline/tests/models/exitpage.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/exitpage.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/exitpage.js should pass jshint.');
  });
});
define('offline/tests/models/finish.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/finish.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/finish.js should pass jshint.');
  });
});
define('offline/tests/models/float.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/float.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/float.js should pass jshint.');
  });
});
define('offline/tests/models/goto.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/goto.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/goto.js should pass jshint.');
  });
});
define('offline/tests/models/hash.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/hash.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/hash.js should pass jshint.');
  });
});
define('offline/tests/models/html.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/html.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/html.js should pass jshint.');
  });
});
define('offline/tests/models/if.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/if.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/if.js should pass jshint.');
  });
});
define('offline/tests/models/image.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | models/image.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/models/interviewer.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/interviewer.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/interviewer.js should pass jshint.');
  });
});
define('offline/tests/models/label.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/label.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/label.js should pass jshint.');
  });
});
define('offline/tests/models/marker.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/marker.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/marker.js should pass jshint.');
  });
});
define('offline/tests/models/message.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/message.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/message.js should pass jshint.');
  });
});
define('offline/tests/models/number.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/number.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/number.js should pass jshint.');
  });
});
define('offline/tests/models/practice.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/practice.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/practice.js should pass jshint.');
  });
});
define('offline/tests/models/question.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/question.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/question.js should pass jshint.');
  });
});
define('offline/tests/models/radio.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/radio.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/radio.js should pass jshint.');
  });
});
define('offline/tests/models/res.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/res.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/res.js should pass jshint.');
  });
});
define('offline/tests/models/respondent.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/respondent.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/respondent.js should pass jshint.');
  });
});
define('offline/tests/models/select.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/select.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/select.js should pass jshint.');
  });
});
define('offline/tests/models/survey.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/survey.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/survey.js should pass jshint.');
  });
});
define('offline/tests/models/suspend.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/suspend.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/suspend.js should pass jshint.');
  });
});
define('offline/tests/models/term.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/term.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/term.js should pass jshint.');
  });
});
define('offline/tests/models/text.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/text.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/text.js should pass jshint.');
  });
});
define('offline/tests/models/textarea.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/textarea.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/textarea.js should pass jshint.');
  });
});
define('offline/tests/models/user.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/user.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/user.js should pass jshint.');
  });
});
define('offline/tests/models/var.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/var.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/var.js should pass jshint.');
  });
});
define('offline/tests/models/variable.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/variable.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/variable.js should pass jshint.');
  });
});
define('offline/tests/resolver.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | resolver.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass jshint.');
  });
});
define('offline/tests/router.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | router.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass jshint.');
  });
});
define('offline/tests/routes/application.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/application.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/application.js should pass jshint.');
  });
});
define('offline/tests/routes/authenticated.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/authenticated.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/authenticated.js should pass jshint.');
  });
});
define('offline/tests/routes/authenticated/admin.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/authenticated/admin.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/authenticated/admin.js should pass jshint.');
  });
});
define('offline/tests/routes/authenticated/index.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/authenticated/index.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/authenticated/index.js should pass jshint.');
  });
});
define('offline/tests/routes/authenticated/projects.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | routes/authenticated/projects.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/routes/authenticated/respview.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/authenticated/respview.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/authenticated/respview.js should pass jshint.');
  });
});
define('offline/tests/routes/authenticated/respview/error.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/authenticated/respview/error.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/authenticated/respview/error.js should pass jshint.');
  });
});
define('offline/tests/routes/authenticated/respview/exitpage.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/authenticated/respview/exitpage.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/authenticated/respview/exitpage.js should pass jshint.');
  });
});
define('offline/tests/routes/authenticated/respview/survey.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/authenticated/respview/survey.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/authenticated/respview/survey.js should pass jshint.');
  });
});
define('offline/tests/routes/authenticated/respview/survey/page.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/authenticated/respview/survey/page.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/authenticated/respview/survey/page.js should pass jshint.');
  });
});
define('offline/tests/routes/login.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/login.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/login.js should pass jshint.');
  });
});
define('offline/tests/routes/login/forgot.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/login/forgot.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/login/forgot.js should pass jshint.');
  });
});
define('offline/tests/routes/login/index.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/login/index.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/login/index.js should pass jshint.');
  });
});
define('offline/tests/routes/solo.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/solo.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/solo.js should pass jshint.');
  });
});
define('offline/tests/serializers/application.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/application.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/application.js should pass jshint.');
  });
});
define('offline/tests/serializers/cell.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/cell.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/cell.js should pass jshint.');
  });
});
define('offline/tests/serializers/element.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/element.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/element.js should pass jshint.');
  });
});
define('offline/tests/serializers/hash.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/hash.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/hash.js should pass jshint.');
  });
});
define('offline/tests/serializers/interviewer.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/interviewer.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/interviewer.js should pass jshint.');
  });
});
define('offline/tests/serializers/message.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/message.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/message.js should pass jshint.');
  });
});
define('offline/tests/serializers/question.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/question.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/question.js should pass jshint.');
  });
});
define('offline/tests/serializers/respondent.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/respondent.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/respondent.js should pass jshint.');
  });
});
define('offline/tests/serializers/survey.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/survey.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/survey.js should pass jshint.');
  });
});
define('offline/tests/serializers/user.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/user.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/user.js should pass jshint.');
  });
});
define('offline/tests/serializers/var.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/var.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/var.js should pass jshint.');
  });
});
define('offline/tests/serializers/variable.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | serializers/variable.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/variable.js should pass jshint.');
  });
});
define('offline/tests/services/capacity.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | services/capacity.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/capacity.js should pass jshint.');
  });
});
define('offline/tests/services/current-project.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | services/current-project.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/current-project.js should pass jshint.');
  });
});
define('offline/tests/services/network.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | services/network.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/network.js should pass jshint.');
  });
});
define('offline/tests/services/poller.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | services/poller.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/poller.js should pass jshint.');
  });
});
define('offline/tests/services/session.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | services/session.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/session.js should pass jshint.');
  });
});
define('offline/tests/services/simple-locks.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | services/simple-locks.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/simple-locks.js should pass jshint.');
  });
});
define('offline/tests/services/storage.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | services/storage.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/storage.js should pass jshint.');
  });
});
define('offline/tests/services/store.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | services/store.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/store.js should pass jshint.');
  });
});
define('offline/tests/services/ui-state.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | services/ui-state.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/ui-state.js should pass jshint.');
  });
});
define('offline/tests/services/upload-status.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | services/upload-status.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/upload-status.js should pass jshint.');
  });
});
define('offline/tests/test-helper', ['exports', 'mocha', 'offline/tests/helpers/resolver', 'ember-mocha'], function (exports, _mocha, _offlineTestsHelpersResolver, _emberMocha) {

    _mocha.mocha.setup({
        timeout: 10000
    });
    (0, _emberMocha.setResolver)(_offlineTestsHelpersResolver['default']);
});
define('offline/tests/test-helper.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | test-helper.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/transforms/object.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | transforms/object.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'transforms/object.js should pass jshint.');
  });
});
define('offline/tests/transforms/set.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | transforms/set.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'transforms/set.js should pass jshint.');
  });
});
define('offline/tests/transforms/slist.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | transforms/slist.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'transforms/slist.js should pass jshint.');
  });
});
define('offline/tests/unit/adapters/application-test', ['exports', 'chai', 'mocha', 'ember-mocha'], function (exports, _chai, _mocha, _emberMocha) {

    (0, _mocha.describe)('Unit | Adapter | application', function () {
        (0, _emberMocha.setupTest)('adapter:application', {
            // Specify the other units that are required for this test.
            // needs: ['serializer:foo']
        });

        // Replace this with your real tests.
        (0, _mocha.it)('exists', function () {
            var adapter = this.subject();
            (0, _chai.expect)(adapter).to.be.ok;
        });
    });
});
define('offline/tests/unit/adapters/application-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/adapters/application-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/controllers/authenticated/admin-test', ['exports', 'chai', 'mocha', 'ember-mocha'], function (exports, _chai, _mocha, _emberMocha) {

    (0, _mocha.describe)('Unit | Controller | authenticated/admin', function () {
        (0, _emberMocha.setupTest)('controller:authenticated/admin', {
            // Specify the other units that are required for this test.
            // needs: ['serializer:foo']
        });

        // Replace this with your real tests.
        (0, _mocha.it)('exists', function () {
            var controller = this.subject();
            (0, _chai.expect)(controller).to.be.ok;
        });
    });
});
define('offline/tests/unit/controllers/authenticated/admin-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/controllers/authenticated/admin-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/helpers/format-date-test', ['exports', 'offline/helpers/format-date', 'chai', 'mocha'], function (exports, _offlineHelpersFormatDate, _chai, _mocha) {

    (0, _mocha.describe)('Unit | Helper | format date', function () {

        (0, _mocha.it)('should format a Date', function () {
            var date = new Date(1484781258371);
            (0, _chai.expect)((0, _offlineHelpersFormatDate.formatDate)([date, 'h:mm A dddd, MMMM D'])).to.equal('3:14 PM Wednesday, January 18');
        });
    });
});
define('offline/tests/unit/helpers/format-date-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/helpers/format-date-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/helpers/question-component-name-test', ['exports', 'ember', 'chai', 'mocha', 'offline/helpers/question-component-name', 'offline/transforms/slist'], function (exports, _ember, _chai, _mocha, _offlineHelpersQuestionComponentName, _offlineTransformsSlist) {

  var slist = _offlineTransformsSlist['default'].create();
  var Question = _ember['default'].Object.create({
    type: 'checkbox'
  });
  var DQQuestion = _ember['default'].Object.create({
    uses: slist.deserialize(['atm1d.8']),
    type: 'checkbox'
  });

  (0, _mocha.describe)('Unit | Helper | question component name', function () {
    // Replace this with your real tests.
    (0, _mocha.it)('prioritizes "uses"', function () {
      (0, _chai.expect)((0, _offlineHelpersQuestionComponentName.questionComponentName)([Question])).to.equal('checkbox-element');
      (0, _chai.expect)((0, _offlineHelpersQuestionComponentName.questionComponentName)([DQQuestion])).to.equal('atm1d-element');
    });
  });
});
define('offline/tests/unit/helpers/question-component-name-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/helpers/question-component-name-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/initializers/intl-test', ['exports', 'chai', 'mocha', 'ember', 'offline/initializers/intl', 'offline/tests/helpers/destroy-app'], function (exports, _chai, _mocha, _ember, _offlineInitializersIntl, _offlineTestsHelpersDestroyApp) {

  (0, _mocha.describe)('Unit | Initializer | intl', function () {
    var application = undefined;

    (0, _mocha.beforeEach)(function () {
      _ember['default'].run(function () {
        application = _ember['default'].Application.create();
        application.deferReadiness();
      });
    });

    (0, _mocha.afterEach)(function () {
      (0, _offlineTestsHelpersDestroyApp['default'])(application);
    });

    // Replace this with your real tests.
    (0, _mocha.it)('works', function () {
      (0, _offlineInitializersIntl.initialize)(application);

      // you would normally confirm the results of the initializer here
      (0, _chai.expect)(true).to.be.ok;
    });
  });
});
define('offline/tests/unit/initializers/intl-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/initializers/intl-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/instance-initializers/current-user-test', ['exports', 'chai', 'mocha', 'ember', 'offline/instance-initializers/current-user', 'offline/tests/helpers/destroy-app'], function (exports, _chai, _mocha, _ember, _offlineInstanceInitializersCurrentUser, _offlineTestsHelpersDestroyApp) {

  (0, _mocha.describe)('Unit | Instance Initializer | current user', function () {
    var application = undefined,
        appInstance = undefined;

    (0, _mocha.beforeEach)(function () {
      _ember['default'].run(function () {
        application = _ember['default'].Application.create();
        appInstance = application.buildInstance();
      });
    });

    (0, _mocha.afterEach)(function () {
      _ember['default'].run(appInstance, 'destroy');
      (0, _offlineTestsHelpersDestroyApp['default'])(application);
    });

    // Replace this with your real tests.
    (0, _mocha.it)('works', function () {
      (0, _offlineInstanceInitializersCurrentUser.initialize)(appInstance);

      // you would normally confirm the results of the initializer here
      (0, _chai.expect)(true).to.be.ok;
    });
  });
});
define('offline/tests/unit/instance-initializers/current-user-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/instance-initializers/current-user-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/instance-initializers/service-worker-test', ['exports', 'chai', 'mocha', 'ember', 'offline/instance-initializers/service-worker', 'offline/tests/helpers/destroy-app'], function (exports, _chai, _mocha, _ember, _offlineInstanceInitializersServiceWorker, _offlineTestsHelpersDestroyApp) {

  (0, _mocha.describe)('Unit | Instance Initializer | service worker', function () {
    var application = undefined,
        appInstance = undefined;

    (0, _mocha.beforeEach)(function () {
      _ember['default'].run(function () {
        application = _ember['default'].Application.create();
        appInstance = application.buildInstance();
      });
    });

    (0, _mocha.afterEach)(function () {
      _ember['default'].run(appInstance, 'destroy');
      (0, _offlineTestsHelpersDestroyApp['default'])(application);
    });

    // Replace this with your real tests.
    (0, _mocha.it)('works', function () {
      (0, _offlineInstanceInitializersServiceWorker.initialize)(appInstance);

      // you would normally confirm the results of the initializer here
      (0, _chai.expect)(true).to.be.ok;
    });
  });
});
define('offline/tests/unit/instance-initializers/service-worker-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/instance-initializers/service-worker-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/models/element-test', ['exports', 'chai', 'mocha', 'ember-mocha'], function (exports, _chai, _mocha, _emberMocha) {

    (0, _mocha.describe)('Unit | Model | element', function () {
        (0, _emberMocha.setupModelTest)('element', {
            // Specify the other units that are required for this test.
            needs: ['model:survey']
        });

        // Replace this with your real tests.
        (0, _mocha.it)('exists', function () {
            var model = this.subject();
            // let store = this.store();
            (0, _chai.expect)(model).to.be.ok;
        });
    });
});
define('offline/tests/unit/models/element-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/models/element-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/models/html-test', ['exports', 'chai', 'ember-mocha'], function (exports, _chai, _emberMocha) {

  (0, _emberMocha.describeModel)('html', 'Unit | Model | html', {
    // Specify the other units that are required for this test.
    needs: ['model:survey', 'model:element']
  }, function () {
    // Replace this with your real tests.
    (0, _emberMocha.it)('exists', function () {
      var model = this.subject();
      // var store = this.store();
      (0, _chai.expect)(model).to.be.ok;
    });
  });
});
define('offline/tests/unit/models/html-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/models/html-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/routes/application-test', ['exports', 'chai', 'mocha', 'ember-mocha'], function (exports, _chai, _mocha, _emberMocha) {

    (0, _mocha.describe)('Unit | Route | application', function () {
        (0, _emberMocha.setupTest)('route:application', {
            // Specify the other units that are required for this test.
            // needs: ['serializer:foo']
        });

        // Replace this with your real tests.
        (0, _mocha.it)('exists', function () {
            var route = this.subject();
            (0, _chai.expect)(route).to.be.ok;
        });
    });
});
define('offline/tests/unit/routes/application-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/routes/application-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/routes/authenticated/admin-test', ['exports', 'chai', 'mocha', 'ember-mocha'], function (exports, _chai, _mocha, _emberMocha) {

    (0, _mocha.describe)('Unit | Route | authenticated/admin', function () {
        (0, _emberMocha.setupTest)('route:authenticated/admin', {
            // Specify the other units that are required for this test.
            // needs: ['serializer:foo']
        });

        // Replace this with your real tests.
        (0, _mocha.it)('exists', function () {
            var route = this.subject();
            (0, _chai.expect)(route).to.be.ok;
        });
    });
});
define('offline/tests/unit/routes/authenticated/admin-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/routes/authenticated/admin-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/serializers/survey-test', ['exports', 'chai', 'ember-mocha'], function (exports, _chai, _emberMocha) {

    (0, _emberMocha.describeModel)('survey', 'Unit | Serializer | survey', {
        // Specify the other units that are required for this test.
        needs: ['serializer:survey', 'model:hash', 'model:asset', 'model:message', 'model:element', 'model:variable', 'model:exitpage', 'model:practice', 'model:respondent', 'model:interviewer']
    }, function () {
        // Replace this with your real tests.
        (0, _emberMocha.it)('serializes records', function () {
            var record = this.subject();

            var serializedRecord = record.serialize();

            (0, _chai.expect)(serializedRecord).to.be.ok;
        });
    });
});
define('offline/tests/unit/serializers/survey-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/serializers/survey-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/services/current-project-test', ['exports', 'chai', 'mocha', 'ember-mocha'], function (exports, _chai, _mocha, _emberMocha) {

  (0, _mocha.describe)('Unit | Service | current project', function () {
    (0, _emberMocha.setupTest)('service:current-project', {
      // Specify the other units that are required for this test.
      // needs: ['service:foo']
    });

    // Replace this with your real tests.
    (0, _mocha.it)('exists', function () {
      var service = this.subject();
      (0, _chai.expect)(service).to.be.ok;
    });
  });
});
define('offline/tests/unit/services/current-project-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/services/current-project-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/services/ui-state-test', ['exports', 'chai', 'mocha', 'ember-mocha'], function (exports, _chai, _mocha, _emberMocha) {

  (0, _mocha.describe)('Unit | Service | ui state', function () {
    (0, _emberMocha.setupTest)('service:ui-state', {
      // Specify the other units that are required for this test.
      // needs: ['service:foo']
    });

    // Replace this with your real tests.
    (0, _mocha.it)('exists', function () {
      var service = this.subject();
      (0, _chai.expect)(service).to.be.ok;
    });
  });
});
define('offline/tests/unit/services/ui-state-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/services/ui-state-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/transforms/object-test', ['exports', 'chai', 'ember-mocha'], function (exports, _chai, _emberMocha) {

  (0, _emberMocha.describeModule)('transform:object', 'Unit | Transform | object', {
    // Specify the other units that are required for this test.
    // needs: ['transform:foo']
  }, function () {
    // Replace this with your real tests.
    (0, _emberMocha.it)('exists', function () {
      var transform = this.subject();
      (0, _chai.expect)(transform).to.be.ok;
    });
  });
});
define('offline/tests/unit/transforms/object-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/transforms/object-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/transforms/set-test', ['exports', 'chai', 'ember-mocha'], function (exports, _chai, _emberMocha) {

  (0, _emberMocha.describeModule)('transform:set', 'Unit | Transform | set', {
    // Specify the other units that are required for this test.
    // needs: ['transform:foo']
  }, function () {
    // Replace this with your real tests.
    (0, _emberMocha.it)('exists', function () {
      var transform = this.subject();
      (0, _chai.expect)(transform).to.be.ok;
    });
  });
});
define('offline/tests/unit/transforms/set-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/transforms/set-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/unit/transforms/slist-test', ['exports', 'chai', 'ember-mocha'], function (exports, _chai, _emberMocha) {

  (0, _emberMocha.describeModule)('transform:slist', 'Unit | Transform | slist', {
    // Specify the other units that are required for this test.
    // needs: ['transform:foo']
  }, function () {
    // Replace this with your real tests.
    (0, _emberMocha.it)('exists', function () {
      var transform = this.subject();
      (0, _chai.expect)(transform).to.be.ok;
    });
  });
});
define('offline/tests/unit/transforms/slist-test.jshint', ['exports'], function (exports) {
  'use strict';

  describe('JSHint | unit/transforms/slist-test.js', function () {
    it('should pass jshint', function () {
      // precompiled test passed
    });
  });
});
define('offline/tests/utils/intl/missing-message.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | utils/intl/missing-message.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'utils/intl/missing-message.js should pass jshint.');
  });
});
define('offline/tests/utils/survey/embedded.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | utils/survey/embedded.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'utils/survey/embedded.js should pass jshint.');
  });
});
define('offline/tests/utils/survey/environment.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | utils/survey/environment.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'utils/survey/environment.js should pass jshint.');
  });
});
define('offline/tests/utils/survey/logic.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | utils/survey/logic.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'utils/survey/logic.js should pass jshint.');
  });
});
define('offline/tests/utils/survey/preorder.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | utils/survey/preorder.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'utils/survey/preorder.js should pass jshint.');
  });
});
define('offline/tests/utils/survey/session.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | utils/survey/session.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'utils/survey/session.js should pass jshint.');
  });
});
/* jshint ignore:start */

require('offline/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;

/* jshint ignore:end */
//# sourceMappingURL=tests.map
