/* global self, caches, fetch */

/**
 * Snapshot of our apps environment at the time of build
 * @type {{ serviceWorker, rootURL, apiNameSpace, authNameSpace }}
 */
'use strict';

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

var CONFIG = JSON.parse('{"modulePrefix":"offline","environment":"mobile","rootURL":"","locationType":"hash","apiNameSpace":"/api/v1/offline","authNameSpace":"http://localhost:5000/admin/ajax/","EmberENV":{"FEATURES":{},"EXTEND_PROTOTYPES":{"Date":false}},"APP":{"rootElement":"body","LOCAL_STORAGE":true,"MEMORY_STORAGE":false,"name":"offline","version":"0.0.0+ea8e7c12"},"serviceWorker":{"register":true,"cacheName":"offline-cache","precache":["assets/vendor.js","assets/vendor.css","assets/offline.js","assets/offline.css","favicon.ico","fonts/OpenSans.ttf","fonts/OpenSans-Bold.ttf","fonts/OpenSans-Light.ttf","fonts/OpenSans-Semibold.ttf","fonts/fontawesome-webfont.woff?v=4.3.0","fonts/fontawesome-webfont.woff2?v=4.3.0"]},"ember-simple-auth":{"baseURL":"","authenticationRoute":"login","routeAfterAuthentication":"authenticated.projects","routeIfAlreadyAuthenticated":"authenticated.projects"},"ember-cli-mirage":{"usingProxy":false,"useDefaultPassthroughs":true},"exportApplicationGlobal":true}');

var OFFLINE_PREFIX = CONFIG.serviceWorker.cacheName;
var URL_REGEXP = /^(https?:)\/\/(([^:\/?#]*)(?::([0-9]+))?)([\/]?[^?#]*)(\?[^#]*|)(#.*|)$/;
var NOCACHE = [CONFIG.apiNameSpace, CONFIG.authNameSpace]; // prefix for requests we should never cache
var ROOT_URL = CONFIG.rootURL;
var CACHED = {};

// some polyfills
if (!Array.prototype.includes) {
    Array.prototype.includes = function includes(item) {
        return this.indexOf(item) > -1;
    };
}
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function startsWith(searchString, position) {
        position = position || 0;
        return this.substr(position, searchString.length) === searchString;
    };
    String.prototype.endsWith = function (searchString, position) {
        var subjectString = this.toString();
        if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
            position = subjectString.length;
        }
        position -= searchString.length;
        var lastIndex = subjectString.lastIndexOf(searchString, position);
        return lastIndex !== -1 && lastIndex === position;
    };
}

function parseURL(url) {
    var match;
    if (!(match = url.match(URL_REGEXP))) {
        return {};
    }
    return {
        protocol: match[1],
        host: match[2],
        hostname: match[3],
        port: match[4],
        pathname: match[5],
        search: match[6],
        hash: match[7]
    };
}

/**
 * http[s] protocol stuff with a cacheable pathname
 * @param {Request} request
 * @returns {Boolean}
 */
function shouldCache(request) {
    if (request.method !== 'GET') {
        return false;
    }
    // only cache requests made from the offline application
    var url = parseURL(request.url);
    var referrer = parseURL(request.referrer);
    return ((referrer.pathname || '').startsWith(ROOT_URL) || request.referrer in CACHED) && url.pathname && !NOCACHE.some(function (prefix) {
        return url.pathname.startsWith(prefix);
    });
}

/**
 * Fetch then cache, or get from cache (and fetch if not in cache) if options.useCache
 * @param {Request | String} request
 * @param {{ useCache }} options
 * @returns {Promise}
 */
function resolveStatic(request, options) {
    options = options || { useCache: false };
    request = typeof request === 'string' ? new Request(request, { method: 'GET' }) : request;

    return caches.open(OFFLINE_PREFIX).then(function (cache) {
        if (!shouldCache(request)) {
            return fetch(request);
        }
        if (options.useCache) {
            return cache.match(request)['finally'](function (response) {
                if (response && response.ok) {
                    return response;
                }
                return resolveStatic(request, { useCache: false });
            });
        }
        return fetch(request).then(function (response) {
            // clone the response so the cache and request can process the stream separately
            var clone = response.clone();
            cache['delete'](request).then(function (purged) {
                console.info(purged ? 'PURGED' : 'NEW', request.url);
                CACHED[request.url] = true;
                cache.put(request, clone);
            });
            return response;
        }, function (error) {
            return cache.match(request)['catch'](function () {
                return new Response(new Blob(), { status: 404, statusText: 'not found (SW)' });
            });
        });
    });
}

self.addEventListener('install', function (event) {
    event.waitUntil(self.skipWaiting().then(function () {
        return caches.open(OFFLINE_PREFIX);
    }));
});

/**
 * By default, a page's fetches won't go through a service worker unless the page request itself went through a service worker.
 * So you'll need to refresh the page to see the effects of the service worker.
 * clients.claim() can override this default, and take control of non-controlled pages.
 */
self.addEventListener('activate', function (event) {
    event.waitUntil(self.clients.claim().then(function () {
        return caches.open(OFFLINE_PREFIX);
    }).then(function (cache) {
        return cache.addAll(CONFIG.serviceWorker.precache).then(function () {
            return cache.keys();
        });
    }).then(function (keys) {
        // update our in-memory cache "set" for synchronous lookup
        CACHED = keys.reduce(function (cached, item) {
            return (cached[item.url] = true) && cached;
        }, {});
    }));
});

/**
 * Transform a request for a route to a request for the rootURL
 * The resource path for static things should remain unaltered
 */
self.addEventListener('fetch', function (event) {
    var request = event.request;
    var parsed = parseURL(request.url);
    if (parsed.pathname && parsed.pathname.startsWith(ROOT_URL)) {
        var splitPath = parsed.pathname.substr(ROOT_URL).split('/'),
            isStatic = splitPath.includes('assets') || splitPath.includes('static'),
            filename = splitPath[splitPath.length - 1];

        if (!(isStatic || filename && /.*\.[a-zA-Z0-9]+$/.test(filename))) {
            return event.respondWith(caches.open(OFFLINE_PREFIX).then(function (cache) {
                return cache.match(parsed.protocol + '//' + parsed.host + ROOT_URL).then(function (response) {
                    return response ? response : fetch(request);
                }, function (error) {
                    return fetch(request);
                });
            }));
        }
    }
    event.respondWith(resolveStatic(event.request));
});

/**
 * Service Worker actions initiated from the host
 */
self.addEventListener('message', function (event) {
    switch (event.data.action) {
        case 'precache':
            caches.open(OFFLINE_PREFIX).then(function (cache) {
                console.info.apply(console, ['caching'].concat(_toConsumableArray(event.data.urls)));
                cache.addAll(event.data.urls).then(function () {
                    return event.ports[0].postMessage(event.data);
                }, function () {
                    return event.ports[0].postMessage({ error: 'failed to cache' });
                });
            });
            break;
        case 'define':
            for (var k in event.data.variables) {
                if (event.data.variables.hasOwnProperty(k)) {
                    self[k] = event.data.variables[k];
                }
            }
            event.ports[0].postMessage(event.data);
            break;
        default:
            event.ports[0].postMessage({ error: 'no action supplied' });
    }
});